<?php




Route::group(['middleware' => 'guest'], function() {

    Route::get('/', 'AccountController@index');

    Route::post('login', 'AccountController@login');

    Route::get('password-recover', 'AccountController@recover_password');
    Route::post('password-recover', 'AccountController@recover_password_post');

});





Route::group(['middleware' => 'auth'], function() {

    //Logout
    Route::get('logout', 'AccountController@logout');

    //Dashboard
    Route::group(['prefix' => 'dashboard'], function() {
        Route::get('/', 'DashboardController@index');
        Route::get('logout', 'AccountController@logout');


        Route::group(['prefix' => 'sistema'], function() {

            Route::group(['prefix' => 'perfil'], function() {
                Route::get('/', 'Sistema\ProfileController@index');
                Route::get('table', 'Sistema\ProfileController@indexTable');
                Route::get('alta', 'Sistema\ProfileController@create');
                Route::post('alta', 'Sistema\ProfileController@store');
                Route::get('editar/{id}', 'Sistema\ProfileController@edit_show');
                Route::post('editar', 'Sistema\ProfileController@editar');
                Route::get('borrar/{id}', 'Sistema\ProfileController@destroy');
                Route::get('editar_getData', 'Sistema\ProfileController@getData');
                Route::get('ver/{id}', 'Sistema\ProfileController@view');
            });

            Route::group(['prefix' => 'usuarios'], function() {
                Route::get('/', 'Sistema\UserController@index');
                Route::get('table', 'Sistema\UserController@indexTable');
                Route::get('alta', 'Sistema\UserController@create');
                Route::post('alta', 'Sistema\UserController@store');
                Route::get('editar/{id}', 'Sistema\UserController@edit');
                Route::post('editar/{id}', 'Sistema\UserController@update');
                Route::get('borrar/{id}', 'Sistema\UserController@destroy');
                Route::post('get_user_profiles', 'Sistema\UserController@getUserProfiles');
            });

        });




        Route::group(['prefix' => 'catalogos'], function() {

            Route::group(['prefix' => 'categorias'], function() {
                Route::get('/', 'Catalogo\CategoriasController@index');
                Route::get('table', 'Catalogo\CategoriasController@indexTable');
                Route::get('alta', 'Catalogo\CategoriasController@create');
                Route::post('alta', 'Catalogo\CategoriasController@store');
                Route::get('editar/{id}', 'Catalogo\CategoriasController@edit');
                Route::post('editar/{id}', 'Catalogo\CategoriasController@update');
                Route::get('borrar/{id}', 'Catalogo\CategoriasController@destroy');
                Route::get('editar_getData', 'Catalogo\CategoriasController@getData');
                Route::get('ver/{id}', 'Catalogo\CategoriasController@view');

                Route::post('subir', 'Catalogo\CategoriasController@subir');
                Route::post('bajar', 'Catalogo\CategoriasController@bajar');

            });

            Route::group(['prefix' => 'repartidores'], function() {
                Route::get('/', 'Catalogo\DeliveryController@index');
                Route::get('table', 'Catalogo\DeliveryController@indexTable');
                Route::get('alta', 'Catalogo\DeliveryController@create');
                Route::post('alta', 'Catalogo\DeliveryController@store');
                Route::get('editar/{id}', 'Catalogo\DeliveryController@edit');
                Route::post('editar/{id}', 'Catalogo\DeliveryController@update');
                Route::get('borrar/{id}', 'Catalogo\DeliveryController@destroy');
                Route::get('editar_getData', 'Catalogo\DeliveryController@getData');
                Route::get('ver/{id}', 'Catalogo\DeliveryController@view');
            });

            Route::group(['prefix' => 'productos'], function() {
                Route::get('/', 'Catalogo\ProductsController@index');
                Route::get('table', 'Catalogo\ProductsController@indexTable');
                Route::get('alta', 'Catalogo\ProductsController@create');
                Route::post('alta', 'Catalogo\ProductsController@store');
                Route::get('editar/{id}', 'Catalogo\ProductsController@edit');
                Route::post('editar/{id}', 'Catalogo\ProductsController@update');
                Route::get('borrar/{id}', 'Catalogo\ProductsController@destroy');
                Route::get('editar_getData', 'Catalogo\ProductsController@getData');
                Route::get('ver/{id}', 'Catalogo\ProductsController@view');
            });

            Route::group(['prefix' => 'promociones'], function() {
                Route::get('/', 'Catalogo\ProductsPromotionController@index');
                Route::get('table', 'Catalogo\ProductsPromotionController@indexTable');
                Route::get('alta', 'Catalogo\ProductsPromotionController@create');
                Route::post('alta', 'Catalogo\ProductsPromotionController@store');
                Route::get('editar/{id}', 'Catalogo\ProductsPromotionController@edit');
                Route::post('editar/{id}', 'Catalogo\ProductsPromotionController@update');
                Route::get('borrar/{id}', 'Catalogo\ProductsPromotionController@destroy');
                Route::get('editar_getData', 'Catalogo\ProductsPromotionController@getData');
                Route::get('ver/{id}', 'Catalogo\ProductsPromotionController@view');
            });

            Route::group(['prefix' => 'gastos_envio'], function() {
                Route::get('/', 'Catalogo\SpendingSendController@index');
                Route::get('table', 'Catalogo\SpendingSendController@indexTable');
                Route::get('alta', 'Catalogo\SpendingSendController@create');
                Route::post('alta', 'Catalogo\SpendingSendController@store');
                Route::get('editar/{id}', 'Catalogo\SpendingSendController@edit');
                Route::post('editar/{id}', 'Catalogo\SpendingSendController@update');
                Route::get('borrar/{id}', 'Catalogo\SpendingSendController@destroy');
                Route::get('editar_getData', 'Catalogo\SpendingSendController@getData');
                Route::get('ver/{id}', 'Catalogo\SpendingSendController@view');
                Route::post('getcoordinates', 'Catalogo\SpendingSendController@getcoordinates');
            });

            Route::group(['prefix' => 'motivos'], function() {
                Route::get('/', 'Catalogo\ReasonController@index');
                Route::get('table', 'Catalogo\ReasonController@indexTable');
                Route::get('alta', 'Catalogo\ReasonController@create');
                Route::post('alta', 'Catalogo\ReasonController@store');
                Route::get('editar/{id}', 'Catalogo\ReasonController@edit');
                Route::post('editar/{id}', 'Catalogo\ReasonController@update');
                Route::get('borrar/{id}', 'Catalogo\ReasonController@destroy');
                Route::get('editar_getData', 'Catalogo\ReasonController@getData');
                Route::get('ver/{id}', 'Catalogo\ReasonController@view');
                Route::post('getcoordinates', 'Catalogo\ReasonController@getcoordinates');
            });

            Route::group(['prefix' => 'negocio'], function() {
                Route::get('/', 'Catalogo\BussinesController@index');
                Route::get('table', 'Catalogo\BussinesController@indexTable');
                Route::get('alta', 'Catalogo\BussinesController@create');
                Route::post('alta', 'Catalogo\BussinesController@store');
                Route::get('editar/{id}', 'Catalogo\BussinesController@edit');
                Route::post('editar/{id}', 'Catalogo\BussinesController@update');
                Route::get('borrar/{id}', 'Catalogo\BussinesController@destroy');
                Route::get('editar_getData', 'Catalogo\BussinesController@getData');
                Route::get('ver/{id}', 'Catalogo\BussinesController@view');
                Route::post('getcoordinates', 'Catalogo\BussinesController@getcoordinates');
            });

            Route::group(['prefix' => 'splash_screen'], function() {
                Route::get('/', 'Catalogo\SplashScreenController@index');
                Route::get('table', 'Catalogo\SplashScreenController@indexTable');
                Route::get('alta', 'Catalogo\SplashScreenController@create');
                Route::post('alta', 'Catalogo\SplashScreenController@store');
                Route::get('editar/{id}', 'Catalogo\SplashScreenController@edit');
                Route::post('editar/{id}', 'Catalogo\SplashScreenController@update');
                Route::get('borrar/{id}', 'Catalogo\SplashScreenController@destroy');
                Route::get('editar_getData', 'Catalogo\SplashScreenController@getData');
                Route::get('ver/{id}', 'Catalogo\SplashScreenController@view');
                Route::get('active/{id}', 'Catalogo\SplashScreenController@active');
            });


        });


        Route::group(['prefix' => 'clientes'], function(){
            Route::get('/', 'Client\ClientsController@index');
            Route::get('table', 'Client\ClientsController@indexTable');
            Route::get('disable/{id}', 'Client\ClientsController@disable');
            Route::get('enable/{id}', 'Client\ClientsController@enable');
            Route::get('history/{id}', 'Client\ClientsController@history');
        });

        Route::group(['prefix' => 'pedidos'], function(){
            Route::get('/', 'Pedidos\OrdersController@index');
            Route::get('table', 'Pedidos\OrdersController@indexTable');
            Route::get('ver/{id}', 'Pedidos\OrdersController@ver');
            Route::get('asignar/{id}', 'Pedidos\OrdersController@asignar');
            Route::post('set_delivery', 'Pedidos\OrdersController@set_delivery');
            Route::post('set_entregado', 'Pedidos\OrdersController@set_entregado');
            Route::post('set_cancelado', 'Pedidos\OrdersController@set_cancelado');
            Route::post('set_notificar', 'Pedidos\OrdersController@set_notificar');
        });

        Route::group(['prefix' => 'historial_pedidos'], function(){
            Route::get('/', 'Pedidos\OrdersController@index_historical');
            Route::get('table', 'Pedidos\OrdersController@indexTable_historical');
            Route::get('ver/{id}', 'Pedidos\OrdersController@ver_historical');
        });




    });


});





Route::group(['prefix' => 'api', 'middleware' => 'cors'], function () {
    Route::post('create_account', 'API\ApiController@create_account');
    Route::post('create_account_social_network', 'API\ApiController@create_account_social_network');
    Route::post('upload_photo', 'API\ApiController@upload_photo');
    Route::post('upload_url', 'API\ApiController@upload_url');

    Route::post('check_user_exists_social_network', 'API\ApiController@check_user_exists_social_network');
    Route::post('login_email', 'API\ApiController@login_email');
    Route::post('username_exists_recover_password', 'API\ApiController@username_exists_recover_password');
    Route::post('recover_password', 'API\ApiController@recover_password');
    Route::post('aclaration_user_banned', 'API\ApiController@aclaration_user_banned');
    Route::post('is_user_have_credit_card', 'API\ApiController@is_user_have_credit_card');
    Route::post('get_client_info', 'API\ApiController@get_client_info');
    Route::post('edit_email_profile', 'API\ApiController@edit_email_profile');
    Route::post('edit_password_profile', 'API\ApiController@edit_password_profile');
    Route::post('edit_movil_profile', 'API\ApiController@edit_movil_profile');
    Route::post('edit_upload_photo', 'API\ApiController@edit_upload_photo');
    Route::post('set_notification_settings', 'API\ApiController@set_notification_settings');
    Route::post('send_contacto_ayuda', 'API\ApiController@send_conupdate_uuidtacto_ayuda');
    Route::post('get_reasons', 'API\ApiController@get_reasons');
    Route::post('delete_account', 'API\ApiController@delete_account');
    Route::post('get_credit_cards', 'API\ApiController@get_credit_cards');
    Route::post('get_credit_cards_purchase', 'API\ApiController@get_credit_cards_purchase');

    Route::post('get_states', 'API\ApiController@get_states');
    Route::post('get_cities', 'API\ApiController@get_cities');
    Route::post('add_credit_card', 'API\ApiController@add_credit_card');
    Route::post('delete_credit_card', 'API\ApiController@delete_credit_card');
    Route::post('send_aclaration', 'API\ApiController@send_aclaration');
    Route::post('send_invitation', 'API\ApiController@send_invitation');

    Route::post('get_categories', 'API\ApiController@get_categories');

    Route::post('get_shipping_cost', 'API\ApiController@get_shipping_cost');
    Route::post('is_user_location', 'API\ApiController@is_user_location');
    Route::post('store_purchase', 'API\ApiController@store_purchase');
    Route::post('store_purchase_efectivo', 'API\ApiController@store_purchase_efectivo');
    Route::post('store_purchase_solicitar_terminal', 'API\ApiController@store_purchase_solicitar_terminal');
    Route::post('get_purchase', 'API\ApiController@get_purchase');
    Route::post('cancelar_pedido', 'API\ApiController@cancelar_pedido');

    Route::post('cancelar_pedido_sin_cargos', 'API\ApiController@cancelar_pedido_sin_cargos');
    Route::post('cancelar_pedido_con_cargos', 'API\ApiController@cancelar_pedido_con_cargos');
    Route::post('get_credit_card', 'API\ApiController@get_credit_card');

    Route::post('get_purchase_by_user', 'API\ApiControllerpedidos@get_purchase_by_user');
    Route::get('pagar_pedido', 'API\ApiController@pagar_pedido');

    Route::post('update_uuid', 'API\ApiController@update_uuid');

    Route::post('get_splashscreen', 'API\ApiController@get_splashscreen');

    //GET PRODUCTS LIST
    Route::post('get_products', 'API\ApiController@get_products');
    Route::post('get_complements', 'API\ApiController@get_complements');
    Route::post('get_mixeds', 'API\ApiController@get_mixeds');
    Route::post('get_promotions', 'API\ApiController@get_promotions');

    Route::post('add_product_shopping_cart', 'API\ApiController@add_product_shopping_cart');
    Route::post('less_product_shopping_cart', 'API\ApiController@less_product_shopping_cart');
    Route::post('return_products', 'API\ApiController@return_products');

    Route::post('is_user_banned', 'API\ApiController@is_user_banned');
    Route::post('is_open_store', 'API\ApiController@is_open_store');

    Route::any('is_open_store_test', 'API\ApiController@is_open_store_test');


    Route::get('test', 'API\ApiController@send_push');


});

