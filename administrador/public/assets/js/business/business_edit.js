var id_selects = 0;
var select_servicios = "";
var markers_city = new Array();
var polygon;
var map;
var contador_marcadores = 0;

$(document).ready(function() {

	var id = $("#id").val();
	var data = {};
	data.id = id;

	$.ajax({
		url: "/dashboard/business/getPolygon",
		type: "GET",
		async: false,
		data: data,
		dataType: "JSON",
		error: function(e){
			console.log(e);
		},
		success: function(result){
			console.log (result.polygon);
			putMap(result.polygon);
		}
	});


	$(".eliminar_marcadores").on("click", function(){
		map.removeMarkers();
		markers_city = new Array();
		contador_marcadores = 0;
		polygon.setMap(null);
	});

	$(this).on('submit', 'form', function (e) {
		e.preventDefault();

		var route = $(this).attr('action');

		//Serialize form inputs
		var formData = new FormData(this);

		formData.append('markers_city', markers_city);

		$.ajax({
			type       	: 'POST',
			data 		: formData,  
			contentType	: false,        
			cache 		: false,              
			processData : false,
			dataType   	: 'json',
			url        	: route,
			beforeSend 	: function () {
				blockForm();
			},
			error      : function (jqXHR) {
				unblockForm();

				if (jqXHR.status == 422)
				{
					displayErrors(jqXHR);
				}
				else
				{
					displayMessage("Experimentamos fallas técnicas, intentelo más tarde.");
				}
			},
			success    : function (response) {
				unblockForm();
				console.log( response );
				if (response.status == true){
					bootbox.alert(response.message, function () {
						window.location = response.route;
					});
				}
				else{
 					bootbox.alert(response.message, function () {});						
				}
			}
		});
	});

	$('input.typeahead').typeahead({
	    ajax: "/dashboard/business/getCP"
	});

	$('.dropdown-menu').on('click', function(e) {
		e.preventDefault();

		var post = $('.postcode').val();

		$.ajax({
			type       	: 'GET',  
			contentType	: false,        
			cache 		: false,              
			processData : false,
			dataType   	: 'json',
			url        	: '/dashboard/business/getCity/'+post,
			beforeSend 	: function () {
				blockForm();
			},
			error      : function (jqXHR) {
				unblockForm();

				if (jqXHR.status == 422)
				{
					displayErrors(jqXHR);
				}
				else
				{
					displayMessage("Experimentamos fallas técnicas, intentelo más tarde.");
				}
			},
			success    : function (response) {
				unblockForm();
				if (response.status == true){
					$('#Ciudad').val(response.city.name);
					//$('#Ciudad').prop("disabled", true);
					$('#Estado').val(response.state.name);
					//$('#Estado').prop("disabled", true);

					var cols = '<option value>- Selecciona una colonia -</option>';
					var colonies = $.parseJSON(response.colonies);

					$.each(colonies, function(index, value){
						cols +='<option value="'+colonies[index].id+'" >'+colonies[index].name+'</option>';
					});
					$('#Colonia').html(cols);
					//console.log(cols);
				}
				else{
 					bootbox.alert(response.message, function () {});						
				}
			}
		});

	});


});

function putMap(coordenadas){


	map = new GMaps({
		el: '#map',
		lat: 25.7330054 ,
		lng: -100.3051636,
		zoom: 12,
		zoomControl : true,
		zoomControlOpt: {
			style : 'SMALL',
			position: 'TOP_LEFT'
		},
		panControl : false,
		click: function(event) {
		    var myLatLng = event.latLng;
		    var lat = myLatLng.lat();
		    var lng = myLatLng.lng();

		    //Agregamos el marcador
			var marker = map.addMarker({
				lat: lat,
				lng: lng,
				title: 'Lima',
				placeId: contador_marcadores++,
				draggable: true,
				click: function(e) {}
			});

			//Guardamos el marcador
			markers_city.push(new Array(lat,lng));

			//console.log(markers_city);
			polygon.setMap(null);

			polygon = map.drawPolygon({
				paths: markers_city, // pre-defined polygon shape
				strokeColor: '#BBD8E9',
				strokeOpacity: 1,
				strokeWeight: 3,
				fillColor: '#BBD8E9',
				fillOpacity: 0.6,
			});


			google.maps.event.addListener(
			    marker,
			    'drag',
			    function(event) {
			    	//console.log(this);
	  		        //console.log( this.position.lat() + " ? " + this.position.lng() );
	  		        markers_city[this.placeId][0] = this.position.lat();
	  		        markers_city[this.placeId][1] = this.position.lng();

					polygon.setMap(null);

					polygon = map.drawPolygon({
						paths: markers_city, // pre-defined polygon shape
						strokeColor: '#BBD8E9',
						strokeOpacity: 1,
						strokeWeight: 3,
						fillColor: '#BBD8E9',
						fillOpacity: 0.6,
					});   
			});

		}
	});
 


	//Generamos el array de coordenadas
	var coordenadas_centrar = new Array();
 	coordenadas = coordenadas.split(";");
	for (var i = 0; i < coordenadas.length; i++) {
		coordenadas[i] = coordenadas[i].replace("[", "");
		coordenadas[i] = coordenadas[i].replace("]", "");
		coordenadas_centrar.push(coordenadas[i].split(","));
   	};	






	//Colocamos los marcadores
	var bounds = [];
	for (var i = 0; i < coordenadas_centrar.length; i++) {
		var latlng = new google.maps.LatLng(coordenadas_centrar[i][0], coordenadas_centrar[i][1]);
		bounds.push(latlng);
		var marker = map.addMarker({
			lat: coordenadas_centrar[i][0],
			lng: coordenadas_centrar[i][1],
			draggable: true,
			placeId: contador_marcadores++,
		});

		google.maps.event.addListener(
		    marker,
		    'drag',
		    function(event) {
		    	//console.log(this);
  		        //console.log( this.position.lat() + " ? " + this.position.lng() );
  		        markers_city[this.placeId][0] = this.position.lat();
  		        markers_city[this.placeId][1] = this.position.lng();

				polygon.setMap(null);

				polygon = map.drawPolygon({
					paths: markers_city, // pre-defined polygon shape
					strokeColor: '#BBD8E9',
					strokeOpacity: 1,
					strokeWeight: 3,
					fillColor: '#BBD8E9',
					fillOpacity: 0.6,
				});   
		});

		//Guardamos las coordenadas de forma local
		markers_city.push(new Array(  parseFloat(coordenadas_centrar[i][0])  ,  parseFloat(coordenadas_centrar[i][1])  ));
	}



	//Dibujamos el poligono
 	polygon = map.drawPolygon({
		paths: markers_city, // pre-defined polygon shape
		strokeColor: '#BBD8E9',
		strokeOpacity: 1,
		strokeWeight: 3,
		fillColor: '#BBD8E9',
		fillOpacity: 0.6
	});


	//Centramos el mapa donde esten los marcadores
	map.fitLatLngBounds(bounds);
}