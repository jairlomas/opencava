var markers_city = new Array();
var polygon;
var contador_marcadores = 0;

$(document).ready(function() {

	$(this).on('submit', 'form', function(e) {
		e.preventDefault();

		var markers_str = "";
		for(var i = 0; i < markers_city.length; i++){
			markers_str += "["+markers_city[i][0]+","+markers_city[i][1]+"];";
		}
		markers_str = markers_str.substr(0, markers_str.length - 1 );

		$("#geocerca").val(markers_str);

		$.ajax({
			type     : 'POST',
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,
			url      : asset() + "catalogos/gastos_envio/alta",
			dataType : 'JSON',
			error    : function(e) {
				console.log(e);
				unblockForm();

				if (e.status == 422)
				{
					displayErrors( JSON.parse(e.responseText) );
				}
				else
				{
					displayMessage("Experimentamos fallas técnicas, intentelo más tarde.");
				}

			},
			success  : function(response) {

				console.log(response);

				if(response.estatus){
					bootbox.dialog({
						message : "Se ha guardado con éxito el registro.",
						title   : "Alta de registro",
						buttons : {
							aceptar : {
								label    : "Aceptar",
								callback : function() {
									bootbox.hideAll();
										window.location = asset() + "catalogos/gastos_envio";
									}
							}
						}
					});
				}else{
					displayErrors(response.errors);
				}


			}
		});

 
	});


 
	var map = new GMaps({
		el: '#basic_map',
		lat: 23.969662 ,
		lng: -102.489250,
		zoom: 5,
		zoomControl : true,
		zoomControlOpt: {
			style : 'SMALL',
			position: 'TOP_LEFT'
		},
		panControl : false,
		click: function(event) {
		    var myLatLng = event.latLng;
		    var lat = myLatLng.lat();
		    var lng = myLatLng.lng();

		    //Agregamos el marcador
			var marker = map.addMarker({
				lat: lat,
				lng: lng,
				title: 'Lima',
				placeId: contador_marcadores++,
				draggable: true,
				click: function(e) {}
			});

			//Guardamos el marcador
			markers_city.push(new Array(lat,lng));

			//console.log(markers_city);
			polygon.setMap(null);

			polygon = map.drawPolygon({
				paths: markers_city, // pre-defined polygon shape
				strokeColor: '#BBD8E9',
				strokeOpacity: 1,
				strokeWeight: 3,
				fillColor: '#BBD8E9',
				fillOpacity: 0.6,
			});


			google.maps.event.addListener(
			    marker,
			    'drag',
			    function(event) {
			    	//console.log(this);
	  		        //console.log( this.position.lat() + " ? " + this.position.lng() );
	  		        markers_city[this.placeId][0] = this.position.lat();
	  		        markers_city[this.placeId][1] = this.position.lng();

					polygon.setMap(null);

					polygon = map.drawPolygon({
						paths: markers_city, // pre-defined polygon shape
						strokeColor: '#BBD8E9',
						strokeOpacity: 1,
						strokeWeight: 3,
						fillColor: '#BBD8E9',
						fillOpacity: 0.6,
					});   
			});

		}
	});


	//Inicializamos la valiable polygon para cuando lo dibuje realmente con los marcadores, lo reinicie.
	var path = [[-12.040397656836609,-77.03373871559225]];
	polygon = map.drawPolygon({
		paths: path,
  	});


	$(".eliminar_marcadores").on("click", function(){
		map.removeMarkers();
		markers_city = new Array();
		contador_marcadores = 0;
		polygon.setMap(null);
		$("#geocerca").val("");
	});


});

 