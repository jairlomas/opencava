var markers_city = new Array();
var polygon;
var map;
var contador_marcadores = 0;

$(document).ready(function() {

 

	$(this).on('submit', 'form', function(e) {
		e.preventDefault();

		var markers_str = "";
		for(var i = 0; i < markers_city.length; i++){
			markers_str += "["+markers_city[i][0]+","+markers_city[i][1]+"];";
		}
		markers_str = markers_str.substr(0, markers_str.length - 1 );

		$("#geocerca").val(markers_str);

		$.ajax({
			type     : 'POST',
			data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,
			url      : $(this).attr("action"),
			dataType : 'JSON',
			error    : function(e) {
				console.log(e);
				unblockForm();

				if (e.status == 422)
				{
					displayErrors( JSON.parse(e.responseText) );
				}
				else
				{
					displayMessage("Experimentamos fallas técnicas, intentelo más tarde.");
				}

			},
			success  : function(response) {

				console.log(response);

				if(response.estatus){
					bootbox.dialog({
						message : "Se ha editado con éxito el registro.",
						title   : "Edición de registro",
						buttons : {
							aceptar : {
								label    : "Aceptar",
								callback : function() {
									bootbox.hideAll();
										window.location = asset() + "catalogos/gastos_envio";
									}
							}
						}
					});
				}else{
					displayErrors(response.errors);
				}


			}
		});

 
	});




 

	$(".eliminar_marcadores").on("click", function(){
		map.removeMarkers();
		markers_city = new Array();
		contador_marcadores = 0;
		polygon.setMap(null);
	});


	var data = {};
	data.id = $("input[name=id]").val();
	$.ajax({
		url: asset() + "catalogos/gastos_envio/getcoordinates",
		type: "POST",
		async: false,
		dataType: "JSON",
		data:data,
		error: function(e){
			console.log(e);
		},
		success: function(result){
			console.log(result);
			putMap(result);
		}
	});



});



 



 function putMap(coordenadas){


	map = new GMaps({
		el: '#basic_map',
		lat: 23.969662 ,
		lng: -102.489250,
		zoom: 5,
		zoomControl : true,
		zoomControlOpt: {
			style : 'SMALL',
			position: 'TOP_LEFT'
		},
		panControl : false,
		click: function(event) {
		    var myLatLng = event.latLng;
		    var lat = myLatLng.lat();
		    var lng = myLatLng.lng();

		    //Agregamos el marcador
			var marker = map.addMarker({
				lat: lat,
				lng: lng,
				title: 'Lima',
				placeId: contador_marcadores++,
				draggable: true,
				click: function(e) {}
			});

			//Guardamos el marcador
			markers_city.push(new Array(lat,lng));

			//console.log(markers_city);
			polygon.setMap(null);

			polygon = map.drawPolygon({
				paths: markers_city, // pre-defined polygon shape
				strokeColor: '#BBD8E9',
				strokeOpacity: 1,
				strokeWeight: 3,
				fillColor: '#BBD8E9',
				fillOpacity: 0.6,
			});


			google.maps.event.addListener(
			    marker,
			    'drag',
			    function(event) {
			    	//console.log(this);
	  		        //console.log( this.position.lat() + " ? " + this.position.lng() );
	  		        markers_city[this.placeId][0] = this.position.lat();
	  		        markers_city[this.placeId][1] = this.position.lng();

					polygon.setMap(null);

					polygon = map.drawPolygon({
						paths: markers_city, // pre-defined polygon shape
						strokeColor: '#BBD8E9',
						strokeOpacity: 1,
						strokeWeight: 3,
						fillColor: '#BBD8E9',
						fillOpacity: 0.6,
					});   
			});

		}
	});
 


	//Generamos el array de coordenadas
	var coordenadas_centrar = new Array();
 	coordenadas = coordenadas.split(";");
	for (var i = 0; i < coordenadas.length; i++) {
		coordenadas[i] = coordenadas[i].replace("[", "");
		coordenadas[i] = coordenadas[i].replace("]", "");
		coordenadas_centrar.push(coordenadas[i].split(","));
   	};	






	//Colocamos los marcadores
	var bounds = [];
	for (var i = 0; i < coordenadas_centrar.length; i++) {
		var latlng = new google.maps.LatLng(coordenadas_centrar[i][0], coordenadas_centrar[i][1]);
		bounds.push(latlng);
		var marker = map.addMarker({
			lat: coordenadas_centrar[i][0],
			lng: coordenadas_centrar[i][1],
			draggable: true,
			placeId: contador_marcadores++,
		});

		google.maps.event.addListener(
		    marker,
		    'drag',
		    function(event) {
		    	//console.log(this);
  		        //console.log( this.position.lat() + " ? " + this.position.lng() );
  		        markers_city[this.placeId][0] = this.position.lat();
  		        markers_city[this.placeId][1] = this.position.lng();

				polygon.setMap(null);

				polygon = map.drawPolygon({
					paths: markers_city, // pre-defined polygon shape
					strokeColor: '#BBD8E9',
					strokeOpacity: 1,
					strokeWeight: 3,
					fillColor: '#BBD8E9',
					fillOpacity: 0.6,
				});   
		});

		//Guardamos las coordenadas de forma local
		markers_city.push(new Array(  parseFloat(coordenadas_centrar[i][0])  ,  parseFloat(coordenadas_centrar[i][1])  ));
	}



	//Dibujamos el poligono
 	polygon = map.drawPolygon({
		paths: markers_city, // pre-defined polygon shape
		strokeColor: '#BBD8E9',
		strokeOpacity: 1,
		strokeWeight: 3,
		fillColor: '#BBD8E9',
		fillOpacity: 0.6
	});


	//Centramos el mapa donde esten los marcadores
	map.fitLatLngBounds(bounds);
}