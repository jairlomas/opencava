$(document).ready(function () {

	$('#archivo1').on('change', function(){
		$('#archivo').val($('#archivo1').val());
	});


	$(this).on('submit', 'form', function (e) {
		e.preventDefault();

		var route = $(this).attr('action');

		//Serialize form inputs
		var formData = new FormData();

		$.ajax({
			type       	: 'POST',
			data 		: new FormData(this),  
			contentType	: false,        
			cache 		: false,              
			processData : false,
			dataType   	: 'json',
			url        	: route,
			beforeSend 	: function () {
				blockForm();
			},
			error      : function (jqXHR) {
				unblockForm();

				if (jqXHR.status == 422)
				{
					displayErrors(jqXHR);
				}
				else
				{
					displayMessage("Experimentamos fallas técnicas, intentelo más tarde.");
				}
			},
			success    : function (response) {
				unblockForm();
				console.log( response );
				if (response.status == true){
					bootbox.alert(response.message, function () {
						window.location = response.route;
					});
				}
				else{
 					bootbox.alert(response.message, function () {});						
				}
			}
		});
	});
});

