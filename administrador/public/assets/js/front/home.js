var WINDOW_HEIGHT = 0;
var WINDOW_WIDTH = 0;

$(document).ready(function () {


    $('.index_slider').bxSlider({
        captions: true,
        controls: false,
    });

    WINDOW_WIDTH = $(window).width();

    /* CENTER VERTICAL SABER MAS */
    var text_slider_height_1 = $(".index_slider .slider_1 .content_slider_inner").outerHeight();
    var button_know_more_slider_height_1 = $(".index_slider .slider_1 .content_slider .know_more").outerHeight();
    var margin_slider_height = (text_slider_height_1 - button_know_more_slider_height_1) / 2;
    $(".index_slider .slider_1 .content_slider .know_more").css("margin-top", margin_slider_height);
    $(".index_slider .slider_1 .content_slider .know_more").css("margin-bottom", margin_slider_height);


    $(".index_slider .content_slider .know_more").hover(
        function () {
            var dom = $(this);
            dom.parent().find(".red_border").css("width", "100%");
        },
        function () {
            var dom = $(this);
            dom.parent().find(".red_border").css("width", "20%");
    });


    if(WINDOW_WIDTH > 414) {

        setTimeout(function () {
            // update_nuestros_productos();
            update_nuestros_servicios();
        }, 150);


        $(window).resize(function () {
            // update_nuestros_productos();
            update_nuestros_servicios();
        });

    }



    var slider_product_list = $('#products_list').bxSlider({
        captions: true,
        controls: false,
        adaptiveHeight: true,
        infiniteLoop: false,
        minSlides: 3,
        touchEnabled: false,
        pager: false,
    });



    /*$(this).on("click", ".process_to_list .service_singular .action_select", function (e) {
        e.preventDefault();

        var type = $(this).attr("type");
        show_specific_service_product(type);

        slider_product_list.goToNextSlide();
    })*/

    $(this).on("click", ".nagivate_products_list .selected_products button", function (e) {
        e.preventDefault();
        slider_product_list.goToNextSlide();
    })

    $(this).on("click", ".process_to_list .back_products", function (e) {
        e.preventDefault();
        slider_product_list.goToPrevSlide();
    })



    $(this).on("click", ".table_options_product_selected td:not(:first-child)", function (e) {
        e.preventDefault();

        var td_selected = $(this);
        if(td_selected.hasClass("selected")){
            td_selected.removeClass("selected");
        }else{
            td_selected.addClass("selected");
        }

    })


});


function show_specific_service_product(product) {

    if(product == ""){
        $(".services.services_bottom .service_singular").show();
    }else{
        $(".services.services_bottom .service_singular:not(."+product+")").hide();
    }

}


function update_nuestros_productos() {

    $(".services_top .service_singular .text_description").each(function(){
        $(this).css("height", "");
    });


    var max_height_description = 0;
    $(".services_top .action_select").each(function(){

        var height = $(this).outerHeight();
        // console.log(height);

        if( height > max_height_description ){
            max_height_description = height;
        }
    });
    $(".services_top .action_select").height(max_height_description);

}



function update_nuestros_servicios() {

    $(".services_bottom .service_singular .text_description").each(function(){
        $(this).css("height", "");
    });


    var max_height_description = 0;
    $(".services_bottom .service_singular .text_description").each(function(){

        var height = $(this).height();
        console.log(height);

        if( height > max_height_description ){
            max_height_description = height;
        }
    });
    $(".services_bottom .service_singular .text_description").height(max_height_description);

}