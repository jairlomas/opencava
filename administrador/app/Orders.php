<?php namespace OpenCava;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model {


	public function client(){
    	return $this->hasOne('OpenCava\Clients', "id", "client_id");
	}

	public function order_products(){
    	return $this->hasMany('OpenCava\OrdersProducts', "order_id", "id")->withTrashed();
	}


	public function credit_card(){
    	return $this->hasOne('OpenCava\CreditCard', "id", "credit_card_id");
	}


}
