<?php namespace OpenCava;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OpenCava\Library\URI;
use OpenCava\User;

class UserProfile extends Model {
	use SoftDeletes;  

	protected $dates = ['created_at', 'updated_at', 'deleted_at'];

	protected $fillable = ['name', 'permits'];

	/**
	 *
	 *
	 */
	public function scopeIndexTable($query, $request)
	{
		$query->where('name', '<>', 'Daemon');

		$response = [
			'lengthQuery' => $query->count()
		];

		if ($request->iDisplayStart != null && $request->iDisplayLength != "-1") {

			$query->take($request->iDisplayLength)->skip($request->iDisplayStart);
			$i = $request->iDisplayStart + 1;

		} else {

			$i = 1;

		}

		//Se ordena los elementos de la tabla
		$aColumns = [
			'name'
		];

		if ($request->iSortCol_0 > 1) {

			//return $request->iSortCol_0;

			for ($i = 0; $i < intval($request->iSortingCols); $i++) {

				$query->orderBy($aColumns[intval($request->iSortCol_.$i)], ($request->sSortDir_.$i === 'asc') ? 'asc' : 'desc');

			}

		}

		$response['length'] = $query->count();

		foreach ($query->get() as $item) {

			$response["data"][] = [
				$i++,
				$item->name,
				'<a href="perfil/editar?id='.$item->id.'" class="btn btn-success btn_editar" data-id="Mg==" data-name="Administrador"><i class="fa fa-pencil"></i> Editar</a>'." ".URI::printButton('delete', $item->id, $item->name),
			];

		}

		return $response;

	}

	/**
	 *
	 *
	 */
	public function users()
	{
		return $this->hasMany('OpenCava\User', 'user_profile_id', 'id');
	}

}
