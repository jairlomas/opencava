<?php

namespace OpenCava;

use Illuminate\Database\Eloquent\Model;


class Room extends Model
{

    function get_gallery(){
        return Gallery::where("room_id", $this->id)->get();
    }

    function get_characteristics(){
        return Characteristic::where("room_id", $this->id)->get();
    }

    function get_calendars(){
        $calendar = Calendar::where("room_id", $this->id)->get();
        $calendar_array = "";
        foreach ($calendar as $item){
            $calendar_array .= $item->available_date.",";
        }

        $calendar_array = substr($calendar_array, 0,strlen($calendar_array) - 1);

        return $calendar_array;
    }

    function get_deal(){
//        Deal::
        return $this->hasOne("OpenCava\Deal", "id", "deal_id");
    }

}
