<?php namespace OpenCava\Library;

use Illuminate\Http\Request;
use Route;
use OpenCava\SystemModule;
use OpenCava\Permission;
use OpenCava\Library\Pastora;

use Session;

class URI {

	public static $uri;

	public static $breadcrum = [];

	/**
	 * Metodo que devuelve los permisos que tiene el usuario
	 *
	 * @return JSON
	 */
	public static function breadcrums()
	{

		if (Route::current()->getUri() != 'dashboard') {

			self::$uri = Route::current()->getUri();

			self::cleanString();

			self::generateBread(self::$uri, true);

			return array_reverse(self::$breadcrum);

		} else {

			return [];

		}
		
	}

	public static function generateBread($module, $id = false)
	{
		if ($id === true) {

			$query = SystemModule::where('url', '=', $module)->first();

		} else {

			$query = SystemModule::find($module);

		}

		//Se agrega el breadcrumb a la variable
		self::$breadcrum[] = [
			'name' => $query->name,
			'url'  => $query->url,
			'icon' => $query->icon
		];

		if ($query->parent > 0) {

			self::generateBread($query->parent);

		}
	}

	/**
	 *
	 *
	 */
	public static function cleanString()
	{
		$inicio = strlen("dashboard/");

		if (self::isAlta()) {

			$final = strpos(self::$uri, "/alta");

		} else if (self::isEditar()) {

			$final = strpos(self::$uri, "/editar");

		} else if (self::isTable()) {

			$final = strpos(self::$uri, "/table");

		} else if (self::isDestroy()) {

			$final = strpos(self::$uri, "/borrar");

		} else {

			$final = false;

		}

		self::$uri = ($final === false) ? substr(self::$uri, $inicio) : substr(self::$uri, $inicio, $final - $inicio);
	}

	/**
	 *
	 *
	 */
	public static function isAlta()
	{
		if (is_null(self::$uri)) {

			self::setURI();

		}
		
		return (preg_match("/alta/", self::$uri)) ? true : false;
	}

	/**
	 *
	 *
	 */
	public static function isEditar()
	{
		if (is_null(self::$uri)) {

			self::setURI();

		}

		return (preg_match("/editar/", self::$uri)) ? true : false;
	}

	/**
	 *
	 *
	 */
	public static function isTable()
	{
		if (is_null(self::$uri)) {

			self::setURI();

		}

		return (preg_match("/table/", self::$uri)) ? true : false;
	}

	/**
	 *
	 *
	 */
	public static function isDestroy()
	{
		if (is_null(self::$uri)) {

			self::setURI();

		}

		return (preg_match("/borrar/", self::$uri)) ? true : false;
	}

	public static function setURI()
	{
//	    $url = Route::current()->getUri();
//	    dd($url);
		self::$uri = Route::current()->getUri();
	}

	/**
	 *
	 *
	 *
	 */
	public static function setRequestType()
	{

		if (self::isAlta()) {

			return "CREATE";

		} else if(self::isEditar()) {

			return "UPDATE";

		} else if(self::isDestroy()) {

			return "DELETE";

		} else {

			return "READ";
		}
			
	}

	/**
	 * Metodo que devuelve los permisos dados en el sistema
	 *
	 * @return Array.
	 */
	public static function permits()
	{

		if (!Session::has("systemPermissions")) {

			$permissions = Permission::all();

			$response = [];

			foreach ($permissions as $permission) {

				$response[$permission->name] = (int)$permission->bit;

			}

			Session::put('systemPermissions', $response);

		} 

		return Session::get('systemPermissions');

	}

	/**
	 * Metodo que devuele el modulo en el que se encuentra el usuario
	 *
	 * @return Object.
	 */
	public static function getModule()
	{
//		self::cleanString();
        $url = self::$uri;
        $url = explode("?", self::$uri);
        $url = $url[0];
        self::$uri = $url;
        return SystemModule::where('url', '=', self::$uri)->first();

	}

	/**
	 * Metodo que determina si un usuario tiene permiso para ejecutar alguna acción dentro del modulo dado
	 *
	 * @return bool.
	 */
	public static function border($userPermissions)
	{
		self::$uri = Route::current()->getUri();

		$requestType = self::setRequestType();

		$permissions = self::permits();

		$module = self::getModule();

 
		if (array_key_exists($module->id, $userPermissions)) {

			return ($userPermissions[$module->id] & $permissions[$requestType]) ? true : false;

		} else {

			return false;

		}

	}

	/**
	 *
	 *
	 *
	 */
	public static function printButton($buttonType, $id = "", $text = "", $properties = null)
	{

		//Se establece la uriprintCreateButton
		self::$uri = explode("dashboard/", Request::capture()->getUri())[1];

		switch ($buttonType) {
			case 'update':
				return self::printUpdateButton($id);
				break;
			case 'delete':
				return self::printDeleteButton($id, $text);
				break;
			case 'create':
				return self::printCreateButton($text);
				break;
			case 'read':
				return self::printReadButton($id, $text);
				break;
		}
	}

	public static function printUpdateButton($id)
	{
		$userPermission = Pastora::jsonToArray(Pastora::userProfile());
		$permissions = self::permits();
		$module = self::getModule();

		if (array_key_exists($module->id, $userPermission)) {

			if ($userPermission[$module->id] & $permissions["UPDATE"]) {

				return "<a href=".url('dashboard/'.self::$uri.'/editar/'.base64_encode($id))." class='btn btn-success'><i class='fa fa-pencil'></i> Editar</a>";

			}

		}
	}

	public static function printDeleteButton($id, $text)
	{
		$userPermission = Pastora::jsonToArray(Pastora::userProfile());
		$permissions = self::permits();
		$module = self::getModule();

		if (array_key_exists($module->id, $userPermission)) {

			if ($userPermission[$module->id] & $permissions["DELETE"]) {

				return "<a href=".url('dashboard/'.self::$uri.'/borrar/'.base64_encode($id))." class='btn btn-danger btnBorrarII btnDelete' data-id='".base64_encode($id)."' data-name='".$text."'><i class='fa fa-trash-o'></i> Borrar</a>";

			}

		}
	}

	public static function printCreateButton($text)
	{
		$userPermission = Pastora::jsonToArray(Pastora::userProfile());
		$permissions = self::permits();
		$module = self::getModule();

		//if($module != null) {
            if (array_key_exists($module->id, $userPermission)) {
                if ($userPermission[$module->id] & $permissions["CREATE"]) {
                    return "<a href=" . url('dashboard/' . self::$uri . "/alta") . " class='btn btn-primary'><i class='fa fa-pencil'></i> " . $text . "</a>";
                }
            }
        //}
	}

	public static function printReadButton($id, $text)
	{
		$userPermission = Pastora::jsonToArray(Pastora::userProfile());
		$permissions = self::permits();
		$module = self::getModule();

		if (array_key_exists($module->id, $userPermission)) {

			if ($userPermission[$module->id] & $permissions["CREATE"]) {

				return "<a href=".url('dashboard/'.self::$uri."/show/".base64_encode($id))." class='btn btn-primary'><i class='fa fa-eye'></i> ".$text."</a>";

			}

		}
	}






	public static function checkPermitionButton($button_type){
	
		self::$uri = Route::current()->getUri();

		$userPermission = Pastora::jsonToArray(Pastora::userProfile());
		$permissions = self::permits();
		$module = self::getModule();


		if (array_key_exists($module->id, $userPermission)) {

			switch ($button_type) {
				case 'edit':
						if ($userPermission[$module->id] & $permissions["UPDATE"]) {	return true;	}else{ return false; }
					break;
				case 'delete':
						if ($userPermission[$module->id] & $permissions["DELETE"]) {	return true;	}else{ return false; }
					break;
				case 'create':
						if ($userPermission[$module->id] & $permissions["CREATE"]) {	return true;	}else{ return false; }
					break;
				case 'view':
						if ($userPermission[$module->id] & $permissions["CREATE"]) {	return true;	}else{ return false; }
					break;					
				default:
					return true;
					break;
			}


		}else{
			return false;
		}
	}




	public static function checkPermitions(){
	
//		self::$uri = Route::current()->getUri();
        self::$uri = explode("dashboard/", Request::capture()->getUri())[1];

		$userPermission = Pastora::jsonToArray(Pastora::userProfile());
		$permissions = self::permits();
		$module = self::getModule();

		$permitions["edit"] = false;
		$permitions["delete"] = false;
		$permitions["create"] = false;
		$permitions["view"] = false;

		//if( $module != null ) {
            if (array_key_exists($module->id, $userPermission)) {

                if ($userPermission[$module->id] & $permissions["UPDATE"]) {
                    $permitions["edit"] = true;
                }

                if ($userPermission[$module->id] & $permissions["DELETE"]) {
                    $permitions["delete"] = true;
                }

                if ($userPermission[$module->id] & $permissions["CREATE"]) {
                    $permitions["create"] = true;
                }

                if ($userPermission[$module->id] & $permissions["CREATE"]) {
                    $permitions["view"] = true;
                }

            }
        //}

 		return $permitions;
	}



}