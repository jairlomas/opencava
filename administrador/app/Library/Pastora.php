<?php namespace OpenCava\Library;

use OpenCava\User;
use Permission;
use Auth;
use Session;
use OpenCava\SystemModule;
use OpenCava\Clients;

require_once("ApnsPHP/Autoload.php");

class Pastora {

	/**
	 * Metodo que devuelve los permisos que tiene el usuario
	 *
	 * @return JSON
	 */

	public static function in_object($value,$object) {
	   	if (is_object($object)) {
	     	foreach($object as $key => $item) {
	       		if ($value==$key) return true;
	     	}
	   	}
	   	return false;
 	}

	public static function userProfile()
	{

		return User::find(Auth::user()->id)->userProfile->permits;
	}

	/**
	 * Metodo que convierte un Json a Array
	 *
	 * @return Array
	 */
	public static function jsonToArray($json)
	{
		$json = (array)json_decode($json);

		$response = [];

		foreach ($json as $key => $value) {

			$response[(int)$key] = (int)$value;

		}

		return $response;
	}


	/**
	 *
	 *
	 */
	public function strongMeter($password)
	{
		$rank = 0;

		$rank += (strlen(trim($password)) > 7) ? 5 : 0;

		$rank += (strlen(trim($password)) > 11 && strlen(trim($password)) < 16) ? 10 : 0;

		$rank += (preg_match("/([A-Z])/", $password)) ? 10 : 0;

		$rank += (preg_match("/([0-9])/", $password)) ? 10 : 0;

	}

	/**
	 * Metodo que construye el arbol de modulos del sistema
	 *
	 * @var $parent integer. ID del papa del módulo
	 *
	 * @return array
	 */
	public static function moduleTree($parent = 0)
	{

		$response = [];

		foreach (SystemModule::where('parent', '=', $parent)->orderBy('order')->get() as $module) {

			$response[] = [
				'id'    => $module->id,
				'name'  => $module->name,
				'url'   => $module->url,
				'icon'  => $module->icon,
				'parent_as_child' => $module->parent_as_child,
				'child' => self::moduleTree($module->id)
			];

		}

		return $response;

	}


	/**
	 *
	 *
	 */
	public static function cleanPhone($phone)
	{
		$mapper = [
			'/',
			'(',
			')',
			'-',
			' '
		];

		foreach ($mapper as $key => $value) {

			$phone = str_replace($value, "", $phone);

		}

		return $phone;
	}



	public static function sendSmsToDelivery($phone_number, $message){

		$json["error"] = false;
		$json["message"] = "";

		$key = "0d7fffa3-4ce1-41c9-971a-fc2c43f6269e";    
		$secret = "NepQJ7gKjEOaA3dF3wcnLA=="; 
		$user = "application\\" . $key . ":" . $secret;    
		$message = array("message"=>$message);    
		$data = json_encode($message);    
		$ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);    
		curl_setopt($ch, CURLOPT_POST, true);    
		curl_setopt($ch, CURLOPT_USERPWD,$user);    
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);    
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));    
		$result = curl_exec($ch);    
		if(curl_errno($ch)) {    
		    //echo 'Curl error: ' . curl_error($ch); 
			$json["error"] = true;
			$json["message"] = "No se ha podido enviar el SMS";

		} else {    
			$json["error"] = false;
			$json["message"] = "Se ha enviado el SMS con exito";
		}   
		
		$json["result"] = $result;

		return $json;
	}



	public static function randomPassword() {
	    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}



	public static function sendSmsNotificationPromotionProducts($message_to_send){


		$clients_uuid = Clients::where("notification_promotion", 1)->get();
		foreach ($clients_uuid as $value) {

			$json["error"] = false;
			$json["message"] = "";

			$key = "0d7fffa3-4ce1-41c9-971a-fc2c43f6269e";    
			$secret = "NepQJ7gKjEOaA3dF3wcnLA=="; 
			$phone_number = "+".$value->cellphone_code.$value->cellphone;
			$user = "application\\" . $key . ":" . $secret;    
			$message = array("message"=>$message_to_send);    
			$data = json_encode($message);    
			$ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);    
			curl_setopt($ch, CURLOPT_POST, true);    
			curl_setopt($ch, CURLOPT_USERPWD,$user);    
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);    
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));    
			$result = curl_exec($ch);    
			if(curl_errno($ch)) {    
			    //echo 'Curl error: ' . curl_error($ch); 
				$json["error"] = true;
				$json["message"] = "No se ha podido enviar el SMS";

			} else {    
				$json["error"] = false;
				$json["message"] = "Se ha enviado el SMS con exito";
			}   
			
			$json["result"] = $result;


		}

		return $json;
 
	}



 
	public static function sendSmsOpenStore($message_to_send){


		$clients_uuid = Clients::where("notification_aperture", 1)->get();
		foreach ($clients_uuid as $value) {

			$json["error"] = false;
			$json["message"] = "";

			$key = "0d7fffa3-4ce1-41c9-971a-fc2c43f6269e";    
			$secret = "NepQJ7gKjEOaA3dF3wcnLA=="; 
			$phone_number = "+".$value->cellphone_code.$value->cellphone;
			$user = "application\\" . $key . ":" . $secret;    
			$message = array("message"=>$message_to_send);    
			$data = json_encode($message);    
			$ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_number);    
			curl_setopt($ch, CURLOPT_POST, true);    
			curl_setopt($ch, CURLOPT_USERPWD,$user);    
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);    
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);    
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));    
			$result = curl_exec($ch);    
			if(curl_errno($ch)) {    
			    //echo 'Curl error: ' . curl_error($ch); 
				$json["error"] = true;
				$json["message"] = "No se ha podido enviar el SMS";

			} else {    
				$json["error"] = false;
				$json["message"] = "Se ha enviado el SMS con exito";
			}   
			
			$json["result"] = $result;


		}

		return $json;
	}






















	public static function sendPushNotification($users, $message_text, $title){

        $singleID = array();
        foreach ($users as $user){
            $singleID[] = $user->uuid;
        }


        $API_ACCESS_KEY = "AAAAvWyHTUc:APA91bEjaOthTd4bDHfP27KFSiObmrAJD2W-k9wlg8hulsfXE7FKmqnfoY3769SRdhe4acEhhiZPP9fuVT539MX-AiCpjxDGUcDmxuPmqq6zxUXaO-IIkzkbq1MufGx599pz0OxtJPDc7I_n5CiXi5thTXbwz3nRXQ";

        $instructions = array();
        $instructions["TEXTO_NOTIFICACION"] = $message_text;

        $fcmMsg = array(
            'body'          => $message_text,
            'title'         => "Open Cava",
            'subtitle'      => "Open Cava",
            'sound'         => 'push_sound',
        );

        $fcmFields = array(
            'registration_ids' => $singleID,
            'priority' => 'high',
            'data'  => $instructions,
            'notification' => $fcmMsg,
        );

        $headers = array(
            'Authorization: key=' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );

        $result = json_decode($result);

        dd($result);

        return $result;

	}




	public static function sendPushIOS($users, $message_text, $title){

		
		// Notification for iOS SANDBOX
		/*
		$push = new \ApnsPHP_Push(
			\ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
			public_path('certs/WenderCastPush.pem')
		);
		*/


		// Notification for iOS PRODUCTION
		$push = new \ApnsPHP_Push(
			\ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
			public_path('certs/WenderCastPush.pem')
		);



		$push->connect();


		$hasNotification = false;
		foreach ($users as $value) {
			//$data = self::params($users);
			if( $value->os_type == "ios" ){
				$message = new \ApnsPHP_Message($value->uuid);
				$message->setText($message_text);
				$message->setSound();

				$iosData = array( 'data' => "test" );

				$message->setCustomProperty('data', $iosData);

				$push->add($message);
				$hasNotification = true;
			}
		}


		if($hasNotification){
			try {
				$push->send();
				$push->disconnect();
			} catch (Exception $e) {
				$e->getMessage();
			}

			$aErrorQueue = $push->getErrors();
			if (!empty($aErrorQueue)) {
				//print_r("Unable to send message");
			}
		}

	}


	public static function sendPushAndroid($user_notification, $message, $title){

		// prep the bundle
		$msg = array
		(
			'message' 	=> $message,
			'title'		=> $title,
			'subtitle'	=> '',
			'tickerText'=> '',
			'vibrate'	=> 1,
			'sound'		=> "push_sound",
			'icon'		=> 'icon',
		);


		$registrationIds = array();
		foreach($user_notification as $u){
			if($u->os_type == "android"){
				$registrationIds[] = $u->uuid;
			}
		}


		if(count($registrationIds) > 0){

			$fields = array(
				'registration_ids' 	=> $registrationIds,
				'data'				=> $msg
			);
			 
			$headers = array(
				'Authorization: key=AIzaSyDjv35yyrW2L2r4vu6KKJ8ciQEHeJbJ7z4',
				'Content-Type: application/json'
			);
			 
			$ch = curl_init();
			curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
			curl_setopt( $ch,CURLOPT_POST, true );
			curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
			curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );

			curl_setopt( $ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );

			curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );

	 
			$result = curl_exec($ch );
			curl_close( $ch );

		}

		print_r($result);

 	}



 






}