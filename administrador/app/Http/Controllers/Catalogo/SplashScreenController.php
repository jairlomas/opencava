<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\SpendingSend;
use OpenCava\Http\Requests\Catalogo\SplashScreenRequest;
use OpenCava\Http\Requests\Catalogo\SplashScreenEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
use OpenCava\SplashScreen;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class SplashScreenController extends Controller {

 
	public function index(Request $request)
	{
		$data = SplashScreen::get();
		return view('catalogo.splashscreen.index', [ 	'createButton' => Uri::printButton('create', '', 'Nuevo Splash'),
														'permitions' => Uri::checkPermitions(),
														'data'	=>	$data]);
	}

 
	public function create()
	{

		return view('catalogo.splashscreen.alta', ['buttonName' => 'Guardar']);
	}

  
	public function store(SplashScreenRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];
 
 		//Se agrega el nuevo registro a base de datos
		if(!is_dir("assets/splash")){
			mkdir("assets/splash", 0777);
		}

		$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

		$data = new SplashScreen();
		$data->name		= $request->nombre;
 		$data->path 	= "assets/splash/".$icon_name;

		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$request->file('splash')->move("assets/splash/", $icon_name);

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = SplashScreen::where("id", $id_decode)->first();
		return view('catalogo.splashscreen.editar', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, SplashScreenEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

			$row = SplashScreen::find($id);
			$row->name		= $request->nombre;

			if($request->hasFile('splash') != ""){
				//Eliminamos el icono si es que lo va a editar
				if(file_exists($row->icon)){
					unlink($row->icon);
				}

				$row->path 		= "assets/splash/".$icon_name;
				$request->file('splash')->move("assets/splash/", $icon_name);				
			}

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

  
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (SplashScreen::where('id', '=', base64_decode($id))->count() == 1) {
 
			//Se borra la cadena
			$data = SplashScreen::find(base64_decode($id));
			if ($data->delete()) {

				if(file_exists($data->path)){
					unlink($data->path);
				}

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el registro.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el registro.'
					]
				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el registro'
				]

			];

		}

			
		echo json_encode($response);
	}


	public function active($id)
	{
		$response;

		SplashScreen::where("id", "!=", 0)->update([ "active" => 0 ]);
		$data = SplashScreen::find(base64_decode($id));
		$data->active = 1;
		$data->save();
			
		echo json_encode(1);
	}








}
