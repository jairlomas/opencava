<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\Categories;
use OpenCava\Products;
use OpenCava\Http\Requests\Catalogo\CategoriasRequest;
use OpenCava\Http\Requests\Catalogo\CategoriasEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class CategoriasController extends Controller {

 
	public function index(Request $request)
	{

		$data = Categories::orderBy("order")->get();
		$count = Categories::count();
		return view('catalogo.categorias.index', [	'createButton' => Uri::printButton('create', '', 'Nueva Categoría'),
													'permitions' => Uri::checkPermitions(),
													'count'	=> $count,
													'data'	=>	$data]);
	}

 
	public function create()
	{
 
		return view('catalogo.categorias.alta', ['buttonName' => 'Guardar']);
	}

  
	public function store(CategoriasRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];


		//Se agrega el nuevo registro a base de datos
		if(!is_dir("assets/category_icon")){
			mkdir("assets/category_icon", 0777);
		}

		$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

		$data = new Categories();
		$data->name		=	$request->categoria;
		$data->type		=	$request->tipo;
		$data->icon 	=	"assets/category_icon/".$icon_name;
		$data->order 	=	Categories::count() + 1;

		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$request->file('icono')->move("assets/category_icon/", $icon_name);

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = Categories::where("id", $id_decode)->first();
		return view('catalogo.categorias.editar', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, CategoriasEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

			$row = Categories::find($id);
			$row->name = $request->categoria;
			$row->type = $request->tipo;

			if($request->hasFile('icono') != ""){
				//Eliminamos el icono si es que lo va a editar
				if(file_exists($row->icon)){
					unlink($row->icon);
				}

				$row->icon 		= "assets/category_icon/".$icon_name;
				$request->file('icono')->move("assets/category_icon/", $icon_name);				
			}

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

 

 
	public function destroy($id)
	{
		$response;

		//Verificamos que ningun producto tenga esa categoria
		if( Products::where("category_id", base64_decode($id))->count() <= 0 ){

			//Se busca el ID
			if (Categories::where('id', '=', base64_decode($id))->count() == 1) {

				
				//Ciclamos para que desde el actual en adelante, disminulla
				$current = $_GET["current"];
				$total = Categories::where("order",">", $_GET["current"])->count();

				for($i = 0; $i < $total; $i++){
					$to_update = $current;
					$current++;
					Categories::where("order", $current)->update(["order" => $to_update]);
				}
	 
				//Se borra la cadena
				if (Categories::destroy(base64_decode($id))) {

					$response = [
						'status' => true,
						'mensaje' => 'Se ha borrado con éxito el registro.'
					];

				} else {

					$response = [
						'status' => false,
						'errors'  => [
							'No se ha podido eliminar el registro.'
						]
					];

				}

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido borrar el registro'
					]

				];

			}
		}else{

			$response = [
				'status' => false,
				'message'  => [
					'No se ha podido borrar el registro, uno o más productos estan ligados a dicha categoría'
				]

			];	

		}
			
		echo json_encode($response);
	}




	public function subir(){

		$last = $_POST["current"] - 1;
		$current = $_POST["current"];

		Categories::where("order", $last)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $last ]);

		echo json_encode("1");
	}


	public function bajar(){

		$next = $_POST["current"] + 1;
		$current = $_POST["current"];

		Categories::where("order", $next)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $next ]);

		echo json_encode("1");
	}


}
