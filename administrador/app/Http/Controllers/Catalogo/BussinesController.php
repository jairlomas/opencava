<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\SpendingSend;
use OpenCava\Http\Requests\Catalogo\ReasonRequest;
use OpenCava\Http\Requests\Catalogo\BussinesEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
use OpenCava\Bussines;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class BussinesController extends Controller {

 
	public function index(Request $request)
	{
		$data = Bussines::first();
		$aperture_day_parse = explode(",", $data->aperture_day);
		$aperture_day_text = "";

		foreach($aperture_day_parse as $ap){

			if($ap == 1){
				$aperture_day_text .= "Lunes<br>";
			}
			if($ap == 2){
				$aperture_day_text .= "Martes<br>";
			}
			if($ap == 3){
				$aperture_day_text .= "Miércoles<br>";
			}
			if($ap == 4){
				$aperture_day_text .= "Jueves<br>";
			}
			if($ap == 5){
				$aperture_day_text .= "Viernes<br>";
			}
			if($ap == 6){
				$aperture_day_text .= "Sabado<br>";
			}
			if($ap == 7){
				$aperture_day_text .= "Domingo<br>";
			}

		}

		$data->aperture_day = $aperture_day_text;
		$data->aperture_time = date("H:i", strtotime($data->aperture_time) );
		$data->close_time = date("H:i", strtotime($data->close_time) );




		return view('catalogo.negocio.index', [			'createButton' => '',
														'permitions' => Uri::checkPermitions(),
														'data'	=>	$data]);
	}

 
	public function create()
	{

		return view('catalogo.negocio.alta', ['buttonName' => 'Guardar']);
	}

  
	public function store(ReasonRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];
 
		$data = new Reason();
		$data->reason			=	$request->motivo;
 
		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = Bussines::where("id", $id_decode)->first();
		$dia_apertura = explode(",", $data->aperture_day);

		$data->aperture_time = date("H:i", strtotime($data->aperture_time) );

		return view('catalogo.negocio.editar', ['id' => $id, "data" =>  $data, "dia_apertura" => $dia_apertura]);
	}

 
	public function update($id, BussinesEditRequest $request){
		$id = base64_decode($id);


		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$row = Bussines::find($id);
			$row->aperture_time = $request->horario_apertura.":00";
			$row->close_time = $request->horario_cierre.":00";

			$apertura = "";
			for($i = 0; $i < count($request->dia_apertura); $i++){
				if($i != (count($request->dia_apertura) - 1)){
					$apertura .= $request->dia_apertura[$i].",";					
				}else{
					$apertura .= $request->dia_apertura[$i];					
				}
			}

			$row->aperture_day = $apertura;


			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

  
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (Reason::where('id', '=', base64_decode($id))->count() == 1) {
 
			//Se borra la cadena
			if (Reason::destroy(base64_decode($id))) {

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el registro.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el registro.'
					]
				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el registro'
				]

			];

		}

			
		echo json_encode($response);
	}

}
