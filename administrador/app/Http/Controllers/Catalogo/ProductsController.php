<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\Products;
use OpenCava\Categories;
use OpenCava\Http\Requests\Catalogo\ProductsRequest;
use OpenCava\Http\Requests\Catalogo\ProductsEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class ProductsController extends Controller {

 
	public function index(Request $request)
	{

		$data = Products::get();
		return view('catalogo.productos.index', [		'createButton' => Uri::printButton('create', '', 'Nuevo Producto'),
														'permitions' => Uri::checkPermitions(),
														'data'	=>	$data,
														 ]);
	}

 
	public function create()
	{
		$categories = Categories::get();
		return view('catalogo.productos.alta', ['buttonName' => 'Guardar',
												'categories' => $categories ]);
	}

  
	public function store(ProductsRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];


		//Se agrega el nuevo registro a base de datos
		if(!is_dir("assets/product_icon")){
			mkdir("assets/product_icon", 0777);
		}

		$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

		$data = new Products();
		$data->name			= $request->nombre;
		$data->price		= $request->precio;
		$data->icon 		= "assets/product_icon/".$icon_name;
		$data->category_id	= $request->categoria;
		$data->type			= $request->tipo;
		$data->cantidad		= $request->cantidad;
		$data->is_drink		= ( ($request->bebida) ? 1 : 0);

		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$request->file('icono')->move("assets/product_icon/", $icon_name);

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = Products::where("id", $id_decode)->first();
		$categories = Categories::get();		
		return view('catalogo.productos.editar', ['id' => $id, "data" =>  $data, 'categories' => $categories]);
	}

 
	public function update($id, ProductsEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

			$row = Products::find($id);
			$row->name			= $request->nombre;
			$row->price			= $request->precio;
			$row->category_id	= $request->categoria;
			$row->type			= $request->tipo;
			$row->cantidad		= $request->cantidad;
	 		$row->is_drink		= ( ($request->bebida) ? 1 : 0);

			if($request->hasFile('icono') != ""){
				//Eliminamos el icono si es que lo va a editar
				if(file_exists($row->icon)){
					unlink($row->icon);
				}

				$row->icon 		= "assets/product_icon/".$icon_name;
				$request->file('icono')->move("assets/product_icon/", $icon_name);				
			}

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

 

 
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (Products::where('id', '=', base64_decode($id))->count() == 1) {
 
			//Se borra la cadena
			if (Products::destroy(base64_decode($id))) {

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el registro.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el registro.'
					]
				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el registro'
				]

			];

		}

			
		echo json_encode($response);
	}




	public function subir(){

		$last = $_POST["current"] - 1;
		$current = $_POST["current"];

		Categories::where("order", $last)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $last ]);

		echo json_encode("1");
	}


	public function bajar(){

		$next = $_POST["current"] + 1;
		$current = $_POST["current"];

		Categories::where("order", $next)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $next ]);

		echo json_encode("1");
	}

}
