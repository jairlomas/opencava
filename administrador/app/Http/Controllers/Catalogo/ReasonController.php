<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\SpendingSend;
use OpenCava\Http\Requests\Catalogo\ReasonRequest;
use OpenCava\Http\Requests\Catalogo\ReasonEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
use OpenCava\Reason;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class ReasonController extends Controller {

 
	public function index(Request $request)
	{
		$data = Reason::get();
		return view('catalogo.motivos.index', [			'createButton' => Uri::printButton('create', '', 'Nuevo Movito'),
														'permitions' => Uri::checkPermitions(),
														'data'	=>	$data]);
	}

 
	public function create()
	{

		return view('catalogo.motivos.alta', ['buttonName' => 'Guardar']);
	}

  
	public function store(ReasonRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];
 
		$data = new Reason();
		$data->reason			=	$request->motivo;
 
		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = Reason::where("id", $id_decode)->first();
		return view('catalogo.motivos.editar', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, ReasonEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$row = Reason::find($id);
			$row->reason			=	$request->motivo;


			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

  
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (Reason::where('id', '=', base64_decode($id))->count() == 1) {
 
			//Se borra la cadena
			if (Reason::destroy(base64_decode($id))) {

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el registro.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el registro.'
					]
				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el registro'
				]

			];

		}

			
		echo json_encode($response);
	}



 

}
