<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\SpendingSend;
use OpenCava\Http\Requests\Catalogo\SpendingSendRequest;
use OpenCava\Http\Requests\Catalogo\SpendingSendEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class SpendingSendController extends Controller {

 
	public function index(Request $request)
	{

		$data = SpendingSend::get();
		return view('catalogo.gasto_envio.index', [	'createButton' => Uri::printButton('create', '', 'Nuevo Gasto de Envío'),
														'permitions' => Uri::checkPermitions(),
														'data'	=>	$data]);
	}

 
	public function create()
	{

		return view('catalogo.gasto_envio.alta', ['buttonName' => 'Guardar']);
	}

  
	public function store(SpendingSendRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];
 
		$data = new SpendingSend();
		$data->name			=	$request->nombre;
		$data->price		=	$request->costo;
		$data->geofence 	=	$request->geocerca;
 
		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = SpendingSend::where("id", $id_decode)->first();
		return view('catalogo.gasto_envio.editar', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, SpendingSendEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$row = SpendingSend::find($id);
			$row->name		=	$request->nombre;
			$row->price		=	$request->costo;
			$row->geofence 	=	$request->geocerca;

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

 
	public function getcoordinates(){
		$data = SpendingSend::find( base64_decode($_POST["id"]) )->geofence;
		echo json_encode($data);
	}

 
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (SpendingSend::where('id', '=', base64_decode($id))->count() == 1) {
 
			//Se borra la cadena
			if (SpendingSend::destroy(base64_decode($id))) {

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el registro.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el registro.'
					]
				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el registro'
				]

			];

		}

			
		echo json_encode($response);
	}




	public function subir(){

		$last = $_POST["current"] - 1;
		$current = $_POST["current"];

		Categories::where("order", $last)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $last ]);

		echo json_encode("1");
	}


	public function bajar(){

		$next = $_POST["current"] + 1;
		$current = $_POST["current"];

		Categories::where("order", $next)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $next ]);

		echo json_encode("1");
	}

}
