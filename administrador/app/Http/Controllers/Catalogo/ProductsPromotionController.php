<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\ProductsPromotion;
use OpenCava\Categories;
use OpenCava\Clients;
use OpenCava\Http\Requests\Catalogo\ProductsPromotionRequest;
use OpenCava\Http\Requests\Catalogo\ProductsPromotionEditRequest;
use OpenCava\Library\URI;
use OpenCava\Library\Pastora;
use OpenCava\User;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class ProductsPromotionController extends Controller {

 
	public function index(Request $request)
	{

		$data = ProductsPromotion::get();
		return view('catalogo.productos_promocion.index', [		'createButton' => Uri::printButton('create', '', 'Nueva Promoción'),
																'permitions' => Uri::checkPermitions(),
																'data'	=>	$data,
																 ]);
	}

 
	public function create()
	{
		$categories = Categories::get();
		return view('catalogo.productos_promocion.alta', ['buttonName' => 'Guardar',
														  'categories' => $categories ]);
	}

  
	public function store(Request $request){

        $response = [
			'estatus' => true,
			'mensaje' => ''
		];


		//Se agrega el nuevo registro a base de datos
		if(!is_dir("assets/product_promotion_icon")){
			mkdir("assets/product_promotion_icon", 0777);
		}

		$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

		$data = new ProductsPromotion();
		$data->name			= $request->nombre;
		$data->price		= $request->precio;
		$data->icon 		= "assets/product_promotion_icon/".$icon_name;
		$data->type			= "";//$request->tipo;
		$data->cantidad		= $request->cantidad;
		$data->is_drink		= ( ($request->bebida) ? 1 : 0);


		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$request->file('icono')->move("assets/product_promotion_icon/", $icon_name);

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

			//$this->sendNotificationSendPush("Nueva promoción para ".$data->name.", ingresa a la app para más información.");

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 	public function sendNotificationSendPush($message){


		$notification = array();

        $clients_uuid = Clients::where("notification_promotion", 1)->get();
        //$clients_uuid = Clients::where("id", 62)->get();
		foreach ($clients_uuid as $value) {
			if($value->os_type != ""){
				if($value->uuid != ""){
					$notification[] = $value;
				}
			}
		}

        Pastora::sendPushNotification( $notification, $message, "Promoción");
	}





 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = ProductsPromotion::where("id", $id_decode)->first();
		return view('catalogo.productos_promocion.editar', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, ProductsPromotionEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$icon_name = md5( date("Y-m-d-H-i-s") ).".png";

			$row = ProductsPromotion::find($id);
			$row->name			= $request->nombre;
			$row->price			= $request->precio;
			$row->type			= "";//$request->tipo;
			$row->cantidad		= $request->cantidad;
	 		$row->is_drink		= ( ($request->bebida) ? 1 : 0);

			 
			if($request->hasFile('icono') != ""){
				//Eliminamos el icono si es que lo va a editar
				if(file_exists($row->icon)){
					unlink($row->icon);
				}

				$row->icon 		= "assets/product_promotion_icon/".$icon_name;
				$request->file('icono')->move("assets/product_promotion_icon/", $icon_name);				
			}

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

 

 
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (ProductsPromotion::where('id', '=', base64_decode($id))->count() == 1) {
 
			//Se borra la cadena
			if (ProductsPromotion::destroy(base64_decode($id))) {

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el registro.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el registro.'
					]
				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el registro'
				]

			];

		}

			
		echo json_encode($response);
	}




	public function subir(){

		$last = $_POST["current"] - 1;
		$current = $_POST["current"];

		Categories::where("order", $last)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $last ]);

		echo json_encode("1");
	}


	public function bajar(){

		$next = $_POST["current"] + 1;
		$current = $_POST["current"];

		Categories::where("order", $next)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $next ]);

		echo json_encode("1");
	}

}
