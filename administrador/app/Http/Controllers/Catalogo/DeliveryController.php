<?php namespace OpenCava\Http\Controllers\Catalogo;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\Delivery;
use OpenCava\Http\Requests\Catalogo\DeliveryRequest;
use OpenCava\Http\Requests\Catalogo\DeliveryEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
use OpenCava\Orders;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class DeliveryController extends Controller {

 
	public function index(Request $request)
	{

		$data = Delivery::get();
		return view('catalogo.repartidores.index', [	'createButton' => Uri::printButton('create', '', 'Nuevo Repartidor'),
														'permitions' => Uri::checkPermitions(),
														'data'	=>	$data]);
	}

 
	public function create()
	{
 
		return view('catalogo.repartidores.alta', ['buttonName' => 'Guardar']);
	}

  
	public function store(DeliveryRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];
 
		$data = new Delivery();
		$data->name		=	$request->nombre_completo;
		$data->phone	=	$request->telefono;
		$data->email 	=	$request->email;
 
		//Se verifica si se a guardado el nuevo usuario
		if ($data->save()) {

			$response = [
				'estatus' => true,
				'mensaje' => 'Se ha guardado con éxito el registro.'
			];

		} else {

			$response = [
				'estatus' => false,
				'errors'  => [
					'No se ha podido guardar el registro'
				]
			];

		}

 

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = Delivery::where("id", $id_decode)->first();
		return view('catalogo.repartidores.editar', ['id' => $id, "data" =>  $data]);
	}

 
	public function update($id, DeliveryEditRequest $request){
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];
 
		if($response["estatus"]){

			$row = Delivery::find($id);
			$row->name		=	$request->nombre_completo;
			$row->phone	=	$request->telefono;
			$row->email 	=	$request->email;

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

		return response()->json($response);
	}

 

 
	public function destroy($id)
	{
		$response;

		//Verificamos si el repartidor no esta asignado a un pedido
		if(Orders::where("delivery_id", base64_decode($id))->count() <= 0){

			//Se busca el ID
			if (Delivery::where('id', '=', base64_decode($id))->count() == 1) {
	 
				//Se borra la cadena
				if (Delivery::destroy(base64_decode($id))) {

					$response = [
						'status' => true,
						'mensaje' => 'Se ha borrado con éxito el registro.'
					];

				} else {

					$response = [
						'status' => false,
						'errors'  => [
							'No se ha podido eliminar el registro.'
						]
					];

				}

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido borrar el registro'
					]

				];

			}

		}else{

			$response = [
				'status' => false,
				'message'  => 'No se ha podido borrar el registro, el repartidor tiene asignado al menos un pedido.'		
			];

		}



			
		echo json_encode($response);
	}




	public function subir(){

		$last = $_POST["current"] - 1;
		$current = $_POST["current"];

		Categories::where("order", $last)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $last ]);

		echo json_encode("1");
	}


	public function bajar(){

		$next = $_POST["current"] + 1;
		$current = $_POST["current"];

		Categories::where("order", $next)->update([ "order" => $current ]);
		Categories::where("id", $_POST["id"])->update([ "order" => $next ]);

		echo json_encode("1");
	}

}
