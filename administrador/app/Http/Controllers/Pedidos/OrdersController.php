<?php namespace OpenCava\Http\Controllers\Pedidos;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\Categories;
use OpenCava\Delivery;
use OpenCava\Orders;
use OpenCava\Http\Requests\Catalogo\CategoriasRequest;
use OpenCava\Http\Requests\Catalogo\CategoriasEditRequest;
use OpenCava\Library\URI;
use OpenCava\User; 
use OpenCava\Clients; 


use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;
use Carbon\Carbon;
use OpenCava\Library\Pastora;

class OrdersController extends Controller {

 
	public function index(Request $request)
    {

//        $data_active = Orders::whereRaw(' ((status != "cancelado" AND payed = 1) OR (purchase_type = "efectivo" and payed = 0 and status != "cancelado") OR (purchase_type = "solicitar_terminal" and payed = 0 and status != "cancelado") ')->orderBy("id", "desc")->get();
        //$data_active = Orders::whereRaw(' status != "cancelado" AND status != "entregado" ')->orderBy("id", "desc")->get();
        $data_active = Orders::where("status", "!=", "cancelado")->where("status", "!=", "entregado")->orderBy("id", "desc")->get();


		$data_active = $this->setTimeRestDelivery($data_active);

		$data_inactive = Orders::where("status", "cancelado")->orderBy("id", "desc")->get();
		$data_inactive = $this->setTimeRestDelivery($data_inactive);

		return view('pedidos.index', [		'createButton' => "",
											'permitions' => Uri::checkPermitions(),
											'data_active'	=>	$data_active,
											'data_inactive'	=>	$data_inactive,
									  ]);
	}

	public function setTimeRestDelivery($data){

		foreach($data as $key => $d){
			$data[$key]->remaining_time = $this->differenceTime( date("Y-m-d H:i:s") , $data[$key]->end_delivery_time );
		}

		return $data;
	}

	public function differenceTime($startime, $endtime){

		$startime = new Carbon($startime);
		$endtime = new Carbon($endtime);

		if($startime < $endtime){

			$difference = $startime->diffInSeconds($endtime);
			$difference = round($difference);
			$difference = sprintf('%02d:%02d:%02d', ($difference/3600),($difference/60%60), $difference%60);

			return $difference;

		}else{
			return "00:00:00";
		}
	}

	public function ver($id){

		$data = Orders::where("id",base64_decode($id))->get();
		$data = $this->setTimeRestDelivery($data);

		return view('pedidos.view', [		'createButton' => "",
											'data'	=>	$data[0]
									  ]);	
	}

  
	public function asignar($id){

		$data = Delivery::get();

		return view('pedidos.editar', [		'id' => $id,
											'createButton' => "",
											'data'	=>	$data,
									  ]);
	}


	public function set_delivery(){                                                                                                                                     

		/*
			pedido_id
			repartidor_id
		*/

		$response = [
			'status' => true,
			'message' => ''
		];

		$sms_texto = "";

		$data = Orders::find(base64_decode($_POST["pedido_id"]));


		//Ponemos la fecha de finalización para que arranque el tiempo de entrega
		$end_delivery_time = new Carbon( date("Y-m-d H:i:s") );
		$delivery_time = $data->delivery_time;
		$delivery_time = explode(":", $delivery_time);

		$end_delivery_time->addHours($delivery_time[0]);
		$end_delivery_time->addMinutes($delivery_time[1]);
		$end_delivery_time->addSecond($delivery_time[2]);

		$data->delivery_id = $_POST["repartidor_id"];
		$data->status = "enviado";

		if($data->save()){

			//Enviamos el mensaje de texto
			$data_delivery = Delivery::find($_POST["repartidor_id"]);
			$sms_texto = "Direccion: " . $data->address." - Codigo: ".$data->code;
			$sms_response = Pastora::sendSmsToDelivery($data_delivery->phone, $sms_texto);

			//Si no hubo error
			if(!$sms_response["error"]){
			
				$response = [
					'status' => true,
					'message' => 'Se ha asignado el repartidor a el pedido.<br>Se ha enviado el SMS con exito.',
					"sms_texto" => $sms_texto,
					"sinch_response" => $sms_response,
				];

			}else{

				$response = [
					'status' => false,
					"sinch_response" => $sms_response,					
					'message' => 'Se ha asignado el repartidor a el pedido.<br>No se ha enviado el SMS. Asigne nuevamente el pedido a el repartidor para volver a enviar el SMS.'
				];

			}

		}else{

			$response = [
				'status' => false,
				'message' => 'Ocurrio un error al intentar asignar el repartidor al pedido, intentelo nuevamente.'
			];

		}

		echo json_encode($response);
	}


	public function set_entregado(){

		$response = [
			'status' => true,
			'message' => ''
		];

		$data = Orders::find(base64_decode($_POST["id"]));
		$data->status = "entregado";

		if($data->purchase_type == "efectivo"){
			$data->payed = 1;
		}

		$client = Clients::find($data->client_id);
		$client->consecutive_order_cancel = 0;

		if($data->save()){

			$client->save();
			
			$response = [
				'status' => true,
				'message' => 'Se ha cambiado el pedido a entregado con exito'
			];

		}else{

			$response = [
				'status' => false,
				'message' => 'Ocurrio un error al intentar cambiar el pedido a entregado, intentelo nuevamente.'
			];

		}

		echo json_encode($response);
	}



	public function set_cancelado(){

		$response = [
			'status' => true,
			'message' => ''
		];

		$data = Orders::find(base64_decode($_POST["id"]));
		//$data->end_delivery_time = "0000-00-00 00:00:00";

		$client = Clients::find($data->client_id);
		$client->increment("consecutive_order_cancel");

		if($data->status == "cancelado"){
			$response = [
				'status' => true,
				'message' => 'El pedido ya se encuentra cancelado'
			];
		}else if($data->delivery_id == 0){

			$data->status = "cancelado";
			if($data->save()){

				$client->save();

				$response = [
					'status' => true,
					'message' => 'Se ha cancelado el pedido con exito'
				];	

				$this->sendNotificationSendPush(base64_decode($_POST["id"]));

			}else{
				$response = [
					'status' => false,
					'message' => 'Ocurrio un error al intentar cancelar el pedido, intentelo nuevamente.'
				];				
			}

		}else{

			$data->status = "cancelado";
			if($data->save()){

				$client->save();

				//Sacamos la informacion del repartidor asignado para enviar el sms
				$data_delivery = Delivery::find($data->delivery_id);	
				$texto_sms = "El pedido ha sido cancelado - Código: ".$data->code;		
				$sms_response = Pastora::sendSmsToDelivery($data_delivery->phone, $texto_sms);

				//Si no hubo error
				if(!$sms_response["error"]){
				
					$response = [
						'status' => true,
						'message' => 'Se ha cancelado el pedido con exito.<br>Se ha enviado el SMS con exito.',
						"texto_sms" => $texto_sms
					];

					$this->sendNotificationSendPush(base64_decode($_POST["id"]));

				}else{

					$response = [
						'status' => true,
						'message' => 'Se ha cancelado el pedido con exito.<br>No se ha enviado el SMS. Cancele nuevamente el pedido para volver a enviar el SMS.'
					];

				}





			}else{

				$response = [
					'status' => false,
					'message' => 'Ocurrio un error al intentar cambiar el pedido a cancelado, intentelo nuevamente.'
				];

			}

		}


		echo json_encode($response);
	}



	public function set_notificar(){
		$response = [
			'status' => true,
			'message' => ''
		];

		$data = Orders::find(base64_decode($_POST["id"]));
		$data->status = "notificado";

		//$client = Clients::find($data->client_id);
		//$client->consecutive_order_cancel = 0;

		if($data->save()){

			//$client->save();
			//Enviamos notificacion push			
			$this->sendNotificationSendPushEntregado( base64_decode($_POST["id"]) );

			$response = [
				'status' => true,
				'message' => 'Usuario notificado con éxito'
			];

		}else{

			$response = [
				'status' => false,
				'message' => 'Ocurrio un error al intentar cambiar el pedido a entregado, intentelo nuevamente.'
			];

		}

		echo json_encode($response);

	}



	public function sendNotificationSendPushEntregado($pedido_id){

		$data = Orders::find($pedido_id);
		$notificacion = array();
		$notification = Clients::where("id", $data->client->id)->get();
		Pastora::sendPushNotification( $notification, "¡Open Cava ha llegado!", "");

	}


	public function sendNotificationSendPush($pedido_id){

		$data = Orders::find($pedido_id);
		$notificacion = array();	
		$notification = Clients::where("id", $data->client->id)->get();
		Pastora::sendPushNotification( $notification, "Tu pedido con el codigo ".$data->code." ha sido cancelado", "Pedido cancelado");

	}













    public function index_historical(Request $request){

	    //        $data_active = Orders::whereRaw(' ((status != "cancelado" AND payed = 1) OR (purchase_type = "efectivo" and payed = 0 and status != "cancelado") OR (purchase_type = "solicitar_terminal" and payed = 0 and status != "cancelado") ')->orderBy("id", "desc")->get();
        $data_active = Orders::whereRaw(' status = "entregado" ')->orderBy("id", "desc")->get();

        $data_active = $this->setTimeRestDelivery($data_active);

        $data_inactive = Orders::where("status", "cancelado")->orderBy("id", "desc")->get();
        $data_inactive = $this->setTimeRestDelivery($data_inactive);

        return view('pedidos_historicos.index', [		'createButton' => "",
            'permitions' => Uri::checkPermitions(),
            'data_active'	=>	$data_active,
            'data_inactive'	=>	$data_inactive,
        ]);
    }

    public function ver_historical($id){

        $data = Orders::where("id",base64_decode($id))->get();
        $data = $this->setTimeRestDelivery($data);

        return view('pedidos_historicos.view', [		'createButton' => "",
            'data'	=>	$data[0]
        ]);
    }

























}


