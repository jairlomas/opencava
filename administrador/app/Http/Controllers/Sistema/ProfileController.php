<?php namespace OpenCava\Http\Controllers\Sistema;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\Library\URI;
use OpenCava\UserProfile;
use OpenCava\User;
use OpenCava\SystemModule;
use OpenCava\Library\Pastora;
use OpenCava\Http\Requests\Sistema\Perfil\ProfileRequest;
 
use Illuminate\Http\Request;
use Auth;
use DB;

class ProfileController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = UserProfile::where("id", "!=", 1)->get();
		return view('sistema.perfil.index', [	'createButton' => Uri::printButton('create', '', 'Nuevo Perfil'),
												'permitions' => Uri::checkPermitions(),
												'data' => $data] );
	}

  
	public function create()
	{

		return view('sistema.perfil.alta', ['modules' => Pastora::moduleTree()]);
	}

 
	public function store(ProfileRequest $request)
	{

		$permissions = [
			'READ'   => 1,
			'CREATE' => 2,
			'UPDATE' => 4,
			'DELETE' => 8
		];

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha guardado con éxito el nuevo perfil.'
		];

		$json = [];

		foreach (array_keys($request->module) as $key => $value) {

			$json[$value] = 0;

			foreach ($request->module[$value] as $clave => $valor) {

				$json[$value] =  $json[$value] + $permissions[$valor];

			}

			//Se revisa si tiene papa el modulo
			$parent = SystemModule::where('id', '=', $value)->where('parent', '<>', 0);
			if ($parent->count() > 0) {
					
				$parent = $parent->first();
				if (!array_key_exists($parent->parent, $json)) {
					$json[$parent->parent] = 15;

				}

			}

		}
 

		DB::table("user_profiles")->insert( [
												'name'    	 => $request->nombre,
												'permits' 	 => json_encode($json),
												'deleted_at'  => null,
												'created_at' => date("Y-m-d H:i:s")
											] );

		echo json_encode($response);
	}





	public function editar(Request $request)
	{

		$permissions = [
			'READ'   => 1,
			'CREATE' => 2,
			'UPDATE' => 4,
			'DELETE' => 8
		];

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha guardado con éxito el nuevo perfil.'
		];

		$json = [];

		foreach (array_keys($request->module) as $key => $value) {

			$json[$value] = 0;

			foreach ($request->module[$value] as $clave => $valor) {

				$json[$value] =  $json[$value] + $permissions[$valor];

			}

			//Se revisa si tiene papa el modulo
			$parent = SystemModule::where('id', '=', $value)->where('parent', '<>', 0);
			if ($parent->count() > 0) {

				$parent = $parent->first();
				if (!array_key_exists($parent->parent, $json)) {

					$json[$parent->parent] = 15;

				}

			}

		}

		$valor_anterior = $this->getProfileDataLog($request->id);

		DB::table("user_profiles")->where( "id", $request->id)->update([ "name" => $request->nombre, 
																		 "permits" => json_encode($json),
																		 "deleted_at" => null
																		 /*, 'reportes' 	=> ( ($request->visualizar_reportes == "on") ? 1 : 0)*/ ]);
		
		$valor_nuevo = $this->getProfileDataLog($request->id);

		return $response;
	}


	public function getProfileDataLog($id){

		$data = DB::table("user_profiles")->where("id", $id)->get();

		return "Nombre: " . $data[0]->name;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit_show($id)
	{
		$id = base64_decode($id);
		return view('sistema.perfil.cambios', ['modules' => Pastora::moduleTree(), 'id' => $id]);

	}

	public function view($id)
	{
		$id = base64_decode($id);
		return view('sistema.perfil.ver', ['modules' => Pastora::moduleTree(), 'id' => $id]);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$response;


		//Verificamos que no exista un usuario con ese perfil
		if( User::where("user_profile_id", base64_decode($id))->count() <= 0 ){

			//Se busca el ID
			if (UserProfile::where('id', '=', base64_decode($id))->count() === 1) {

				$valor_nuevo = $this->getProfileDataLog( base64_decode($id) );


				if ( UserProfile::find(base64_decode($id))->delete() ) {

					$response = [
						'status' => true,
						'mensaje' => 'Se ha borrado con éxito el perfil de usuario.'
					];

				} else {

					$response = [
						'status' => false,
						'errors'  => [
							'No se ha podido eliminar el perfil de usuario.'
						]

					];

				}

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido borrar el elemento'
					]

				];

			}

		} else {

			$response = [
				'status' => false,
				'message'  => 'No se ha podido borrar el elemento, un o más usuarios pertenecen al perfil.'
			];

		}
			
		echo json_encode($response);
	}


	public function getData(){

		$data = DB::table("user_profiles")->where("id", $_GET["id"])->get();
		echo json_encode($data[0]);

	}


}
