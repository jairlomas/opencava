<?php namespace OpenCava\Http\Controllers\Sistema;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\Http\Requests\Sistema\Usuarios\AddUserRequest;
use OpenCava\Http\Requests\Sistema\Usuarios\UpdateUserRequest;
use OpenCava\Library\URI;
use OpenCava\User;

use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$data = User::where("id", "!=", 1)->get();
		return view('sistema.usuarios.index', [	'createButton' => Uri::printButton('create', '', 'Nuevo Usuario'),
												'permitions' => Uri::checkPermitions(),
												'data'	=>	$data]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$response = [];

		foreach (UserProfile::where('id', '<>', 1)->get() as $profile) {

			$response[$profile->id] = $profile->name;

		}

		return view('sistema.usuarios.alta', ['userProfile' => $response, 'buttonName' => 'Guardar']);
	}

  
	public function store(AddUserRequest $request){

		$response = [
			'estatus' => true,
			'mensaje' => ''
		];

		if($request->contrasena != $request->repetir_contrasena){
			$response = [
				'estatus' => false,
				'errors'  => [
					'Las contraseñas no coinciden'
				]
			];
		}


		//Verificamos que tenga al menos una letra en mayuscula
		if($response["estatus"]){
			$pass = str_split( $request->contrasena );
			$con_mayusculas = 0;
			for($i = 0; $i < count($pass); $i++){
				if( ctype_upper($pass[$i]) ){
					$con_mayusculas++;
				}
			}

			if( $con_mayusculas == 0 ){
				$response = [
					'estatus' => false,
					'errors'  => [
						'La contraseña debe de contener al menos una letra mayuscula'
					]
				];	
			}

		}


		//Verificamos que tenga al menos un numero
		if($response["estatus"]){
			$pass = str_split( $request->contrasena );
			$con_numero = 0;
			for($i = 0; $i < count($pass); $i++){
				if( is_numeric ($pass[$i]) ){
					$con_numero++;
				}
			}

			if( $con_numero == 0 ){
				$response = [
					'estatus' => false,
					'errors'  => [
						'La contraseña debe de contener al menos un numero'
					]
				];	
			}

		}



		//Se agrega el nuevo registro a base de datos
		if($response["estatus"]){

			$data = new User();
			$data->name				=	$request->nombre_completo;
			$data->user_name		=	$request->nombre_usuario;
			$data->email			=	$request->correo_electronico;
			$data->password			=	bcrypt($request->contrasena);
			$data->user_profile_id	=	$request->perfil_usuario;

			//Se verifica si se a guardado el nuevo usuario
			if ($data->save()) {

				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha guardado con éxito el nuevo usuario del sistema.'
				];

			} else {

				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido guardar el nuevo usuario en la base de datos'
					]
				];

			}

		}

		return response()->json($response);
	}

 
 

 
	public function edit($id)
	{
		$id_decode = base64_decode($id);
		$data = User::where("id", $id_decode)->first();
		$perfil = UserProfile::where("id", "!=", 1)->get();
		return view('sistema.usuarios.editar', ['id' => $id, "data" =>  $data, "perfil" => $perfil]);
	}

 
	public function update($id, UpdateUserRequest $request)
	{
		$id = base64_decode($id);

		$response = [
			'estatus' => true,
			'mensaje' => 'Se ha actualziado el registro con éxito.'
		];


		//Validamos las contraseñas
		if($request->contrasena != "" || $request->repetir_contrasena != ""){
			if($request->contrasena != $request->repetir_contrasena){
				$response = [
					'estatus' => false,
					'errors'  => [
						'Las contraseñas no coinciden'
					]
				];
			}
		}


		if($response["estatus"]){
			//Validamos que no halla editado un usuario que ya existe
			if( User::where("email", $request->correo_electronico)->where("id", "!=", $id)->count() > 0){
				$response = [
					'estatus' => false,
					'errors'  => [
						'El correo ingresado ya se encuentra registrado'
					]
				];
			}
		}


		if($response["estatus"]){
			//Validamos que no halla editado un usuario que ya existe
			if( User::where("user_name", $request->nombre_usuario)->where("id", "!=", $id)->count() > 0){
				$response = [
					'estatus' => false,
					'errors'  => [
						'El nombre de usuario ingresado ya se encuentra registrado'
					]
				];
			}
		}


		if($response["estatus"]){
			if($request->contrasena != ""){

					//Verificamos que tenga al menos una letra en mayuscula
					if($response["estatus"]){
						$pass = str_split( $request->contrasena );
						$con_mayusculas = 0;
						for($i = 0; $i < count($pass); $i++){
							if( ctype_upper($pass[$i]) ){
								$con_mayusculas++;
							}
						}

						if( $con_mayusculas == 0 ){
							$response = [
								'estatus' => false,
								'errors'  => [
									'La contraseña debe de contener al menos una letra mayuscula'
								]
							];	
						}

					}


					//Verificamos que tenga al menos un numero
					if($response["estatus"]){
						$pass = str_split( $request->contrasena );
						$con_numero = 0;
						for($i = 0; $i < count($pass); $i++){
							if( is_numeric ($pass[$i]) ){
								$con_numero++;
							}
						}

						if( $con_numero == 0 ){
							$response = [
								'estatus' => false,
								'errors'  => [
									'La contraseña debe de contener al menos un numero'
								]
							];	
						}

					}

			}
		}


		if($response["estatus"]){

			if($request->contrasena != ""){
				$row = User::find($id);
				$row->name          	= $request->nombre_completo;
				$row->user_name 		= $request->nombre_usuario;
				$row->email 			= $request->correo_electronico;
				$row->password 			= bcrypt($request->contrasena);
				$row->user_profile_id 	= $request->perfil_usuario;
				$row->deleted_at		= null;				
			}else{
				$row = User::find($id);
				$row->name          	= $request->nombre_completo;
				$row->user_name 		= $request->nombre_usuario;
				$row->email 			= $request->correo_electronico;
				$row->user_profile_id 	= $request->perfil_usuario;
				$row->deleted_at		= null;					
			}

			if($row->save()){
				$response = [
					'estatus' => true,
					'mensaje' => 'Se ha actualziado el registro con éxito.'
				];
			}else{
				$response = [
					'estatus' => false,
					'errors'  => [
						'No se ha podido editar el registro'
					]
				];				
			}

		}

 

		return response()->json($response);
	}

 

 
	public function destroy($id)
	{
		$response;

		//Se busca el ID
		if (User::where('id', '=', base64_decode($id))->count() === 1) {

			//Se borra la cadena
			if (User::destroy(base64_decode($id))) {

				$response = [
					'status' => true,
					'mensaje' => 'Se ha borrado con éxito el perfil de usuario.'
				];

			} else {

				$response = [
					'status' => false,
					'errors'  => [
						'No se ha podido eliminar el perfil de usuario.'
					]

				];

			}

		} else {

			$response = [
				'status' => false,
				'errors'  => [
					'No se ha podido borrar el elemento'
				]

			];

		}

			
		echo json_encode($response);
	}



	public function getUserProfiles(){

		$data = UserProfile::where("id", "!=", 1)->get();
		$option = "<option value=''>Perfil de Usuario</option>";
		foreach($data as $d){
			$option .= "<option value='".$d->id."'>".$d->name."</option>";
		}

		echo json_encode($option);
	}



}
