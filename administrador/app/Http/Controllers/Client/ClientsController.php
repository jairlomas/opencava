<?php namespace OpenCava\Http\Controllers\Client;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\UserProfile;
use OpenCava\Categories;
use OpenCava\Clients;
use OpenCava\Orders;
use OpenCava\Http\Requests\Catalogo\CategoriasRequest;
use OpenCava\Http\Requests\Catalogo\CategoriasEditRequest;
use OpenCava\Library\URI;
use OpenCava\User;
 
use Illuminate\Http\Request;
use Route;
use Validator;
use Mail;
use DB;
use Auth;

class ClientsController extends Controller {

 
	public function index(Request $request)
	{

		$data = Clients::get();
		return view('clientes.index', [		'createButton' => "",
											'permitions' => Uri::checkPermitions(),
											'data'	=>	$data,
									  ]);
	}

 


	public function disable($id){
		
		$response = [
			'status' => true,
			'message' => ''
		];

		$id = base64_decode($id);
		if( Clients::where("id", $id)->count() == 1 ){

			$data = Clients::find($id);
			$data->active = 0;
			if( $data->save() ){

				$response = [
					'status' => true,
					'message' => 'Se ha baneado el cliente con exito'
				];

			}else{

				$response = [
					'status' => false,
					'message' => 'No se ha podido banear el elemento seleccionado'
				];

			}

		}else{

			$response = [
				'status' => false,
				'message' => 'No se ha podido banear el elemento seleccionado'
			];

		}

		echo json_encode($response);
	}

 	public function enable($id){
		
		$response = [
			'status' => true,
			'message' => ''
		];

		$id = base64_decode($id);
		if( Clients::where("id", $id)->count() == 1 ){

			$data = Clients::find($id);
			$data->active = 1;
			if( $data->save() ){

				$response = [
					'status' => true,
					'message' => 'Se ha activado el cliente con exito'
				];

			}else{

				$response = [
					'status' => false,
					'message' => 'No se ha podido activar el elemento seleccionado'
				];

			}

		}else{

			$response = [
				'status' => false,
				'message' => 'No se ha podido activar el elemento seleccionado'
			];

		}

		echo json_encode($response);
 	}



 	public function history($id){

		$id = base64_decode($id);
		$data = Orders::where("client_id", $id)->orderBy("id", "desc")->get();
		return view('clientes.history', [	
										'data'	=>	$data,
							  			]);

 	}

  
}
