<?php namespace OpenCava\Http\Controllers\API;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\Http\Requests\API\CreateAccountEmailRequest;
use OpenCava\Http\Requests\API\CreateAccountSocialNetworkRequest;
use OpenCava\Http\Requests\API\LoginEmailRequest;
use OpenCava\Http\Requests\API\RecoverPasswordRequest;
use OpenCava\Http\Requests\API\SendAclarationMessageRequest;
use OpenCava\Http\Requests\API\EditEmailProfileRequest;
use OpenCava\Http\Requests\API\EditPasswordProfileRequest;
use OpenCava\Http\Requests\API\EditMovilProfileRequest;
use OpenCava\Http\Requests\API\DeleteAccountRequest;
use OpenCava\Http\Requests\API\AddCreditCardRequest;
use OpenCava\Http\Requests\API\SendAclarationRequest;
use OpenCava\Http\Requests\API\SendInvitationRequest;
use OpenCava\Http\Requests\API\StorePurchaseRequest;

use OpenCava\Library\Pastora;

use OpenCava\Clients; 
use OpenCava\CreditCard;
use OpenCava\Reason;
use OpenCava\State;
use OpenCava\City;
use OpenCava\Categories;
use OpenCava\Products;
use OpenCava\ProductsPromotion; 
use OpenCava\SpendingSend;
use OpenCava\Orders;
use OpenCava\OrdersProducts;
use OpenCava\Bussines;
use OpenCava\SplashScreen;
use OpenCava\RequestLocationHistory;

use Carbon\Carbon;
use Illuminate\Http\Request;
use URL; 
use DB;

require_once("conekta-php/lib/Conekta.php");


class ApiController extends Controller {



	public function create_account(CreateAccountEmailRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
			"user_id"	=> 0,
			"name"		=> ""
		];

		$data = new Clients();
		$data->name 			= $request->nombre_y_apelllidos;
		$data->username 		= $request->nombre_de_usuario;
		$data->email 			= $request->correo_electronico;
		$data->password 		= $request->contrasena;
		$data->cellphone_code 	= $request->codigo_pais;
		$data->cellphone 		= $request->movil;	
		$data->birthday			= $request->anio."-".$request->mes."-".$request->dia;
		$data->login_type 		= "email";	
		$data->notification_aperture 		= 1;
		$data->notification_promotion 		= 1;

		if($data->save()){

			$response = [
				"error" 	=> false,
				"message" 	=> "Usuario registrado con exito",
				"user_id"	=> $data->id,
				"name"		=> $data->name
			];			

		}else{

			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha podido registrar el usuario, intentelo de nuevo"
			];

		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function create_account_social_network(CreateAccountSocialNetworkRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
			"user_id"	=> 0,
			"name"		=> ""
		];

		$data = new Clients();
		$data->name 			= $request->nombre_y_apelllidos;
		$data->username 		= $request->nombre_de_usuario;
		$data->email 			= $request->correo_electronico;
		$data->password 		= $request->contrasena;
		$data->cellphone_code 	= $request->codigo_pais;
		$data->cellphone 		= $request->movil;
		$data->birthday			= $request->anio."-".$request->mes."-".$request->dia;
		$data->login_type 		= $request->tipo_red_social;
		$data->social_id 		= (($request->social_id == null) ? "" : $request->social_id);
		$data->notification_aperture 		= 1;
		$data->notification_promotion 		= 1;

		if($data->save()){

			$response = [
				"error" 	=> false,
				"message" 	=> "Usuario registrado con exito",
				"user_id"	=> $data->id,
				"name"		=> $data->name
			];			

		}else{

			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha podido registrar el usuario, intentelo de nuevo"
			];

		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function upload_photo(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];		

		if(!is_dir("assets/profile_picture_client")){
			mkdir("assets/profile_picture_client", 0777);
		}

		$user_id = $request->file('file')->getClientOriginalName();

		$imagen = md5(date("Y_m_d_H_i_s"))."_".$user_id.".png";
		$data = Clients::find($user_id);

		if( $request->file('file')->move("assets/profile_picture_client/", $imagen) ){

			$data->profile_image = url("assets/profile_picture_client/".$imagen);
			$data->save();

			$response["error"] = false;
			$response["message"] = "Se ha subido la imagen con exito";	
			$response["profile_photo"] = $data->profile_image;

		}else{

			$response["error"] = true;
			$response["message"] = "Error al intentar subir la foto de perfil, intentelo nuevamente.";

		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}	


	public function upload_url(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

		$data = Clients::find($request->user_id);
		$data->profile_image = $request->image_url;

		if($data->save()){
			$response["error"] = false;
			$response["message"] = "Se ha actualizado la imagen con exito";	
			$response["profile_photo"] = $data->profile_image;			
		}else{
			$response["error"] = true;
			$response["message"] = "Error al intentar subir la foto de perfil, intentelo nuevamente.";			
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function check_user_exists_social_network(Request $request){

		$response = [
			"exists" 		=> "",
			"user_id" 		=> "",
			"name" 			=> "",
			"profile_photo" => "",
		];			

 		//Si existe la variable social_id en request, quiere decir que se esta logeando desde la actualizacion de la app
		if( $request->social_id != null ){

			//Verificamos que exista ese id de facebook
			$data = Clients::where("social_id", $request->social_id)->where("login_type", $request->login_type);
			//Si existe ese id de facebook verificamos si esta o no esta baneado
			if($data->count() == 1){

				$data = $data->first();
				if($data->active == 1){
					$response["exists"] 			= true;
					$response["usuario_cancelado"]	= false;							
					$response["user_id"] 			= $data->id;
					$response["name"] 				= $data->name;
					$response["profile_photo"] 		= $data->profile_image;
				}else{
					$response["exists"] 			= true;
					$response["usuario_cancelado"]	= true;
					$response["message"]			= "Esta cuenta o dispositivo ha sido bloqueado del servidor por infringir nuestros Términos y condiciones o nuestras políticas de cancelación. Si necesitas más información ponte en contacto con nosotros";
				}

			}else{

				//Si no existe ese id de facebook, verificamos que exista por el correo que entrega facebook
				$data = Clients::where("email", $request->email)->where("login_type", $request->login_type);
				if($data->count() == 1){

					$data = $data->first();
					//Si existe el usuario, por actualizacion, tendremos que actualizar el id de facebook de dicho usuario, ya que de ahora en adelante el login de facebook sera por id de facebook
					$data->social_id = $request->social_id;
					$data->save();
										
					if($data->active == 1){
						$response["exists"] 			= true;
						$response["usuario_cancelado"]	= false;							
						$response["user_id"] 			= $data->id;
						$response["name"] 				= $data->name;
						$response["profile_photo"] 		= $data->profile_image;
					}else{
						$response["exists"] 			= true;
						$response["usuario_cancelado"]	= true;
						$response["message"]			= "Esta cuenta o dispositivo ha sido bloqueado del servidor por infringir nuestros Términos y condiciones o nuestras políticas de cancelación. Si necesitas más información ponte en contacto con nosotros";
					}					

				}else{

					//Si de plano no existe el usuario por id de facebook o por email, entonces el usuario es nuevo
					$response["exists"] 		= false;

				}

			}

		}else{

			$data = Clients::where("email", $request->email)->where("login_type", $request->login_type);
			if($data->count() == 1){

				$data = $data->first();
				if($data->active == 1){
					$response["exists"] 			= true;
					$response["usuario_cancelado"]	= false;							
					$response["user_id"] 			= $data->id;
					$response["name"] 				= $data->name;
					$response["profile_photo"] 		= $data->profile_image;
				}else{
					$response["exists"] 			= true;
					$response["usuario_cancelado"]	= true;
					$response["message"]			= "Esta cuenta o dispositivo ha sido bloqueado del servidor por infringir nuestros Términos y condiciones o nuestras políticas de cancelación. Si necesitas más información ponte en contacto con nosotros";
				}

			}else{

				$response["exists"] 		= false;

			}

		}
 

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function login_email(LoginEmailRequest $request){

		$response = [
			"error" 		=> false,
			"user_id" 		=> "",
			"name" 			=> "",
			"profile_photo" => "",
		];			

		$data = Clients::where( DB::raw('BINARY username'), $request->nombre_de_usuario)->where( DB::raw('BINARY password'), $request->contrasena);
		
		if($data->count() == 1){

			$data = $data->first();
			if($data->active == 1){
				$response["error"] 				= false;
				$response["user_id"] 			= $data->id;
				$response["name"] 				= $data->name;
				$response["profile_photo"] 		= $data->profile_image;
				$response["usuario_cancelado"]	= false;				
			}else{
				$response["error"] 				= false;
				$response["usuario_cancelado"]	= true;
				$response["message"]			= "Esta cuenta o dispositivo ha sido bloqueado del servidor por infringir nuestros Términos y condiciones o nuestras políticas de cancelación. Si necesitas más información ponte en contacto con nosotros";
			}

		}else{

			$response["error"] 			= true;
			$response["message"] 		= "Nombre de Usuario o Contraseña incorrectos";

		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function username_exists_recover_password(RecoverPasswordRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];		


		$data = Clients::where("username", $request->nombre_de_usuario)->orWhere('email', $request->nombre_de_usuario);
		if($data->count() == 1){

			$data = $data->first();
			$response = [
				"error" 	=> false,
				"message" 	=> "",
				"cellphone" => "+".$data->cellphone_code . " " . $data->cellphone,
				"email"		=> $data->email,
				"id"		=> $data->id
			];			

		}else{

			$response = [
				"error" 	=> true,
				"message" 	=> "Usuario no registrado",
			];	

		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function recover_password(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];		

 
		$new_password = Pastora::randomPassword();
		$data = Clients::find($request->user_id);
		$data->password = $new_password;
		if($data->save()){

			if($request->type == "mensaje_texto"){
				//Enviamos un sms con la nueva contraseña
				$sms_response = Pastora::sendSmsToDelivery("+".$data->cellphone_code.$data->cellphone, "Se ha cambiado la contraseña, Contraseña: ".$new_password." - Usuario: ".$data->username);	
				$response["sms_response"] = $sms_response;	
			}else{
				//Enviamos un email con la nueva contraseña

 				$subject = "";
 				$headers = "";
 				$message = "";

				$subject = "Recuperación de contraseña";

				$headers = "From: OpenCava@opencava.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

				$message .= '<p>Se ha cambiado la contraseña a tu cuenta:</p><strong>Contraseña: </strong>'.$new_password."<br><strong>Usuario: </strong>".$data->username;


				mail($data->email, $subject, $message, $headers);

				//mail($data->email, "Recuperación de contraseña", "Se ha cambiado la contraseña a tu cuenta: ".$new_password);
			}

			$response = [
				"error" 	=> false,
				"message" 	=> "",
			];	

		}else{

			$response = [
				"error" 	=> true,
				"message" 	=> "No se pudo cambiar la contraseña, intentelo nuevamente",
			];	

		}

		 
		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function aclaration_user_banned(SendAclarationMessageRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];

		/*
			correo_electronico
			mensaje
		*/


 
		$subject = 'Baneo';

		$headers = "From: app@opencava.com \r\n";
		//$headers .= "Reply-To: ". strip_tags($_POST['req-email']) . "\r\n";
		//$headers .= "CC: susan@example.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$message = "Mensaje de aclaración de baneo de cuenta:";
		$message .= "<br>";
		$message .= "Correo: ".$request->correo_electronico;
		$message .= "<br>";
		$message .= "Mensaje: ".$request->mensaje;
		
		if(mail('baneo@opencava.com', $subject, $message, $headers)){
			mail("rvaldes@opencava.com", $subject, $message, $headers);
			$response = [
				"error" 	=> false,
				"message" 	=> "Se ha enviado la aclaración con exito",
			];
		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha podido enviar la aclaración con exito, intentelo nuevamente",
			];
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function is_user_have_credit_card(Request $request){

		$credit_cards = false;
		$data = Clients::find($request->user_id);
		if( count($data->get_credit_cards) > 0 ){
			$credit_cards = true;
		}else{
			$credit_cards = false;
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($credit_cards);
	}


	public function get_client_info(Request $request){		
	
		$data = Clients::find($request->user_id);
		header('Access-Control-Allow-Origin: *');
		echo json_encode($data);		

	}


	public function edit_email_profile(EditEmailProfileRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

		$data = Clients::find($request->user_id);
		if($data->password == $request->contrasena){

			$data->email = $request->nuevo_correo_electronico;
			if($data->save()){
				$response = [
					"error" 	=> false,
					"message" 	=> "Cambios realizados con éxito",
				];				
			}else{
				$response = [
					"error" 	=> true,
					"message" 	=> "No se han podido editar los cambios, intentelo de nuevo",
				];					
			}

		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "Contraseña incorrecta",
			];			
		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function edit_password_profile(EditPasswordProfileRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	
		
		if($request->nueva_contrasena == $request->confirmar_contrasena){

			$data = Clients::find($request->user_id);
			if($data->password == $request->antigua_contrasena){

				$data->password = $request->nueva_contrasena;
				if($data->save()){
					$response = [
						"error" 	=> false,
						"message" 	=> "Cambios realizados con éxito",
					];				
				}else{
					$response = [
						"error" 	=> true,
						"message" 	=> "No se han podido editar los cambios, intentelo de nuevo",
					];					
				}

			}else{
				$response = [
					"error" 	=> true,
					"message" 	=> "Contraseña incorrecta",
				];			
			}

		}else{

			$response = [
				"error" 	=> true,
				"message" 	=> "El campo nueva contraseña y confirmar contraseña no coinciden",
			];

		}




		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function edit_movil_profile(EditMovilProfileRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	
		
		$data = Clients::find($request->user_id);
		if($data->password == $request->contrasena){

			$data->cellphone_code 	= $request->codigo_pais;
			$data->cellphone 		= $request->movil;
			if($data->save()){
				$response = [
					"error" 	=> false,
					"message" 	=> "Cambios realizados con éxito",
				];				
			}else{
				$response = [
					"error" 	=> true,
					"message" 	=> "No se han podido editar los cambios, intentelo de nuevo",
				];					
			}

		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "Contraseña incorrecta",
			];			
		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}


	public function edit_upload_photo(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];		

		if(!is_dir("assets/profile_picture_client")){
			mkdir("assets/profile_picture_client", 0777);
		}

		$user_id = $request->file('file')->getClientOriginalName();

		$imagen = md5(date("Y_m_d_H_i_s"))."_".$user_id.".png";
		$data = Clients::find($user_id);

		if( $request->file('file')->move("assets/profile_picture_client/", $imagen) ){

			$data->profile_image = url("assets/profile_picture_client/".$imagen);
			$data->save();

			$response["error"] = false;
			$response["message"] = "Cambios realizados con éxito";	
			$response["profile_photo"] = $data->profile_image;

		}else{

			$response["error"] = true;
			$response["message"] = "Error al intentar subir la foto de perfil, intentelo nuevamente.";

		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
	}	


 	public function set_notification_settings(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

		$data = Clients::find($request->user_id);
		$data->notification_aperture = $request->apertura;
		$data->notification_promotion = $request->promociones;

		if($data->save()){
			$response = [
				"error" 	=> false,
				"message" 	=> "Se ha editado el registro con exito",
			];	
		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha editado los datos, intentelo nuevamente",
			];	
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);		
 	}


 	public function send_contacto_ayuda(Request $request){


		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];

		$usuario = Clients::find($request->user_id);
  

		$subject = 'Ayuda';

		$headers = "From: app@opencava.com \r\n";
		//$headers .= "Reply-To: rvaldes@opencava.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$message =   "Nombre: ".$usuario->name."<br>";
		$message .=  "Username: ".$usuario->username."<br>";
		$message .=  "Email: ".$usuario->email."<br>";
		$message .=  "Cellphone: +".$usuario->cellphone_code." ".$usuario->cellphone."<br>";
		$message .=  "Mensaje: ".$request->mensaje;
		
		if(mail('ayuda@opencava.com', $subject, $message, $headers)){
			mail("rvaldes@opencava.com", $subject, $message, $headers);
			$response = [
				"error" 	=> false,
				"message" 	=> "Se ha enviado la aclaración con exito",
			];
		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha podido enviar la aclaración con exito, intentelo nuevamente",
			];
		}
 
 
		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
 	}


 	public function get_reasons(){

 		$data = Reason::get();
 		$select = "";
 		$select .= "<option value='' >¿CUÁL FUE EL MOTIVO?</option>"; 		
 		foreach($data as $d){
 			$select .= "<option value='".$d->id."' >".$d->reason."</option>";
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($select); 		
 	}


 	public function delete_account(DeleteAccountRequest $request){

 		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

		$data = Clients::find($request->user_id);
		if($data->password == $request->contrasena){

			$data->reason_id = $request->motivo;
			$data->save();
			$data->delete();

	 		$response = [
				"error" 	=> false,
				"message" 	=> "¡Lamentamos que nos dejes! :(<br>Toda tu información ha sido eliminada con éxito",
			];

		}else{
	 		$response = [
				"error" 	=> true,
				"message" 	=> "Contraseña incorrecta",
			];	
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function get_credit_cards(Request $request){

		$data = CreditCard::where("client_id", $request->user_id)->get()->toArray();

		foreach ($data as $key => $value) {
			if($data[$key]["type"] == "AMERICAN EXPRESS"){
				$data[$key]["type"] = "AE";
				$data[$key]["type_complete"] = "AMERICAN EXPRESS";
			}else if($data[$key]["type"] == "MASTERCARD"){
				$data[$key]["type"] = "MC";
				$data[$key]["type_complete"] = "MASTERCARD";
			}else if($data[$key]["type"] == "VISA"){
				$data[$key]["type_complete"] = "VISA";
			}

			$last_four_digits = substr($data[$key]["number"], (strlen($data[$key]["number"]) - 4), strlen($data[$key]["number"]) );
			$data[$key]["number"]= "XXXX-XXXX-XXXX-".$last_four_digits;
		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode($data);
 	}


    public function get_credit_cards_purchase(Request $request){

	    //Por el momento vamos a omitir las tarjetas como forma de pago
//        $data = CreditCard::where("client_id", $request->user_id)->get()->toArray();
        $data = CreditCard::where("client_id", -1)->get()->toArray();

        foreach ($data as $key => $value) {
            if($data[$key]["type"] == "AMERICAN EXPRESS"){
                $data[$key]["type"] = "AE";
                $data[$key]["type_complete"] = "AMERICAN EXPRESS";
            }else if($data[$key]["type"] == "MASTERCARD"){
                $data[$key]["type"] = "MC";
                $data[$key]["type_complete"] = "MASTERCARD";
            }else if($data[$key]["type"] == "VISA"){
                $data[$key]["type_complete"] = "VISA";
            }

            $last_four_digits = substr($data[$key]["number"], (strlen($data[$key]["number"]) - 4), strlen($data[$key]["number"]) );
            $data[$key]["number"]= "XXXX-XXXX-XXXX-".$last_four_digits;
        }

        //Si son las nuevas versiones, ponemos la forma de pago en efectivo
        if( $request->APP_VERSION != null ){

            $cobro_efectivo = array(
                "id" => 0,
                "client_id" => $request->user_id,
                "type" => "Pago en efectivo",
                "number" => "Pago en efectivo",
                "month_expiration" => 0,
                "year_expiration" => 0,
                "cvv" => "0",
                "street" => "",
                "exterior_number" => "",
                "interior_number" => "",
                "colony" => "",
                "postal_code" => "",
                "state_id" => "",
                "municipality_id" => "",
                "deleted_at" => null,
                "created_at" => "",
                "updated_at" => "",
                "type_complete" => "Pago en efectivo"
            );

            array_unshift($data, $cobro_efectivo);

            if($request->APP_VERSION == 6) {
                $solicitar_tarjeta = array(
                    "id" => -1,
                    "client_id" => $request->user_id,
                    "type" => "Solicitar terminal",
                    "number" => "Solicitar terminal",
                    "month_expiration" => 0,
                    "year_expiration" => 0,
                    "cvv" => "0",
                    "street" => "",
                    "exterior_number" => "",
                    "interior_number" => "",
                    "colony" => "",
                    "postal_code" => "",
                    "state_id" => "",
                    "municipality_id" => "",
                    "deleted_at" => null,
                    "created_at" => "",
                    "updated_at" => "",
                    "type_complete" => "Solicitar terminal"
                );
                array_unshift($data, $solicitar_tarjeta);
            }

        }

        header('Access-Control-Allow-Origin: *');
        echo json_encode($data);
    }


 	public function get_states(Request $request){

 		$data = State::orderBy("name")->get(); 	
 		$html_select = "<option value=''>ESTADO</option>";
 		foreach($data as $d){
 			$html_select .= "<option value='".$d->id."'>".$d->name."</option>";
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($html_select);  		
 	}


 	public function get_cities(Request $request){

 		$data = City::where("state_id", $request->state_id)->orderBy("name")->get(); 	
 		$html_select = "<option value=''>MUNICIPIO</option>";
 		foreach($data as $d){
 			$html_select .= "<option value='".$d->id."'>".$d->name."</option>";
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($html_select);  		
 	}


 	public function add_credit_card(AddCreditCardRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	


 		/*
			$request->mes_vencimiento
			$request->anio_vencimiento
 		*/

		$year_date = substr(date("Y"), 2,4);
		$month_date = date("m");
 

		if( intval($request->anio_vencimiento) <= $year_date ){

			if( intval($request->mes_vencimiento) <= $month_date ){
				$response = [
					"error" 	=> true,
					"message" 	=> "Fecha de vencimiento invalido",
				];	
			}
		}



		if(!$response["error"]){

			$data = new CreditCard();
			$data->client_id = $request->user_id;
			$data->type = $request->tipo_de_tarjeta;
			$data->number = $request->numero_de_tarjeta;		
			$data->month_expiration = $request->mes_vencimiento;
			$data->year_expiration = $request->anio_vencimiento;
			$data->cvv = $request->cvv;
			$data->street = $request->calle;
			$data->exterior_number = $request->numero_exterior;
			$data->interior_number = $request->interior;
			$data->colony = $request->colonia;
			$data->postal_code = $request->cp;
			$data->state_id = $request->estado;
			$data->municipality_id = $request->municipio;

			if($data->save()){

				$response = [
					"error" 	=> false,
					"message" 	=> "Tarjeta agregada",
				];

			}else{

				$response = [
					"error" 	=> false,
					"message" 	=> "¡Ups! Ha ocurrido un error",
				];

			}

		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function delete_credit_card(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

		$data = CreditCard::find($request->card_id);
		if($data->delete()){
			$response = [
				"error" 	=> false,
				"message" 	=> "Tarjeta Eliminada",
			];
		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha podido eliminar la tarjeta, intentelo nuevamente",
			];
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function send_aclaration(SendAclarationRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

		$data = Clients::find($request->user_id);		



		$subject = '';
		$to = 'aclaraciones@opencava.com';
		//$to = "jair.lomas@metodika.mx";
		if(strtolower($request->type) == "pagos"){
			$subject = 'Aclaración pago';
		}else{
			$subject = 'Aclaración envío';
		}


		$headers = "From: " . strip_tags( $data->email ) . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

		$message = "";
		$message .= 'Nombre: '.$data->name."<br>";
		$message .= 'Username: '.$data->username."<br>";
		$message .= 'Email: '.$data->email."<br>";
		$message .= 'Cellphone: +'.$data->cellphone_code." ".$data->cellphone."<br>";	
		$message .= 'Codigo de compra: '.$request->codigo_compra."<br>";
		$message .= 'Mensaje: '.$request->mensaje."<br>";
		$message .= 'Tipo de aclaración: '.$request->type."<br>";

		if(mail($to, $subject, $message, $headers)){
			mail("rvaldes@opencava.com", $subject, $message, $headers);
			$response = [
				"error" 	=> false,
				"message" 	=> "Se ha enviado el email correctamente",
			];
		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "¡Ups! Ha ocurrido un error",
			];
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function send_invitation(SendInvitationRequest $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];		
 
		$mensaje = "";
		$mensaje = $request->mensaje . " Android: https://goo.gl/5x6M65 - IOS: https://goo.gl/pGaC7q";

		$numero = "";
		if (strpos($request->telefono, '+52') !== false) {
			$numero = $request->telefono;
		}else{
			$numero = "+52".$request->telefono;			
		}


		//Enviamos un sms con la invitacion
		$sms_response = Pastora::sendSmsToDelivery(str_replace(" ","", $numero), $mensaje);	
 
		$response = [
			"error" 		=> false,
			"message" 		=> "Se ha enviado la invitacion con exito",
			"sms_response" 	=> json_encode($sms_response)
		];	

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response);
 	}


 	public function get_categories(){

 		$data = Categories::orderBy("order")->where("type", "Vinos")->get();

 		foreach ($data as $key => $value) {
 			$data[$key]->icon = url($data[$key]->icon);
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($data); 		

 	}


 	public function get_products(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

 		$data = Products::where("category_id", $request->category_id)->where("type", $request->type)->where("cantidad", ">" , 0)->orderBy("name");
 		if($data->count() > 0){

 			$data = $data->get();
 			foreach ($data as $key => $value) { 				
 				$data[$key]->icon = url($data[$key]->icon);
 				$data[$key]->name = str_replace("'", "", $data[$key]->name);
 				$data[$key]->price = number_format($data[$key]->price, 2, ".", "");			
 			}

			$response = [
				"error" 	=> false,
				"message" 	=> "Existen productos",
				"data"		=> $data
			];
 		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No existen productos con los criterios seleccionados",
			];
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function get_complements(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

 		$data = Products::join("categories", "products.category_id", "=", "categories.id")->where("categories.type", "Complementos")->where("products.cantidad", ">" , 0)->orderBy("products.name");;
 		if($data->count() > 0){

 			$data = $data->get(["products.*"]);
 			foreach ($data as $key => $value) { 				
 				$data[$key]->icon = url($data[$key]->icon);
 				$data[$key]->name = str_replace("'", "", $data[$key]->name); 	
 				$data[$key]->price = number_format($data[$key]->price, 2, ".", ""); 							
 			}

			$response = [
				"error" 	=> false,
				"message" 	=> "Existen productos",
				"data"		=> $data
			];
 		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No existen productos con los criterios seleccionados",
			];
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function get_mixeds(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

 		$data = Products::join("categories", "products.category_id", "=", "categories.id")->where("categories.type", "Mezcladores")->where("products.cantidad", ">" , 0)->orderBy("products.name");;
 		if($data->count() > 0){

 			$data = $data->get(["products.*"]);
 			foreach ($data as $key => $value) { 				
 				$data[$key]->icon = url($data[$key]->icon);
 				$data[$key]->name = str_replace("'", "", $data[$key]->name); 
 				$data[$key]->price = number_format($data[$key]->price, 2, ".", ""); 								
 			}

			$response = [
				"error" 	=> false,
				"message" 	=> "Existen productos",
				"data"		=> $data
			];
 		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No existen productos con los criterios seleccionados",
			];
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	}


 	public function get_promotions(Request $request){

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];	

 		$data = ProductsPromotion::where("cantidad", ">", 0)->orderBy("name");
 		if($data->count() > 0){

 			$data = $data->get();
 			foreach ($data as $key => $value) { 				
 				$data[$key]->icon = url($data[$key]->icon);
 				$data[$key]->price = number_format($data[$key]->price, 2, ".", ""); 				
 			}

			$response = [
				"error" 	=> false,
				"message" 	=> "Existen productos",
				"data"		=> $data
			];
 		}else{
			$response = [
				"error" 	=> true,
				"message" 	=> "No existen productos con los criterios seleccionados",
			];
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode($response); 
 	} 	


 	public function get_shipping_cost(Request $request){

  		$data = SpendingSend::get();

 		foreach ($data as $key => $value) {

 			$splited_coordinates = $this->split_coordinates($data[$key]->geofence);

			$vertices_x = $splited_coordinates[0];    	// x-coordinates of the vertices of the polygon
			$vertices_y = $splited_coordinates[1]; 		// y-coordinates of the vertices of the polygon

			$points_polygon = count($vertices_x) - 1;  	// number vertices - zero-based array
			$longitude_x = (double)($request->lat);  		// x-coordinate of the point to test
			$latitude_y = (double)($request->lon);    	// y-coordinate of the point to test

			if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){
				
				$json["precio"] = $data[$key]->price;
				$json["latitud"] = $longitude_x;
				$json["longitud"] = $latitude_y;
				$json["x"] = $splited_coordinates[0];
				$json["y"] = $splited_coordinates[1];

				header('Access-Control-Allow-Origin: *');				
			  	echo json_encode( $json );
			  	exit;
			}

 		}

		$json["precio"] = 0;
		$json["latitud"] = (double)($request->lat);
		$json["longitud"] = (double)($request->lon);
		$json["x"] = "";
		$json["y"] = "";
		header('Access-Control-Allow-Origin: *');
	  	echo json_encode($json);	
 	}


	public function split_coordinates($coordinates){

		$x = array();
		$y = array();
		$data = explode(";",$coordinates);
		for ($i = 0; $i < count($data); $i++) {
			$data[$i] = str_replace("[", "", $data[$i]);
			$data[$i] = str_replace("]", "", $data[$i]);
			$data[$i] = str_replace('"', '', $data[$i]);
			$data[$i] = str_replace('"', '', $data[$i]);		
			$data[$i] = explode(",", $data[$i]);
	
			array_push($x,  floatval($data[$i][0]) );
			array_push($y,  floatval($data[$i][1]) );
		}

		return array($x,$y);
	}


	public function is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y){
	  $i = $j = $c = 0;
	  for ($i = 0, $j = $points_polygon ; $i < $points_polygon; $j = $i++) {
	    if ( (($vertices_y[$i]  >  $latitude_y != ($vertices_y[$j] > $latitude_y)) &&
	     ($longitude_x < ($vertices_x[$j] - $vertices_x[$i]) * ($latitude_y - $vertices_y[$i]) / ($vertices_y[$j] - $vertices_y[$i]) + $vertices_x[$i]) ) )
	       $c = !$c;
	  }
	  return $c;
	} 


	public function store_purchase(StorePurchaseRequest $request){


		//Eliminamos todos los pedidos que no hallan sido pagados del usuario, ya que puede que halla registrado el producto pero no halla pasado el pago, entonces tendremos basura en la base de datos
		Orders::where("client_id", $request->user_id)->where("payed", 0)->delete();

		$response = [
			"error" 	=> false,
			"message" 	=> "",
		];

		$driving_distance = $this->getDrivingDistance($request->lat, $request->lon, 19.306202, -99.634521);

		$data = new Orders();
		$data->client_id = $request->user_id;
		$data->credit_card_id = $request->credit_card;
		$data->spending_cost = $request->shipping;
		$data->subtotal = $request->subtotal;
		$data->lat = $request->lat;
		$data->lon = $request->lon;
 		$data->address = $this->get_address($request->lat, $request->lon);
 		$data->delivery_time = $driving_distance["time"];
 		$data->code = $this->generatePurchaseCode();
        $data->purchase_type = "tarjeta";


		$end_delivery_time = new Carbon( date("Y-m-d H:i:s") );
		$end_delivery_time->addSeconds($driving_distance["seconds"]);
		$data->end_delivery_time = $end_delivery_time;
		$data->start_delivery_time = date("Y-m-d H:i:s");

 		if($data->save()){

 			//Insertamos los productos
 			foreach($request->productos as $p){
 				$op = new OrdersProducts();
 				$op->order_id = $data->id;
 				$op->quantity = $p["cantidad"];
 				if($p["tipo"] == "producto"){
 					$op->product_id = $p["product_id"];
 				}else{
 					$op->promotion_id = $p["product_id"];
 				}
 				$op->save();
 			}

			$response = [
				"error" 			=> false,
				"message" 			=> "Se ha guardado el pedido con exito",
				"code"				=> $data->code,
				"end_delivery_time"	=> $data->end_delivery_time,
				"total_payment"		=> $data->spending_cost + $data->subtotal,				
			];

 		}else{

			$response = [
				"error" 	=> true,
				"message" 	=> "No se ha realizado el pedido, intentelo nuevamente",
			];

 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );		
	}


    public function store_purchase_efectivo(StorePurchaseRequest $request){

        //Eliminamos todos los pedidos que no hallan sido pagados del usuario, ya que puede que halla registrado el producto pero no halla pasado el pago, entonces tendremos basura en la base de datos
        Orders::where("client_id", $request->user_id)->where("payed", 0)->where("status", "")->delete();

        $response = [
            "error" 	=> false,
            "message" 	=> "",
        ];

        $total_pagar = $request->shipping + $request->subtotal;
        if( $request->cantidad_efectivo_pagar < $total_pagar ){

            $response = [
                "error" 	=> true,
                "message" 	=> "La cantidad en efectivo con el que pagarás el pedido es menor al total a pagar.<br>Total a pagar de pedido $".number_format($total_pagar,2,'.',',')
            ];

        }


        if( !$response["error"] ){

            $driving_distance = $this->getDrivingDistance($request->lat, $request->lon, 19.306202, -99.634521);

            $data = new Orders();
            $data->client_id = $request->user_id;
            $data->spending_cost = $request->shipping;
            $data->subtotal = $request->subtotal;
            $data->lat = $request->lat;
            $data->lon = $request->lon;
            $data->address = $this->get_address($request->lat, $request->lon);
            $data->delivery_time = $driving_distance["time"];
            $data->code = $this->generatePurchaseCode();
            $data->purchase_type = "efectivo";
            $data->cash_receiver = $request->cantidad_efectivo_pagar;

            $end_delivery_time = new Carbon(date("Y-m-d H:i:s"));
            $end_delivery_time->addSeconds($driving_distance["seconds"]);
            $data->end_delivery_time = $end_delivery_time;
            $data->start_delivery_time = date("Y-m-d H:i:s");


            if ($data->save()) {

                //Insertamos los productos
                foreach ($request->productos as $p) {
                    $op = new OrdersProducts();
                    $op->order_id = $data->id;
                    $op->quantity = $p["cantidad"];
                    if ($p["tipo"] == "producto") {
                        $op->product_id = $p["product_id"];
                    } else {
                        $op->promotion_id = $p["product_id"];
                    }
                    $op->save();
                }

                $response = ["error" => false, "message" => "Se ha guardado el pedido con exito", "code" => $data->code, "end_delivery_time" => $data->end_delivery_time, "total_payment" => $data->spending_cost + $data->subtotal,];

            } else {

                $response = ["error" => true, "message" => "No se ha realizado el pedido, intentelo nuevamente",];

            }

        }

        header('Access-Control-Allow-Origin: *');
        echo json_encode( $response );
    }


	public function get_address($lat, $long){

		$url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false&key=AIzaSyDg8FqA9KePUQW37jCmZaeWcwdRrB6VZ2E";

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_ENCODING, "");
		$curlData = curl_exec($curl);
		curl_close($curl);

		$address = json_decode($curlData);

		if(count($address->results) > 0){
            return $address->results[0]->formatted_address;
        }else{
            return "No se encontro la dirección.";
        }

	}


	function getDrivingDistance($lat1, $long1, $lat2, $long2){

	    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".floatval($lat1).",".floatval($long1)."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL&key=AIzaSyDg8FqA9KePUQW37jCmZaeWcwdRrB6VZ2E";
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	    $response = curl_exec($ch);
	    curl_close($ch);
	    $response_a = json_decode($response, true);
	    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
	    $time = $response_a['rows'][0]['elements'][0]['duration']['value'];	   

	    /*if($time <= 1260){
	    	$time = rand(1560, 1800);
	    }*/

	    $time = $time + 1200;

	    $seconds = $time;

		$hours = floor($time / 3600);
		$mins = floor($time / 60 % 60);
		$secs = floor($time % 60);

		$tiempo_text = $hours.":".$mins.":".$secs;

	    return array('distance' => $dist, 'time' => $tiempo_text, 'seconds' => $seconds, 'url' => "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".floatval($lat1).",".floatval($long1)."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL&key=AIzaSyDg8FqA9KePUQW37jCmZaeWcwdRrB6VZ2E" );
	}


	function generatePurchaseCode(){

		$code = "";
		do{
	 	    $alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		    $pass = array(); //remember to declare $pass as an array
		    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		    for ($i = 0; $i < 6; $i++) {
		        $n = rand(0, $alphaLength);
		        $pass[] = $alphabet[$n];
		    }

		    $code = implode($pass); //turn the array into a string
		}while( Orders::where("code", $code)->count() > 0 );

		return $code;
 	}


 	function get_purchase(Request $request){

 		$data = Orders::where("code", $request->code)->first();
		$start_delivery_time = new Carbon( $data->start_delivery_time );
		$today_date = new Carbon();
		$difference = $start_delivery_time->diffInSeconds($today_date);

		$response = [
			"current_time"		=> date("Y-m-d H:i:s"),
			"end_delivery_time"	=> $data->end_delivery_time,
			"passed_time"		=> $difference,
			"status"			=> $data->status,
			"total_payment"		=> $data->spending_cost + $data->subtotal,
			"payed"				=> $data->payed
		];

		if($data->status == "cancelado"){
            $response["message"] = "El pedido ha sido cancelado, ponte en contacto con Open Cava para más detalles";
            $response["message_cancel_by_user"] = "El pedido ha sido cancelado";
		}else if($data->status == "entregado"){
			$response["message"] = "El pedido ha sido entregado";
		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );	
 	}


 	function cancelar_pedido(Request $request){

 		$response = [
 			"message" 			=> "",
 			"do_charger" 		=> false,
 			"charger_amount" 	=> 0,
 		];

 		$data = Orders::where("code", $request->code)->first();
		$start_delivery_time = new Carbon( $data->start_delivery_time );
		$today_date = new Carbon();
		$difference = $start_delivery_time->diffInSeconds($today_date);

        if($request->APP_VERSION != null){

            if( $data->purchase_type == "efectivo" || $data->purchase_type == "solicitar_terminal" ){

                $response = [
                    "message" 			=> "¿Seguro que quieres cancelar tu pedido?",
                    "do_charger" 		=> false,
                    "close_modal"		=> true,
                    "charger_amount" 	=> number_format(45,2,".",""),
                    "difference"		=> $difference,
                ];

            }else{

                $response = [
                    "message" 			=> "¿Seguro que quieres cancelar tu pedido?, Se realizará un cargo por:",
                    "do_charger" 		=> true,
                    "close_modal"		=> true,
                    "charger_amount" 	=> number_format(45,2,".",""),
                    "difference"		=> $difference,
                ];

            }


        }else{

            $response = [
                "message" 			=> "¿Seguro que quieres cancelar tu pedido?, Se realizará un cargo por:",
                "do_charger" 		=> true,
                "close_modal"		=> true,
                "charger_amount" 	=> number_format(45,2,".",""),
                "difference"		=> $difference,
            ];

        }


		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );	
 	}


 	function cancelar_pedido_sin_cargos(Request $request){

		$response = [
			"error" => false,
			"message" => "",
		];

 		$data = Orders::where("code", $request->code)->first();
 		$data->status = "cancelado";

 		if($data->save()){

 			$user = Clients::find($request->user_id);
 			$user->increment("consecutive_order_cancel");
 			$user->save();

			$response = [
				"error" => false,
				"message" => "Se ha cancelado el pedido",
			];
 		}else{
			$response = [
				"error" => false,
				"message" => "No se ha cancelado el pedido, intentelo nuevamente",
			];
 		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );	
 	}


 	function get_credit_card(Request $request){

		$response = [
			"error" => false,
			"message" => "",
		];

		$data = Orders::where("code", $request->code)->first();
		$credit_card = CreditCard::find($data->credit_card_id);
		
		$response = [
			"error" 	=> false,
			"message" 	=> "Tarjeta con exito",
			"data"		=> $credit_card
		];

		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );	
 	}


 	function cancelar_pedido_con_cargos(Request $request){

 		$response = [
 			"error" => false,
 			"message" => "",
 		];

 		/*
		    data.code = localStorage.getItem("code");
		    data.user_id = localStorage.getItem("user_id");
		    data.token_id = token.id;
		    data.token_params = tokenParams_cancel_order;
 		*/


		\Conekta::setApiKey(env('CONEKTA_API_SECRET'));

		try{

			$user = Clients::find($request->user_id);

			$charge = \Conekta_Charge::create(
				array(
				  	"description"=> "Pago de cancelación Vinateria",
				  	"amount"=> ($request->token_params["payment"])*100,
				  	"currency"=> "mxn",
				  	"reference_id"=> "{ user_id: ".$request->user_id.", order_code: ".$request->code." }",
				  	"card"=> $request->token_id,
				  	"details"=> array(
					    "name"=> "Open Cava",
					    "phone"=> $user->cellphone,
					    "email"=> $user->email,
					    "customer"=> array(
						    "corporation_name"=> "Open Cava",
						    "logged_in"=> true,
						    "successful_purchases"=> 1,
						    "created_at"=> strtotime(date("Y-m-d H:i:s")),
						    "updated_at"=> strtotime(date("Y-m-d H:i:s")),
						    "offline_payments"=> 1,
						    "score"=> 10
					    ),
					    "line_items"=> [array(
					      	"name"=> "Vino",
					      	"description"=> "Producto proporcionado por Open Cava",
					      	"unit_price"=> ($request->token_params["payment"])*100,
					      	"quantity"=> 1,
					      	"sku"=> "none",
					      	"type"=> "liquid"
					    )],

						"shipment" => array(
							"carrier"=>"Open Cava",
							"service"=>"Local",
							"price"=> 0,
							"address"=> array(
								"street1"=> "-",
								"city"=>"-",
								"state"=>"-",
								"zip"=>"-",
								"country"=>"Mexico"
							)
						)

				  	),		
				)
			);




			if($charge == "error"){

		 		$response = [
		 			"error" => true,
		 			"message" => "No se ha podido cancelar el pedido, intentelo nuevamente.",
		 			"values" => json_decode($charge),		 			
	 	 		];

			}else{

		 		$response = [
		 			"error" => false,
		 			"message" => "Se realizo el cargo",
		 			"values" => json_decode($charge),
	 	 		];

				$data = Orders::where("code", $request->code)->first();
				$data->status = "cancelado";
				$data->cancel_price = $request->token_params["payment"];
				$data->save();
			}

		}catch (Conekta_Error $e){

	 		$response = [
	 			"error" => true,
	 			"message" => "El cargo no pudo ser procesado. <br>ERROR: ".$e->getMessage()." ",
 	 		];
		}


		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );	
 	}


 	function get_purchase_by_user(Request $request){

 		$html = "";
 		$data = Orders::where("client_id", $request->user_id)->where("payed", 1)->orderBy("created_at", "desc");
 		if($data->count() > 0){
 			$data = $data->get();
 			foreach($data as $d){
			    $html .= '<div class="compras_realizadas_row">';
			    $html .= '    <div class="header">';
			    $html .= '        <div class="title"><div class="color_orange">'.$this->getDateFormat( $d->created_at ).'</div><div class="precio">$'.number_format(($d->spending_cost + $d->subtotal),2,".","").'</div></div>';
			    $html .= '        <div class="icon_plus"><img src="img/plus_icon.png" class="open_close_tab"/></div>';
			    $html .= '    </div>';
			    $html .= '    <div class="contents">';

			    foreach($d->order_products as $op){
			    	if($op->product_id != 0){
					    $html .= '	  <div class="row_content">';
					    $html .= '	      <div class="producto">'.$op->product->name.'<br>'.$op->quantity.' PIEZA(S)</div>';
					    $html .= '	      <div class="precio">$'.number_format( ($op->product->price * $op->quantity),2,".","").'</div>';
					    $html .= '	  </div>';
			    	}else{
					    $html .= '	  <div class="row_content">';
					    $html .= '	      <div class="producto">'.$op->promotion->name.'<br>'.$op->quantity.' PIEZA(S)</div>';
					    $html .= '	      <div class="precio">$'.number_format( ($op->promotion->price * $op->quantity),2,".","").'</div>';
					    $html .= '	  </div>';
			    	}

			    }

			    $html .= '        <br>';
			    $html .= '        <div class="row_content">';
			    $html .= '            <div class="producto">ESTATUS DE LA COMPRA</div>';
			    $html .= '            <div class="precio">'.strtoupper($d->status).'</div>';
			    $html .= '        </div>   ';
			    $html .= '        <div class="row_content">';
			    $html .= '            <div class="producto">CODIGO</div>';
			    $html .= '            <div class="precio">'.$d->code.'</div>';
			    $html .= '        </div>                             ';
			    $html .= '    </div>';
			    $html .= '</div> 				';
 			}
 		}else{
			$html .= '<p class="text-center color_purple title_type_1 fs20px nombre_usuario_texto">No cuentas con compras</p>';
 		}

		header('Access-Control-Allow-Origin: *');
		echo json_encode( $html );	
 	}

 	function getDateFormat($date){

 		$today = explode("-", date("d-m-Y", strtotime($date) ) );
 		$dia = $today[2];
 		$month = $today[1];
 		$year = $today[0];
 	
 		switch ($month) {
 			case '1':	$month = "ENERO";break; 		
 			case '2':	$month = "FEBRERO";break; 		
 			case '3':	$month = "MARZO";break; 		
 			case '4':	$month = "ABRIL";break; 		
 			case '5':	$month = "MAYO";break; 		
 			case '6':	$month = "JUNIO";break; 		
 			case '7':	$month = "JULIO";break; 		
 			case '8':	$month = "AGOSTO";break; 		
 			case '9':	$month = "SEPTIEMBRE";break; 		
 			case '10':	$month = "OCTUBRE";break; 		
 			case '11':	$month = "NOVIEMBRE";break; 		
 			case '12':	$month = "DICIEMBRE";break; 		
 		}	
		
 		return $dia . " " . $month . " " . $year;
 	}

 	function getTodayDate(){

 		$today = explode("-", date("Y-m-d"));
 		$dia = $today[2];
 		$month = $today[1];
 		$year = $today[0];

 		switch ($month) {
 			case '1':	$month = "ENERO";break; 		
 			case '2':	$month = "FEBRERO";break; 		
 			case '3':	$month = "MARZO";break; 		
 			case '4':	$month = "ABRIL";break; 		
 			case '5':	$month = "MAYO";break; 		
 			case '6':	$month = "JUNIO";break; 		
 			case '7':	$month = "JULIO";break; 		
 			case '8':	$month = "AGOSTO";break; 		
 			case '9':	$month = "SEPTIEMBRE";break; 		
 			case '10':	$month = "OCTUBRE";break; 		
 			case '11':	$month = "NOVIEMBRE";break; 		
 			case '12':	$month = "DICIEMBRE";break; 		
 		}

 		return $dia . " " . $month . " " . $year;
 	}

 	function pagar_pedido(Request $request){

 		$response = [
 			"error" => false,
 			"message" => "",
 			"response" => $request
 		];
 
 		/*
		    data.code = localStorage.getItem("code");
		    data.user_id = localStorage.getItem("user_id");
		    data.token_id = token.id;
		    data.token_params = tokenParams_cancel_order;
 		*/


		\Conekta::setApiKey(env('CONEKTA_API_SECRET'));

		$user = "";
		$user = Clients::find($request->user_id);


		//Aqui solamente el usuario con el id 62 no pasará por conekta y hace el pago en automatico
		if( $request->user_id == 62 ){

	 		$response = [
	 			"error" => false,
	 			"message" => "Se realizo el cargo prueba",
	 			"token"	=> $request->token_id
 	 		];

			$data = Orders::where("code", $request->code)->first();
			$data->payed = 1;
			$data->save();

			$user->consecutive_order_cancel = 0;
			$user->save();

		}else{


			try{

				$charge = \Conekta_Charge::create(
					array(
					  	"description"=> "Pago de Vinateria - ",
					  	"amount"=> ($request->token_params["payment"])*100,
					  	"currency"=> "MXN",
					  	"reference_id"=> "{ user_id: ".$request->user_id.", order_code: ".$request->code." }",				  	
					  	"card"=> $request->token_id,
					  	"details"=> array(
						    "name"=> "Open Cava",
						    "phone"=> $user->cellphone,
						    "email"=> $user->email,
						    "customer"=> array(
							    "corporation_name"=> "Open Cava",
							    "logged_in"=> true,
							    "successful_purchases"=> 1,
							    "created_at"=> strtotime(date("Y-m-d H:i:s")),
							    "updated_at"=> strtotime(date("Y-m-d H:i:s")),
							    "offline_payments"=> 1,
							    "score"=> 10
						    ),
						    "line_items"=> [array(
						      	"name"=> "Vino",
						      	"description"=> "Producto proporcionado por Open Cava",
						      	"unit_price"=> ($request->token_params["payment"])*100,
						      	"quantity"=> 1,
						      	"sku"=> "none",
						      	"type"=> "liquid"
						    )],

							"shipment" => array(
								"carrier"=>"Open Cava",
								"service"=>"Local",
								"price"=> 0,
								"address"=> array(
									"street1"=> "-",
									"city"=>"-",
									"state"=>"-",
									"zip"=>"-",
									"country"=>"Mexico"
								)
							)

					  	),		
					)
				);

	 
				if($charge == "error"){

			 		$response = [
			 			"error" => true,
			 			"message" => "No se realizo el cargo",
			 			"values" => $charge,
			 			"token"	=> $request->token_id
		 	 		];

		 	 		//Si no se realiza el cargo, eliminamos el pedido
		 	 		Orders::where("code", $request->code)->delete();

				}else{

			 		$response = [
			 			"error" => false,
			 			"message" => "Se realizo el cargo",
			 			"values" => $charge,
			 			"token"	=> $request->token_id
		 	 		];

					$data = Orders::where("code", $request->code)->first();
					$data->payed = 1;
					$data->save();

					$user->consecutive_order_cancel = 0;
					$user->save();

				}

			}catch ( Conekta_Error $e ){

		 		$response = [
		 			"error" => true,
		 			"message" => "El cargo no pudo ser procesado. <br>ERROR: ".$e->getMessage()." ",
	 	 		];
			}


		}




		header('Access-Control-Allow-Origin: *');
		echo json_encode( $response );	
 	}


 	function update_uuid(Request $request){

 		$data = Clients::find($request->user_id);
 		$data->os_type 	= $request->os_type;
 		$data->uuid 	= $request->uuid;
 		$data->save();

 		echo "1";
 	}




 	public function is_user_location(Request $request){

 		/*
		header('Access-Control-Allow-Origin: *');
	  	echo json_encode(true); 		
		*/

	  	if( $request->user_id != null ){

			if( $request->user_id == 62 ){

				$json = true;
				header('Access-Control-Allow-Origin: *');				
			  	echo json_encode( $json );
			  	exit;

			}else{

		  		$data = SpendingSend::get();
		  		
		  		$history = new RequestLocationHistory();
		  		$history->lat = (double)($request->lat);
		  		$history->lon = (double)($request->lon);
		  		$history->save();
				

		 		foreach ($data as $key => $value) {

		 			$splited_coordinates = $this->split_coordinates($data[$key]->geofence);

					$vertices_x = $splited_coordinates[0];    	// x-coordinates of the vertices of the polygon
					$vertices_y = $splited_coordinates[1]; 		// y-coordinates of the vertices of the polygon

					$points_polygon = count($vertices_x) - 1;  	// number vertices - zero-based array
					$longitude_x = (double)($request->lat);  		// x-coordinate of the point to test
					$latitude_y = (double)($request->lon);    	// y-coordinate of the point to test

					if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){			
						$json = true;
						header('Access-Control-Allow-Origin: *');				
					  	echo json_encode( $json );
					  	exit;
					}

		 		}

			}


	  	}else{

	  		$data = SpendingSend::get();
	  		
	  		$history = new RequestLocationHistory();
	  		$history->lat = (double)($request->lat);
	  		$history->lon = (double)($request->lon);
	  		$history->save();
			

	 		foreach ($data as $key => $value) {

	 			$splited_coordinates = $this->split_coordinates($data[$key]->geofence);

				$vertices_x = $splited_coordinates[0];    	// x-coordinates of the vertices of the polygon
				$vertices_y = $splited_coordinates[1]; 		// y-coordinates of the vertices of the polygon

				$points_polygon = count($vertices_x) - 1;  	// number vertices - zero-based array
				$longitude_x = (double)($request->lat);  		// x-coordinate of the point to test
				$latitude_y = (double)($request->lon);    	// y-coordinate of the point to test

				if ($this->is_in_polygon($points_polygon, $vertices_x, $vertices_y, $longitude_x, $latitude_y)){			
					$json = true;
					header('Access-Control-Allow-Origin: *');				
				  	echo json_encode( $json );
				  	exit;
				}

	 		}

 		}

		$json = false;
		header('Access-Control-Allow-Origin: *');
	  	echo json_encode($json);
 	}




 	function add_product_shopping_cart(Request $request){

 		/*
			tipo
			product_id
			cantidad
 		*/
        $response = ["error" => false, "is_open_store" => null, "message" => "" ];

 		//Todo_ el proceso es igual, solo que cuando entra a alguna version nueva hay que hacer diferente procedimiento
        if( $request->APP_VERSION != null ) {

            if( $request->APP_VERSION ==  "5" || $request->APP_VERSION ==  "6" ){
                $response = ["error" => false, "is_open_store" => null ];

                $is_open_store = $this->is_open_store(false, $request->user_id);
                $response["is_open_store"] = $is_open_store;

                if( $is_open_store ) {
                    $data = "";
                    if ($request->tipo == "producto") {
                        $data = Products::where("id", $request->product_id)->first();
                    } else {
                        $data = ProductsPromotion::where("id", $request->product_id)->first();
                    }


                    if ($data->cantidad <= 0) {
                        $response["error"] = true;
                    } else {
                        $data->decrement("cantidad");
                        if ($data->save()) {
                            $response["error"] = false;
                        } else {
                            $response["error"] = true;
                        }
                    }
                }else{

                    $response["message"] = "Por el momento Open Cava esta cerrado, recibirás una notificación cuando estemos en servicio<br><br><a style=\"background: #321a38;padding: 10px 15px;border-radius: 5px;\" onclick='window.open(\"http://opencava.com/index.html#horarios\", \"_system\");'>Ver horarios</a><br><br>";

                }


            }else{

                $response = ["error" => false];

                $data = "";
                if ($request->tipo == "producto") {
                    $data = Products::where("id", $request->product_id)->first();
                } else {
                    $data = ProductsPromotion::where("id", $request->product_id)->first();
                }


                if ($data->cantidad <= 0) {
                    $response["error"] = true;
                } else {
                    $data->decrement("cantidad");
                    if ($data->save()) {
                        $response["error"] = false;
                    } else {
                        $response["error"] = true;
                    }
                }

            }

        }else{

            $response = ["error" => false];

            $data = "";
            if ($request->tipo == "producto") {
                $data = Products::where("id", $request->product_id)->first();
            } else {
                $data = ProductsPromotion::where("id", $request->product_id)->first();
            }


            if ($data->cantidad <= 0) {
                $response["error"] = true;
            } else {
                $data->decrement("cantidad");
                if ($data->save()) {
                    $response["error"] = false;
                } else {
                    $response["error"] = true;
                }
            }

        }

		header('Access-Control-Allow-Origin: *');
	  	echo json_encode($response);
 	}




 	function less_product_shopping_cart(Request $request){

 		/*
			tipo
			product_id
			cantidad
 		*/

		$response = ["error" =>  false];

		$data = "";
		if($request->tipo == "producto"){
			$data = Products::where("id", $request->product_id)->first();
		}else{
			$data = ProductsPromotion::where("id", $request->product_id)->first();
		}


		$data->increment("cantidad");
		if($data->save()){
			$response["error"] = false;
		}else{
			$response["error"] = true;				
		}
 

		header('Access-Control-Allow-Origin: *');
	  	echo json_encode($response);
 	}






 	function return_products(Request $request){

 		/*
			productos
				tipo
				product_id
				cantidad
 		*/

		if( count($request->productos) > 0 ){
			foreach ($request->productos as $value) {

				$data = "";
				if($value["tipo"] == "producto"){
					$data = Products::where("id", $value["product_id"])->first();
				}else{
					$data = ProductsPromotion::where("id", $value["product_id"])->first();
				}

				$data->increment("cantidad", $value["cantidad"]);
				$data->save();

			}
		}

		header('Access-Control-Allow-Origin: *');
	  	echo json_encode("1");
 	}



 	function is_user_banned(Request $request){

 		$response = [];
 		$data = Clients::find($request->user_id);
 		$user_banned = (( $data->active == 1 ) ? false : true);

 		if( $user_banned ){

 			$response = [
 				"banned"  => true,
 				"message" => "Esta cuenta o dispositivo ha sido bloqueado del servidor por infringir nuestros Términos y condiciones o nuestras políticas de cancelación. Si necesitas más información ponte en contacto con nosotros"
 			];

 		}else{

 			$response = [
 				"banned"  => false,
 				"message" => ""
 			];

 		}

 

		header('Access-Control-Allow-Origin: *');
	  	echo json_encode($response);
 	}


 	function is_open_store($is_webservice = true, $user_id = 0){

        /*
 		if(isset($_POST["user_id"])){
 			if($_POST["user_id"] == 62){
				$is_open = true;
				header('Access-Control-Allow-Origin: *');
	  			echo json_encode($is_open);
	  			exit();				
 			}
 		}
        */

        if($user_id == 62){
            $is_open = true;
            if( $is_webservice ) {
                header('Access-Control-Allow-Origin: *');
                echo json_encode($is_open);
                exit();
            }else{
                return $is_open;
            }
        }

 		$data = Bussines::first();
 		$aperture_time = $data->aperture_time;
 		$close_time = $data->close_time;
 		$today_time = date("d-m-Y H:i:s");

 		$aperture_time 	= strtotime("Y-m-d") + strtotime($aperture_time);
 		$close_time 	= strtotime("Y-m-d") + strtotime($close_time);
 		$today_time 	= strtotime($today_time);

 		$is_open = false;

 		if($close_time <= $aperture_time){
 			$close_time += 86400;
 		}


 		$dias_apertura = explode(",", $data->aperture_day);
 		$day_of_week = date('N');


 		$aperture_time = 	$this->makeAssociativeArray( explode(":", Carbon::createFromTimestamp($aperture_time)->format('H:i:A') 				) );
 		$today_time = 		$this->makeAssociativeArray( explode(":", Carbon::createFromTimestamp($today_time)->format('H:i:A') 				) );
 		$close_time = 		$this->makeAssociativeArray( explode(":", Carbon::createFromTimestamp($close_time)->format('H:i:A') 				) );

 		if( in_array($day_of_week, $dias_apertura) ){ //Si esta en el dia de la semana

	 		if( $aperture_time["tipo"] == "AM" && $close_time["tipo"] == "AM" ){	//Validación normal

	 			if( $close_time["segundos"] < $aperture_time["segundos"] ){
	 				if( $today_time["tipo"] == "AM" ){
	 					if( $today_time["segundos"] >= $aperture_time["segundos"] || $today_time["segundos"] <= $close_time["segundos"]){
	 						$is_open = true;
	 					}
	 				}else{
	 					$is_open = true;
	 				}
	 			}else{

		 			if($today_time["segundos"] >= $aperture_time["segundos"] && $today_time["segundos"] <= $close_time["segundos"]){
		 				$is_open = true;
		 			}

	 			}

	 		}else if( $aperture_time["tipo"] == "PM" && $close_time["tipo"] == "PM" ){	//Validación normal

	 			if( $close_time["segundos"] < $aperture_time["segundos"] ){
	 				if( $today_time["tipo"] == "PM" ){
	 					if( $today_time["segundos"] >= $aperture_time["segundos"] || $today_time["segundos"] <= $close_time["segundos"]){
	 						$is_open = true;
	 					}
	 				}else{
	 					$is_open = true;
	 				}
	 			}else{

		 			if($today_time["segundos"] >= $aperture_time["segundos"] && $today_time["segundos"] <= $close_time["segundos"]){
		 				$is_open = true;
		 			}

	 			}

	 		}else if( $aperture_time["tipo"] == "PM" && $close_time["tipo"] == "AM" ){	//Validación especial

	 			if( $today_time["tipo"] == "PM" ){
	 				if( $today_time["segundos"] >= $aperture_time["segundos"] ){
	 					$is_open = true;
	 				}
	 			}else if( $today_time["tipo"] == "AM" ){
	 				if( $today_time["segundos"] <= $close_time["segundos"] ){
	 					$is_open = true;
	 				}
	 			}

	 		}else if( $aperture_time["tipo"] == "AM" && $close_time["tipo"] == "PM" ){	//Validación especial

	 			if( $today_time["tipo"] == "PM" ){
	 				if( $today_time["segundos"] <= $close_time["segundos"] ){
	 					$is_open = true;
	 				}
	 			}else if( $today_time["tipo"] == "AM" ){
	 				if( $today_time["segundos"] >= $aperture_time["segundos"] ){
	 					$is_open = true;
	 				}
	 			}

	 		}
 
 		}else{
 			
	 		$is_open = false;
 		}

 		if( $is_webservice ) {
            header('Access-Control-Allow-Origin: *');
            echo json_encode($is_open);
        }else{
 		    return $is_open;
        }
 	}


    function is_open_store_test(){

 		echo "<table width='600px'>";
 		echo "<tr><td>Apertura</td><td>Hora actual</td><td>Cierre</td><td>Status</td></tr>";
 		for($i = 0; $i < 220; $i++){
 			$this->test_function(14 * $i);
 		}
 		echo "</table>";
 		echo "<style>table{border: 1px solid black; border-collapse: collapse; }table td{border: 1px solid black;}body{ text-align: center; }</style>";
 	}


 	function test_function($minutes_add){

 		//Se comentó todo el código de abajo de esta función ya que apple no autorizaba este uso, una vez que la aprueben de descomenta
 		$data = Bussines::first();
 		$aperture_time = $data->aperture_time;
 		$close_time = $data->close_time;
 		$today_time = date("d-m-Y H:i:s");

 		$aperture_time 	= strtotime("Y-m-d") + strtotime($aperture_time);
 		$close_time 	= strtotime("Y-m-d") + strtotime($close_time);
 		$today_time 	= strtotime($today_time);

 		$is_open = false;

 		if($close_time <= $aperture_time){
 			$close_time += 86400;
 		}


 		$dias_apertura = explode(",", $data->aperture_day);
 		$day_of_week = date('N');


 		$aperture_time = 	$this->makeAssociativeArray( explode(":", Carbon::createFromTimestamp($aperture_time)->format('H:i:A') 				) );
 		$today_time = 		$this->makeAssociativeArray( explode(":", Carbon::createFromTimestamp($today_time)->addMinutes($minutes_add)->format('H:i:A') 	) );
 		$close_time = 		$this->makeAssociativeArray( explode(":", Carbon::createFromTimestamp($close_time)->format('H:i:A') 				) );


  
 		//echo "Apertura: " . print_r($aperture_time)."<br>";
 		//echo "Actual: " . print_r($today_time)."<br>";
 		//echo "Cerrado: " . print_r($close_time)."<br>";

 		echo "<tr>";
 		echo "	<td>".$aperture_time["hora-normal"]."</td>";
		echo "	<td>".$today_time["hora-normal"]."</td>";
		echo "	<td>".$close_time["hora-normal"]."</td>";


 		if( $aperture_time["tipo"] == "AM" && $close_time["tipo"] == "AM" ){	//Validación normal

 			if( $close_time["segundos"] < $aperture_time["segundos"] ){
 				if( $today_time["tipo"] == "AM" ){
 					if( $today_time["segundos"] >= $aperture_time["segundos"] || $today_time["segundos"] <= $close_time["segundos"]){
 						$is_open = true;
 					}
 				}else{
 					$is_open = true;
 				}
 			}else{

	 			if($today_time["segundos"] >= $aperture_time["segundos"] && $today_time["segundos"] <= $close_time["segundos"]){
	 				$is_open = true;
	 			}

 			}

 		}else if( $aperture_time["tipo"] == "PM" && $close_time["tipo"] == "PM" ){	//Validación normal

 			if( $close_time["segundos"] < $aperture_time["segundos"] ){
 				if( $today_time["tipo"] == "PM" ){
 					if( $today_time["segundos"] >= $aperture_time["segundos"] || $today_time["segundos"] <= $close_time["segundos"]){
 						$is_open = true;
 					}
 				}else{
 					$is_open = true;
 				}
 			}else{

	 			if($today_time["segundos"] >= $aperture_time["segundos"] && $today_time["segundos"] <= $close_time["segundos"]){
	 				$is_open = true;
	 			}

 			}

 		}else if( $aperture_time["tipo"] == "PM" && $close_time["tipo"] == "AM" ){	//Validación especial

 			if( $today_time["tipo"] == "PM" ){
 				if( $today_time["segundos"] >= $aperture_time["segundos"] ){
 					$is_open = true;
 				}
 			}else if( $today_time["tipo"] == "AM" ){
 				if( $today_time["segundos"] <= $close_time["segundos"] ){
 					$is_open = true;
 				}
 			}

 		}else if( $aperture_time["tipo"] == "AM" && $close_time["tipo"] == "PM" ){	//Validación especial

 			if( $today_time["tipo"] == "PM" ){
 				if( $today_time["segundos"] <= $close_time["segundos"] ){
 					$is_open = true;
 				}
 			}else if( $today_time["tipo"] == "AM" ){
 				if( $today_time["segundos"] >= $aperture_time["segundos"] ){
 					$is_open = true;
 				}
 			}

 		}
  
  		if($is_open){
 			echo "	<td style='background: green;'>ABIERTO</td>";
  		}else{
  			echo "	<td style='background: red;'>CERRADO</td>";
  		}
	  	echo "</tr>";
	 
 	}



 	function makeAssociativeArray($hour){

 		return array(	"segundos"	=> ($hour[0] * 60) + $hour[1],
 						"hora" 		=> $hour[0],
 						"minuto" 	=> $hour[1],
 						"tipo" 		=> $hour[2],
 						"hora-normal" => $hour[0].":".$hour[1]." ".$hour[2]);

 	}

	function check_in_range($start_date, $end_date, $date_from_user){
	  // Convert to timestamp
	  $start_ts = strtotime($start_date);
	  $end_ts = strtotime($end_date);
	  $user_ts = strtotime($date_from_user);

	  // Check that user date is between start & end
	  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	}


 	function get_splashscreen(){

 		$data = SplashScreen::where("active", 1);
 		$splash = "";

 		if( $data->count() == 1 ){
 			$data = $data->first();
 			$splash = url($data->path);
 		}else{
 			$splash = "";
 		}

		header('Access-Control-Allow-Origin: *');
	  	echo json_encode($splash);
 	}

 

	public function send_push(){



	}



 	function test(){
 		echo "test";
 	}



    public function store_purchase_solicitar_terminal(StorePurchaseRequest $request){

        //Eliminamos todos los pedidos que no hallan sido pagados del usuario, ya que puede que halla registrado el producto pero no halla pasado el pago, entonces tendremos basura en la base de datos
        Orders::where("client_id", $request->user_id)->where("payed", 0)->where("status", "")->delete();

        $response = [
            "error" 	=> false,
            "message" 	=> "",
        ];

        $request->subtotal = (($request->subtotal * 0.03) + $request->subtotal);

        $total_pagar = $request->shipping + $request->subtotal;

        if( !$response["error"] ){

            $driving_distance = $this->getDrivingDistance($request->lat, $request->lon, 19.306202, -99.634521);

            $data = new Orders();
            $data->client_id = $request->user_id;
            $data->spending_cost = $request->shipping;
            $data->subtotal = $request->subtotal;
            $data->lat = $request->lat;
            $data->lon = $request->lon;
            $data->address = $this->get_address($request->lat, $request->lon);
            $data->delivery_time = $driving_distance["time"];
            $data->code = $this->generatePurchaseCode();
            $data->purchase_type = "solicitar_terminal";
            $data->cash_receiver = 0;

            $end_delivery_time = new Carbon(date("Y-m-d H:i:s"));
            $end_delivery_time->addSeconds($driving_distance["seconds"]);
            $data->end_delivery_time = $end_delivery_time;
            $data->start_delivery_time = date("Y-m-d H:i:s");
            //$data->credit_card_id = 0;
            //$data->delivery_id = 0;

            if ($data->save()) {

                //Insertamos los productos
                foreach ($request->productos as $p) {
                    $op = new OrdersProducts();
                    $op->order_id = $data->id;
                    $op->quantity = $p["cantidad"];
                    if ($p["tipo"] == "producto") {
                        $op->product_id = $p["product_id"];
                    } else {
                        $op->promotion_id = $p["product_id"];
                    }
                    $op->save();
                }

                $response = ["error" => false, "message" => "Se ha guardado el pedido con exito", "code" => $data->code, "end_delivery_time" => $data->end_delivery_time, "total_payment" => $data->spending_cost + $data->subtotal,];

            } else {

                $response = ["error" => true, "message" => "No se ha realizado el pedido, intentelo nuevamente",];

            }

        }

        header('Access-Control-Allow-Origin: *');
        echo json_encode( $response );
    }



}
