<?php namespace OpenCava\Http\Controllers;

use OpenCava\Http\Requests;
use OpenCava\Http\Controllers\Controller;
use OpenCava\Http\Requests\Account\LoginRequest;
use OpenCava\Library\Pastora;

use Illuminate\Http\Request;
use Auth;
use Session;
use Response;
use OpenCava\User;
use DB;

class AccountController extends Controller {

 
	public function index()
	{

    	return view('account.home');
	}

 
	public function login(LoginRequest $request)
	{

		//Se verifica si el usuario tiene credenciales correctas
		if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

			Session::put('userPermissions', Pastora::jsonToArray(Pastora::userProfile())   );

			$menu = Pastora::jsonToArray(Pastora::userProfile());

			//Verificamos si existe el menu de pagos asignado a los permisos del usuario logeado
			return redirect("dashboard");

		} else {

			//Se le informa al usuario del error
			Session::flash('loginError', true);
			return redirect('/');

		}

	}

 
	public function logout()
	{
		Auth::logout();
		return redirect('/');
	}


	public function recover_password(){
    	return view('account.passwordrecover');
	}


	public function recover_password_post(){
		$json["status"] = false;
		$json["message"] = "";

		if(!isset($_POST["nombre_usuario"]) || $_POST["nombre_usuario"] == "" && !$json["status"]){
			$json["status"] = true;
			$json["message"] = "El campo Email es obligatorio";
		}

		//Verificamos que exista el usuario
		if(!$json["status"]){
			if( User::where("email", $_POST["nombre_usuario"])->count() <= 0){
				$json["status"] = true;
				$json["message"] = "No existe el Email ingresado";
			}
		}

		//Cambiamos la contraseña
		if(!$json["status"]){
			$password = $this->randomPassword();

			$to = $_POST["nombre_usuario"];

			$subject = 'Recuperación de contraseña';

			$headers = "From: uorc@no-responder.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

			$message = 'Se ha restablecido la siguiente contraseña a tu cuenta: <br> <strong>Email:</strong> '.$_POST["nombre_usuario"].'<br> <strong>Password:</strong> '.$password;

			if( mail($to, $subject, $message, $headers) ){
				User::where("email", $_POST["nombre_usuario"])->update( ["password" => bcrypt($password)] );
				$json["status"] = false;
				$json["message"] = "Se ha enviado a el correo ingresado la nueva contraseña.";
				$json["route"] = url("/");
			}else{
				$json["status"] = true;
				$json["message"] = "No se ha podido restablecer la contraseña, intentelo nuevamente.";
			}
		}

		echo json_encode($json);
	}




	function randomPassword() {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < 8; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}


}
