<?php namespace OpenCava\Http\Composers;

use Illuminate\Contracts\View\View;
use OpenCava\Library\LateralMenu;
use OpenCava\Library\URI;
use Auth;

class DashboardComposer {

	/**
	 * @author  Edgar Yerena <edgar.yerena@metodika.mx>
	 * @version 1.0 2015-03-26
	 *
	 * @return void
	 *
	 * Manda la información del usuario
	 */
	public function profile($view)
	{
		$view->with('profile', Auth::user());
	}

	public function menulateral($view)
	{
		$view->with('menu', LateralMenu::menu());
	}

	/**
	 * @author  Edgar Yerena <edgar.yerena@metodika.mx>
	 * @version 1.0 2015-03-26
	 *
	 * @return void
	 *
	 * Manda la información del usuario
	 */
	public function breadcrums($view)
	{
		$view->with('breadcrums', URI::breadcrums());
	}

}