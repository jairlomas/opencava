<?php namespace OpenCava\Http\Requests\Sistema\Usuarios;

use OpenCava\Http\Requests\Sistema\Usuarios\UserRequest;

class AddUserRequest extends UserRequest {

		/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'nombre_completo'		=>	'required',
			'nombre_usuario'		=>	'required|unique:users,user_name',
			'correo_electronico'	=>	'required|email|unique:users,email',
			'contrasena'			=>	'required|min:6',
			'repetir_contrasena'	=>	'required',
			'perfil_usuario'		=>	'required'
		];
	}

}
