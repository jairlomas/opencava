<?php namespace OpenCava\Http\Requests\Sistema\Usuarios;

use OpenCava\Http\Requests\Sistema\Usuarios\UserRequest;

class UpdateUserRequest extends UserRequest {

		/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{

		return [
			'nombre_completo'		=>	'required',
			'nombre_usuario'		=>	'required',
			'correo_electronico'	=>	'required',
			'contrasena'			=>	'min:6',
			'perfil_usuario'		=>	'required'
		];

	}

}
