<?php namespace OpenCava\Http\Requests\Catalogo;

use OpenCava\Http\Requests\Request;

class ProductsEditRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nombre"	=> "required|unique:products,name,".base64_decode($this->get('id')).",id",
			"precio"	=> "required",
			"icono"		=> "mimes:png",
			"categoria"	=> "required",
			"tipo"		=> "required",
			"cantidad"	=> "required",
		];
	}

}
