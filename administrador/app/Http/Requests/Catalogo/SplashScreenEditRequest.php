<?php namespace OpenCava\Http\Requests\Catalogo;

use OpenCava\Http\Requests\Request;

class SplashScreenEditRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nombre" => "required",
			"splash" => "mimes:jpeg,png",
		];
	}

}
