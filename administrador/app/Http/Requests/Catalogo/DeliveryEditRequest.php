<?php namespace OpenCava\Http\Requests\Catalogo;

use OpenCava\Http\Requests\Request;

class DeliveryEditRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nombre_completo" 	=> "required|unique:deliveries,name,".base64_decode($this->get('id')).",id",
			"telefono" 			=> "required|unique:deliveries,phone,".base64_decode($this->get('id')).",id",
			"email" 			=> "required|unique:deliveries,email,".base64_decode($this->get('id')).",id|email"
		];
	}

}
