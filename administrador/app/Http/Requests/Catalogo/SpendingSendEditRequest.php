<?php namespace OpenCava\Http\Requests\Catalogo;

use OpenCava\Http\Requests\Request;

class SpendingSendEditRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nombre"		=>	"required|unique:spending_sends,name,".base64_decode($this->get('id')).",id",
			"costo"			=>	"required",
			"geocerca"		=>	"required"	
		];
	}

}
