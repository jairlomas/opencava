<?php namespace OpenCava\Http\Requests\Catalogo;

use OpenCava\Http\Requests\Request;

class CategoriasEditRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"categoria" => "required|unique:categories,name,".base64_decode($this->get('id')).",id",
			"tipo"		=> "required",
			"icono"		=> "mimes:png"			
		];
	}

}
