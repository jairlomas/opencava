<?php namespace OpenCava\Http\Requests\Account;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class LoginRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		//Se verifica que el usuario NO este logueado
		if (Auth::check()) {

			return false;

		} else {

			return true;

		}
		
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'email'      => 'required|email',
			'password'   => 'required'
		];
	}

}
