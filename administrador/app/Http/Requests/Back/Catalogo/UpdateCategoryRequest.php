<?php

namespace OpenCava\Http\Requests\Back\Catalogo;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "banner_principal" => "image",
            //"banner_articulo" => "image",
            "imagen_categoria" => "image",
            "categoria" => "required",
        ];
    }

    public function messages()
    {
        return [
            "categoria.required" => "La Categoria es requerida.",
            "banner_principal.image" => "La Imagen de banner principal debe de ser una imagen valida.",
            //"banner_articulo.image" => "La Imagen de banner en articulo debe de ser una imagen valida.",
            "imagen_categoria.image" => "La Imagen de categoria debe de ser una imagen valida.",
        ];
    }
}
