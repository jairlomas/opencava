<?php

namespace OpenCava\Http\Requests\Back\Catalogo;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "category" => "required",
            "imagen_articulo" => "image",
            "banner_articulo" => "image",
            "titulo" => "required",
            "vista_previa_descripcion" => "required",
            "descripcion" => "required",
        ];
    }

    public function messages()
    {
        return [
            "category.required" => "La Categoria es requerida.",
            "imagen_articulo.image" => "La Imagen de articulo debe de ser una imagen valida.",
            "banner_articulo.image" => "La Imagen de banner en articulo debe de ser una imagen valida.",
            "titulo.required" => "El Titulo es requerido.",
            "vista_previa_descripcion.required" => "La Vista previa de contenido es requerida.",
            "descripcion.required" => "El Contenido es requerido.",
        ];
    }

}
