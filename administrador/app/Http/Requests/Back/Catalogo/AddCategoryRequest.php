<?php

namespace OpenCava\Http\Requests\Back\Catalogo;

use Illuminate\Foundation\Http\FormRequest;

class AddCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "banner_principal" => "required|image",
            //"banner_articulo" => "required|image",
            "imagen_categoria" => "required|image",
            "categoria" => "required",
        ];
    }

    public function messages()
    {
        return [
            "banner_principal.required" => "La Imagen de banner principal es requerida.",
            //"banner_articulo.required" => "La Imagen de banner en articulo es requerida.",
            "imagen_categoria.required" => "La Imagen de categoria es requerida.",
            "categoria.required" => "La Categoria es requerida.",
            "banner_principal.image" => "La Imagen de banner principal debe de ser una imagen valida.",
            //"banner_articulo.image" => "La Imagen de banner en articulo debe de ser una imagen valida.",
            "imagen_categoria.image" => "La Imagen de categoria debe de ser una imagen valida.",
        ];
    }

}
