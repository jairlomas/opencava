<?php

namespace OpenCava\Http\Requests\Back\Catalogo;

use Illuminate\Foundation\Http\FormRequest;

class AddDictionaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "palabra" => "required",
            "descripcion" => "required",
        ];
    }

    public function messages()
    {
        return [
            "palabra.required" => "La Palabra es requerida.",
            "descripcion.required" => "La Descripcion es requerida.",
        ];
    }

}
