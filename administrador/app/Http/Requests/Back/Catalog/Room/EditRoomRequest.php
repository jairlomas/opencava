<?php

namespace OpenCava\Http\Requests\Back\Catalog\Room;

use Illuminate\Foundation\Http\FormRequest;

class EditRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     *

    //  caracteristica_name[]
    //  deal
     */
    public function rules()
    {

        $rules = array();

        $rules = [
            "name" => "required",
//            "principal_photo" => "required",
//            "imagen_galeria" => "required",
            "maximum_people" => "required",
            "price_sencilla" => "required",
            "price_doble"   => "required",
        ];

        $rules["caracteristica_name.*"] = "required";
        $rules["fecha_disponibilidad"] = "required";

        return $rules;
    }


    public function messages()
    {
        $messages = array();
        $messages = [
            "name.required"                 => "El campo 'Nombre de la habitación' es requerido.",
            "principal_photo.required"      => "El campo 'Fotografía principal' es requerido.",
            "imagen_galeria.required"       => "El campo 'Galería de habitación' es requerido.",
            "maximum_people.required"       => "El campo 'Capacidad de personas' es requerido.",
            "price_sencilla.required"       => "El campo 'Costo por persona en habitación de tipo sencilla ' es requerido.",
            "price_doble.required"          => "El campo 'Costo por persona en habitación de tipo doble' es requerido.",
            "fecha_disponibilidad.required" => "El campo 'Calendario de disponibilidad' es requerido.",
        ];

        if($this->request->get('caracteristica_name') != null) {
            foreach ($this->request->get('caracteristica_name') as $key => $val) {
                $messages['caracteristica_name.' . $key . '.required'] = "El campo " . ($key + 1) . " 'Características de la habitación' es requerido.";
            }
        }

        return $messages;
    }

}
