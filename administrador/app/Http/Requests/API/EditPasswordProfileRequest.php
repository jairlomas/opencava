<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class EditPasswordProfileRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"antigua_contrasena"	=>	"required",
			"nueva_contrasena"		=>	"required",
			"confirmar_contrasena"	=>	"required",
		];
	}

	public function messages(){
		return [
    		'antigua_contrasena.required'   => 'El campo antigua contraseña es obligatorio',
    		'nueva_contrasena.required'   	=> 'El campo nueva contraseña es obligatorio',
    		'confirmar_contrasena.required' => 'El campo confirmar contraseña es obligatorio',
		];
	}

}
