<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class EditMovilProfileRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"codigo_pais"	=> "required",
			"movil" 		=> "required|unique:clients,cellphone,".$this->get('user_id').",id",
			"contrasena"	=> "required"
		];
	}

	public function messages(){
		return [
    		'contrasena.required'   	=> 'El campo contraseña es obligatorio',
    		'movil.unique'   			=> 'Este teléfono móvil ya esta registrado',
		];
	}

}
