<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class DeleteAccountRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"contrasena"	=> "required",
			"motivo"		=> "required",
		];
	}

	public function message()
	{
		return [
			"contrasena.required"	=> "El campo contraseña es requerido",
			"motivo"				=> "Selecciona un motivo por el cual deseas eliminar tu cuenta",
		];
	}

}
