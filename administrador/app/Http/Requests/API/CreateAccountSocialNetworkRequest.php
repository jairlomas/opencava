<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class CreateAccountSocialNetworkRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nombre_y_apelllidos"	=>	"required",
			"nombre_de_usuario"		=>	"required|unique:clients,username",
			"correo_electronico"	=>	"required|email|unique:clients,email",
			"contrasena"			=>	"required",
			"codigo_pais"			=>	"required",
			"movil"					=>	"required|unique:clients,cellphone",
			"anio"					=>	"required",
			"mes"					=>	"required",
			"dia"					=>	"required"
		];
	}

	public function messages(){
		return [
    		'contrasena.required'   	=> 'El campo contraseña es obligatorio',
    		'anio.required'   			=> 'El campo año es obligatorio',
    		'nombre_de_usuario.unique' 	=> 'Tu Nombre de Usuario ya esta en uso, prueba otro',
    		'movil.unique' 				=> 'Este teléfono móvil ya esta registrado',
    		'correo_electronico.unique' => 'El correo eléctronico proporcionado por la red social ya esta registrado',
		];
	}

}
