<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class AddCreditCardRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"tipo_de_tarjeta"		=>		"required",
			"numero_de_tarjeta"		=>		"required|min:15|max:16",
			"mes_vencimiento"		=>		"required",
			"anio_vencimiento"		=>		"required",
			"cvv"					=>		"required",
			"calle"					=>		"required",
			"numero_exterior"		=>		"required",
			"colonia"				=>		"required",
			"cp"					=>		"required",
			"estado"				=>		"required",
			"municipio"				=>		"required",
		];
	}


	public function messages(){
		return [
    		'mes_vencimiento.required'   			=> 'El campo mes de vencimiento es obligatorio',
    		'anio_vencimiento.required'   			=> 'El campo año de vencimiento es obligatorio',
 		];
	}


}
