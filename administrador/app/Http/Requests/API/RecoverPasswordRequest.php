<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class RecoverPasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nombre_de_usuario" => "required"
		];
	}

}
