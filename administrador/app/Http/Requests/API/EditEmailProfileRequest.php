<?php namespace OpenCava\Http\Requests\API;

use OpenCava\Http\Requests\Request;

class EditEmailProfileRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			"nuevo_correo_electronico" 	=> "required|email|unique:clients,email,".$this->get('user_id').",id",
			"contrasena"				=> "required"
		];
	}

	public function messages(){
		return [
    		'contrasena.required'   				=> 'El campo contraseña es obligatorio',
    		'nuevo_correo_electronico.unique'   	=> 'Este correo electrónico ya esta registrado',
		];
	}

}
