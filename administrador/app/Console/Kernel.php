<?php namespace OpenCava\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;


use OpenCava\Bussines;
use OpenCava\Clients;
use OpenCava\Library\Pastora;
use DB;

use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;


class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //'OpenCava\Console\Commands\Inspire',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function () {

            $data = Bussines::first();
            $now_day = date("N");
            $now_hour = date("H:i");

            if( in_array($now_day,  explode(",", $data->aperture_day) )){

                $hour_open = date("H:i", strtotime($data->aperture_time));

                echo $now_hour ." ". $hour_open;
                if($now_hour == $hour_open){

                    $notification = array();
                    //$clients_uuid = DB::table("clients")->where("notification_aperture", 1)->get();
                    $clients_uuid = DB::table("clients")->where("id", 62)->get();
                    foreach ($clients_uuid as $value) {
                        if($value->os_type != ""){
                            if($value->uuid != ""){
                                $notification[] = (object)["os_type" => $value->os_type, "uuid" => $value->uuid ];
                            }
                        }
                    }

                    Pastora::sendPushNotification( $notification, "¡Open Cava se encuentra en servicio!", "Open Cava");
                }
            }

        })->cron('* * * * * *')->sendOutputTo("OUTPUT_CRON.txt");

    }

}