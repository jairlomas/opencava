<?php

namespace OpenCava\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use OpenCava\SystemModule;

class SidebarMenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('back.layout.partials.sidebar-menu', function ($view) {
            $parents = SystemModule::where('parent', 0)->get();
            $data = array('parents'=>$parents,);
            foreach ($parents as $kParent => $parent) {
                $children = SystemModule::where('parent', $parent->id)->orderBy('id')->get();
                $data[$parent->id]['children'] = $children;
            }
            $view->with('menuItems',$data);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
