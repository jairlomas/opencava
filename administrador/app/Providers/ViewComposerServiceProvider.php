<?php namespace OpenCava\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->profile();

		$this->menulateral();

		$this->breadcrums();
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * @author  Edgar Yerena <edgar.yerena@metodika.mx>
	 * @version 1.0 2015-03-26
	 *
	 * @return void
	 *
	 * Metodo que manda generar el menu lateral de navegación
	 */
	public function profile() {

		view()->composer('layouts.partials.profile', 'OpenCava\Http\Composers\DashboardComposer@profile');

	}


	/**
	 * @author  Edgar Yerena <edgar.yerena@metodika.mx>
	 * @version 1.0 2015-03-26
	 *
	 * @return void
	 *
	 * Metodo que manda generar el menu lateral de navegación
	 */
	public function menulateral() 
	{

		view()->composer('layouts.partials.menulateral', 'OpenCava\Http\Composers\DashboardComposer@menulateral');

	}


	/**
	 * @author  Edgar Yerena <edgar.yerena@metodika.mx>
	 * @version 1.0 2015-03-26
	 *
	 * @return void
	 *
	 * Metodo que manda generar el menu lateral de navegación
	 */
	public function breadcrums() 
	{

		view()->composer('layouts.partials.breadcrums', 'OpenCava\Http\Composers\DashboardComposer@breadcrums');

	}

}
