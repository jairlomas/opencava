<?php namespace OpenCava;

use Illuminate\Database\Eloquent\Model;
 
class Clients extends Model {

 
	public function get_orders_finished(){
		return $this->hasMany("OpenCava\Orders", "client_id", "id")->where("status", "entregado");
	}

	public function get_orders_cancel(){
		return $this->hasMany("OpenCava\Orders", "client_id", "id")->where("status", "cancelado");
	}

	public function get_credit_cards(){
		return $this->hasMany("OpenCava\CreditCard", "client_id", "id");
	}

}
