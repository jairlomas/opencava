<?php namespace OpenCava;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model {

	use SoftDeletes;

    public function category(){
    	return $this->hasOne('OpenCava\Categories', "id", "category_id");
    }

}
