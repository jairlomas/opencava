<?php namespace OpenCava;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrdersProducts extends Model {

	use SoftDeletes;

	public function product(){
    	return $this->hasOne('OpenCava\Products', "id", "product_id")->withTrashed();
	}

	public function promotion(){
    	return $this->hasOne('OpenCava\ProductsPromotion', "id", "promotion_id")->withTrashed();
	}

}
