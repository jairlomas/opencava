@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Ver Pedido
@stop

{{-- Module Name --}}
@section('moduleName')
	Ver Pedido
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/pedidos') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}


				<div class="row">
					<div class="form-group">
                        <label>Nombre Completo:</label>
                        <input type="text" class="form-control" value="{{ $data->client->name }}" disabled>
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Teléfono:</label>
                        <input type="text" class="form-control" value="+{{ $data->client->cellphone_code }} {{ $data->client->cellphone }}" disabled>
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Dirección:</label>
                        <input type="text" class="form-control" value="{{ $data->address }}" disabled>
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Resumen de pedido:</label>

                        	<table width="100%">
                        		<tr>
                        			<th style="text-align: center;">Producto</th>
                        			<th style="text-align: center;">Cantidad</th>
                        		</tr>

								@foreach($data->order_products as $op)
                        			<tr>								
										@if($op->product_id != 0)
											<td>&middot;{{ $op->product["name"] }}</td>
											<td align="center">{{ $op->quantity }}</td>
										@endif
										@if($op->promotion_id != 0)
											<td>&middot;{{ $op->promotion["name"] }}</td>
											<td align="center">{{ $op->quantity }}</td>										
										@endif
									</tr>									
								@endforeach

                        	</table>


                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Costo total:</label>
                        <input type="text" class="form-control" value="${{ number_format(($data->spending_cost + $data->subtotal), 2,".","") }}" disabled>
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
						<label>Metodo de pago:</label>
						@if( $data->purchase_type == "efectivo" )
							<input type="text" class="form-control" value="Efectivo" disabled>
						@else
							<input type="text" class="form-control" value="Tarjeta" disabled>
						@endif
					</div>
				</div>

				<div class="row">
					<div class="form-group">
						<label>Efectivo a recibir:</label>
						<input type="text" class="form-control" value="${{ number_format(($data->cash_receiver), 2,".","") }}" disabled>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Tiempo de entrega:</label>
                        <input type="text" class="form-control" value="{{ $data->remaining_time }}" disabled>
                    </div>					
				</div>
 
 				<div class="row">
					<div class="form-group" >
						<label class="col-sm-3 control-label">Geocerca</label>
						<input type="hidden" value="{{ $data->lat }}" id="lat" />
						<input type="hidden" value="{{ $data->lon }}" id="lon" />
						<div class="col-md-12" id="basic_map" style="height: 500px;"></div>
					</div>
				</div>

			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
	<script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDD0NwQEwoMHRpDFesDklu2zk3t-cQSd0g"></script>
	{!! Html::script('assets/js/catalogo/gasto_envio/gmaps.js') !!}
	{!! Html::script('assets/js/pedidos/view.js') !!}
@stop