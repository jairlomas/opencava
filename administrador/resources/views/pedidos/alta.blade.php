@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Alta de Categoría
@stop

{{-- Module Name --}}
@section('moduleName')
	Alta de Categoría
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/categorias') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}

				<div class="row">
					<div class="form-group">
                        <label>Categoria:</label>
                        <input type="text" class="form-control" name="categoria">
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Tipo:</label>
                        <select class="form-control" name="tipo">
                        	<option value="">Selecciona Tipo</option>
                        	<option value="Vinos">Vinos</option>
                        	<option value="Mezcladores">Mezcladores</option>
                        	<option value="Complementos">Complementos</option>
                        </select>
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Icono:</label>
                        <input type="file" class="form-control" name="icono" accept="image/png">
                    </div>					
				</div>

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
{!! Html::script('assets/js/catalogo/categorias/alta.js') !!}
@stop