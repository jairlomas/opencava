@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Perfil de Usuario
@stop

{{-- Module Name --}}
@section('moduleName')
	Perfil de Usuario
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>
						<th width="10%">No.</th>

						<th width="50%">Nombre</th>

						<th width="40%" class="text-right">Acciones</th>
					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td width="10%">{!! $centinel++ !!}</th>

							<td width="50%">{!! $item->name !!}</th>

							<td width="40%" class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/sistema/perfil/editar/'.base64_encode($item->id)) !!}" class="btn btn-info">Editar</a>
								@endif
								@if( $permitions["delete"] )
									<a href="{!! url('dashboard/sistema/perfil/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" class="btn btn-danger btnDelete">Borrar</a>
								@endif
								@if( $permitions["view"] )
									<a href="{!! url('dashboard/sistema/perfil/ver/'.base64_encode($item->id)) !!}" class="btn btn-primary">Ver</a>
								@endif								
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
@stop
