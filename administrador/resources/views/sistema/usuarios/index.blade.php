@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Usuarios
@stop

{{-- Module Name --}}
@section('moduleName')
	Usuarios
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>
						<th width="10%">No.</th>

						<th width="40%">Nombre</th>

						<th width="40%">Perfil de Usuario</th>

						<th width="10%" class="text-right">Acciones</th>
					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td width="10%">{{ $centinel++ }}</th>

							<td width="35%">{{ $item->name }}</th>

							<td width="35%">{{ $item->user_profile["name"] }}</th>

							<td width="20%" class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/sistema/usuarios/editar/'.base64_encode($item->id)) !!}" class="btn btn-info">Editar</a>
								@endif
								@if( $permitions["delete"] )
									<a href="{!! url('dashboard/sistema/usuarios/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" class="btn btn-danger btnDelete">Borrar</a>
								@endif
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
@stop
