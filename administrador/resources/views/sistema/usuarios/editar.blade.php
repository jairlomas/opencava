@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de nuevo Usuario
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de nuevo Usuario
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/sistema/usuarios') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/sistema/usuarios/editar/".$id) )) !!}

 				<div class="row">
					<div class="form-group">
                        <label>Nombre completo:</label>
                        <input type="text" class="form-control" name="nombre_completo" value="{{ $data->name }}">
                    </div>
					<div class="form-group">
                        <label>Nombre de Usuario:</label>
                        <input type="text" class="form-control" name="nombre_usuario" value="{{ $data->user_name }}">
                    </div>
					<div class="form-group">
                        <label>Correo Electrónico:</label>
                        <input type="text" class="form-control" name="correo_electronico" value="{{ $data->email }}" >
                    </div>
					<div class="form-group">
                        <label>Contraseña:</label>
                        <input type="password" class="form-control" name="contrasena">
                    </div>
					<div class="form-group">
                        <label>Repetir Contraseña:</label>
                        <input type="password" class="form-control" name="repetir_contrasena">
                    </div>
					<div class="form-group">
                        <label>Perfil de Usuario:</label>
                        <select class="form-control" name="perfil_usuario">
                        	<option value="">Perfil de Usuario</option>
                        	@foreach($perfil as $p)
                        		@if( $p->id ==  $data->user_profile_id)
                        			<option value="{{ $p->id }}" selected>{{ $p->name }}</option>
                        		@else
                        			<option value="{{ $p->id }}">{{ $p->name }}</option>
                        		@endif
                        	@endforeach
                        </select>
                    </div>
				</div>

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/sistema/usuarios/editar.js') !!}
@stop