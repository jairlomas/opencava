@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Asignación de Playlist
@stop

{{-- Module Name --}}
@section('moduleName')
	Asignación de Playlist <br />Establecimiento: {!! $business->name !!}
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/playlist_assign') !!}" class="btn btn-primary">Regresar</a>
@stop

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}
				<input type="hidden" value="{!! $id !!}" class="business_id"/>
				<div class="form-group">
					<label>Nombre del Playlist</label>
					<select name="playlist" id="playlistVideo">
					<option value="0">- Selecciona un playlist de video - </option>
					@foreach ($playlist as $value)
						<option value="{!! $value->id !!}">{!! $value->name !!}</option>
					@endforeach
					</select>	
					&nbsp;o
					<select name="playlistAudio" id="playlistAudio">
					<option value="0">- Selecciona un playlist de audio - </option>
					@foreach ($playlistAudio as $value)
						<option value="{!! $value->id !!}">{!! $value->name !!}</option>
					@endforeach
					</select>	
				</div>

				<div class="form-group">
					<label>Hora de inicio (24 horas)</label>
					<input type="text" name="hr_inicio" class="form-control input-small timepicker_inp" placeholder="12:00" id="hr_inicio"/>
				</div>

				<div class="form-group">
					<label>Hora de término (24 horas)</label>
					<input type="text" name="hr_fin" class="form-control input-small timepicker_inp" placeholder="12:00" id="hr_fin" />
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-1">
							<input type="checkbox" name="lunes" />
						</div>
						<div class="col-sm-2">
							<label for="lunes">Lunes</label>
						</div>
						<div class="col-sm-1">
							<input type="checkbox" name="martes" />
						</div>
						<div class="col-sm-2">
							<label for="martes">Martes</label>
						</div>
						<div class="col-sm-1">
							<input type="checkbox" name="miercoles" />
						</div>
						<div class="col-sm-2">				
							<label for="miercoles">Miercoles</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-1">
							<input type="checkbox" name="jueves" />
						</div>
						<div class="col-sm-2">					
							<label for="jueves">Jueves</label>
						</div>
						<div class="col-sm-1">
							<input type="checkbox" name="viernes" />
						</div>
						<div class="col-sm-2">
							<label for="viernes">Viernes</label>
						</div>
						<div class="col-sm-1">
							<input type="checkbox" name="sabado" />
						</div>
						<div class="col-sm-2">
							<label for="sabado">Sabado</label>
						</div>
					</div>
				</div>
				<div class="form-group">	
					<div class="row">
						<div class="col-sm-1">
							<input type="checkbox" name="domingo" />
						</div>
						<div class="col-sm-2">
							<label for="domingo">Domingo</label>
						</div>
					</div>
				</div>

				<div class="col-sm-12">
					<div class="playlist">						
						<div class="row">
							<div class="col-md-12">
								<table class="table">
									<thead>
										<tr>
											<th width="10%">No.</th>

											<th width="20%">Playlist</th>

											<th width="10%">Tipo</th>

											<th width="10%">Hora de Inicio</th>

											<th width="10%">Hora de Termino</th>

											<th width="20%">Dias</th>

											<th width="40%"></th>
										</tr>
									</thead>

									<tbody>
										@foreach ($assigned as $item)
											<tr>
												<td width="10%">{!! $centinel++ !!}</th>

												<td width="20%">{!! ucwords(strtolower($item->name)) !!}</th>

												<td width="10%">Video</td>

												<td width="10%">{!! $item->start_time !!}</th>

												<td width="10%">{!! $item->end_time !!}</th>

												<td width="10%">
												@if ($item->monday == 1)
													Lunes
												@endif
												@if ($item->tuesday == 1)
													Martes
												@endif
												@if ($item->wednesday == 1)
													Miercoles
												@endif
												@if ($item->thursday == 1)
													Jueves
												@endif
												@if ($item->friday == 1)
													Viernes
												@endif
												@if ($item->saturday == 1)
													Sabado
												@endif
												@if ($item->sunday == 1)
													Domingo
												@endif

												<td width="40%" class="text-right">
													<a href="{!! url('dashboard/playlist_assign/deassign_video/'.base64_encode($item->id)) !!}" class="btn btn-danger btnDelete" data-name="{!! $item->name !!}">Desasignar Playlist</a>
												</th>
											</tr>
										@endforeach
										@foreach ($assigned1 as $item)
											<tr>
												<td width="10%">{!! $centinel++ !!}</th>

												<td width="20%">{!! ucwords(strtolower($item->name)) !!}</th>

												<td width="10%">Audio</td>


												<td width="10%">{!! $item->start_time !!}</th>

												<td width="10%">{!! $item->end_time !!}</th>

												<td width="10%">
												@if ($item->monday == 1)
													Lunes
												@endif
												@if ($item->tuesday == 1)
													Martes
												@endif
												@if ($item->wednesday == 1)
													Miercoles
												@endif
												@if ($item->thursday == 1)
													Jueves
												@endif
												@if ($item->friday == 1)
													Viernes
												@endif
												@if ($item->saturday == 1)
													Sabado
												@endif
												@if ($item->sunday == 1)
													Domingo
												@endif

												<td width="40%" class="text-right">
													<a href="{!! url('dashboard/playlist_assign/deassign_audio/'.base64_encode($item->id)) !!}" class="btn btn-danger btnDelete" data-name="{!! $item->name !!}">Desasignar Playlist</a>
												</th>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop

{{-- Page JS --}}
@section('pageJS')
	{!! Html::script('assets/js/playlist_assign/playlist_assign.js') !!}
@stop