 
@extends('layouts.master')

@section('pageTitle')
	Catálogo de Sitios
@stop


{{-- Content --}}
@section('content')
<?php $centinel = 0; ?>

<div class='content-wrapper'>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <section class="box">
            <div class="box">
                <header class="panel_header">
                    <h2 class="title pull-left">Listado de Sitios</h2>
                </header>

                <div class="content-body padding-bottom-0">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-right">
								<a class="btn btn-primary" href="">Crear</a>                                 
                            </p>
                            <table class="table" id="dataTable">
                                <thead>
                                    <tr>
                                        <th width="15%">No.</th>
                                        <th width="20%">Nombre del Sitio</th>
                                        <th width="25%">Fecha de Inicio</th>
                                        <th width="25%">Fecha estimada de entrega</th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>No.</td>
                                        <td>Nombre del Sitio</td>
                                        <td>Fecha de Inicio</td>
                                        <td>Fecha estimada de entrega</td>
                                        <td></td>
                                    </tr>                                	
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@stop

{{-- Page JS --}}
@section('pageJS')
@stop

<!---->
