<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    {{-- Page Title --}}
    <title>
        @yield('pageTitle') - {!! ENV('PROJECT_NAME') !!}
    </title>

 

    @* Page CSS *@
    @RenderSection("pageCSS", required: false)

	{{-- Theme CSS --}}
	{!! Html::style('assets/css/bootstrap.min.css') !!}
	{!! Html::style('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css') !!}
	{!! Html::style('https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css') !!}
	{!! Html::style('assets/css/perfect-scrollbar/perfect-scrollbar.css') !!}
	{!! Html::style('assets/js/timepicker/css/timepicker.less') !!}
	{!! Html::style('assets/font-awesome/css/font-awesome.css') !!}
	{!! Html::style('assets/css/app.css') !!}
	{!! Html::style('assets/css/menuleft.css') !!}
	{!! Html::style('assets/css/style.css') !!}
	{!! Html::style('assets/css/CustomCss.css') !!}
     <link href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css" rel="stylesheet" type="text/css">

    {!! Html::style('assets/js/time/jquery.timeentry.css') !!}


	{{-- Page CSS --}}
	@section('pageCSS')
	@show

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>
    <!-- /// BARRA SUPERIOR DE MENU TOPBAR ///-->
    <div class="container-fluid dashboard">
        <div class="page-topbar">
            <div class="logo-area">
            </div>
            <div class="quick-area">
                <div class="pull-left slantit">
                    <ul class="info-menu left-links list-inline list-unstyled">
                        <li class="sidebar-toggle-wrap">
                            <a data-toggle="sidebar" class="sidebar_toggle">
                                <i class="material-icons fa fa-bars" data-icon1="fa fa-bars" data-icon2="fa fa-bars"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="pull-left pagetitle">
                    <span class="line"></span>
                    <h1>Open Cava</h1>
                    <div></div>
                </div>
                <!--
                <div class="pull-left quickleft" style="">
                    <ul class="info-menu right-links list-inline list-unstyled">

                        <li class="dropdown tab " uib-dropdown>
                            <a class="dropdown-toggle toggle" data-toggle="dropdown">
                                <i class="material-icons fa fa-bell"></i>
                                <span class="badge badge-accent" id="notificationNumber"></span>
                            </a>
                            <ul class="dropdown-menu  notifications animated fadeIn">
                                <li class="total">
                                    <span class="small">
                                        <font id="InnerNumberNotification">
                                            No tienes ninguna actualización nueva
                                        </font>
                                        <a href="javascript:;" class="pull-right" id="readAllItem" data-number="0">Marcar Leidas</a>
                                    </span>
                                </li>
                                <li class="list" id="WheelDemo">
                                    <perfect-scrollbar wheel-propagation="true" suppress-scroll-x="true" min-scrollbar-length="20" class='ps-scrollbar'>
                                        <ul class="dropdown-menu-list list-unstyled" id="notificationsList">
                                            <li class="read">
                                                <a href="javascript:;">
                                                    <div class="notice-icon">
                                                        <i class="material-icons fa fa-steam">Steam</i>
                                                    </div>
                                                    <div>
                                                        <span class="name">
                                                            <font>Mensaje</font>
                                                            <span class="time small">12:30</span>
                                                        </span>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </perfect-scrollbar>
                                </li>
                                <li class="external">
                                    <a href="javascript:;">
                                        <span>Leer todas</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                -->

                <div class='pull-right quickright'>
                    <ul class="info-menu right-links list-inline list-unstyled">

                        <li class="dropdown profile tab" uib-dropdown>
                            <a class="dropdown-toggle toggle" data-toggle="dropdown" uib-dropdown-toggle>
                                <img src="{{ url('/') }}/assets/img/avatar-1.png" alt="user-image" class="img-circle img-inline">

                                <span class='hidden-sm hidden-md hidden-xs'>{!! Auth::user()->name !!}</span>
                                <i class="material-icons hidden-md hidden-sm hidden-xs"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right profile animated fadeIn">
                                <li>
                                    <a href="{{ url('/') }}/logout">
                                        <i class="material-icons fa fa-lock"></i> Cerrar Sesión
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="chat-toggle-wrapper chat-slant">
                            <a data-toggle="chatbar" class="toggle_chat">

                            </a>
                        </li>
                    </ul>
                </div>


            </div>
        </div>
    </div>
    <!-- /// TERMINA BARRA SUPERIOR DE MENU TOPBAR ///-->

    <!-- SIDEBAR - START -->
    <!-- MAIN MENU - START -->
    <div class="container-fluid">
        <div class="page-container row-fluid">
            <div class="page-sidebar expandit">
                <perfect-scrollbar class="page-sidebar-wrapper" id="main-menu-wrapper" wheel-propagation="true" suppress-scroll-x="true" min-scrollbar-length="20" menuheight>
                    <!-- USER INFO - START -->
                    <div class="profile-info">
                        <div class="profile-image col-md-12 col-sm-12 col-xs-12 text-center">
                            <a>
                                <img src="{{ url('/') }}/assets/img/avatar-1.png" class="img-responsive">
                            </a>
                        </div>
                        <div class="profile-details col-md-12 col-sm-12 col-xs-12">
                            <h3>
                                <a>Open Cava</a>

                                <!-- Available statuses: online, idle, busy, away and offline -->
                            </h3>
                            <p class="profile-title">{!! Auth::user()->name !!}</p>
                        </div>
                    </div>
                    <!-- USER INFO - END -->
                    <!-- nav -->
                    <!-- /// MENU DE NAVEGACIÓN DEL SITIO -->

                    <div id='cssmenu'>
                        <ul class="wraplist wrapper-menu">
       
						@include('layouts.partials.menulateral')

                        <!--
                            <li class='auto has-sub active'>
                                <a href='javascript:;'>
                                    <i class="material-icons fa fa-file-excel-o"></i>
                                    <span class="title">Clientes</span>
                                </a>
                                <ul class='sub_menu'>
                                    <li class="">
                                        <a href='dashboard/clientes/empleados'>Empleados</a>
                                    </li>
                                    <li class="">
                                        <a href='dashboard/clientes/patrones'>Patrones</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="no-sub">
                                <a href='dashboard/reportes'>
                                    <i class="material-icons fa fa-book"></i>
                                    <span class="title">Reportes</span>
                                </a>
                            </li>
 					-->
                        </ul>
                    </div>

                    <!-- /// TERMINA MENU DE NAVEGACION DEL SITIO -->
                </perfect-scrollbar>
            </div>
        </div>
    </div>

    <!-- MAIN MENU - END -->
    <!--  SIDEBAR - END -->

    {{-- Main Content --}}
        <div class="container-fluid">
            <div class="page-container row-fluid">
                <section id="main-content">
                    <section class="wrapper main-wrapper" style="margin-top: 0;">
                        <div class="content-wrapper">
                            <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                                <section class="box">
                                    <header class="panel_header">
                                        <h2 class="title pull-left">
                                            @yield('moduleName')
                                        </h2>
                                    </header>
                                        <div class="pull-right padding-top-20 padding-right-20">
                                            @yield('addButton')
                                        </div>
                                    <div class="content-body padding-bottom-15">
                                        @yield('content')
                                    </div>
                                </section>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </div>
 

 
	{{-- Theme JS --}}
    {!! Html::script('assets/js/jquery-latest.min.js') !!}
	{!! Html::script('assets/js/menuleft.js') !!}
	{!! Html::script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js') !!}
	{!! Html::script('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') !!}
 	{!! Html::script('assets/js/jquery.easing.js') !!}
	{!! Html::script('assets/js/bootstrap.js') !!}
	{!! Html::script('assets/js/bootstrap/dropdown.js') !!}
	{!! Html::script('assets/js/bootstrap/collapse.js') !!}
	{!! Html::script('assets/js/bootbox.min.js') !!}
	{!! Html::script('assets/js/timepicker/js/bootstrap-timepicker.js') !!}
	{!! Html::script('assets/js/perfect-scrollbar/min/perfect-scrollbar.min.js') !!}
	{!! Html::script('assets/js/theme.js') !!}
	{!! Html::script('assets/js/pastora.js') !!}
	{!! Html::script('assets/js/currency.js') !!}

    {!! Html::script('assets/js/time/jquery.plugin.min.js') !!}
    {!! Html::script('assets/js/time/jquery.timeentry.js') !!}

 
	{{--  Page JS --}}
	@section('pageJS')
	@show






</body>
</html>