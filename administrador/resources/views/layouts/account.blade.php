<!DOCTYPE html>
<html lang="es">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">

    {{-- Page Title --}}
	<title>
		@yield('pageTitle') - {!! ENV('PROJECT_NAME') !!}
	</title>

	{{-- Theme CSS --}}
	{!! Html::style('assets/css/bootstrap.min.css') !!}
	{!! Html::style('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css') !!}
	{!! Html::style('assets/css/perfect-scrollbar/perfect-scrollbar.css') !!}
	{!! Html::style('assets/font-awesome/css/font-awesome.css') !!}
	{!! Html::style('assets/css/app.css') !!}
	{!! Html::style('assets/css/menuleft.css') !!}
	{!! Html::style('assets/css/style.css') !!}

	{{-- Page CSS --}}
	@section('pageCSS')
	@show

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->	

</head>
<body>
	<div class="container-fluid login_page">
		<div class="row">
			<div class="col-lg-offset-4 col-lg-4 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-offset-2 col-xs-8 loginpage">
				<h1>
					<a href="#" title="Login Page" tabindex="-1">AutoDJ</a>
				</h1>

				{{-- Content --}}
				@yield('content')
			</div>
		</div>
	</div>
	
	{{-- ThemeJS --}}
	{!! Html::script('assets/js/jquery-latest.min.js') !!}
	{!! Html::script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js') !!}
	{!! Html::script('assets/js/bootstrap.js') !!}
	{!! Html::script('assets/js/theme.js') !!}

	{{-- Page JS --}}
	@section('pageJS')
	@show
</body>
</html>
