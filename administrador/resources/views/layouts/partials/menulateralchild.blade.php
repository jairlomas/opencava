@if (count($children['child']) > 0)
	<li class="has-sub">
		<a href="javascript:;" class="">
			<i class="fa {!! $children['icon'] !!} fa-fw"></i>
			<span  class="sub-menu-text">{!! $children['name'] !!}</span>
			<span class="arrow"></span>
		</a>

		<ul class="sub">
			@foreach ($children['child'] as $children)
				@include('layouts.partials.menulateralchild', $children)
			@endforeach
		</ul>
@else

	<li>
		<a href="{!! url('dashboard/'.$children['url'])!!}">
			<i class="fa {!! $children['icon'] !!} fa-fw"></i>
			<span  class="sub-menu-text">{!! $children['name'] !!}</span>
		</a>

@endif

</li>