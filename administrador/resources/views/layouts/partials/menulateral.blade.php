
@foreach ($menu as $item)

	@if ( count($item['child']) > 0)

		<li class='auto has-sub active'>
		    <a href='javascript:;'>
		        <i class="material-icons fa {!! $item['icon'] !!}"></i>
		        <span class="title">{!! $item['name'] !!}</span>
		    </a>
		    <ul class='sub_menu'>
				@foreach ($item['child'] as $children)
			        <li class="">
						<a href="{!! url('dashboard/'.$children['url'])!!}">
			            	{!! $children['name'] !!}
			            </a>
			        </li>
				@endforeach
		    </ul>
		</li>

	@else

        <li class="no-sub">
            <a href="{!! url('dashboard/'.$item['url'])!!}">
                <i class="material-icons {!! $item['icon'] !!}"></i>
                <span class="title">{!! $item['name'] !!}</span>
            </a>
        </li>

   	@endif


@endforeach
