<div class="container-fluid">
		<div class="page-container row-fluid">
			<div class="page-sidebar expandit">
				<perfect-scrollbar class="page-sidebar-wrapper" id="main-menu-wrapper" wheel-propagation="true" suppress-scroll-x="true" min-scrollbar-length="20" menuheight>
				    <!-- USER INFO - START -->
				    <div class="profile-info">
				        <div class="profile-image col-md-12 col-sm-12 col-xs-12 text-center">
				            <a>
				                <img src="img/avatar-1.png" class="img-responsive">
				                <span class="profile-status available"></span>
				            </a>
				        </div>
				        <div class="profile-details col-md-12 col-sm-12 col-xs-12">
				            <h3>
				                    <a>John Carter</a>

				                    <!-- Available statuses: online, idle, busy, away and offline -->
				                </h3>
				            <p class="profile-title">Developer</p>
				        </div>
				    </div>
				    <!-- USER INFO - END -->
				    <!-- nav -->

				    <!-- /// MENU DE NAVEGACIÓN DEL SITIO -->

				    <div id='cssmenu'>
						<ul class="wraplist wrapper-menu">
   							<li class="">
   								<a class="nosub" >
   									<i class="material-icons fa fa-th-large"></i>
   									<span class="title">Dashboard</span>
   								</a>
   							</li>
   							<li class='auto has-sub active' >
   								<a href='javascript:;'>
   									<i class="material-icons fa fa-steam-square"></i>
   									<span class="title">Catalogos</span>
   								</a>
      							<ul class='sub_menu'>
        							<li class="">
         								<a href='{{ url("dashboard/users") }}'>Usuarios</a>
         							</li>    
                              <li class="">
                                 <a href='{{ url("dashboard/profile") }}'>Perfíl de Usuarios</a>
                              </li> 
         							<li class="">
         								<a href='{{ url("dashboard/business") }}'>Establecimientos</a>
         							</li> 
         							<li class="">
         								<a href='{{ url("dashboard/artists") }}'>Artistas</a>
         							</li>
         							<li class="">
         								<a href='{{ url("dashboard/music-types") }}'>Tipos de música</a>
         							</li>
                              <li class="">
                                 <a href='{{ url("dashboard/videos") }}'>Videos</a>
                              </li> 
         							<li class="">
         								<a href='{{ url("dashboard/music") }}'>Musica</a>
         							</li> 
         							<li class="">
         								<a href='{{ url("dashboard/playlist_video") }}'>Playlist de video</a>
         							</li>
                              <li class="">
                                 <a href='{{ url("dashboard/playlist_music") }}'>Playlist de música</a>
                              </li>
                              <li class="">
                                 <a href='{{ url("dashboard/playlist_assign") }}'>Asignación de Playlist</a>
                              </li>
                              <li class="">
                                 <a href='{{ url("dashboard/promotional_videos") }}'>Videos Promocionales</a>
                              </li>

      							</ul>
   							</li>
						</ul>
					</div>

					<!-- /// TERMINA MENU DE NAVEGACION DEL SITIO -->				    
				</perfect-scrollbar>				
			</div>
		</div>
	</div>