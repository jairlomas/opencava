	<div class="container-fluid dashboard">
		<div class="page-topbar">
			<div class="logo-area">
			</div>
			<div class="quick-area">
				<div class="pull-left slantit">
					<ul class="info-menu left-links list-inline list-unstyled">
						<li class="sidebar-toggle-wrap">
							<a href data-toggle="sidebar" class="sidebar_toggle">
								<i class="material-icons fa fa-bars" data-icon1="fa fa-bars" data-icon2="fa fa-bars"></i>
							</a>
						</li>
					</ul>
				</div>
				<div class="pull-left pagetitle">
					<span class="line"></span>
					<h1>{!! ENV('PROJECT_NAME') !!}</h1>
					<div></div>
				</div>
				
			    
			    <div class='pull-right quickright'>
			        <ul class="info-menu right-links list-inline list-unstyled">
			            
			            <li class="dropdown profile tab" uib-dropdown>
			                <a href class="dropdown-toggle toggle" data-toggle="dropdown" uib-dropdown-toggle>
			                    <img src="{{ asset('assets/img/profile.png') }}" alt="user-image" class="img-circle img-inline">
			                    <span class="profile-status available"></span>
			                    
			                        <span class='hidden-sm hidden-md hidden-xs'>John Carter </span>
			                        <i class="material-icons hidden-md hidden-sm hidden-xs"></i>
			                </a>
			                <ul class="dropdown-menu dropdown-menu-right profile animated fadeIn">
			                    <li>
			                        <a >
			                            <i class="material-icons fa fa-gear"></i> Settings
			                        </a>
			                    </li>
			                    <li>
			                        <a >
			                            <i class="material-icons fa fa-user"></i> Profile
			                        </a>
			                    </li>
			                    <li class="last">
			                        <a >
			                            <i class="material-icons fa fa-question-circle"></i> Help
			                        </a>
			                    </li>
			                    <li>
			                        <a >
			                            <i class="material-icons fa fa-lock"></i> Logout
			                        </a>
			                    </li>
			                </ul>
			            </li>
			            <li class="chat-toggle-wrapper chat-slant">
			                <a href data-toggle="chatbar" class="toggle_chat">
			                    
			                </a>
			            </li>
			        </ul>
			    </div>


	        </div>	
        </div>
	</div>