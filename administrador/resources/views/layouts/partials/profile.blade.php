<li class="dropdown user" id="header-user">
	<a class="dropdown-toggle" data-toggle="dropdown">
		{!! Html::image($profile->image) !!}
		<span class="username">{{ $profile->name }}</span>
		<i class="fa fa-angle-down"></i>
	</a>

	<ul class="dropdown-menu">
		<li>
			<a href="{{ url('logout') }}"><i class="fa fa-power-off"></i> Cerrar sesión</a>
		</li>
	</ul>
</li>