@extends('layouts.account')

{{-- Page Title --}}
@section('pageTitle')
	Recuperar Contraseña
@stop

{{-- Content --}}
@section('content')
	{!! Form::open() !!}

		<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
 		<p>
			<input type="password" name="password_static" class="input" placeholder="Contraseña">
		</p>
		<p>
			<input type="password" name="password" class="input" placeholder="Nueva contraseña">
		</p>
		<p>
			<input type="password" name="password_confirmation" class="input" placeholder="Confirmar contraseña">
		</p>
		<p class="submit">
			<button type="submit" class="btn btn-accent btn-block">Cambiar Contraseña</button>
		</p>
		<p class="submit">
			<a href="{{ url('/') }}" class="btn btn-primary btn-block">Cancelar</a>
		</p>
	{!! Form::close() !!}
@stop


{!! Html::script('http://code.jquery.com/jquery-latest.min.js') !!}
{!! Html::script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js') !!}
{!! Html::script('assets/js/jquery.easing.js') !!}
{!! Html::script('assets/js/bootstrap.js') !!}
{!! Html::script('assets/js/bootstrap/dropdown.js') !!}
{!! Html::script('assets/js/bootstrap/collapse.js') !!}
{!! Html::script('assets/js/bootbox.min.js') !!}
{!! Html::script('assets/js/timepicker/js/bootstrap-timepicker.js') !!}
{!! Html::script('assets/js/perfect-scrollbar/min/perfect-scrollbar.min.js') !!}
{!! Html::script('assets/js/theme.js') !!}
{!! Html::script('assets/js/pastora.js') !!}



<script>

	$(document).ready(function(){


		$(this).on('submit', 'form', function(e) {
			e.preventDefault();

			$.ajax({
				type       : 'POST',
				data       : $(this).serialize(),
				dataType   : 'json',
				url        : $(this).attr('action'),
				beforeSend : function () {
					blockForm();
				},
				error      : function (jqXHR) {
					unblockForm();

					if (jqXHR.status == 422)
					{
						displayErrors(jqXHR);
					}
					else
					{
						//displayMessage("Experimentamos fallas técnicas, intentelo más tarde.");
					}
				},
				success    : function (response) {
					unblockForm();

					if (response.status == true)
					{
						bootbox.alert(response.message, function () {
							window.location = response.route;
						});
					}
					else
					{
						bootbox.alert(response.message, function () {
							//window.location = response.route;
						});
					}
				}
			});
		});		


	});


</script>