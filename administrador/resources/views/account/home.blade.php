@extends('layouts.account')

{{-- Page Title --}}
@section('pageTitle')
	Inicio de Sesión
@stop

{{-- Content --}}
@section('content')
	{!! Form::open(array('url' => 'login')) !!}
		{{-- Display login errors --}}


		<p>
			<input type="email" name="email" class="input" placeholder="Email">
		</p>
						
		<p>
			<input type="password" name="password" class="input" placeholder="Contraseña">
		</p>

		<div class="row text-center">
			<a href="{{ url('password-recover') }}" >
				Recuperar contraseña
			</a>
		</div>

		<p class="submit">
			<button type="submit" class="btn btn-accent btn-block">Ingresar</button>
		</p>
	{!! Form::close() !!}
@stop