@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Clientes
@stop

{{-- Module Name --}}
@section('moduleName')
	Clientes
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="10%">No. 									</th>
						<th width="13%">Nombre									</th>
						<th width="13%">Email									</th>
						<th width="13%">Teléfono Móvil							</th>
						<th width="13%">Tipo de Tarjeta							</th>
						<th width="13%">No. de Tarjeta							</th>						
						<th width="5%">No. de pedidos completos					</th>
						<th width="5%">No. de Pedidos cancelados				</th>
						<th width="5%">No. de pedidos cancelados consecutivos	</th>
						<th width="10%" class="text-right">Acciones				</th>
 
  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>

 							<td>{{ $item->name }}</th>
							<td>{{ $item->email }}</th>
							<td>{{ $item->cellphone }}</th>
 							

 							<td>
 								@foreach($item->get_credit_cards as $c)
 									{{ $c->type }}
 								@endforeach
 							</th>
							<td>
 								@foreach($item->get_credit_cards as $c)
 									<?php echo substr($c->number,  (strlen($c->number) - 4), strlen($c->number) ); ?>
 								@endforeach
							</th>


							<td>{{ count($item->get_orders_finished) }}</th>
							<td>{{ count($item->get_orders_cancel) }}</th>
							<td>{{ $item->consecutive_order_cancel }}</th>

							<td class="text-right">
								@if( $permitions["view"] )
									<a href="{!! url('dashboard/clientes/history/'.base64_encode($item->id)) !!}" class="btn btn-success" data-name="{{ $item->name }}">Historial</a>								
								@endif

								@if( $permitions["edit"] )
									@if($item->active == 1)
										<a href="{!! url('dashboard/clientes/disable/'.base64_encode($item->id)) !!}" class="btn btn-primary banear" data-name="{{ $item->name }}">Banear</a>
									@else
										<a href="{!! url('dashboard/clientes/enable/'.base64_encode($item->id)) !!}" class="btn btn-danger reactivar" data-name="{{ $item->name }}">Reactivar</a>
									@endif
								@endif
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/clientes/index.js') !!}	
@stop

