@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Historial de Compras
@stop

{{-- Module Name --}}
@section('moduleName')
	Historial de Compras
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/clientes') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>
						<th>No. 									</th>
						<th>Dirección							</th>

						<th>Metodo de pago						</th>
						<th>Costo de envio						</th>
						<th>Subtotal								</th>
						<th>Total								</th>

						<th>Efectivo a recibir</th>

						<th>Status								</th>


						<th>Tipo de Tarjeta						</th>
						<th>Num. de Tarjeta						</th>



						<th>Código de compra						</th>
						<th>Fecha de Envio						</th>
						<th>Productos comprados					</th>
  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>


					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>
 							<td>{{ $item->address }}</th>

							<td>
								@if( $item->purchase_type == "efectivo" )
									Efectivo
								@else
									Tarjeta
								@endif
							</td>

 							<td>${{ number_format($item->spending_cost, 2, ".", ",") }}</th>
 							<td>${{ number_format($item->subtotal, 2, ".", ",") }}</th>
 							<td>${{ number_format($item->spending_cost + $item->subtotal, 2, ".", ",") }}</th>
							
							<td>${{ number_format($item->cash_receiver, 2, ".", ",") }}</td>

							<td>{{ strtoupper($item->status) }}</th>
							<td>
								@if( isset( $item->credit_card->type ) )
									{{ strtoupper( $item->credit_card->type ) }}
								@else
									-
								@endif
							</th>
							<td>
								@if( isset( $item->credit_card->number ) )
									{{ substr($item->credit_card->number, (strlen($item->credit_card->number) - 4), 4 ) }}
								@else
									-
								@endif
							</th>
							
							<td>{{ $item->code }}</th>
							<td>{{ date("Y-m-d H:i", strtotime($item->start_delivery_time)) }}</th>
							<td>

								<div class="row">
								<div class="col-md-8 col-sm-8 col-xs-8 text-center" style="display: inline-block;">
									Producto
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 text-center" style="display: inline-block;">
									Cantidad
								</div>
								</div>
								@foreach($item->order_products as $p)

									@if($p->product_id != 0)
										<div class="row">
											<div class="col-md-8 col-sm-8 col-xs-8" style="display: inline-block;">
												&middot;{{ $p->product->name }}
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4 text-center" style="display: inline-block;">
												{{ $p->quantity }}
											</div>
										</div>
									@else
										<div class="row">
											<div class="col-md-8 col-sm-8 col-xs-8" style="display: inline-block;">
												&middot;{{ $p->promotion->name }}
											</div>
											<div class="col-md-4 col-sm-4 col-xs-4 text-center" style="display: inline-block;">
												{{ $p->quantity }}
											</div>
										</div>
									@endif
								@endforeach
							</th>

						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/clientes/historial.js') !!}
@stop