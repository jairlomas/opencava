@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de Categoría
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de Categoría
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/categorias') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/catalogos/categorias/editar/".$id) )) !!}

                <input type="hidden" value="{{ $id }}" name="id" />
				<div class="row">
					<div class="form-group">
                        <label>Categoria:</label>
                        <input type="text" class="form-control" name="categoria" value="{{ $data->name }}">
                    </div>					
				</div>

				<div class="row">
					<div class="form-group">
                        <label>Tipo:</label>
                        <select class="form-control" name="tipo">
                        	<option value=""				<?php if($data->type == ""){ echo "selected"; } ?> 				>Selecciona Tipo</option>
                        	<option value="Vinos"			<?php if($data->type == "Vinos"){ echo "selected"; } ?> 		>Vinos</option>
                        	<option value="Mezcladores"		<?php if($data->type == "Mezcladores"){ echo "selected"; } ?> 	>Mezcladores</option>
                        	<option value="Complementos"	<?php if($data->type == "Complementos"){ echo "selected"; } ?> 	>Complementos</option>
                        </select>
                    </div>					
				</div>

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/catalogo/categorias/editar.js') !!}
@stop