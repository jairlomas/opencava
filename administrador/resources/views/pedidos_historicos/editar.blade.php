@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Asignar Pedido
@stop

{{-- Module Name --}}
@section('moduleName')
	Asignar Pedido
@stop
 

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/pedidos') !!}" class="btn btn-primary">Regresar</a>
@stop

{{-- Content --}}
@section("content")
	<div class="row">
  
		<div class="col-md-12">
 			
 			<input type="hidden" id="id_pedido" value="{{ $id }}" />
			<table class="table" id="table">
				<thead>
					<tr>
						<th width="10%">No.</th>
						<th width="30%">Nombre</th>
						<th width="30%">Teléfono</th>
						<th width="30%" class="text-right">Acciones</th>
					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{{ $centinel++ }}</td>
							<td>{{ $item->name }}</td>
							<td>{{ $item->phone }}</td>
							<td class="text-right">
								<a href="#" class="btn btn-info btnAsignar" data-id="{{ $item->id }}" >Asignar</a>
							</td>
						</tr>
					@endforeach
				</tbody>
 
			</table>

		</div>

	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
	{!! Html::script('assets/js/pedidos/asignar.js') !!}
@stop
