@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Historial de Pedidos
@stop

{{-- Module Name --}}
@section('moduleName')
	Historial de Pedidos
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">


		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#active">Activos</a></li>
			<li><a data-toggle="tab" href="#inactive">Cancelados</a></li>
		</ul>
		<div class="tab-content">
			<div id="active" class="tab-pane fade in active row">

				<div class="col-md-12">
		 
					<table class="table" id="table">
						<thead>
							<tr>
								<th>No.</th>
								<th>Cliente</th>
								<th>Metodo de pago</th>
								<th>Resumen de productos</th>
								<th>Costo</th>
								<th>Efectivo a recibir</th>
								<th>Tiempo de entrega</th>
								<th>Código de compra</th>
								<th>Estatus</th>
								<th class="text-right">Acciones</th>
							</tr>
						</thead>
		 
						<tbody>
							<?php $centinel = 1; ?>
							@foreach ($data_active as $item)
								@if( $item->client != null )
 								<tr>
									<td>{{ $centinel++ }}</td>
									<td>{{ $item->client->name }}</td>
									<td>
										@if($item->purchase_type == "efectivo")
											Efectivo
										@elseif($item->purchase_type == "solicitar_terminal")
											Solicitud de terminal
										@else
											Tarjeta
										@endif
									</td>

									<td>
										@foreach($item->order_products as $op)
											@if($op->product_id != 0)
												&middot;{{ $op->product["name"] }} <br>
											@endif
											@if($op->promotion_id != 0)
												&middot;{{ $op->promotion["name"] }} <br>
											@endif
										@endforeach
									</td>
									<td>${{ number_format(  ($item->spending_cost + $item->subtotal)  ,2,".","") }}</td>

									<td>${{ number_format(  ($item->cash_receiver)  ,2,".","") }}</td>

									<td>{{ $item->remaining_time }}</td>

									<td>{{ $item->code }}</td>

									<td>{{ strtoupper($item->status) }}</td>									
									<td class="text-right">
										@if( $permitions["view"] )
											<a href="{{ url('dashboard/historial_pedidos/ver') }}/{{ base64_encode($item->id) }}" class="btn btn-primary" >Ver Pedido</a>
										@endif
										@if( $permitions["edit"] )										
											{{--<a href="{{ url('dashboard/pedidos/asignar') }}/{{ base64_encode($item->id) }}" class="btn btn-info" >Enviado</a>--}}
 											{{--<a href="#" class="btn btn-success btnEntregado" data-id="{{ base64_encode($item->id) }}">Entregado</a>--}}
 											{{--<a href="#" class="btn btn-danger btnCancelado"  data-id="{{ base64_encode($item->id) }}">Cancelado</a>--}}
 											{{--<a href="#" class="btn btn-primary btnNotificar"  data-id="{{ base64_encode($item->id) }}">Notificar</a>--}}
										@endif								
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
		 
					</table>

				</div>

			</div>

			<div id="inactive" class="tab-pane fade row">
				<div class="col-md-12">
					<table class="table" id="table">
						<thead>
							<tr>
								<th>No.</th>
								<th>Cliente</th>
								<th>Metodo de pago</th>
								<th>Resumen de productos</th>
								<th>Costo</th>
								<th>Efectivo a recibir</th>
								<th>Tiempo de entrega</th>
								<th>Estatus</th>
								<th class="text-right">Acciones</th>
							</tr>
						</thead>
 						<tbody>
							<?php $centinel = 1; ?>
							@foreach ($data_inactive as $item)
								@if( $item->client != null )
								<tr>
									<td>{{ $centinel++ }}</td>
									<td>{{ $item->client->name }}</td>
									<td>
										@if($item->purchase_type == "efectivo")
											Efectivo
										@else
											Tarjeta
										@endif
									</td>
									<td>
										@foreach($item->order_products as $op)
											@if($op->product_id != 0)
												&middot;{{ $op->product["name"] }} <br>
											@endif
											@if($op->promotion_id != 0)
												&middot;{{ $op->promotion["name"] }} <br>
											@endif
										@endforeach
									</td>
									<td>${{ number_format(  ($item->spending_cost + $item->subtotal)  ,2,".","") }}</td>
									<td>${{ number_format(  ($item->cash_receiver)  ,2,".","") }}</td>
									<td>{{ $item->remaining_time }}</td>
									<td>{{ strtoupper($item->status) }}</td>
									<td class="text-right">
										@if( $permitions["view"] )
											<a href="{{ url('dashboard/pedidos/ver') }}/{{ base64_encode($item->id) }}" class="btn btn-primary" >Ver Pedido</a>
										@endif
										@if( $permitions["edit"] )
 											<!--<a href="#" class="btn btn-info " >Enviado</a>
 											<a href="#" class="btn btn-success">Entregado</a>
 											<a href="#" class="btn btn-danger">Cancelado</a>-->
										@endif
									</td>
								</tr>
								@endif
							@endforeach
						</tbody>
 					</table>
				</div>
			</div>



		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
	{!! Html::script('assets/js/pedidos/index.js') !!}
@stop
