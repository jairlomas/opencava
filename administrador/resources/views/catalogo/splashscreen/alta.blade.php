@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Alta de Splash
@stop

{{-- Module Name --}}
@section('moduleName')
	Alta de Splash
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/splash_screen') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}
 
 				<div class="row">
					<div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>					
				</div>
 
 				<div class="row">
					<div class="form-group">
                        <label>Splash:</label>
                        <input type="file" class="form-control" name="splash">
                    </div>					
				</div>


				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
{!! Html::script('assets/js/catalogo/splashscreen/alta.js') !!}
@stop