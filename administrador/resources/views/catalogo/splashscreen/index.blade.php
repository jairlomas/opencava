@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	SplashScreen
@stop

{{-- Module Name --}}
@section('moduleName')
	SplashScreen
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")

	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="10%">No. 						</th>
						<th width="20%">Nombre						</th>
						<th width="30%">Splash						</th>
						<th width="10%">Activo						</th>
						<th width="30%" class="text-right">Acciones	</th>

  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>
 							<td>{!! $item->name !!}</th>
 							<td><img src="{{ url($item->path) }}" style="max-height: 150px;" /></th>
 							<td align="center">
 								@if($item->active == 1)
 									<strong>Activo</strong>
 								@endif
 							</th> 								
							<td class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/catalogos/splash_screen/editar/'.base64_encode($item->id)) !!}" class="btn btn-info">Editar</a>

 									@if($item->active == 0)
										<a href="{!! url('dashboard/catalogos/splash_screen/active/'.base64_encode($item->id)) !!}" class="btn btn-success active_splash">Activar</a>
 									@endif

									@if( $permitions["delete"] )
										<a href="{!! url('dashboard/catalogos/splash_screen/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" class="btn btn-danger btnDelete">Borrar</a>
									@endif

								@endif
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
	{!! Html::script('assets/js/catalogo/splashscreen/index.js') !!}	
@stop
