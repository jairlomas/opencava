{{----}}
<div class="form-group">
	{!! Form::label('nombre', 'Nombre:', ['class' => 'col-sm-3 control-label']) !!}

	<div class="col-sm-7">
		@if (!isset($modelo->name))
			{!! Form::text('nombre', '', ['class' => 'form-control']) !!}
		@else
			{!! Form::text('nombre', $modelo->name, ['class' => 'form-control']) !!}
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('email', 'Email:', ['class' => 'col-sm-3 control-label']) !!}

	<div class="col-sm-7">
		@if (isset($modelo->email))
			{!! Form::email('email', $modelo->email, ['class' => 'form-control']) !!}
		@else
			{!! Form::email('email', '', ['class' => 'form-control']) !!}
		@endif
	</div>
</div>

<div class="form-group">
	{!! Form::label('password', 'Contraseña:', ['class' => 'col-sm-3 control-label']) !!}

	<div class="col-sm-7">
		{!! Form::password('password', ['class' => 'form-control']) !!}
		<p class="passwordMeter"></p>
		<div class="progress">
			<div class="progress-bar progress-bar-danger" style="width: 0%"></div>
			<div class="progress-bar progress-bar-warning" style="width: 0%"></div>
			<div class="progress-bar progress-bar-success" style="width: 0%"></div>
		</div>
	</div>
</div>

<div class="form-group">
	{!! Form::label('password_confirmation', 'Confirmar Contraseña:', ['class' => 'col-sm-3 control-label']) !!}

	<div class="col-sm-7">
		{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
	</div>
</div>

<div class="form-group">
	{!! Form::label('perfilDeUsuario', 'Perfil de Usuario:', ['class' => 'col-sm-3 control-label']) !!}

	<div class="col-sm-7">
		@if (isset($modelo->user_profile_id))
			{!! Form::select('perfilDeUsuario', $userProfile, $modelo->user_profile_id, ['class' => 'form-control']) !!}
		@else
			{!! Form::select('perfilDeUsuario', $userProfile, null, ['class' => 'form-control']) !!}
		@endif
	</div>
</div>


<div class="form-group">
	<label for="perfilDeUsuario" class="col-sm-3 control-label">Imagen de perfil:</label>
	<div class="col-sm-7">
		{!! Form::file('thefile') !!}
	</div>
</div>


<div class="form-group">
	<div class="col-sm-7 col-sm-offset-3">
		{!! Form::submit($buttonName, ['class' => 'btn btn-success btnSend']) !!}
	</div>
</div>


