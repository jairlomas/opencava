@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Alta de Motivo
@stop

{{-- Module Name --}}
@section('moduleName')
	Alta de Motivo
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/motivos') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}
 
 				<div class="row">
					<div class="form-group">
                        <label>Motivo:</label>
                        <input type="text" class="form-control" name="motivo">
                    </div>					
				</div>
 

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
{!! Html::script('assets/js/catalogo/motivo/alta.js') !!}
@stop