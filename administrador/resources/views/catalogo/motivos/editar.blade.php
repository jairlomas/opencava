@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de Motivos
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de Motivos
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/motivos') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/catalogos/motivos/editar/".$id) )) !!}

                <input type="hidden" value="{{ $id }}" name="id" />
 				<div class="row">
					<div class="form-group">
                        <label>Motivo:</label>
                        <input type="text" class="form-control" name="motivo" value="{{ $data->reason }}">
                    </div>					
				</div>

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/catalogo/motivo/editar.js') !!}
@stop