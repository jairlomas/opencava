@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Productos
@stop

{{-- Module Name --}}
@section('moduleName')
	Productos
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="10%">No.</th> 
						<th width="12%">Icono</th>
						<th width="12%">Nombre</th>
						<th width="12%">Categoría</th>
						<th width="12%">Precio</th>
						<th width="12%">Cantidad</th>
						<th width="30%" class="text-right">Acciones</th>

  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>
							<td><img src="{{ url($item->icon) }}" style="max-height: 200px;"/></th>
							<td>{{ $item->name }}</th>
							<td>{{ $item->category->name }}</th>
							<td>${{ number_format($item->price,2,".","") }}</th>
							<td>{{ $item->cantidad }}</th>

							<td class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/catalogos/productos/editar/'.base64_encode($item->id)) !!}" class="btn btn-info">Editar</a>
								@endif
								@if( $permitions["delete"] )
									<a href="{!! url('dashboard/catalogos/productos/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" class="btn btn-danger btnDelete">Borrar</a>
								@endif
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
@stop
