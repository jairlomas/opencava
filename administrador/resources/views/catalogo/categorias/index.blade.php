@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Categorías
@stop

{{-- Module Name --}}
@section('moduleName')
	Categorías
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="14%">No. 		</th>
						<th width="14%">Icono		</th>
						<th width="14%">Categoría	</th>
						<th width="14%">Tipo		</th>
						<th width="5%">Orden		</th>
						<th width="39%" class="text-right">Acciones	</th>

  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>
							<td><img src="{{ url($item->icon) }}" width="100%"/></th>
							<td>{{ $item->name }}</th>
							<td>{{ $item->type }}</th>
							<td>{{ $item->order }}</th>


							<td class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/catalogos/categorias/editar/'.base64_encode($item->id)) !!}" class="btn btn-info">Editar</a>
								@endif
								@if( $permitions["delete"] )
									<a href="{!! url('dashboard/catalogos/categorias/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" id="{{ $item->id }}" current="{{ $item->order }}" class="btn btn-danger borrar_order">Borrar</a>
								@endif
								@if( $permitions["edit"] )

									@if( $item->order+1 > $count )
										<a class="btn btn-primary " disabled>Bajar</a>
									@else
										<a href="" class="btn btn-primary bajar" id="{{ $item->id }}" current="{{ $item->order }}" >Bajar</a>
									@endif


									@if( $item->order-1 == 0 )
										<a class="btn btn-primary" disabled>Subir</a>
									@else
										<a href="" class="btn btn-primary subir" id="{{ $item->id }}" current="{{ $item->order }}" >Subir</a>
									@endif


								@endif

							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
	{!! Html::script('assets/js/catalogo/categorias/index.js') !!}	
@stop
