@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de Negocio
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de Negocio
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/negocio') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')

	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/catalogos/negocio/editar/".$id) )) !!}

                <input type="hidden" value="{{ $id }}" name="id" />

                <!-- 	
					dia_apertura
					data
    	        -->
 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Lunes</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="1"  <?php if(in_array(1, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Martes</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="2"  <?php if(in_array(2, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>
				
 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Miércoles</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="3"  <?php if(in_array(3, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>
				
 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Jueves</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="4"  <?php if(in_array(4, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>
				
 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Viernes</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="5"  <?php if(in_array(5, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>
				
 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Sabado</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="6"  <?php if(in_array(6, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>
				
 				<div class="row">
					<div class="form-group" style="text-left">
                        <div class="col-md-3"><label>Domingo</label></div>
                        <div class="col-md-1"><input type="checkbox" class="form-control" name="dia_apertura[]" value="7"  <?php if(in_array(7, $dia_apertura)){ echo "checked"; } ?> style="width: auto; display: inline-block; vertical-align: bottom;" ></div>
                    </div>					
				</div>
 

 				<div class="row">
					<div class="form-group">
                        <label>Horario apertura:</label>
                        <input type="text" class="form-control" name="horario_apertura" id="horario_apertura" value="{{ $data->aperture_time }}" data-timeEntry="show24Hours: true" >
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Horario cierre:</label>
                        <input type="text" class="form-control" name="horario_cierre" id="horario_cierre" value="{{ $data->close_time }}" data-timeEntry="show24Hours: true" >
                    </div>					
				</div>


				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/catalogo/negocio/editar.js') !!}
@stop