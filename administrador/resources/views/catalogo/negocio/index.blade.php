@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Negocio
@stop

{{-- Module Name --}}
@section('moduleName')
	Negocio
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")

	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="10%">No. 				</th>
						<th width="20%">Dia de apertura		</th>
						<th width="20%">Hora de apertura	</th>
						<th width="20%">Hora de cierre		</th>
						<th width="30%" class="text-right">Acciones	</th>

  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
						<tr>
							<td>{!! $centinel++ !!}</th>
 							<td>{!! $data->aperture_day !!}</th>
 							<td>{{ $data->aperture_time }}</th>
 							<td>{{ $data->close_time }}</th>
							<td class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/catalogos/negocio/editar/'.base64_encode($data->id)) !!}" class="btn btn-info">Editar</a>
								@endif
							</th>
						</tr>
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
@stop
