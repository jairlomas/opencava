@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Promociones
@stop

{{-- Module Name --}}
@section('moduleName')
	Promociones
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="10%">No.</th> 
						<th width="15%">Icono</th>
						<th width="15%">Nombre</th>
						<th width="15%">Precio</th>
						<th width="15%">Cantidad</th>
						<th width="30%" class="text-right">Acciones</th>

  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>
							<td><img src="{{ url($item->icon) }}" style="max-height: 200px;" /></th>
							<td>{{ $item->name }}</th>
							<td>${{ number_format($item->price,2,".","") }}</th>
							<td>{{ $item->cantidad }}</th>

							<td class="text-right">
								@if( $permitions["edit"] )
									<a onclick="toEdit('{!! url('dashboard/catalogos/promociones/editar/'.base64_encode($item->id)) !!}');" class="btn btn-info">Editar</a>
								@endif
								@if( $permitions["delete"] )
									<a href="{!! url('dashboard/catalogos/promociones/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" class="btn btn-danger btnDelete">Borrar</a>
								@endif
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
@stop


<script>

	function toEdit(url){

		var table = $("#table, .table").DataTable();
		var info = table.page.info();

		window.location = url + "?page="+info.page+"&lenght="+info.length;

	}

</script>