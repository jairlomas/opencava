@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de Promoción
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de Promoción
@stop

{{-- Action Button --}}
@section('addButton')
    <a href="{!! url('dashboard/catalogos/promociones') !!}<?php echo "?page=".$_GET["page"]."&lenght=".$_GET["lenght"]; ?>" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/catalogos/promociones/editar/".$id) )) !!}


                <input type="hidden" value="{{ $id }}" name="id" />
				<div class="row">
					<div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" name="nombre" value="{{ $data->name }}">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Precio:</label>
                        <input type="number" class="form-control" name="precio" step="any" value="{{ number_format($data->price,2,'.','') }}">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Icono:</label>
                        <br>
                        <div class="text-center">
                        	<img src="{{ url($data->icon) }}" style="max-width: 400px;" />
                    	</div>
                        <input type="file" class="form-control" name="icono" accept="image/png" >
                    </div>					
				</div>
 
                <!--
  				<div class="row">
					<div class="form-group">
                        <label>Tipo:</label>
                        <select class="form-control" name="tipo">
                        	<option value=""				<?php if($data->type == ""){			echo "selected";	} ?> 	>Seleccione Tipo</option>
                        	<option value="Individual"		<?php if($data->type == "Individual"){	echo "selected";	} ?> 	>Individual</option>
                        	<option value="Paquete"			<?php if($data->type == "Paquete"){		echo "selected";	} ?> 	>Paquete</option>
                        </select>
                    </div>					
				</div>
                -->

                <div class="row">
                    <div class="form-group">
                        <label>Cantidad:</label>
                        <input type="number" class="form-control" name="cantidad" value="{{ $data->cantidad }}">
                    </div>                  
                </div>

                <div class="row">
                    <div class="form-group">
                        <label>Bebida:</label>
                        @if($data->is_drink == 1)
                            <input type="checkbox" class="form-control" name="bebida"  style="width: 20px;" checked>
                        @else
                            <input type="checkbox" class="form-control" name="bebida"  style="width: 20px;">
                        @endif
                    </div>                  
                </div>                

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/catalogo/productos_promocion/editar.js') !!}
@stop