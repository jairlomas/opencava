@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Alta de Promoción
@stop

{{-- Module Name --}}
@section('moduleName')
	Alta de Promoción
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/promociones') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}

				<div class="row">
					<div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Precio:</label>
                        <input type="number" class="form-control" name="precio" step="any">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Icono:</label>
                        <input type="file" class="form-control" name="icono" accept="image/png">
                    </div>					
				</div>

                <!--
  				<div class="row">
					<div class="form-group">
                        <label>Tipo:</label>
                        <select class="form-control" name="tipo">
                        	<option value="">Seleccione Tipo</option>
                        	<option value="Individual">Individual</option>
                        	<option value="Paquete">Paquete</option>
                        </select>
                    </div>					
				</div>
                -->

                <div class="row">
                    <div class="form-group">
                        <label>Cantidad:</label>
                        <input type="number" class="form-control" name="cantidad">
                    </div>                  
                </div>

                <div class="row">
                    <div class="form-group">
                        <label>Bebida:</label>
                        <input type="checkbox" class="form-control" name="bebida"  style="width: 20px;">
                    </div>                  
                </div>

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
{!! Html::script('assets/js/catalogo/productos_promocion/alta.js') !!}
@stop