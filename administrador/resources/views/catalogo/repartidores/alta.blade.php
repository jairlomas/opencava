@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Alta de Repartidor
@stop

{{-- Module Name --}}
@section('moduleName')
	Alta de Repartidor
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/repartidores') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}

				<div class="row">
					<div class="form-group">
                        <label>Nombre completo:</label>
                        <input type="text" class="form-control" name="nombre_completo">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Teléfono:</label>
                        <input type="text" class="form-control" name="telefono">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Email:</label>
                        <input type="text" class="form-control" name="email">
                    </div>					
				</div>
 

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
{!! Html::script('assets/js/catalogo/repartidores/alta.js') !!}
@stop