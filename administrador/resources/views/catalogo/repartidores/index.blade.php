@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Repartidores
@stop

{{-- Module Name --}}
@section('moduleName')
	Repartidores
@stop
 

{{-- Action Button --}}
@section('addButton')
	{!! $createButton !!}
@stop

{{-- Content --}}
@section("content")
	<div class="row">
		<div class="col-md-12">
			<table class="table" id="table">
				<thead>
					<tr>

						<th width="10%">No. 				</th>
						<th width="20%">Nombre Completo		</th>
						<th width="20%">Teléfono			</th>
						<th width="20%">Email				</th>
						<th width="30%" class="text-right">Acciones	</th>

  					</tr>
				</thead>
 
				<tbody>
					<?php $centinel = 1; ?>
					@foreach ($data as $item)
						<tr>
							<td>{!! $centinel++ !!}</th>
							<td>{{ $item->name }}</th>
							<td>{{ $item->phone }}</th>
							<td>{{ $item->email }}</th>


							<td class="text-right">
								@if( $permitions["edit"] )
									<a href="{!! url('dashboard/catalogos/repartidores/editar/'.base64_encode($item->id)) !!}" class="btn btn-info">Editar</a>
								@endif
								@if( $permitions["delete"] )
									<a href="{!! url('dashboard/catalogos/repartidores/borrar/'.base64_encode($item->id)) !!}" data-name="{!! $item->name !!}" class="btn btn-danger btnDelete">Borrar</a>
								@endif
							</th>
						</tr>
					@endforeach
				</tbody>
 


			</table>

 
		</div>
	</div>
@stop


@section('pageJS')
	{!! Html::script('assets/js/simple-catalog.js') !!}
@stop
