@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de Repartidor
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de Repartidor
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/repartidores') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/catalogos/repartidores/editar/".$id) )) !!}

                <input type="hidden" value="{{ $id }}" name="id" />
				<div class="row">
					<div class="form-group">
                        <label>Nombre completo:</label>
                        <input type="text" class="form-control" name="nombre_completo" value="{{ $data->name }}">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Teléfono:</label>
                        <input type="text" class="form-control" name="telefono" value="{{ $data->phone }}">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Email:</label>
                        <input type="text" class="form-control" name="email" value="{{ $data->email }}">
                    </div>					
				</div>

				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
{!! Html::script('assets/js/catalogo/repartidores/editar.js') !!}
@stop