@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Edición de Gasto de Envío
@stop

{{-- Module Name --}}
@section('moduleName')
	Edición de Gasto de Envío
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/gastos_envio') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open(array("url" => url("dashboard/catalogos/gastos_envio/editar/".$id) )) !!}

                <input type="hidden" value="{{ $id }}" name="id" />
				<input type="hidden" name="geocerca" id="geocerca" value="{{ $data->geofence }}" />
				<div class="row">
					<div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" name="nombre" value="{{ $data->name }}">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Costo:</label>
                        <input type="number" step="any" class="form-control" name="costo" value="{{ number_format($data->price,2,'.','') }}">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group" >
						<label class="col-sm-3 control-label">Geocerca</label>
						<br><br>
						<a class="btn btn-primary eliminar_marcadores"></i> Eliminar marcadores</a>
						<br><br>
						<div class="col-md-12" id="basic_map" style="height: 500px;"></div>
					</div>
				</div>

				<br><br>
				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop


@section('pageJS')
	<script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDD0NwQEwoMHRpDFesDklu2zk3t-cQSd0g"></script>
	{!! Html::script('assets/js/catalogo/gasto_envio/gmaps.js') !!}
	{!! Html::script('assets/js/catalogo/gasto_envio/editar.js') !!}
@stop