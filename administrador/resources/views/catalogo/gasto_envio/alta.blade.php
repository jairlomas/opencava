@extends('layouts.master')

{{-- Page Title --}}
@section('pageTitle')
	Alta de Gasto de Envío
@stop

{{-- Module Name --}}
@section('moduleName')
	Alta de Gasto de Envío
@stop

{{-- Action Button --}}
@section('addButton')
	<a href="{!! url('dashboard/catalogos/gastos_envio') !!}" class="btn btn-primary">Regresar</a>
@stop

 

{{-- Content --}}
@section('content')
	<div class="row">
		<div class="col-md-12">
			{!! Form::open() !!}

				<input type="hidden" name="geocerca" id="geocerca" value="" />
				<div class="row">
					<div class="form-group">
                        <label>Nombre:</label>
                        <input type="text" class="form-control" name="nombre">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group">
                        <label>Costo:</label>
                        <input type="number" step="any" class="form-control" name="costo">
                    </div>					
				</div>

 				<div class="row">
					<div class="form-group" >
						<label class="col-sm-3 control-label">Geocerca</label>
						<br><br>
						<a class="btn btn-primary eliminar_marcadores"></i> Eliminar marcadores</a>
						<br><br>
						<div class="col-md-12" id="basic_map" style="height: 500px;"></div>
					</div>
				</div>

				<br><br>
				<div class="form-group text-right">
					<button class="btn btn-primary" type="submit">Guardar</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@stop
  
@section('pageJS')
	<script src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyDD0NwQEwoMHRpDFesDklu2zk3t-cQSd0g"></script>
	{!! Html::script('assets/js/catalogo/gasto_envio/gmaps.js') !!}
	{!! Html::script('assets/js/catalogo/gasto_envio/alta.js') !!}
@stop