
<html>
    <head>
        <title>{{ env('PROJECT_NAME') }} - @yield('title')</title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" type="text/css" href="{{ url('assets/js/front/bootstrap-4.0.0-dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('assets/js/front/bootstrap-4.0.0-dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('assets/css/general.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ url('assets/css/general-responsive.css') }}">

        @section('pageCSS')
        @show

    </head>
    <body>


        <header>
            <div class="section_navbar">

                <div class="header_top">
                    <div class="email">
                        <img src="{{ url('assets/img/front/email-navbar-icon.svg') }}">
                        <a href="mailto:ventas@aceromex.com" >ventas@aceromex.com</a>
                    </div>
                    <div class="phone">
                        <img src="{{ url('assets/img/front/telefono-navbar-icon.svg') }}">
                        <a href="" >01 800 AceroMX (223 7669)</a>
                    </div>
                    {{--<div class="flag_mexico" title="Versión en Español">--}}
                        {{--<img src="{{ url('assets/img/front/esp-mexico-bandera-on-btn.svg') }}">--}}
                    {{--</div>--}}
                    <div class="flag_eua" title="English Version">
                        {{--<img src="{{ url('assets/img/front/eng-eua-bandera-on-btn.svg') }}">--}}
                    </div>
                </div>

                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img src="{{ url('assets/img/front/aceromex-navbar-id.svg') }}" />
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a href="{{ url('/') }}" class="nav-link">Productos</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('/#servicios') }}" class="nav-link">Servicios</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('nosotros') }}" class="nav-link">Nosotros</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ url('contacto') }}" class="nav-link">Contacto</a>
                            </li>
                            <li class="nav-item">
                                <div>
                                    {{--<input value="" type="text" placeholder="Estoy buscando..." class="search_input" />--}}
                                    <img class="search_button" src="{{ url('assets/img/front/buscar-off-btn.svg') }}" id="btnOpenSearch">
                                </div>
                            </li>
                            <li class="nav-item cotizar_button">
                                <img src="{{ url('assets/img/front/cotizar-icon.svg') }}">
                                <a class="nav-link ">( 0 )</a>
                            </li>
                        </ul>

                    </div>
                </nav>

            </div>
        </header>
        <div class="search" id="searchBar">
            <div class="content_search">
                <input value="" placeholder="Estoy buscando..." class="search-bar">
                <div class="search_icon">
                    <img src="{{ url("assets/img/front/buscar-off-btn.svg") }}" />
                </div>
            </div>
        </div>

        @yield('content')


        <div class="mtk-footer-bg container-fluid">
            <div class="container-fluid footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="description">Aceromex es una empresa mexicana enfocada a la comercialización y transformación de productos de acero para la industria metalmecánica y de la construcción.</p>
                        </div>
                        <div class="col-md-6 text-center">
                            <p class="title_contacto">QUEREMOS SER PARTE DE TUS PROYECTOS.</p>
                            <br>
                            <a href="{{ url('contacto') }}" class="contacto_button">Contáctanos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="experience container-fluid text-center">
            <div class="container">
                <div class="col-sm-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 col-xl-10 offset-xl-1 offset-md-1">
                    <p class="title mtk-my-title">Certificaciones y reconocimientos</p>
                    <p class="subtitle mtk-subtitle">Más de 50 años de experiencia en la industria metalmecánica y de la construcción, hablan de nuestro compromiso con la calidad y nuestra prioridad por satisfacer las exigencias de nuestros clientes.</p>
                    <div class="row">
                        <div class="col-md-10 offset-md-1">
                            <p class="description">Cumplimos con los más altos estándares de calidad, servicio y precio lo que nos ha llevado a obtener certificaciones y reconocimientos. Sin embargo, seguimos en constante innovación, exigiéndonos nuevos y grandes retos.</p>
                        </div>
                    </div>
                </div>
                <div class="row certifications">
                    <div class="col-4 col-sm-3 col-md-2">
                        <img src="{{ url("assets/img/front/reconocimiento-1-anab-img.png") }}" />
                    </div>
                    <div class="col-4 col-sm-3 col-md-2">
                        <img src="{{ url("assets/img/front/reconocimiento-2-mgmt-sys-img.png") }}" />
                    </div>
                    <div class="col-4 col-sm-3 col-md-2">
                        <img src="{{ url("assets/img/front/reconocimiento-3-iso-img.png") }}" />
                    </div>
                    <div class="col-4 col-sm-3 col-md-2">
                        <img src="{{ url("assets/img/front/reconocimiento-4-ilac-mra-img.png") }}" />
                    </div>
                    <div class="col-4 col-sm-3 col-md-2">
                        <img src="{{ url("assets/img/front/reconocimiento-5-pjla-img.png") }}" />
                    </div>
                    <div class="col-4 col-sm-3 col-md-2">
                        <img src="{{ url("assets/img/front/reconocimiento-6-cfe-img.png") }}" />
                    </div>
                </div>
            </div>
        </div>




        <div class="privacy container-fluid text-center">
            <a href="#">POLÍTICAS DE PRIVACIDAD</a>
        </div>

        <div class="footer_bottom">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <div class="row">
                            {{--col-sm-12 col-md-4 col-lg-3 col-xl-3--}}
                            <div class="col-sm-12 col-md-12 offset-md-2 col-lg-5 offset-lg-2 col-xl-5 offset-xl-2">
                                <a href="#" class="legales">LEGALES</a>
                            </div>
                            <div class="col-sm-12 col-md-12 offset-md-2 col-lg-5 offset-lg-0 col-xl-5 offset-xl-0">
                                <a href="#" class="legales">MAPA DE SITIO</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <p class="copyright">&copy; 2018 Aceromex. Todos los derechos reservados.</p>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <a href="#" class="social-network facebook col-md-2">
                                {{--<img class="facebook" src="{{ url('assets/img/front/redes-sociales/fb-footer-off-btn.svg') }}">--}}
                            </a>
                            <a href="#" class="social-network linkedin col-md-2">
                                {{--<img class="" src="{{ url('assets/img/front/redes-sociales/linkedin-footer-off-btn.svg') }}">--}}
                            </a>
                            <a href="#" class="social-network twitter col-md-2">
                                {{--<img class="" src="{{ url('assets/img/front/redes-sociales/tw-footer-off-btn.svg') }}">--}}
                            </a>
                            <a href="#" class="social-network youtube col-md-2">
                                {{--<img class="" src="{{ url('assets/img/front/redes-sociales/yt-footer-off-btn.svg') }}">--}}
                            </a>
                            <div class="col-md-4">&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ url('assets/js/front/jquery-3.2.1.min.js') }}"></script>
        <script src="{{ url('assets/js/front/popper.min.js') }}"></script>
        <script src="{{ url('assets/js/front/bootstrap-4.0.0-dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ url('assets/js/front/general.js') }}"></script>

        @section('pageSCRIPTS')
        @show

    </body>
</html>