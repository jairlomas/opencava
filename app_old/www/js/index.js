var debug = false;
// var webservice = "http://localhost/open-cava/administrador/public/api/";
var webservice = "http://metodika.com.mx/dsr/opencava/administrador/public/api/";
var APP_VERSION = "6";

//PRODUCTION
var conekta_private_key = "key_hwv8ZYjUqNnrRMNfSM6qDw";
var conekta_public_key =  "key_RsDV8zsWD8nMxTddPyTgrig";

//SANDBOX
//var conekta_private_key = "key_rFQDVsfSGnBBr5hrjPAE7A";
//var conekta_public_key =  "key_CyUNLJBNGpK9QYzUm3yvbjA";

var errorResponseHandler_cancel_order, successResponseHandler_cancel_order, tokenParams_cancel_order;
var errorResponseHandler_purchase_order, successResponseHandler_purchase_order, tokenParams_purchase_order;

var foto_red_social = 0;
var email_red_social = "";
var tipo_red_social = "";
var celular_recuperar_contrasena = "";
var email_recuperar_contrasena = "";
var id_usuario_recuperar_contrasena = "";
var disabled_perfil = false;
var texto_invitar_amigo = "";
var db;
var map;
var bucle_to_pay = false;
var timer_down = null;
var push;
var tiempo_inactividad = -1;
var tiempo_splash_promocional = 5000;
var tiempo_splash = 2000;
var time_out = 60000;
var parar_conteo = false;
var CANCELADO_POR_USUARIO = false;

var LATITUDE_service = "";
var LONGITUDE_service = "";

loading(true);
if(!debug){
    //The event fires when the application is ready
    document.addEventListener("deviceready", inicializar_app, false);
    //The event fires when an application is put into the background.
    document.addEventListener("pause", onPause, false);
    //The event fires when an application is retrieved from the background.
    document.addEventListener("resume", onResumen, false);
}

document.addEventListener("backbutton", onBackKeyDown, false);
function onBackKeyDown() {
    var active_page = $.mobile.activePage.attr("id");
    if(active_page != "perfil" && active_page != "pedido_camino" && active_page != "inicio"){
        $.mobile.back();
    }
}





function onPause(){
    var active_page = $.mobile.activePage.attr("id");
    if( active_page == "pedido_camino" ){
        console.log("Paramos el contador");
        parar_conteo = true;
        timer_down.setDate( new Date( Date.now()-(864E5*2) ) );
    }
}

function onResumen(){
    var active_page = $.mobile.activePage.attr("id");
    if( active_page == "pedido_camino" ){        
        loading(true);
        parar_conteo = true;
        timer_down.setDate( new Date( Date.now()-(864E5*2) ) );        
        toFinCompra();
    }
}






function devideReadyNotification(){


    if( localStorage.getItem("user_id") != undefined ){

        //Registramos el usuario
        // window.FirebasePlugin.setUserId(localStorage.getItem("user_id"));

        // Enables analytics collection
        // window.FirebasePlugin.setAnalyticsCollectionEnabled(true);

        //Register device
        window.FirebasePlugin.getToken(function(token) {
            // save this server-side and use it to push notifications to this device
            console.log(token);

            //Actualizamos el uuid del dispositivo
            var data_s = {};
            data_s.user_id    =   localStorage.getItem("user_id");
            data_s.os_type    =   device.platform.toLowerCase();
            data_s.uuid       =   token;
            data_s.APP_VERSION=   APP_VERSION;
            $.ajax({
                url: WEBSERVICE_URL + "update_uuid",
                data: data_s,
                error: function(e){
                    //alert("TERMINO MAL")
                    console.log(e);
                },
                success: function(result){
                    //alert("TERMINO BIEN")
                    console.log(result);
                }
            });

        }, function(error) {
            console.error(error);
        });


        if(device.platform.toLowerCase() == "ios"){
            window.FirebasePlugin.hasPermission(function(data){
                if(data.isEnabled == false){
                    window.FirebasePlugin.grantPermission();
                }
            });
        }

    }


}



$(document).ready(function(){

    Conekta.setPublishableKey(conekta_public_key);

    timer_down = new uiTimeBomb({
        elements: document.getElementById("timer")
        ,localTime:false
        ,method:"hours"
        ,format:"{h}:{i}:{s}"
    });

    db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
    db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });

    if(debug){
        inicializar_app();
    }
    fillYearsField();

    $("#lada_crear_cuenta").intlTelInput({
        preferredCountries: [ "mx" ],
    });

    $("#lada_crear_cuenta").on("countrychange", function(e, countryData) {
        $("#value_lada_crear_cuenta").html("+"+countryData.dialCode);
    });

    $("#lada_crear_cuenta_correo").intlTelInput({
        preferredCountries: [ "mx" ],
    });

    $("#lada_crear_cuenta_correo").on("countrychange", function(e, countryData) {
        $("#value_lada_crear_cuenta_correo").html("+"+countryData.dialCode);
    });

    $("#lada_editar_movil").intlTelInput({
        preferredCountries: [ "mx" ],
    });

    $("#lada_editar_movil").on("countrychange", function(e, countryData) {
        $("#value_lada_editar_movil").html("+"+countryData.dialCode);
    });


    $(this).on("click", ".herramienta_icon", function(){

        show_menu_lateral();

    });

    //Cada vez que cambie anio y mes al momento de registrar el usuario, calculamos cuantos dias tiene para generar los option
    $(this).on("change", "#crear_cuenta_correo select[name='anio'], #crear_cuenta_correo select[name='mes']", function(){

        var anio = $("#crear_cuenta_correo select[name='anio']").val();
        var month = $("#crear_cuenta_correo select[name='mes']").val();

        if(anio != "" && month != ""){        
            var number_daysInMonth = daysInMonth( month , anio );
            var select_html = "";
            for(var i = 1; i <= number_daysInMonth; i++){
                select_html += "<option value='"+i+"'>"+i+"</option>";
            }
            $("#crear_cuenta_correo select[name='dia']").html("<option value=''>DÍA</option>");            
            $("#crear_cuenta_correo select[name='dia']").append(select_html);
        }else{
            $("#crear_cuenta_correo select[name='dia']").html("<option value=''>DÍA</option>");
        }

    });

    //Cada vez que cambie anio y mes al momento de registrar el usuario con red social, calculamos cuantos dias tiene para generar los option
    $(this).on("change", "#crear_cuenta_red_social select[name='anio'], #crear_cuenta_red_social select[name='mes']", function(){

        var anio = $("#crear_cuenta_red_social select[name='anio']").val();
        var month = $("#crear_cuenta_red_social select[name='mes']").val();

        if(anio != "" && month != ""){        
            var number_daysInMonth = daysInMonth( month , anio );
            var select_html = "";
            for(var i = 1; i <= number_daysInMonth; i++){
                select_html += "<option value='"+i+"'>"+i+"</option>";
            }
            $("#crear_cuenta_red_social select[name='dia']").html("<option value=''>DÍA</option>");            
            $("#crear_cuenta_red_social select[name='dia']").append(select_html);
        }else{
            $("#crear_cuenta_red_social select[name='dia']").html("<option value=''>DÍA</option>");
        }

    });


    //Login con facebook
    $(this).on("click", "#inicio .facebook", function(e){
        e.preventDefault();
    
        CordovaFacebook.login({
           permissions: ['email', 'public_profile'],
           onSuccess: function(result) {

              if(result.declined.length > 0) {
                    //alert("The User declined something!");
                    showModalAlert("Es necesario aceptar todos los permisos que requiere facebook");
              }else{
                
                if(result.error == 1){
                    showModalAlert("Facebook experimenta fallas técnicas, intente otro modo de login/registro.")
                }else{

                         //Todo bien
                        loading(true);
                        console.log(result);
                        $.ajax({
                            url: "https://graph.facebook.com/v2.7/"+result.userID+"?fields=email,picture.type(large),name&access_token="+result.accessToken+"",
                            type: "GET",
                            dataType: "JSON",
                            timeout   : time_out,
                            error: function(e){
                                console.log(e);
                                loading(false);
                                foto_red_social = 0;                          
                                $.mobile.changePage( "#crear_cuenta_red_social", { transition: "slide" });                                  
                            },
                            success: function(result_social){
                                

                                console.log(result_social);

                                //userID
                                /*if(result_social.email == undefined){
                                    result_social.email = result_social.id+"@facebook.com";
                                }*/

 
                                console.log(result_social);
                                //Primero verificamos si ya existe el email de la red social registrado, si ya existe significa que ya se habia registrado anteriormente                                
                                var data = {};
                                data.email = result_social.email;
                                data.login_type = "facebook";
                                data.social_id = result_social.id;
                                data.APP_VERSION = APP_VERSION;
                                $.ajax({
                                    url: webservice + "check_user_exists_social_network",
                                    data: data,
                                    dataType: "JSON",
                                    type: "POST",
                                    timeout   : time_out,
                                    error: function(e){
                                        console.log(e);
                                    },
                                    success: function(result){

                                        console.log(result);
                                        loading(false);
                                        if(result.exists){

                                            if(result.usuario_cancelado){
                                                showModal("cuenta_baneada");
                                            }else{
                                                localStorage.setItem("user_id", result.user_id);
                                                localStorage.setItem("name",    result.name);
                                                localStorage.setItem("foto_perfil", result.profile_photo);
                                                refreshUUID();
                                                setInfoUserLoged();
                                                toPerfil();
                                            }

                                        }else{

                                            $("#elige_foto_perfil .imagen_perfil_ruta").val(result_social.picture.data.url);
                                            $("#elige_foto_perfil .image_perfil_icon").css("background", "url("+result_social.picture.data.url+") center" ); 
                                            $("#crear_cuenta_red_social input[name='nombre_y_apelllidos']").val(result_social.name); 
                                            $("#crear_cuenta_red_social input[name='social_id']").val(result_social.id); 

                                            if(result_social.email != undefined){
                                                $("#crear_cuenta_red_social input[name='correo_electronico']").val(result_social.email); 
                                            }

                                            foto_red_social = 1; 
                                            tipo_red_social = "facebook";
                                            //email_red_social = result_social.email;                                                       
                                            $.mobile.changePage( "#crear_cuenta_red_social", { transition: "slide" }); 

                                        }

                                    }
                                });

                           
                             
                            }   
                        }); 
                    
                }
              }

           },
           onFailure: function(result) {
              if(result.cancelled) {

              } else if(result.error) {
                 console.log("There was an error:" + result.errorLocalized);
                 showModalAlert("Facebook experimenta fallas técnicas, intente otro modo de login/registro.")
              }
           }
        });

    });

    //Login con twitter
    $(this).on("click", "#inicio .twitter", function(e){
        e.preventDefault();


        TwitterConnect.login(
          function(result) {
            

                console.log(result);
                TwitterConnect.showUser(
                  function(result_twitter) {


                        console.log(result_twitter);
                        //Primero verificamos si ya existe el email de la red social registrado, si ya existe significa que ya se habia registrado anteriormente                                
                        var data = {};
                        data.email = result_twitter.id+"@twitter.com";
                        data.login_type = "twitter";
                        data.social_id = result_twitter.id;
                        data.APP_VERSION = APP_VERSION;
                        $.ajax({
                            url: webservice + "check_user_exists_social_network",
                            data: data,
                            dataType: "JSON",
                            timeout   : time_out,
                            type: "POST",
                            error: function(e){
                                console.log(e);
                            },
                            success: function(result){

                                console.log(result);
                                loading(false);
                                if(result.exists){

                                    if(result.usuario_cancelado){
                                        showModal("cuenta_baneada")
                                    }else{
                                        localStorage.setItem("user_id", result.user_id);
                                        localStorage.setItem("name",    result.name);
                                        localStorage.setItem("foto_perfil", result.profile_photo);
                                        refreshUUID();
                                        setInfoUserLoged();
                                        toPerfil();
                                    }

                                }else{

                                    $("#elige_foto_perfil .imagen_perfil_ruta").val(  result_twitter.profile_image_url.replace("_normal", "")  );
                                    $("#elige_foto_perfil .image_perfil_icon").css("background", "url("+result_twitter.profile_image_url.replace("_normal", "")+") center" );
                                    $("#crear_cuenta_red_social input[name='nombre_y_apelllidos']").val(result_twitter.name); 
                                    $("#crear_cuenta_red_social input[name='social_id']").val(result_twitter.id); 

                                    if(result_twitter.email != undefined){
                                        $("#crear_cuenta_red_social input[name='correo_electronico']").val(result_twitter.email); 
                                    }

                                    foto_red_social = 1; 
                                    tipo_red_social = "twitter";
                                    //email_red_social = result_twitter.id+"@twitter.com";                                                 
                                    $.mobile.changePage( "#crear_cuenta_red_social", { transition: "slide" }); 

                                }

                            }
                        });                    


                  }, function(error) {

                    console.log(error);
                    showModalAlert("Twitter experimenta fallas técnicas, intente otro modo de login/registro.")

                  }
                );


          }, function(error) {
            //console.log('Error logging in');
            //console.log(error);
          }
        );

    });


    $( '#notificaciones #apertura, #notificaciones #promociones' ).bind( "change", function(event, ui) {
 
        loading(true);
        var apertura = $('#notificaciones #apertura').val();
        var promociones = $('#notificaciones #promociones').val();        
        var data = {};
        data.apertura = apertura;
        data.promociones = promociones;
        data.user_id = localStorage.getItem("user_id");
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "set_notification_settings",
            type: "POST",
            dataType: "JSON",
            timeout   : time_out,
            data: data,
            error: function(e){
                console.log(e);
            },
            success: function(result){

                loading(false);
                console.log(result);
                if(!result.error){                                

                }else{
                    showModalAlert(result.message);
                }                

            }
        });

    });

    
    $(this ).on("pagebeforechange ", function( event ) { 
        show_menu_lateral(false);
        show_menu_lateral_resumen(false);
    });

 
    $(this).on("change", "#agregar_tarjeta select[name='estado']", function(e){

        loading(true);
        var data = {};
        data.state_id = $(this).val();
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "get_cities",
            type: "POST",
            dataType: "JSON",
            data: data,
            timeout   : time_out,
            error : function(e) {
                
                loading(false);
                console.log(e);
                if (e.status == 422){
                    showModalAlertAjax( JSON.parse(e.responseText) );
                }else{
                    showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
                }

            },
            success: function(result){

                loading(false);
                console.log(result);
                $("#agregar_tarjeta select[name='municipio']").html(result);
     
            }

        });        

    });

    
    $(this).on("click", ".open_close_tab", function(e){
        e.preventDefault();

        var dis = $(this).parent().parent().parent().find(".contents");
        $(".compras_realizadas_row .icon_plus img").attr("src", "img/plus_icon.png");

        if(dis.css("display") == "none"){
            $(".compras_realizadas_row .contents").css("display", "none");
            $(this).attr("src", "img/less_icon_2.png");
            dis.css("display", "inline-block");
        }else{
            dis.css("display", "none");
        }

    });
    
 
    $(this ).on("pagechange ", function( event ) {  

        var active_page = $.mobile.activePage.attr("id");
        if( active_page == "productos" || active_page == "complementos" || active_page == "mezcladores" || active_page == "productos_promociones" ){
            refreshQuantityProducts();
        }  

    });


    $(this ).on("pagechange ", function( event ) {  

        var active_page = $.mobile.activePage.attr("id");
        if( active_page != "inicio" && active_page != "crear_cuenta_red_social" && active_page != "crear_cuenta_correo" && active_page != "elige_foto_perfil" && active_page != "recuperar_contrasena" && active_page != "recuperar_contrasena_opciones" && active_page != "pedido_camino"){
            incrementTiempoInactividad(true);
        }else{
            tiempo_inactividad = -1;
            incrementTiempoInactividad(false);
        }

    });

    $("input").keypress(function(){

        var active_page = $.mobile.activePage.attr("id");
        if( active_page != "inicio" && active_page != "crear_cuenta_red_social" && active_page != "crear_cuenta_correo" && active_page != "elige_foto_perfil" && active_page != "recuperar_contrasena" && active_page != "recuperar_contrasena_opciones" && active_page != "pedido_camino"){
            incrementTiempoInactividad(true);
        }else{
            tiempo_inactividad = -1;
            incrementTiempoInactividad(false);
        }

    });
 

    setInterval(function(){ incrementTiempoInactividad(false); }, (1000 * 60) );
    setTimeout(function(){ checkUserBanned(); }, (1000 * 5) );                                                           


});




function checkUserBanned(){

    console.log("REVISA BANNEO");
    var active_page = $.mobile.activePage.attr("id");
    if(active_page != "pedido_camino"){

        if(localStorage.getItem("user_id") != undefined){
            var data = {};
            data.user_id = localStorage.getItem("user_id");
            data.APP_VERSION = APP_VERSION;
            $.ajax({
                url: webservice + "is_user_banned",
                type: "POST",
                dataType: "JSON",
                timeout   : time_out,
                data: data,
                error: function(e){
                    console.log(e);
                    setTimeout(function(){ checkUserBanned(); }, (1000 * 5) );
                },
                success: function(result_baneo){
                    console.log(result_baneo);

                    //CUENTA BANEADA
                    if(result_baneo.banned){

                            //REGRESAMOS LOS PRODUCTOS AGREGADOS
                            db.transaction(function (tx) {
                                tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

                                    var productos = new Array();
                                    for(var i = 0; i < rs.rows.length; i++){
                                        var row_pro = {};
                                        row_pro.tipo = rs.rows.item(i).tipo;
                                        row_pro.product_id = rs.rows.item(i).product_id;
                                        row_pro.cantidad = rs.rows.item(i).cantidad;
                                        productos.push(row_pro);
                                    }

                                    //Regresamos todos los pedidos, ya que se cancelo
                                    var data = {};
                                    data.productos = productos;
                                    data.APP_VERSION = APP_VERSION;
                                    $.ajax({
                                        url: webservice + "return_products",
                                        data:data,
                                        type:"POST",
                                        timeout   : time_out,
                                        dataType: "JSON",
                                        error: function(e){
                                            console.log(e);
                                            setTimeout(function(){ checkUserBanned(); }, (1000 * 5) );
                                        },
                                        success: function(result_return_products){

                                            //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                                            console.log(result_return_products);
                                            if(!result_return_products.error){

                                                db.transaction(function (tx) {
                                                    tx.executeSql("DROP TABLE productos", [], function(tx, rs){
                                                        
                                                        db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                                        db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });
                                                
                                                            loading(false);
                                                            localStorage.removeItem("category_id");
                                                            localStorage.removeItem("code");
                                                            localStorage.removeItem("end_delivery_time");
                                                            localStorage.removeItem("LAT_coordinate");
                                                            localStorage.removeItem("LON_coordinate");
                                                            localStorage.removeItem("shipping");
                                                            localStorage.removeItem("type");  
                                                            localStorage.removeItem("foto_perfil");
                                                            localStorage.removeItem("name");
                                                            localStorage.removeItem("user_id");
                                                            LATITUDE_service = "";
                                                            LONGITUDE_service = "";

                                                            closeModal("pedido_cancelado_exito");   //Por asi acaso cerramos estos son modales    
                                                            closeModal("pedido_pagado_exito");      //Por asi acaso cerramos estos son modales
                                                            showModalAlert(result_baneo.message);
                                                            $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });
                                                            tiempo_inactividad = -1;

                                                    }, function(error){
                                                        console.log(error.message);
                                                    });
                                                }); 


                                            }else{
                                                loading(false);                                        
                                                showModalAlert(result.message);
                                            }

                                        }
                                    });
                            

                                }, function(error){
                                    console.log(error.message);
                                });
                            }); 

                    }
                    setTimeout(function(){ checkUserBanned(); }, (1000 * 5) );                                                           
                }
            });
        }else{
            setTimeout(function(){ checkUserBanned(); }, (1000 * 5) );            
        }

    }else{
        setTimeout(function(){ checkUserBanned(); }, (1000 * 5) );
    }

}


function incrementTiempoInactividad(reset){


    console.log(tiempo_inactividad);
    if(reset){

        tiempo_inactividad = 0; 
 
    }else{

        if(tiempo_inactividad < 0){
            //se para el tiempo de inactividad
        }else if(tiempo_inactividad >= 5){
            //Cerramos la session
            closeSession();
            tiempo_inactividad = -1;

        }else{
            tiempo_inactividad = parseInt(tiempo_inactividad) + 1;            
        }

    }


}


function closeSession(){

    //REGRESAMOS LOS PRODUCTOS AGREGADOS
    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

            var productos = new Array();
            for(var i = 0; i < rs.rows.length; i++){
                var row_pro = {};
                row_pro.tipo = rs.rows.item(i).tipo;
                row_pro.product_id = rs.rows.item(i).product_id;
                row_pro.cantidad = rs.rows.item(i).cantidad;
                productos.push(row_pro);
            }

            //Regresamos todos los pedidos, ya que se cancelo
            var data = {};
            data.productos = productos;
            data.APP_VERSION = APP_VERSION;
            $.ajax({
                url: webservice + "return_products",
                data:data,
                type:"POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result_return_products){

                    //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                    console.log(result_return_products);
                    if(!result_return_products.error){


                        localStorage.removeItem("category_id");
                        localStorage.removeItem("foto_perfil");
                        localStorage.removeItem("LAT_coordinate");
                        localStorage.removeItem("LON_coordinate");
                        localStorage.removeItem("name");
                        localStorage.removeItem("shipping");
                        localStorage.removeItem("type");
                        localStorage.removeItem("user_id");
                        localStorage.removeItem("code");
                        LATITUDE_service = "";
                        LONGITUDE_service = "";

                        db.transaction(function (tx) {
                            tx.executeSql("DROP TABLE productos", [], function(tx, rs){

                                db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });

                            }, function(error){
                                console.log(error.message);
                            });
                        }); 

                        $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });
                        tiempo_inactividad = -1;


                    }else{
                        loading(false);                                        
                        showModalAlert(result.message);
                    }

                }
            });
    

        }, function(error){
            console.log(error.message);
        });
    }); 
 
}



function refreshUUID(){

    document.addEventListener("deviceready", devideReadyNotification, false);

}

function inicializarDB(d){
    var create_table = "";

    create_table += "CREATE TABLE IF NOT EXISTS `productos` (";
    create_table += "  `descripcion` text DEFAULT NULL,";
    create_table += "  `tipo` text DEFAULT NULL,";
    create_table += "  `product_id` int(11) DEFAULT NULL,";
    create_table += "  `cantidad` int(11) DEFAULT 0,";
    create_table += "  `precio` double DEFAULT 0,";
    create_table += "  `is_drink` int(11) DEFAULT 0";
    create_table += ")";
    d.executeSql(create_table);

}


var finish_animation_menu_lateral = true;
function show_menu_lateral(show){

    if(finish_animation_menu_lateral){
        finish_animation_menu_lateral = false;
        if(show == undefined){
            console.log("ENTR 1");
            var right = $(".menu_settings").css("right");
            if(right == "-200px"){
                $(".menu_settings").animate({
                    "right": 0
                }, 300, function(){ finish_animation_menu_lateral = true; });
            }else{
                $(".menu_settings").animate({
                    "right": "-200px"
                }, 300, function(){ finish_animation_menu_lateral = true; });  
            }
        }else{

            if(show == true){
                $(".menu_settings").animate({
                    "right": 0
                }, 300, function(){ finish_animation_menu_lateral = true; });
            }else{
                $(".menu_settings").animate({
                    "right": "-200px"
                }, 300, function(){ finish_animation_menu_lateral = true; });             
            }

        }
    }

}

function show_menu_lateral_resumen(show){

    if(show == undefined){

        var right = $(".menu_settings_resumen").css("left");
        if(right == "-200px"){
            $(".menu_settings_resumen").animate({
                "left": 0
            }, 300);
        }else{
            $(".menu_settings_resumen").animate({
                "left": "-200px"
            }, 300);  
        }
    }else{

        if(show == true){
            $(".menu_settings_resumen").animate({
                "left": 0
            }, 300);
        }else{
            $(".menu_settings_resumen").animate({
                "left": "-200px"
            }, 300);             
        }

    }

}

function aceptar_foto_editar(){

    loading(true);
    var ft = new FileTransfer();
    ft.upload(
        $("#editar_perfil .imagen_perfil_ruta").val(),
        encodeURI(webservice + "edit_upload_photo"),
        function(success){

            loading(false);
            var json_response = success.response;
            json_response = JSON.parse( json_response );

            console.log(json_response);
            if(!json_response.error){
                $("#editar_perfil .imagen_perfil_ruta").val("");
                localStorage.setItem("foto_perfil", json_response.profile_photo);
                setInfoUserLoged();
                showModalAlert(json_response.message);
            }else{
                showModalAlert(json_response.message);
            }

        },
        function(error){
            loading(false);
            showModalAlert("Error al intentar editar la foto de perfil, intentelo nuevamente.");
        },
        {
            fileKey : "file",
            mimeType : "image/png",
            chunkedMode : false,  
            fileName : localStorage.getItem("user_id"),      
        }
    );

}

function iniciar_sesion_email(){

    loading(true);
    var data = {};
    data.nombre_de_usuario  = $("#inicio input[name='email_login']").val();
    data.contrasena         = $("#inicio input[name='password']").val();
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url : webservice + "login_email",        
        type : 'POST',
        timeout   : time_out,
        data : data,
        dataType : 'JSON',
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success  : function(response) {

            loading(false);
            console.log(response);
            if(!response.error){

                if(response.usuario_cancelado){
                    showModal("cuenta_baneada")
                }else{
                    //$("#inicio input[name='email_login']").val("");
                    $("#inicio input[name='password']").val("");
                    localStorage.setItem("user_id", response.user_id );
                    localStorage.setItem("name",    response.name );
                    localStorage.setItem("foto_perfil",  response.profile_photo );   
                    refreshUUID();         
                    setInfoUserLoged();                
                    toPerfil(); 
                }               

            }else{
                showModalAlert(response.message);
            }

        }
    });    

}

function inicializar_app(){

    if(!debug){
        if( device.platform.toLowerCase() == "ios" ){
            $(".extension_lada .value").css("margin-top", 0);
            $(".extension_lada .value").css("position", "absolute");
            $(".extension_lada .value").css("margin-left", -90);
        }
    }

    if(!debug){
        navigator.splashscreen.show();
    }


    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url : webservice + "get_splashscreen",        
        type : 'POST',
        data : data,
        timeout   : time_out,
        dataType : 'JSON',     
        error : function(e) {   

            console.log("ERROR SPLASHSCREEN");     
            loading(false);

            if(!debug){
                //Primero escondemos el splash screen y despues hacemos el conteo para esconder el splash promocional
                window.setTimeout(function () {
                    navigator.splashscreen.hide();
                    check_login_status();
                }, tiempo_splash);
            }

            
        },
        success  : function(response) {

            console.log(response);
            loading(false);
            if(response != ""){
                $("#aux_splashscreen").css("display", "block");
                $("#aux_splashscreen").css("background", "url("+response+")");

                    //Primero escondemos el splash screen y despues hacemos el conteo para esconder el splash promocional
                    window.setTimeout(function () {
                            if(!debug){
                                navigator.splashscreen.hide();
                            }

                            window.setTimeout(function () {
                                check_login_status();
                                $("#aux_splashscreen").css("display", "none");
                            }, tiempo_splash_promocional);

                    }, tiempo_splash);

            }else{

                if(!debug){
                    //Primero escondemos el splash screen y despues hacemos el conteo para esconder el splash promocional
                    window.setTimeout(function () {
                        navigator.splashscreen.hide();
                        check_login_status();
                    }, tiempo_splash);
                }else{
                    check_login_status();                
                }

            }
 



            
        }
    });
 


}



function check_login_status(){

    loading(true);
    if( localStorage.getItem("user_id") != "" && localStorage.getItem("user_id") != undefined && localStorage.getItem("user_id") != "undefined" && localStorage.getItem("user_id") != "null" && localStorage.getItem("user_id") != null ){
        
        loading(false);
        if( localStorage.getItem("foto_perfil") != "" && localStorage.getItem("foto_perfil") != undefined && localStorage.getItem("foto_perfil") != "undefined" && localStorage.getItem("foto_perfil") != "null" && localStorage.getItem("foto_perfil") != null ){
            
            if( localStorage.getItem("code") != "" && localStorage.getItem("code") != undefined && localStorage.getItem("code") != "undefined" && localStorage.getItem("code") != "null" && localStorage.getItem("code") != null ){
                refreshUUID();
                setInfoUserLoged();
                toFinCompra();
            }else{
                refreshUUID();
                setInfoUserLoged();
                toPerfil();
            }

        }else{
            refreshUUID();
            $.mobile.changePage( "#elige_foto_perfil" );            
        }

    }else{
        loading(false);
        $.mobile.changePage( "#inicio"); 
    }  

}




function create_account(){

    var data = {};
    data.nombre_y_apelllidos    = $("#crear_cuenta_correo input[name='nombre_y_apelllidos']").val();
    data.nombre_de_usuario      = $("#crear_cuenta_correo input[name='nombre_de_usuario']").val();
    data.correo_electronico     = $("#crear_cuenta_correo input[name='correo_electronico']").val();
    data.contrasena             = $("#crear_cuenta_correo input[name='contrasena']").val();
    data.codigo_pais            = $("#value_lada_crear_cuenta_correo").text();
    data.movil                  = $("#crear_cuenta_correo input[name='movil']").val();
    data.mes                    = $("#crear_cuenta_correo select[name='mes']").val();
    data.dia                    = $("#crear_cuenta_correo select[name='dia']").val();
    data.anio                   = $("#crear_cuenta_correo select[name='anio']").val();
    data.APP_VERSION = APP_VERSION;

    $.ajax({
        url : webservice + "create_account",        
        type : 'POST',
        timeout   : time_out,
        data : data,
        dataType : 'JSON',
        beforeSend: function(){ loading(true);  },
        complete : function() { loading(false); },      
        error : function(e) {
            
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success  : function(response) {

            console.log(response);
            if(!response.error){

                localStorage.setItem("user_id", response.user_id);
                localStorage.setItem("name",    response.name);
                localStorage.setItem("foto_perfil",    "");
                $.mobile.changePage( "#elige_foto_perfil", { transition: "slide" });                

            }else{
                showModalAlert(response.message);
            }

        }
    });
 
}

function subir_fotografia(){

    loading(true);
    var ruta_imagen = $("#elige_foto_perfil .imagen_perfil_ruta").val();

    if(foto_red_social == 0){
        var ft = new FileTransfer();
        ft.upload(
            ruta_imagen,
            encodeURI(webservice + "upload_photo"),
            function(success){

                loading(false);
                var json_response = success.response;
                json_response = JSON.parse( json_response );

                if(!json_response.error){
                    localStorage.setItem("foto_perfil", json_response.profile_photo);
                    refreshUUID();
                    setInfoUserLoged();
                    toPerfil();
                }else{
                    showModalAlert(json_response.message);
                }

            },
            function(error){
                loading(false);
                showModalAlert("Error al intentar subir la foto de perfil, intentelo nuevamente.");
            },
            {
                fileKey : "file",
                mimeType : "image/png",
                chunkedMode : false,  
                fileName : localStorage.getItem("user_id"),      
            }
        );
    }else{

        //Actualizamos la imagen en servidor
        var data = {};
        data.image_url = ruta_imagen;
        data.user_id = localStorage.getItem("user_id");
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "upload_url",
            data: data,
            type: "POST",
            timeout   : time_out,
            dataType: "JSON",
            error: function(e){
                loading(false);
                console.log(e);
            },
            success: function(result){

                console.log(result);
                loading(false);
                if(!result.error){
                    localStorage.setItem("foto_perfil", result.profile_photo);
                    refreshUUID();
                    setInfoUserLoged();
                    toPerfil();
                }else{
                    showModalAlert(result.message);
                }

            }
        }); 

    }
}

function tomarFoto(){

    closeModal('opcion_elegir_foto_perfil');
    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        foto_red_social = 0;
        $("#elige_foto_perfil .imagen_perfil_ruta").val(imageUri);
        $("#elige_foto_perfil .image_perfil_icon").css("background", "url("+imageUri+") center" );

    }, function cameraError(error) {
        //showModalAlert("No se pudo tomar la fotografía.");
    }, {
        destinationType: Camera.DestinationType.FILE_URI,
        mediaType: Camera.MediaType.PICTURE,        
        targetHeight : 300,
        targetWidth : 300,
        quality : 80,
        allowEdit : false,
        encodingType : 1, //PNG
        saveToPhotoAlbum : true,
        correctOrientation: true,
    });

}

function carrete(){

    closeModal('opcion_elegir_foto_perfil');
    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        foto_red_social = 0;
        $("#elige_foto_perfil .imagen_perfil_ruta").val(imageUri);
        $("#elige_foto_perfil .image_perfil_icon").css("background", "url("+imageUri+") center" );

    }, function cameraError(error) {
        //showModalAlert("No se pudo tomar la fotografía.");
    }, {
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: Camera.DestinationType.FILE_URI,
        mediaType: Camera.MediaType.PICTURE,
        targetHeight : 300,
        targetWidth : 300,
        quality : 80,
        allowEdit : false,
        encodingType : 1, //PNG
        saveToPhotoAlbum : true,
        correctOrientation: true,
    });

}

function tomarFoto_editar(){

    closeModal('editar_foto_perfil');
    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        $("#editar_perfil .imagen_perfil_ruta").val(imageUri);
        $("#editar_perfil .image_perfil_icon").css("background", "url("+imageUri+") center" );

    }, function cameraError(error) {
        //showModalAlert("No se pudo tomar la fotografía.");
    }, {
        destinationType: Camera.DestinationType.FILE_URI,
        mediaType: Camera.MediaType.PICTURE,        
        targetHeight : 300,
        targetWidth : 300,
        quality : 80,
        allowEdit : false,
        encodingType : 1, //PNG
        saveToPhotoAlbum : true,
        correctOrientation: true,
    });

}

function carrete_editar(){

    closeModal('editar_foto_perfil');
    navigator.camera.getPicture(function cameraSuccess(imageUri) {

        $("#editar_perfil .imagen_perfil_ruta").val(imageUri);
        $("#editar_perfil .image_perfil_icon").css("background", "url("+imageUri+") center" );

    }, function cameraError(error) {
        //showModalAlert("No se pudo tomar la fotografía.");
    }, {
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        destinationType: Camera.DestinationType.FILE_URI,
        mediaType: Camera.MediaType.PICTURE,
        targetHeight : 300,
        targetWidth : 300,
        quality : 80,
        allowEdit : false,
        encodingType : 1, //PNG
        saveToPhotoAlbum : true,
        correctOrientation: true,
    });

}

function create_account_red_social(){

    var data = {};
    data.nombre_y_apelllidos    = $("#crear_cuenta_red_social input[name='nombre_y_apelllidos']").val();
    data.nombre_de_usuario      = $("#crear_cuenta_red_social input[name='nombre_de_usuario']").val();
    data.contrasena             = $("#crear_cuenta_red_social input[name='contrasena']").val();
    data.codigo_pais            = $("#value_lada_crear_cuenta").text();    
    data.movil                  = $("#crear_cuenta_red_social input[name='movil']").val();
    data.mes                    = $("#crear_cuenta_red_social select[name='mes']").val();
    data.dia                    = $("#crear_cuenta_red_social select[name='dia']").val();
    data.anio                   = $("#crear_cuenta_red_social select[name='anio']").val();
    data.correo_electronico     = $("#crear_cuenta_red_social input[name='correo_electronico']").val();
    data.social_id              = $("#crear_cuenta_red_social input[name='social_id']").val();                                 
    data.tipo_red_social        = tipo_red_social;
    data.APP_VERSION = APP_VERSION;

    loading(true);
    $.ajax({
        url : webservice + "create_account_social_network",        
        type : 'POST',
        timeout   : time_out,
        data : data,
        dataType : 'JSON',
        complete : function() { loading(false); },      
        error : function(e) {
            
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success  : function(response) {

            console.log(response);
            if(!response.error){

                localStorage.setItem("user_id", response.user_id);
                localStorage.setItem("name",    response.name);
                localStorage.setItem("foto_perfil",    "");
                $.mobile.changePage( "#elige_foto_perfil", { transition: "slide" });                

            }else{
                showModalAlert(response.message);
            }

        }
    });
 
}

function cancelar_recuperacion_contrasena(){

    $("#recuperar_contrasena input[name='nombre_usuario']").val("");
    $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });

}

function check_username_exists_recuperar_contrasena(){

    loading(true);
    var nombre_de_usuario = $("#recuperar_contrasena input[name='nombre_usuario']").val();
    var data = {};
    data.nombre_de_usuario = nombre_de_usuario;
    data.APP_VERSION = APP_VERSION;

    $.ajax({
        url: webservice + "username_exists_recover_password",
        type: "POST",
        timeout   : time_out,
        data: data,
        dataType: "JSON",
        error: function(e){

            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            if(!result.error){

                //Asignamos los valores a las ventanas modales
                $("#enviar_sms_nueva_contrasena .title_type_4").text(result.cellphone);
                $("#se_envio_sms_contrasena .title_type_4").text(result.cellphone);

                $("#enviar_email_nueva_contrasena .title_type_4").text(result.email);
                $("#se_envio_email_contrasena .title_type_4").text(result.email);
                
                id_usuario_recuperar_contrasena = result.id;
                $("#recuperar_contrasena input[name='nombre_usuario']").val("");
                $.mobile.changePage( "#recuperar_contrasena_opciones", { transition: "slide" });                

            }else{
                showModalAlert(result.message);
            }

        }
    });

}

function toPerfil(reverse){


    loading(true);
    navigator.geolocation.getCurrentPosition(
        function(position) {

            console.log( position.coords.latitude + " - " + position.coords.longitude );

            var data = {};
            data.lat = position.coords.latitude;
            data.lon = position.coords.longitude;
            data.APP_VERSION = APP_VERSION;
            data.user_id = localStorage.getItem("user_id");
            $.ajax({
                url: webservice + "is_user_location",
                data: data,
                type: "POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result){

                    loading(false);
                    if(result){

                            /*
                            if(localStorage.getItem("already_show_remember_forma_pago") != "true" && localStorage.getItem("already_show_remember_forma_pago") != true){
                                showModal("recuerda_registrar_forma_pago");
                                localStorage.setItem("already_show_remember_forma_pago", true);
                            }
                            */

                            /*
                            loading(true);
                            //Revisamos si tiene una tarjeta de credito acregada
                            var data = {};
                            data.user_id = localStorage.getItem("user_id");
                            data.APP_VERSION = APP_VERSION;
                            $.ajax({
                                url: webservice + "is_user_have_credit_card",
                                data: data,
                                type: "POST",
                                timeout   : time_out,
                                dataType: "JSON",
                                error: function(e){
                                    console.log(e);
                                },
                                success: function(result){

                                    loading(false);
                                    //console.log(result);
                                    if(result){
                                        disabled_perfil = false;
                                    }else{
                                        disabled_perfil = true;
                                    }   

                                    console.log("Disabled perfil: "+disabled_perfil);
                                */
                                    if(reverse){
                                        $.mobile.changePage( "#perfil", { transition: "slide", reverse: true });
                                    }else{
                                        $.mobile.changePage( "#perfil", { transition: "slide" });
                                    }
                                /*
                                }
                            });*/

                    }else{

                        showModalAlert("No te encuentras en una area disponible de venta, intentalo en un lugar diferente");
                        $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });

                    }

                }
            });


        }, 
        function(error) {
            loading(false);
            showModal("activar_opcion_ubicacion");
        },{ maximumAge: 3000, timeout: 6000, enableHighAccuracy: true }
    );

 
}




function openSettings(){

    if(typeof cordova.plugins.settings.openSetting != undefined){
        cordova.plugins.settings.openSetting("settings", function(){
                console.log("opened nfc settings")
            },
            function(){
                console.log("failed to open nfc settings")
            });
    }

}



function toNotificaciones(){

    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_client_info",
        data: data,
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        error: function(e){
            console.log(e);
        },
        success: function(result){

            console.log(result);
            loading(false);
                  
            $('#notificaciones #apertura').slider();
            $('#notificaciones #promociones').slider();                    
            $('#notificaciones #apertura').val(result.notification_aperture).slider("refresh");
            $('#notificaciones #promociones').val(result.notification_promotion).slider("refresh");

            show_menu_lateral(false);
            $.mobile.changePage( "#notificaciones", { transition: "slide" }); 

        }
    });
   
}

function cancelar_contacto_ayuda(){

    $("#contacto_y_ayuda textarea[name='mensaje']").val("");
    $.mobile.back();

}

function enviar_contacto_ayuda(){

    loading(true);
    var data = {};
    data.mensaje = $("#contacto_y_ayuda textarea[name='mensaje']").val();
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "send_contacto_ayuda",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            loading(false);
            console.log(result);
            if(!result.error){
                $("#contacto_y_ayuda textarea[name='mensaje']").val("");
                showModal("se_envio_sms_contrasena");
            }else{
                showModalAlert(result.message);
            }

        }
    });

}

function toEliminarCuenta(){

    closeModal('eliminar_cuenta_modal'); 
    loading(true);
    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_reasons",
        type: "POST",
        data: data,
        timeout   : time_out,
        dataType: "JSON",
        error: function(e){
            console.log(e);
        },
        success: function(result){

            $("#eliminar_cuenta select[name='motivo']").html(result);
            loading(false);
            show_menu_lateral(false); 
            $.mobile.changePage( '#eliminar_cuenta', { transition: 'slide' });

        }
    });

}

function delete_account(){

    loading(true);
    var contrasena = $("#eliminar_cuenta input[name='contrasena']").val();
    var motivo = $("#eliminar_cuenta select[name='motivo']").val();
    
    var data = {};
    data.contrasena = contrasena;
    data.motivo = motivo;
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "delete_account",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            console.log(result);
            if(!result.error){


 
                    db.transaction(function (tx) {
                        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

                            var productos = new Array();
                            for(var i = 0; i < rs.rows.length; i++){
                                var row_pro = {};
                                row_pro.tipo = rs.rows.item(i).tipo;
                                row_pro.product_id = rs.rows.item(i).product_id;
                                row_pro.cantidad = rs.rows.item(i).cantidad;
                                productos.push(row_pro);
                            }

                            //Regresamos todos los pedidos, ya que se cancelo
                            var data = {};
                            data.productos = productos;
                            data.APP_VERSION = APP_VERSION;
                            $.ajax({
                                url: webservice + "return_products",
                                data:data,
                                type:"POST",
                                timeout   : time_out,
                                dataType: "JSON",
                                error: function(e){
                                    console.log(e);
                                },
                                success: function(result){

                                    //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                                    console.log(result);
                                    if(!result.error){

                                        db.transaction(function (tx) {
                                            tx.executeSql("DROP TABLE productos", [], function(tx, rs){
                                                
                                                db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                                db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });
                                        
                                                    loading(false);
                                                    showModal("cuenta_eliminada");

                                            }, function(error){
                                                console.log(error.message);
                                            });
                                        }); 


                                    }else{
                                        loading(false);                                        
                                        showModalAlert(result.message);
                                    }

                                }
                            });
                    

                        }, function(error){
                            console.log(error.message);
                        });
                    }); 

 


            }else{
                loading(false);
                showModalAlert(result.message);
            }

        }

    });

}


function cancelarEliminarCuenta(){

    $("#eliminar_cuenta input[name='contrasena']").val("");
    $.mobile.back();

}

 


function cargarTarjetasAgregadas(reverse){

    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_credit_cards",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
             
            if(result.length > 0){
                var html = "";
                for(var i = 0; i < result.length; i++){
                    html += '<div class="form_group input_icon_right c_w82">';
                    html += '    <div class="color_orange FjallaOne value" >'+result[i].type+' '+result[i].number+'</div>';
                    html += '    <div class="image_credit_card_catalog"><img src="img/trash_card.png" onclick="eliminarTarjeta('+result[i].id+', \''+result[i].type_complete+'\', \''+result[i].number+'\');"   /></div>';
                    html += '</div> ';
                    html += '<br><br>';
                } 
                $("#tarjetas #tarjetas_agregadas").html(html);
            }else{
                $("#tarjetas #tarjetas_agregadas").html('<p class="text-center color_purple title_type_1 fs20px nombre_usuario_texto">No cuentas con tarjetas</p>');
            }
            if(reverse == undefined){
                $.mobile.changePage( '#tarjetas', { transition: 'slide' }); 
            }else if(reverse){
                $.mobile.changePage( '#tarjetas', { transition: 'slide', reverse: true });                 
            }

        }

    });    


}


function check_use_credit_card(card_id){

    $("#resumen .credit_card").attr("src", "img/unchecked_purple.png");
    $("#resumen img[data-id='"+card_id+"']").attr("src", "img/checked_purple.png");

}


function eliminarTarjeta(id, type, number){

    $("#desea_eliminar_tarjeta .title").html("¿Eliminar tarjeta "+type+"<br>"+number+"?");
    $("#desea_eliminar_tarjeta input[name='card_id']").val(id);
    showModal("desea_eliminar_tarjeta");

}


function eliminar_tarjeta(){

    loading(true);
    var data = {};
    data.card_id = $("#desea_eliminar_tarjeta input[name='card_id']").val();
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "delete_credit_card",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            if(!result.error){
                showModal("tarjeta_eliminada_exito");
            }else{
                showModalAlert(result.message);
            }

        }

    });     

}


function toTarjetasBack(){

    $("#agregar_tarjeta select[name='tipo_de_tarjeta']").val("");
    $("#agregar_tarjeta input[name='numero_de_tarjeta']").val("");
    $("#agregar_tarjeta input[name='fecha_vencimiento']").val("");
    $("#agregar_tarjeta input[name='cvv']").val("");
    $("#agregar_tarjeta input[name='calle']").val("");
    $("#agregar_tarjeta input[name='numero_exterior']").val("");
    $("#agregar_tarjeta input[name='interior']").val("");
    $("#agregar_tarjeta input[name='colonia']").val("");
    $("#agregar_tarjeta input[name='cp']").val("");
    $("#agregar_tarjeta select[name='estado']").html("<option value=''>ESTADO</option>");
    $("#agregar_tarjeta select[name='municipio']").html("<option value=''>MUNICIPIO</option>");
    $.mobile.back();

}



function toAgregarTarjeta(){



    //Inicializamos el select de año
    var current_year = new Date();
    var option_year = "";
    current_year = current_year.getFullYear().toString().substr(2,2);
    option_year += "<option value=''>AÑO</option>";    
    for(var i = current_year; i < (parseInt(current_year) + 10); i++ ){
        option_year += "<option value='"+i+"'>"+i+"</option>";
    }  
    $("#agregar_tarjeta #anio_vencimiento").html(option_year);


    loading(true);
    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_states",
        type: "POST",
        data: data,
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            $("#agregar_tarjeta select[name='estado']").html(result);
            $.mobile.changePage( '#agregar_tarjeta', { transition: 'slide' }); 
 
        }

    }); 
 

}


function agregarTarjeta(){

    loading(true);
    var data = {};
    data.user_id            =   localStorage.getItem("user_id");
    data.tipo_de_tarjeta    =   $("#agregar_tarjeta select[name='tipo_de_tarjeta']").val();
    data.numero_de_tarjeta  =   $("#agregar_tarjeta input[name='numero_de_tarjeta']").val();
   
    data.mes_vencimiento  =   $("#agregar_tarjeta select[name='mes_vencimiento']").val();
    data.anio_vencimiento  =   $("#agregar_tarjeta select[name='anio_vencimiento']").val();


    data.cvv                =   $("#agregar_tarjeta input[name='cvv']").val();
    data.calle              =   $("#agregar_tarjeta input[name='calle']").val();
    data.numero_exterior    =   $("#agregar_tarjeta input[name='numero_exterior']").val();
    data.interior           =   $("#agregar_tarjeta input[name='interior']").val();
    data.colonia            =   $("#agregar_tarjeta input[name='colonia']").val();
    data.cp                 =   $("#agregar_tarjeta input[name='cp']").val();
    data.estado             =   $("#agregar_tarjeta select[name='estado']").val();
    data.municipio          =   $("#agregar_tarjeta select[name='municipio']").val();
    data.APP_VERSION = APP_VERSION;




    $.ajax({
        url: webservice + "add_credit_card",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){

            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);            
            console.log(result);
            if(!result.error){     
                $("#agregar_tarjeta select[name='tipo_de_tarjeta']").val("");
                $("#agregar_tarjeta input[name='numero_de_tarjeta']").val("");

                $("#agregar_tarjeta select[name='mes_vencimiento']").val("");
                $("#agregar_tarjeta select[name='anio_vencimiento']").val("");


                $("#agregar_tarjeta input[name='cvv']").val("");
                $("#agregar_tarjeta input[name='calle']").val("");
                $("#agregar_tarjeta input[name='numero_exterior']").val("");
                $("#agregar_tarjeta input[name='interior']").val("");
                $("#agregar_tarjeta input[name='colonia']").val("");
                $("#agregar_tarjeta input[name='cp']").val("");
                $("#agregar_tarjeta select[name='estado']").html("<option value=''>ESTADO</option>");
                $("#agregar_tarjeta select[name='municipio']").html("<option value=''>MUNICIPIO</option>");                                  
                showModal("tarjeta_agregada_exito");
            }else{
                showModalAlert(result.message);
            }

        }
    });    

}


function set_checked_option(set_check){
 
    if(set_check == "pagos"){
        $("#aclaraciones .envios").removeClass("checked");
        $("#aclaraciones .pagos").addClass("checked");
    }else{
        $("#aclaraciones .pagos").removeClass("checked");
        $("#aclaraciones .envios").addClass("checked");
    }

}
 

function enviar_aclaraciones_send(){

    loading(true);
    var data = {};
    data.codigo_compra  = $("#aclaraciones input[name='codigo_de_compra']").val();
    data.mensaje        = $("#aclaraciones textarea[name='mensaje']").val();
    data.user_id        = localStorage.getItem("user_id");
    data.type           = ( $("#aclaraciones .envios").hasClass("checked") ) ? "ENVIOS" : "PAGOS";
    data.APP_VERSION    = APP_VERSION;
    $.ajax({
        url: webservice + "send_aclaration",
        data: data,
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            if(!result.error){
                showModal("aclaracion_enviada_exito");
            }else{
                showModalAlert(result.message);
            }

        }
    });

}


function toPagosFromAclaracionModal(){

    set_checked_option("pagos");
    $("#aclaraciones input[name='codigo_de_compra']").val("");
    $("#aclaraciones textarea[name='mensaje']").val("");    
    $.mobile.back();
    closeModal("aclaracion_enviada_exito");

}


function toInvitarAmigos(){

    if(disabled_perfil){
        showModalAlert("Agrega una forma de pago para poder usar esta función");
    }else{
        //texto_invitar_amigo = "Tu amigo "+localStorage.getItem("name")+" te está invitando a utilizar la aplicación Open Cava. Descargalo de {liga}.";
        texto_invitar_amigo = "Tu amigo "+localStorage.getItem("name")+" te está invitando a utilizar la aplicación Open Cava.";
        $("#invitar_amigo textarea[name='mensaje']").val(texto_invitar_amigo);
        $.mobile.changePage( "#invitar_amigo", { transition: "slide" });        
    }

}



function enviar_invitacion_amigo(){

    loading(true);
    var data = {};
    data.telefono   = $("#invitar_amigo input[name='telefono']").val();
    data.mensaje    = $("#invitar_amigo textarea[name='mensaje']").val();
    data.APP_VERSION = APP_VERSION;

    $.ajax({
        url: webservice + "send_invitation",
        data: data,
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            if(!result.error){
                showModal("invitacion_enviada_exito");
            }else{
                showModalAlert(result.message);
            }

        }
    });

}


function toPerfilFromInvitacionModal(){

    $("#invitar_amigo input[name='telefono']").val("");
    $("#invitar_amigo textarea[name='mensaje']").val(texto_invitar_amigo);    
    $.mobile.back();
    closeModal("invitacion_enviada_exito");

}


function editar_perfil_correo_electronico(){

    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.nuevo_correo_electronico = $("#editar_perfil_correo_electronico input[name='correo_electronico']").val();
    data.contrasena = $("#editar_perfil_correo_electronico input[name='contrasena']").val();
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "edit_email_profile",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){

            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);            
            console.log(result);
            if(!result.error){

                //Actualizamos el nuevo correo electronico en la vista de editar perfil      
                $("#editar_perfil input[name='correo_electronico']").val(data.nuevo_correo_electronico);
                $.mobile.back();
                showModalAlertOk(result.message);  
                $("#editar_perfil_correo_electronico input[name='correo_electronico']").val("");
                $("#editar_perfil_correo_electronico input[name='contrasena']").val("");

            }else{
                showModalAlert(result.message);
            }

        }
    });

}

function editar_perfil_contrasena(){

    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.antigua_contrasena = $("#editar_perfil_contrasena input[name='antigua_contrasena']").val();
    data.nueva_contrasena = $("#editar_perfil_contrasena input[name='nueva_contrasena']").val();
    data.confirmar_contrasena = $("#editar_perfil_contrasena input[name='confirmar_contrasena']").val();
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "edit_password_profile",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){

            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);            
            console.log(result);
            if(!result.error){

                //Actualizamos el nuevo correo electronico en la vista de editar perfil      
                $.mobile.back();
                showModalAlertOk(result.message);  
                $("#editar_perfil_contrasena input[name='antigua_contrasena']").val("");
                $("#editar_perfil_contrasena input[name='nueva_contrasena']").val("");
                $("#editar_perfil_contrasena input[name='confirmar_contrasena']").val("");

            }else{
                showModalAlert(result.message);
            }

        }
    });

}

function editar_perfil_movil(){

    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.codigo_pais = $("#editar_perfil_movil #value_lada_editar_movil").text();
    data.movil = $("#editar_perfil_movil input[name='movil']").val();
    data.contrasena = $("#editar_perfil_movil input[name='contrasena']").val();
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "edit_movil_profile",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){

            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);            
            console.log(result);
            if(!result.error){

                //Actualizamos el movil en la vista de editar perfil      
                $("#editar_perfil input[name='movil']").val(data.codigo_pais+data.movil);
                $.mobile.back();
                showModalAlertOk(result.message);  
                $("#editar_perfil_movil input[name='contrasena']").val("");
                $("#editar_perfil_movil input[name='movil']").val("");
                $("#lada_editar_movil").intlTelInput("setCountry", "mx");
                $("#value_lada_editar_movil").text("+52");

            }else{
                showModalAlert(result.message);
            }

        }
    });

}

function toEditarPerfil(reverse){

    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_client_info",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            loading(false);
            console.log(result);
            $("#editar_perfil input[name='nombre_y_apelllidos']").val(result.name);
            $("#editar_perfil input[name='nombre_usuario']").val(result.username);
            $("#editar_perfil input[name='correo_electronico']").val(result.email);
            $("#editar_perfil input[name='movil']").val("+"+result.cellphone_code + "" + result.cellphone);                    
            show_menu_lateral(false);
            
            if(reverse == undefined){
                $.mobile.changePage( "#editar_perfil", { transition: "slide" });                     
            }else{
                $.mobile.changePage( "#editar_perfil", { transition: "slide", reverse: true });                     
            }

        }
    });

}

function cancelar_editar_movil(){

    $.mobile.back();
    $("#editar_perfil_movil input[name='contrasena']").val("");
    $("#editar_perfil_movil input[name='movil']").val("");
    $("#lada_editar_movil").intlTelInput("setCountry", "mx");
    $("#value_lada_editar_movil").text("+52");

}

function cancelar_editar_correo_electronico(){

    $.mobile.back();
    $("#editar_perfil_correo_electronico input[name='correo_electronico']").val("");
    $("#editar_perfil_correo_electronico input[name='contrasena']").val("");

}

function cancelar_editar_contrasena(){

    $.mobile.back();
    $("#editar_perfil_contrasena input[name='antigua_contrasena']").val("");
    $("#editar_perfil_contrasena input[name='nueva_contrasena']").val("");
    $("#editar_perfil_contrasena input[name='confirmar_contrasena']").val("");

}

function cancelar_recuperar_contrasena_opciones(){
    
    celular_recuperar_contrasena = 0;
    email_recuperar_contrasena   = 0;  
    id_usuario_recuperar_contrasena = 0;
    $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });                

}

function enviar_aclaraciones(){
    
    loading(true);
    var correo_electronico  = $("#aclaraciones_cuenta_baneada input[name='correo_electronico']").val();
    var mensaje             = $("#aclaraciones_cuenta_baneada textarea[name='mensaje']").val();

    var data = {};
    data.correo_electronico = correo_electronico;
    data.mensaje = mensaje;
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "aclaration_user_banned",
        data: data,
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        error: function(e){
            
            console.log(e);
            loading(false);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }   

        },
        success: function(result){
            
            loading(false);
            console.log(result);
            if(!result.error){

                $("#aclaraciones_cuenta_baneada input[name='correo_electronico']").val("");
                $("#aclaraciones_cuenta_baneada textarea[name='mensaje']").val("");
                showModalAlert(result.message);
                $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });

            }else{
                showModalAlert(result.message);
            }

        }
    });

}

function recuperar_contrasena(type){

    closeModal("enviar_sms_nueva_contrasena");
    closeModal("enviar_email_nueva_contrasena");
    if(id_usuario_recuperar_contrasena == 0 || id_usuario_recuperar_contrasena == undefined){
        showModalAlert("No existe una referencia del usuario, intentelo nuevamente");
        return;              
    }

    loading(true);
    if(type == "mensaje_texto"){

        var data = {};
        data.type = "mensaje_texto";
        data.user_id = id_usuario_recuperar_contrasena;
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "recover_password",
            type: "POST",
            timeout   : time_out,
            dataType: "JSON",
            data:data,
            error: function(e){
                loading(false);
                console.log(e);
            },
            success: function(response){

                loading(false);
                console.log(response);
                if(!response.error){
                    showModal("breve_recibiras_email"); 
                }else{
                    showModalAlert(response.message);
                }  

            }
        });

    }else{

        var data = {};
        data.type = "correo_electronico";
        data.user_id = id_usuario_recuperar_contrasena;
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "recover_password",
            type: "POST",
            timeout   : time_out,
            dataType: "JSON",
            data:data,
            error: function(e){
                loading(false);
                console.log(e);
            },
            success: function(response){

                loading(false);
                console.log(response);
                if(!response.error){
                    showModal("se_envio_email_contrasena");                                      
                }else{
                    showModalAlert(response.message);
                }

            }
        });

    }

}

function setInfoUserLoged(){

    $("#perfil .image_perfil_icon").css("background", "url("+localStorage.getItem("foto_perfil")+") center" );
    $("#editar_perfil .image_perfil_icon").css("background", "url("+localStorage.getItem("foto_perfil")+") center" );
    $("#perfil .nombre_usuario_texto").text( localStorage.getItem("name") );

}

function cancelarRegistroCorreo(){

    closeModal("seguro_cancelar_registro");
    $.mobile.changePage( "#inicio", { transition: "slide", reverse: true });

    //Formulario de correo
    $("#crear_cuenta_correo input[name='nombre_y_apelllidos']").val("");
    $("#crear_cuenta_correo input[name='nombre_de_usuario']").val("");
    $("#crear_cuenta_correo input[name='correo_electronico']").val("");
    $("#crear_cuenta_correo input[name='contrasena']").val("");
        
    $("#lada_crear_cuenta_correo").intlTelInput("setCountry", "mx");
    $("#value_lada_crear_cuenta_correo").text("+52");

    $("#crear_cuenta_correo input[name='movil']").val("");
    $("#crear_cuenta_correo select[name='anio'] option[value='']").prop('selected', true);
    $("#crear_cuenta_correo select[name='mes'] option[value='']").prop('selected', true);
    $("#crear_cuenta_correo select[name='dia']").html('<option value="">DÍA</option>');

    //Formulario de red social
    $("#crear_cuenta_red_social input[name='nombre_y_apelllidos']").val("");
    $("#crear_cuenta_red_social input[name='nombre_de_usuario']").val("");
    $("#crear_cuenta_red_social input[name='contrasena']").val("");
    $("#crear_cuenta_red_social input[name='correo_electronico']").val("");
    $("#crear_cuenta_red_social input[name='social_id']").val("");
 
    $("#lada_crear_cuenta").intlTelInput("setCountry", "mx");
    $("#value_lada_crear_cuenta").text("+52");

    $("#crear_cuenta_red_social input[name='movil']").val("");
    $("#crear_cuenta_red_social select[name='anio'] option[value='']").prop('selected', true);
    $("#crear_cuenta_red_social select[name='mes'] option[value='']").prop('selected', true);
    $("#crear_cuenta_red_social select[name='dia']").html('<option value="">DÍA</option>');

}


function toHacerPedido(reverse){

    if(disabled_perfil){
        showModalAlert("Agrega una forma de pago para poder usar esta función");
    }else{

        loading(true);
        //Primero revisamos si esta abierto el negocio
        /*
        var data = {};
        data.user_id = localStorage.getItem("user_id");
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "is_open_store",
            type: "POST",
            data: data,
            timeout   : time_out,
            dataType: "JSON",
            error: function(e){
                console.log(e);
            },
            success: function(is_open){

                if(is_open){
         */
                    var data = {};
                    data.APP_VERSION = APP_VERSION;
                    $.ajax({
                        url: webservice + "get_categories",
                        type: "POST",
                        data: data,
                        timeout   : time_out,
                        dataType: "JSON",
                        error : function(e) {
                            
                            loading(false);
                            console.log(e);
                            if (e.status == 422){
                                showModalAlertAjax( JSON.parse(e.responseText) );
                            }else{
                                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
                            }

                        },
                        success: function(result){

                            console.log(result);
                            var html_ = "";
                            html_ += '<div class="ui-grid-a text-center">';
                            for(var i = 0; i < result.length; i++){
                                if(i % 2 == 0){
                                    html_ += '<div class="ui-block-a" onclick="toComoLoQuieres(\''+result[i].id+'\');" >';
                                }else{
                                    html_ += '<div class="ui-block-b" onclick="toComoLoQuieres(\''+result[i].id+'\');" >';
                                }
                                html_ += '    <img src="'+result[i].icon+'" class="w80pxm0auto">';
                                html_ += '    <p class="title_type_4 color_purple">'+result[i].name+'</p>';
                                html_ += '</div>';
                            }                  
                            html_ += '</div>'; 
                            loading(false);                            
                            $("#categorias #productos_list").html(html_);
                            if(reverse == undefined){
                                $.mobile.changePage( "#categorias", { transition: "slide" });
                            }else{
                                $.mobile.changePage( "#categorias", { transition: "slide", reverse: true });                    
                            }

                        }
                    }); 
        /*
                }else{

                    loading(false);
                    showModalAlert("Por el momento Open Cava esta cerrado, recibirás una notificación cuando estemos en servicio");
                }

            }
        });*/
       
    }       

}


function toPromociones(){

    if(disabled_perfil){
        showModalAlert("Agrega una forma de pago para poder usar esta función");
    }else{

        /*
        loading(true);
        //Primero revisamos si esta abierto el negocio
        var data = {};
        data.user_id = localStorage.getItem("user_id");
        data.APP_VERSION = APP_VERSION;
        $.ajax({
            url: webservice + "is_open_store",
            type: "POST",
            data: data,
            timeout   : time_out,
            dataType: "JSON",
            error: function(e){
                console.log(e);
            },
            success: function(is_open){

                if(is_open){
                    loading(false);
         */
                    toPromocionesProductos();
         /*
                }else{

                    loading(false);
                    showModalAlert("Por el momento Open Cava esta cerrado, recibirás una notificación cuando estemos en servicio");
                }

            }
        });
        */
    }       


}



function toComoLoQuieres(category_id){

    localStorage.setItem("category_id", category_id);
    $.mobile.changePage( "#como_lo_quieres", { transition: "slide" });

}


function toProductos(tipo){

    loading(true);
    localStorage.setItem("type", tipo);
    var data = {};
    data.category_id = localStorage.getItem("category_id");
    data.type = localStorage.getItem("type");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_products",
        type: "POST",
        timeout   : time_out,
        data: data,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            var html_ = "";
            if(!result.error){
                
                for(var i = 0; i < result.data.length; i++){
                    html_ += '<div class="producto_item">';
                    html_ += '    <div class="imagen_producto">';
                    html_ += '        <img src="'+result.data[i].icon+'" />';
                    html_ += '    </div>';
                    html_ += '    <div class="descripcion_producto">';
                    html_ += '        <p class="descripcion_producto_label">'+result.data[i].name+'</p>';
                    html_ += '        <p class="precio_producto_label">$ '+result.data[i].price+'</p>';
                    html_ += '        <div class="cantidad_producto">';
                    html_ += '            <div class="less_product" onclick="lessProduct(\''+result.data[i].name+'\', \'producto\',\'productos\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '            <div class="quantity_product"><input type="number" value="0" data-role="none" product-id="'+result.data[i].id+'" data-type="producto" data-price="'+result.data[i].price+'" readonly/></div>';
                    html_ += '            <div class="add_product" onclick="addProduct(\''+result.data[i].name+'\', \'producto\',\'productos\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '        </div>';
                    html_ += '    </div>';
                    html_ += '</div>';
                }

                $("#productos #productos_list_purchase").html(html_);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM productos WHERE tipo = 'producto'", [], function(tx, rs){   

                        for(var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $("#productos #productos_list_purchase input[product-id='"+row.product_id+"']").val(row.cantidad);
                        }
                        calcular_total();

                    }, function(error){
                        console.log(error.message);
                    });
                }); 

                enable_next_purchase();
                $.mobile.changePage( "#productos", { transition: "slide" });

            }else{
                showModalAlert(result.message);
            }

        }
    });       

}


function toPromocionesProductos(tipo){

    loading(true);
    //localStorage.setItem("type", tipo);
    var data = {};
    //data.type = localStorage.getItem("type");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_promotions",
        type: "POST",
        timeout   : time_out,
        data: data,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            var html_ = "";
            if(!result.error){
                
                for(var i = 0; i < result.data.length; i++){
                    html_ += '<div class="producto_item">';
                    html_ += '    <div class="imagen_producto">';
                    html_ += '        <img src="'+result.data[i].icon+'" />';
                    html_ += '    </div>';
                    html_ += '    <div class="descripcion_producto">';
                    html_ += '        <p class="descripcion_producto_label">'+result.data[i].name+'</p>';
                    html_ += '        <p class="precio_producto_label">$ '+result.data[i].price+'</p>';
                    html_ += '        <div class="cantidad_producto">';
                    html_ += '            <div class="less_product" onclick="lessProduct(\''+result.data[i].name+'\', \'promocion\',\'productos_promociones\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '            <div class="quantity_product"><input type="number" value="0" data-role="none" product-id="'+result.data[i].id+'" data-type="promocion" data-price="'+result.data[i].price+'" readonly/></div>';
                    html_ += '            <div class="add_product" onclick="addProduct(\''+result.data[i].name+'\', \'promocion\',\'productos_promociones\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '        </div>';
                    html_ += '    </div>';
                    html_ += '</div>';
                }

                $("#productos_promociones #productos_list_purchase").html(html_);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM productos WHERE tipo = 'promocion'", [], function(tx, rs){   

                        for(var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $("#productos_promociones #productos_list_purchase input[product-id='"+row.product_id+"']").val(row.cantidad);
                        }
                        calcular_total();

                    }, function(error){
                        console.log(error.message);
                    });
                }); 

                 enable_next_purchase();
                $.mobile.changePage( "#productos_promociones", { transition: "slide" });

            }else{
                showModalAlert(result.message);
            }

        }
    });       

}
function toComplementos(){

    loading(true);
    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_complements",
        type: "POST",
        data: data,
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            var html_ = "";
            if(!result.error){
                
                for(var i = 0; i < result.data.length; i++){
                    html_ += '<div class="producto_item">';
                    html_ += '    <div class="imagen_producto">';
                    html_ += '        <img src="'+result.data[i].icon+'" />';
                    html_ += '    </div>';
                    html_ += '    <div class="descripcion_producto">';
                    html_ += '        <p class="descripcion_producto_label">'+result.data[i].name+'</p>';
                    html_ += '        <p class="precio_producto_label">$ '+result.data[i].price+'</p>';
                    html_ += '        <div class="cantidad_producto">';
                    html_ += '            <div class="less_product" onclick="lessProduct(\''+result.data[i].name+'\', \'producto\',\'complementos\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '            <div class="quantity_product"><input type="number" value="0" data-role="none" product-id="'+result.data[i].id+'" data-type="producto" data-price="'+result.data[i].price+'" readonly/></div>';
                    html_ += '            <div class="add_product" onclick="addProduct(\''+result.data[i].name+'\', \'producto\',\'complementos\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '        </div>';
                    html_ += '    </div>';
                    html_ += '</div>';
                }

                $("#complementos #productos_list_purchase").html(html_);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM productos WHERE tipo = 'producto'", [], function(tx, rs){   

                        for(var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $("#complementos #productos_list_purchase input[product-id='"+row.product_id+"']").val(row.cantidad);
                        }
                        calcular_total();

                    }, function(error){
                        console.log(error.message);
                    });
                }); 

                $.mobile.changePage( "#complementos", { transition: "slide" });

            }else{
                showModalAlert(result.message);
            }

        }
    });       

}

function toMezcladores(){

    loading(true);
    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_mixeds",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            var html_ = "";
            if(!result.error){
                
                for(var i = 0; i < result.data.length; i++){
                    html_ += '<div class="producto_item">';
                    html_ += '    <div class="imagen_producto">';
                    html_ += '        <img src="'+result.data[i].icon+'" />';
                    html_ += '    </div>';
                    html_ += '    <div class="descripcion_producto">';
                    html_ += '        <p class="descripcion_producto_label">'+result.data[i].name+'</p>';
                    html_ += '        <p class="precio_producto_label">$ '+result.data[i].price+'</p>';
                    html_ += '        <div class="cantidad_producto">';
                    html_ += '            <div class="less_product" onclick="lessProduct(\''+result.data[i].name+'\', \'producto\',\'mezcladores\', '+result.data[i].id+', false, '+result.data[i].is_drink+' );"></div>';
                    html_ += '            <div class="quantity_product"><input type="number" value="0" data-role="none" product-id="'+result.data[i].id+'" data-type="producto" data-price="'+result.data[i].price+'" readonly/></div>';
                    html_ += '            <div class="add_product" onclick="addProduct(\''+result.data[i].name+'\', \'producto\',\'mezcladores\', '+result.data[i].id+', false, '+result.data[i].is_drink+' );"></div>';
                    html_ += '        </div>';
                    html_ += '    </div>';
                    html_ += '</div>';
                }

                $("#mezcladores #productos_list_purchase").html(html_);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM productos WHERE tipo = 'producto'", [], function(tx, rs){   

                        for(var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $("#mezcladores #productos_list_purchase input[product-id='"+row.product_id+"']").val(row.cantidad);
                        }
                        calcular_total();

                    }, function(error){
                        console.log(error.message);
                    });
                }); 

                $.mobile.changePage( "#mezcladores", { transition: "slide" });

            }else{
                showModalAlert(result.message);
            }

        }
    });       

}
 


function lessProduct(descripcion, tipo, data_id, product_id, is_resumen, is_drink){
    var current_val = $("#"+data_id+" input[product-id='"+product_id+"']").val();
    var precio = $("#"+data_id+" input[product-id='"+product_id+"']").attr("data-price");
    
    if(is_resumen == true){
    
        if(current_val > 1){

            if(current_val > 0){

                loading(true);
                var data = {};
                data.tipo = tipo;
                data.product_id = product_id;
                data.cantidad = (parseInt(current_val) + 1);
                data.APP_VERSION = APP_VERSION;
                $.ajax({
                    url: webservice + "less_product_shopping_cart",
                    data: data,
                    type: "POST",
                    timeout   : time_out,
                    dataType: "JSON",
                    error: function(e){
                        console.log(e);
                    },
                    success: function(result){

                        loading(false);
                        console.log(result);
                        if(!result.error){
                            current_val = parseInt(current_val) - 1;
                            $("#"+data_id+" input[product-id='"+product_id+"']").val(current_val);
                            cambiar_cantidad(descripcion, tipo, product_id, current_val, precio, is_drink); 
                        }else{
                            //showModal(result.message);
                        }

                    }
                });

            }else{
                cambiar_cantidad(descripcion, tipo, product_id, current_val, precio, is_drink);                
            }

        }

    }else{

        if(current_val > 0){

            loading(true);
            var data = {};
            data.tipo = tipo;
            data.product_id = product_id;
            data.cantidad = (parseInt(current_val) + 1);
            data.APP_VERSION = APP_VERSION;
            $.ajax({
                url: webservice + "less_product_shopping_cart",
                data: data,
                type: "POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result){

                    loading(false);
                    console.log(result);
                    if(!result.error){
                        current_val = parseInt(current_val) - 1;
                        $("#"+data_id+" input[product-id='"+product_id+"']").val(current_val);
                        cambiar_cantidad(descripcion, tipo, product_id, current_val, precio, is_drink);
                    }else{
                        //showModal(result.message);
                    }

                }
            });

        }else{
            cambiar_cantidad(descripcion, tipo, product_id, current_val, precio, is_drink);            
        }

    }

}

function addProduct(descripcion, tipo, data_id, product_id, is_resumen, is_drink){

    var current_val = $("#"+data_id+" input[product-id='"+product_id+"']").val();
    var precio = $("#"+data_id+" input[product-id='"+product_id+"']").attr("data-price");

    loading(true);
    var data = {};
    data.tipo = tipo;
    data.product_id = product_id;
    data.user_id = localStorage.getItem("user_id");
    data.cantidad = (parseInt(current_val) + 1);
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "add_product_shopping_cart",
        data: data,
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        error: function(e){
            console.log(e);
        },
        success: function(result){

            loading(false);
            console.log(result);

            if( result.is_open_store ){
                
                if(!result.error){
                    current_val = parseInt(current_val) + 1;
                    $("#"+data_id+" input[product-id='"+product_id+"']").val(current_val);
                    cambiar_cantidad(descripcion,tipo, product_id, current_val, precio, is_drink);
                }else{
                    //showModal(result.message);
                }

            }else{

                showModalAlert(result.message);

            }


        }
    });


}

function cambiar_cantidad(descripcion, tipo, product_id, cantidad, precio, is_drink){

    loading(true);
    //Si la cantidad es 0, eliminamos el producto de la base de datos interna
    if(cantidad == 0){

        db.transaction(function (tx) {
            tx.executeSql("DELETE FROM productos WHERE product_id = ? AND tipo = ?", [product_id, tipo], function(tx, rs){    
                calcular_total();         
                enable_next_purchase();        
            }, function(error){
                console.log(error.message);
            });
        });

    }else{

        db.transaction(function (tx) {
            tx.executeSql("SELECT * FROM productos WHERE product_id = ? AND tipo = ?", [product_id, tipo], function(tx, rs){

                //Si ya existe, editamos la cantidad
                if(rs.rows.length == 1){

                    db.transaction(function (tx) {
                        tx.executeSql("UPDATE productos SET cantidad = ?, precio = ?, descripcion = ?, is_drink = ? WHERE product_id = ? AND tipo = ?", [cantidad, precio, descripcion, is_drink, product_id, tipo], function(tx, rs){   
                            calcular_total();
                            enable_next_purchase();                  
                        }, function(error){
                            console.log(error.message);
                        });
                    });

                }else{

                    db.transaction(function (tx) {
                        tx.executeSql("INSERT INTO productos (cantidad, precio, product_id, tipo, descripcion, is_drink) VALUES (?,?,?,?,?, ?)", [cantidad, precio, product_id, tipo, descripcion, is_drink], function(tx, rs){  
                            calcular_total();  
                            enable_next_purchase();                 
                        }, function(error){
                            console.log(error.message);
                        });
                    });

                }
             
            }, function(error){
                console.log(error.message);
            });
        });       

    }

}


function enable_next_purchase(){

    loading(true);
    //Cantidad de productos comprados que son bebidas
    db.transaction(function (tx) {
        tx.executeSql("SELECT SUM(is_drink) as cantidad_bebidas FROM productos", [], function(tx, rs){     

            console.log(rs);
            var total_comprado = rs.rows.item(0).cantidad_bebidas;
            if(total_comprado == null){
                total_comprado = 0;
            }

            if(total_comprado <= 0){
                console.log("esconde");
                $(".next_row_header").css("display", "none");
            }else{
                console.log("muestra");
                $(".next_row_header").css("display", "block");
            }
            loading(false);

        }, function(error){
            console.log(error.message);
        });
    });  

}


 
function calcular_total(){

    //Cantidad Pagar
    db.transaction(function (tx) {
        tx.executeSql("SELECT SUM(cantidad * precio) as total_price FROM productos", [], function(tx, rs){     

            console.log(rs);
            var total_comprado = rs.rows.item(0).total_price;
            total_comprado = parseFloat(total_comprado);
            total_comprado = total_comprado.toFixed(2);
            if(isNaN(total_comprado)){
                total_comprado = "0.00";
            }
            $(".cantidad_pagar .precio_").text("$ " + total_comprado);
            $(".cantidad_pagar .precio_").text("$ " + total_comprado);
            
            if(localStorage.getItem("shipping") != undefined){
                var total = parseFloat(parseFloat(total_comprado) + parseFloat(localStorage.getItem("shipping"))).toFixed(2);
                $("#resumen #total_pagar").text("$ "+total);
            }

        }, function(error){
            console.log(error.message);
        });
    });

    //Cantidad de productos comprados
    db.transaction(function (tx) {
        tx.executeSql("SELECT SUM(cantidad) as cantidad FROM productos", [], function(tx, rs){     

            console.log(rs);
            var total_comprado = rs.rows.item(0).cantidad;
            if(total_comprado == null){
                total_comprado = 0;
            }
            $(".cantidad_pagar .cantidad_comprado").text(total_comprado);
            
        }, function(error){
            console.log(error.message);
        });
    });

}


function check_no_volver_mostrar(){
    
    var img_value = $("#desliza_mapa_ubicacion .check_img").attr("src");
    if(img_value == "img/uncheck.png"){
        $("#desliza_mapa_ubicacion .check_img").attr("src", "img/check.png");
        localStorage.setItem("already_show_location", true);        
    }else{
        $("#desliza_mapa_ubicacion .check_img").attr("src", "img/uncheck.png");  
        localStorage.setItem("already_show_location", false);
    }
 
 }


function toSeleccionarUbicacion(){

    //Primero verificamos que tenga productos
    loading(true);    
    db.transaction(function (tx) {
        tx.executeSql("SELECT SUM(cantidad) as cantidad FROM productos", [], function(tx, rs){     

            console.log(rs);
            var total_comprado = rs.rows.item(0).cantidad;
            if(total_comprado == null){
                total_comprado = 0;
            }

            if(total_comprado > 0){

                    $.mobile.changePage( "#correcta_ubicacion", { transition: "slide" });
                    if(localStorage.getItem("already_show_location") != "true" && localStorage.getItem("already_show_location") != true){
                        showModal("desliza_mapa_ubicacion");
                    }

                    var window_height = $(window).height() - 60;
                    $("#map").css("height", window_height);

                    navigator.geolocation.getCurrentPosition(
                        function(position) {
                            showMap(position.coords.latitude, position.coords.longitude, 16);
                        }, 
                        function(error) {
                            showMap(19.282623, -99.655665, 14);
                        },
                        { maximumAge: 3000, timeout: 10000, enableHighAccuracy: false }
                    );

            }else{  
                loading(false);  
                showModalAlert("Elige productos para generar tu compra");
            }         
            
        }, function(error){
            console.log(error.message);
        });
    });    

}

function showMap(lat, lon, zoom){

    console.log("LAT: " + lat);     
    console.log("LON: " + lon); 
    localStorage.setItem("LAT_coordinate", lat);
    localStorage.setItem("LON_coordinate", lon);
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lon},
        zoom: zoom,
        disableDefaultUI: true
    });

  var image = {
    url: 'img/marker_transparent.png',
  };

    var myLatlng = new google.maps.LatLng(lat,lon);
    var marker = new google.maps.Marker({
          position: myLatlng,
          map: map,
          icon: image
    });

    var window_width = $(window).width();
    var window_height = $(window).height();
    window_width = (window_width/2) - 17;
    window_height = (window_height/2) - 25;
    $("#icono_gps").css("left", window_width);
    $("#icono_gps").css("top", window_height);

    google.maps.event.addListener(map, 'center_changed', function() {
        var center = map.getCenter();
        marker.setPosition(center);
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();   
        localStorage.setItem("LAT_coordinate", lat);
        localStorage.setItem("LON_coordinate", lng);          
        console.log("LAT: " + lat);     
        console.log("LON: " + lng);           
    });

    loading(false);
    $.mobile.changePage( "#correcta_ubicacion", { transition: "slide" });

}

 
function eliminarPedido(){


    closeModal('seguro_cancelar_pedido');
    loading(true);

    //REGRESAMOS LOS PRODUCTOS AGREGADOS PARA VOLVER A ALIMENTAR EL ALMACEN
    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

            var productos = new Array();
            for(var i = 0; i < rs.rows.length; i++){
                var row_pro = {};
                row_pro.tipo = rs.rows.item(i).tipo;
                row_pro.product_id = rs.rows.item(i).product_id;
                row_pro.cantidad = rs.rows.item(i).cantidad;
                productos.push(row_pro);
            }

            //Regresamos todos los pedidos, ya que se cancelo
            var data = {};
            data.productos = productos;
            data.APP_VERSION = APP_VERSION;
            $.ajax({
                url: webservice + "return_products",
                data:data,
                type:"POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result_return_products){

                    //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                    console.log(result_return_products);
                    if(!result_return_products.error){

                        db.transaction(function (tx) {
                            tx.executeSql("DROP TABLE productos", [], function(tx, rs){
                                
                                db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });
                        
                                    loading(false);
                                    localStorage.removeItem("category_id");
                                    localStorage.removeItem("code");
                                    localStorage.removeItem("end_delivery_time");
                                    localStorage.removeItem("LAT_coordinate");
                                    localStorage.removeItem("LON_coordinate");
                                    localStorage.removeItem("shipping");
                                    localStorage.removeItem("type");  
                                    LATITUDE_service = "";
                                    LONGITUDE_service = "";

                                    closeModal("pedido_cancelado_exito");   //Por asi acaso cerramos estos son modales    
                                    closeModal("pedido_pagado_exito");      //Por asi acaso cerramos estos son modales

                                    showModalAlertOk("Pedido eliminado con exito");

                                    $.mobile.changePage( "#perfil", { transition: "slide", reverse: true });

                            }, function(error){
                                console.log(error.message);
                            });
                        }); 


                    }else{

                        loading(false);                                        
                        showModalAlert(result.message);
                    }

                }
            });
    

        }, function(error){
            console.log(error.message);
        });
    });







/*
    closeModal('seguro_cancelar_pedido');
    loading(true);
    db.transaction(function (tx) {
        tx.executeSql("DROP TABLE productos", [], function(tx, rs){

            db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
            db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });

            calcular_total();
            loading(false);
            $.mobile.changePage( "#perfil", { transition: "slide", reverse: true });
        }, function(error){
            console.log(error.message);
        });
    });    
*/
}

function toMezcladoresFromResumen(){

    loading(true);
    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_mixeds",
        type: "POST",
        data: data,
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            var html_ = "";
            if(!result.error){
                
                for(var i = 0; i < result.data.length; i++){
                    html_ += '<div class="producto_item">';
                    html_ += '    <div class="imagen_producto">';
                    html_ += '        <img src="'+result.data[i].icon+'" />';
                    html_ += '    </div>';
                    html_ += '    <div class="descripcion_producto">';
                    html_ += '        <p class="descripcion_producto_label">'+result.data[i].name+'</p>';
                    html_ += '        <p class="precio_producto_label">$ '+result.data[i].price+'</p>';
                    html_ += '        <div class="cantidad_producto">';
                    html_ += '            <div class="less_product" onclick="lessProduct(\''+result.data[i].name+'\', \'producto\',\'mezcladores_resumen\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '            <div class="quantity_product"><input type="number" value="0" data-role="none" product-id="'+result.data[i].id+'" data-type="producto" data-price="'+result.data[i].price+'" readonly/></div>';
                    html_ += '            <div class="add_product" onclick="addProduct(\''+result.data[i].name+'\', \'producto\',\'mezcladores_resumen\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '        </div>';
                    html_ += '    </div>';
                    html_ += '</div>';
                }

                $("#mezcladores_resumen #productos_list_purchase").html(html_);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM productos WHERE tipo = 'producto'", [], function(tx, rs){   

                        for(var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $("#mezcladores_resumen #productos_list_purchase input[product-id='"+row.product_id+"']").val(row.cantidad);
                        }
                        calcular_total();

                    }, function(error){
                        console.log(error.message);
                    });
                }); 

                $.mobile.changePage( "#mezcladores_resumen", { transition: "slide" });

            }else{
                showModalAlert(result.message);
            }

        }
    });       

}

function toComplementosFromResumen(){

    loading(true);
    var data = {};
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_complements",
        type: "POST",
        data: data,
        timeout   : time_out,
        dataType: "JSON",
        error : function(e) {
            
            loading(false);
            console.log(e);
            if (e.status == 422){
                showModalAlertAjax( JSON.parse(e.responseText) );
            }else{
                showModalAlert("Experimentamos fallas técnicas, intentelo más tarde.");
            }

        },
        success: function(result){

            loading(false);
            console.log(result);
            var html_ = "";
            if(!result.error){
                
                for(var i = 0; i < result.data.length; i++){
                    html_ += '<div class="producto_item">';
                    html_ += '    <div class="imagen_producto">';
                    html_ += '        <img src="'+result.data[i].icon+'" />';
                    html_ += '    </div>';
                    html_ += '    <div class="descripcion_producto">';
                    html_ += '        <p class="descripcion_producto_label">'+result.data[i].name+'</p>';
                    html_ += '        <p class="precio_producto_label">$ '+result.data[i].price+'</p>';
                    html_ += '        <div class="cantidad_producto">';
                    html_ += '            <div class="less_product" onclick="lessProduct(\''+result.data[i].name+'\', \'producto\',\'complementos_resumen\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '            <div class="quantity_product"><input type="number" value="0" data-role="none" product-id="'+result.data[i].id+'" data-type="producto" data-price="'+result.data[i].price+'" readonly/></div>';
                    html_ += '            <div class="add_product" onclick="addProduct(\''+result.data[i].name+'\', \'producto\',\'complementos_resumen\', '+result.data[i].id+', false, '+result.data[i].is_drink+');"></div>';
                    html_ += '        </div>';
                    html_ += '    </div>';
                    html_ += '</div>';
                }

                $("#complementos_resumen #productos_list_purchase").html(html_);

                db.transaction(function (tx) {
                    tx.executeSql("SELECT * FROM productos WHERE tipo = 'producto'", [], function(tx, rs){   

                        for(var i = 0; i < rs.rows.length; i++) {
                            var row = rs.rows.item(i);
                            $("#complementos_resumen #productos_list_purchase input[product-id='"+row.product_id+"']").val(row.cantidad);
                        }
                        calcular_total();

                    }, function(error){
                        console.log(error.message);
                    });
                }); 

                $.mobile.changePage( "#complementos_resumen", { transition: "slide" });

            }else{
                showModalAlert(result.message);
            }

        }
    });       

}

function correctaUbicacion(reverse){
    //show_menu_lateral_resumen

    loading(true);

    //Verificamos el costo de envio dependiendo de la ubicacion
    LATITUDE_service = localStorage.getItem("LAT_coordinate");
    LONGITUDE_service = localStorage.getItem("LON_coordinate");

    var data = {};
    data.lat = localStorage.getItem("LAT_coordinate");
    data.lon = localStorage.getItem("LON_coordinate");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_shipping_cost",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

                console.log(result);

                localStorage.setItem("shipping", result.precio);
                //Calculamos el total comprado a pagar
                db.transaction(function (tx) {
                    tx.executeSql("SELECT SUM(cantidad * precio) as total_price FROM productos", [], function(tx, rs){     

                        console.log(rs);
                        var total_comprado = rs.rows.item(0).total_price;
                        total_comprado = parseFloat(total_comprado);
                        total_comprado = total_comprado.toFixed(2);
                        if(isNaN(total_comprado)){
                            total_comprado = "0.00";
                        }
                        $("#resumen #shipping_cost").text("$ "+ (parseFloat(localStorage.getItem("shipping"))).toFixed(2) );
                        $("#resumen .resumen_top .total_pagar").html('<div class="cantidad_pagar"><div class="precio_">$'+total_comprado+'</div></div>');                        
                        $("#resumen #total_pagar").text("$ "+ (parseFloat( parseFloat(total_comprado) +  parseFloat(localStorage.getItem("shipping")) )).toFixed(2) );



                        //Vamos a enlistar todos los productos para el resumen
                        db.transaction(function (tx) {
                            tx.executeSql("SELECT * FROM productos", [], function(tx, rs){     

                                    var html = "";
                                    for(var i = 0; i < rs.rows.length; i++){

                                        html += '<div class="ui-grid-a block_producto">';
                                        html += '    <div class="ui-block-a label_resumen">';
                                        html += '       '+rs.rows.item(i).descripcion+'';
                                        html += '    </div>';
                                        html += '    <div class="ui-block-b descripcion_producto_resumen">';
                                        html += '        <div class="cantidad_producto">            ';
                                        html += '            <div class="less_product" onclick="lessProduct(\''+rs.rows.item(i).descripcion+'\', \''+rs.rows.item(i).tipo+'\', \'productos_resumen .'+rs.rows.item(i).tipo+' \', '+rs.rows.item(i).product_id+', true, '+rs.rows.item(i).is_drink+');" ></div>';
                                        html += '            <div class="quantity_product '+rs.rows.item(i).tipo+'">';
                                        html += '                <input type="number" value="'+rs.rows.item(i).cantidad+'" data-role="none" product-id="'+rs.rows.item(i).product_id+'" data-price="'+rs.rows.item(i).precio+'" readonly="">';
                                        html += '            </div>            ';
                                        html += '            <div class="add_product" onclick="addProduct(\''+rs.rows.item(i).descripcion+'\', \''+rs.rows.item(i).tipo+'\', \'productos_resumen .'+rs.rows.item(i).tipo+' \', '+rs.rows.item(i).product_id+', true, '+rs.rows.item(i).is_drink+');"></div>        ';
                                        html += '        </div>';
                                        html += '    </div>';
                                        html += '</div>';

                                     }
              
                                    $("#resumen #productos_resumen").html(html);  

                                    //Traemos todas las tarjetas de credito registradas
                                    var data = {};
                                    data.user_id = localStorage.getItem("user_id");
                                    data.APP_VERSION = APP_VERSION;
                                    $.ajax({
                                        url: webservice + "get_credit_cards_purchase",
                                        data: data,
                                        type: "POST",
                                        timeout   : time_out,
                                        dataType: "JSON",
                                        error: function(e){
                                            console.log(e);
                                        },
                                        success: function(result){

                                            var html = "";
                                            for(var i = 0; i < result.length; i++){
                                                html += '<div class="form_group input_icon_right c_w82">';
                                                html += '    <div class="color_orange FjallaOne value">'+result[i].number+'</div>';
                                                html += '    <div class="image_credit_card">';
                                                html += '        <img src="img/unchecked_purple.png" class="credit_card" onclick="check_use_credit_card('+result[i].id+');" card-type="'+result[i].type+'" card-number="'+result[i].number+'" data-id="'+result[i].id+'">';
                                                html += '    </div>';
                                                html += '</div>';
                                            }             

                                            $("#resumen .resumen_top .fecha").text(todayDateResumen());                  
                                            $("#resumen #tarjetas_agregadas_list").html(html);
                                            loading(false);

                                            if(reverse == undefined){
                                                $.mobile.changePage( "#resumen", { transition: "slide" });
                                            }else{
                                                $.mobile.changePage( "#resumen", { transition: "slide", reverse: true });
                                            }

                                        }
                                    });


                            }, function(error){
                                console.log(error.message);
                            });
                        });



                    }, function(error){
                        console.log(error.message);
                    });
                });

        }
    });

}

 

function alertSiguienteResumen(){

    //Obtenemos el id de la tarjeta seleccionada
    var credit_card = $("#resumen img[src='img/checked_purple.png']").attr("data-id");
    if(credit_card == undefined){
        showModalAlert("Selecciona la tarjeta a la que quieres realizar el cargo o realiza el pago en efectivo");
    }else{

        //Primero verificamos que tenga productos
        db.transaction(function (tx) {
            tx.executeSql("SELECT SUM(cantidad) as cantidad FROM productos", [], function(tx, rs){     

                console.log(rs);
                var total_comprado = rs.rows.item(0).cantidad;
                if(total_comprado == null){
                    total_comprado = 0;
                }

                if(total_comprado > 0){

                        db.transaction(function (tx) {
                            tx.executeSql("SELECT SUM(cantidad * precio) as total_price FROM productos", [], function(tx, rs){     

                                console.log(rs);
                                var total_comprado = rs.rows.item(0).total_price;
                                total_comprado = parseFloat(total_comprado);
                                total_comprado = total_comprado.toFixed(2);
                                if(isNaN(total_comprado)){
                                    total_comprado = "0.00";
                                }

                                var subtotal = parseFloat(total_comprado);
                                var shipping = localStorage.getItem("shipping");           
                                var total = ( parseFloat(total_comprado) + parseFloat(shipping) ).toFixed(2);                 
                                var tarjeta = $("img[data-id='"+credit_card+"']").attr("card-number");
                                var tipo = $("img[data-id='"+credit_card+"']").attr("card-type");
                 
                                if(tipo == "AE"){
                                    tipo = "AMERICAN EXPRESS";
                                }else if(tipo == "MC"){
                                    tipo = "MASTERCARD";
                                }

                                if(credit_card != 0 && credit_card != -1){

                                    $("#seguro_realizar_cargo .title").html("Se realizara un cargo a tu tarjeta "+tipo+" "+tarjeta+" por la cantidad de");
                                    $("#seguro_realizar_cargo .title_type_1").text("$"+total);
                                    $("#seguro_realizar_cargo .aceptar").attr("onclick", "siguienteResumen('"+subtotal+"');");
                                    showModal("seguro_realizar_cargo");

                                }else if(credit_card == -1){

                                    $("#pago_terminal_modal .aceptar").attr("onclick", "siguienteResumenTerminal('"+subtotal+"');");
                                    showModal("pago_terminal_modal");
                                    disableVerticalScroll(false);

                                }else{

                                    $("#cantidad_pago_en_efectivo .aceptar").attr("onclick", "siguienteResumenEfectivo('"+subtotal+"');");
                                    showModal("cantidad_pago_en_efectivo");
                                    disableVerticalScroll(false);

                                }


                            }, function(error){
                                console.log(error.message);
                            });
                        });                    

                }else{  
                    showModalAlert("Elige productos para generar tu compra");
                }         
                
            }, function(error){
                console.log(error.message);
            });
        }); 

 
    } 

}


function siguienteResumen(subtotal){

    closeModal('seguro_realizar_cargo');
    loading(true);

    //Obtenemos el id de la tarjeta seleccionada
    var credit_card = $("#resumen img[src='img/checked_purple.png']").attr("data-id");
    if(credit_card == undefined){
        credit_card = "";
    }


    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){     

            var productos = new Array();
            for(var i = 0; i < rs.rows.length; i++){
                var row_pro = {};
                row_pro.tipo = rs.rows.item(i).tipo;
                row_pro.product_id = rs.rows.item(i).product_id;
                row_pro.cantidad = rs.rows.item(i).cantidad;
                productos.push(row_pro);
            }


            var data = {};
            data.code   = localStorage.getItem("code");
            data.user_id = localStorage.getItem("user_id");
            data.shipping = localStorage.getItem("shipping");
            data.subtotal = subtotal;
            data.credit_card = credit_card;
            data.lat = LATITUDE_service; //localStorage.getItem("LAT_coordinate");
            data.lon = LONGITUDE_service; //localStorage.getItem("LON_coordinate");
            data.productos = productos;
            data.APP_VERSION = APP_VERSION;
            $.ajax({
                url: webservice + "store_purchase",
                data: data,
                type: "POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result){
                    
                    console.log(result);
                    if(!result.error){


                                    localStorage.setItem("code", result.code);
                                    localStorage.setItem("end_delivery_time", result.end_delivery_time);

                                    var data = {};
                                    data.code = localStorage.getItem("code");
                                    data.APP_VERSION = APP_VERSION;
                                    $.ajax({
                                        url: webservice + "get_credit_card",
                                        type: "POST",
                                        timeout   : time_out,
                                        dataType: "JSON",
                                        data: data,
                                        error: function(e){
                                            console.log(e);
                                        },
                                        success: function(result_credit_card){

                                            console.log(result_credit_card);
                                            if(!result_credit_card.error){

                                                    tokenParams_purchase_order = {
                                                          "card": {
                                                            "number": result_credit_card.data.number,
                                                            "name": result_credit_card.data.type,
                                                            "exp_year": result_credit_card.data.year_expiration,
                                                            "exp_month": result_credit_card.data.month_expiration,
                                                            "cvc": result_credit_card.data.cvv,
                                                            "address": {
                                                                "street1": "-",
                                                                "street2": "-",
                                                                "city": "Ciudad de Mexico",
                                                                "state": "-",
                                                                "zip": "-",
                                                                "country": "Mexico"
                                                             }
                                                          },
                                                          "payment": result.total_payment,                                           
                                                    };

                                                    console.log(tokenParams_purchase_order);
                                                    Conekta.token.create(tokenParams_purchase_order, successResponseHandler_purchase_order, errorResponseHandler_purchase_order);

                                            }else{
                                                loading(false);
                                                showModalAlert(result_credit_card.message);
                                            }

                                        }
                                    });
                                    //toFinCompra();




 
                    }else{

                        localStorage.removeItem("code");
                        loading(false);
                        showModal(result.message);
                    }


                }
            });

        }, function(error){
            console.log(error.message);
        });
    });

}

successResponseHandler_purchase_order = function(token) {

    var data = {};
    data.code = localStorage.getItem("code");
    data.user_id = localStorage.getItem("user_id");
    data.token_id = token.id;
    data.token_params = tokenParams_purchase_order;
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "pagar_pedido",
        type: "GET",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            console.log(result);
            if(!result.error){

                toFinCompra();

            }else{

                localStorage.removeItem("code");
                loading(false);
                showModal("forma_pago_invalida");
            }

        }
    });

};

errorResponseHandler_purchase_order = function(error) {

    localStorage.removeItem("code");
    loading(false);
    console.log(error);
    showModal("forma_pago_invalida");

};

function siguienteResumenEfectivo(subtotal){

    closeModal('cantidad_pago_en_efectivo');
    closeModal('seguro_realizar_cargo');
    loading(true);

    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

            var productos = new Array();
            for(var i = 0; i < rs.rows.length; i++){
                var row_pro = {};
                row_pro.tipo = rs.rows.item(i).tipo;
                row_pro.product_id = rs.rows.item(i).product_id;
                row_pro.cantidad = rs.rows.item(i).cantidad;
                productos.push(row_pro);
            }


            var data = {};
            data.code   = localStorage.getItem("code");
            data.user_id = localStorage.getItem("user_id");
            data.shipping = localStorage.getItem("shipping");
            data.subtotal = subtotal;
            data.lat = LATITUDE_service; //localStorage.getItem("LAT_coordinate");
            data.lon = LONGITUDE_service; //localStorage.getItem("LON_coordinate");
            data.productos = productos;
            data.cantidad_efectivo_pagar = $("#cantidad_pago_en_efectivo #efectivo_pagar").val();
            data.APP_VERSION = APP_VERSION;

            $("#cantidad_pago_en_efectivo #efectivo_pagar").val("");

            $.ajax({
                url: webservice + "store_purchase_efectivo",
                data: data,
                type: "POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result){

                    console.log(result);
                    if(!result.error){

                        localStorage.setItem("code", result.code);
                        localStorage.setItem("end_delivery_time", result.end_delivery_time);
                        toFinCompra()

                    }else{

                        localStorage.removeItem("code");
                        loading(false);
                        showModalAlert(result.message);

                    }


                }
            });

        }, function(error){
            console.log(error.message);
        });
    });

}



function siguienteResumenTerminal(subtotal){

    closeModal('pago_terminal_modal');
    loading(true);

    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

            var productos = new Array();
            for(var i = 0; i < rs.rows.length; i++){
                var row_pro = {};
                row_pro.tipo = rs.rows.item(i).tipo;
                row_pro.product_id = rs.rows.item(i).product_id;
                row_pro.cantidad = rs.rows.item(i).cantidad;
                productos.push(row_pro);
            }


            var data = {};
            data.code   = localStorage.getItem("code");
            data.user_id = localStorage.getItem("user_id");
            data.shipping = localStorage.getItem("shipping");
            data.subtotal = subtotal;
            data.lat = LATITUDE_service; //localStorage.getItem("LAT_coordinate");
            data.lon = LONGITUDE_service; //localStorage.getItem("LON_coordinate");
            data.productos = productos;
            data.APP_VERSION = APP_VERSION;

            $.ajax({
                url: webservice + "store_purchase_solicitar_terminal",
                data: data,
                type: "POST",
                timeout   : time_out,
                dataType: "JSON",
                error: function(e){
                    console.log(e);
                },
                success: function(result){

                    console.log(result);
                    if(!result.error){

                        localStorage.setItem("code", result.code);
                        localStorage.setItem("end_delivery_time", result.end_delivery_time);
                        toFinCompra()

                    }else{

                        localStorage.removeItem("code");
                        loading(false);
                        showModalAlert(result.message);

                    }

                }
            });

        }, function(error){
            console.log(error.message);
        });
    });

}



function toFinCompra(){

    loading(true);
    var data = {};
    data.code = localStorage.getItem("code");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_purchase",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            console.log(result);
            loading(false);

            localStorage.setItem("end_delivery_time", result.end_delivery_time);
            $("#pedido_camino #codigo_compra").text( localStorage.getItem("code") );

            timer_down.setstartDate( createDateFromString( result.current_time ) );
            timer_down.setDate( createDateFromString( result.end_delivery_time ) );
            timer_down.reset();
            timer_down.update();

            /*timer_down = new uiTimeBomb({
                elements: document.getElementById("timer")
                ,date: createDateFromString( localStorage.getItem("end_delivery_time") )
                ,localTime:false
                ,method:"hours"
                ,format:"{h}:{i}:{s}"
                ,startDate: createDateFromString( result.current_time )           
            });*/
            CANCELADO_POR_USUARIO = false;
            $.mobile.changePage( "#pedido_camino", { transition: "slide" });
            parar_conteo = false;
            calcularPagoVunMinutos();

        }
    });

}


function createDateFromString( string_date ){

    string_date = string_date.split(" ");
    var fecha = string_date[0];
    var hora = string_date[1];
    
    fecha = fecha.split("-");
    hora = hora.split(":");
    
    return new Date(fecha[0], parseInt(fecha[1]) - 1, fecha[2], hora[0], hora[1], hora[2], 00);
}



function calcularPagoVunMinutos(){

    var data = {};
    data.code = localStorage.getItem("code");
    data.APP_VERSION = APP_VERSION;
    if(data.code != undefined){

        $.ajax({
            url: webservice + "get_purchase",
            type: "POST",
            timeout   : time_out,
            dataType: "JSON",
            data: data,
            error: function(e){
                console.log(e);
                if(!parar_conteo){                
                    setTimeout(function(){ calcularPagoVunMinutos(); }, (1000 * 3) );                
                }
            },
            success: function(result){


                console.log(result);

                if(result.status == "cancelado"){

                        loading(true);
                        //REGRESAMOS LOS PRODUCTOS AGREGADOS PARA VOLVER A ALIMENTAR EL ALMACEN
                        db.transaction(function (tx) {
                            tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

                                var productos = new Array();
                                for(var i = 0; i < rs.rows.length; i++){
                                    var row_pro = {};
                                    row_pro.tipo = rs.rows.item(i).tipo;
                                    row_pro.product_id = rs.rows.item(i).product_id;
                                    row_pro.cantidad = rs.rows.item(i).cantidad;
                                    productos.push(row_pro);
                                }

                                //Regresamos todos los pedidos, ya que se cancelo
                                var data = {};
                                data.productos = productos;
                                data.APP_VERSION = APP_VERSION;
                                $.ajax({
                                    url: webservice + "return_products",
                                    data:data,
                                    type:"POST",
                                    timeout   : time_out,
                                    dataType: "JSON",
                                    error: function(e){
                                        console.log(e);
                                    },
                                    success: function(result_return_products){

                                        //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                                        console.log(result_return_products);
                                        if(!result_return_products.error){

                                            db.transaction(function (tx) {
                                                tx.executeSql("DROP TABLE productos", [], function(tx, rs){
                                                    
                                                    db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                                    db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });
                                            
                                                        loading(false);
                                                        timer_down.setDate( new Date( Date.now()-(864E5*2) ) );
                                                        localStorage.removeItem("category_id");
                                                        localStorage.removeItem("code");
                                                        localStorage.removeItem("end_delivery_time");
                                                        localStorage.removeItem("LAT_coordinate");
                                                        localStorage.removeItem("LON_coordinate");
                                                        localStorage.removeItem("shipping");
                                                        localStorage.removeItem("type");  
                                                        LATITUDE_service = "";
                                                        LONGITUDE_service = "";

                                                        closeModal("pedido_cancelado_exito");   //Por asi acaso cerramos estos son modales    
                                                        closeModal("pedido_pagado_exito");      //Por asi acaso cerramos estos son modales

                                                        //Si el pedido fue cancelado por el usuario, mostramos el mensaje que el lo cancelo
                                                        if( CANCELADO_POR_USUARIO ){
                                                            showModalAlert(result.message_cancel_by_user);
                                                        }else{
                                                            showModalAlert(result.message);
                                                        }
                                                        CANCELADO_POR_USUARIO = false;
                                                        $.mobile.changePage( "#perfil", { transition: "slide", reverse: true });

                                                }, function(error){
                                                    console.log(error.message);
                                                });
                                            }); 


                                        }else{
                                            loading(false);                                        
                                            showModalAlert(result.message);
                                        }

                                    }
                                });
                        

                            }, function(error){
                                console.log(error.message);
                            });
                        }); 

 
                }else if(result.status == "entregado"){

                    loading(true);
                    //Si ya fue entregado el pedido, eliminamos todos los productos que tenga agregados en base de datos de localstorage
                    db.transaction(function (tx) {
                        tx.executeSql("DROP TABLE productos", [], function(tx, rs){

                            db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                            db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });

                                loading(false);
                                timer_down.setDate( new Date( Date.now()-(864E5*2) ) );
                                localStorage.removeItem("category_id");
                                localStorage.removeItem("code");
                                localStorage.removeItem("end_delivery_time");
                                localStorage.removeItem("LAT_coordinate");
                                localStorage.removeItem("LON_coordinate");
                                localStorage.removeItem("shipping");
                                localStorage.removeItem("type");
                                LATITUDE_service = "";
                                LONGITUDE_service = "";

                                closeModal("pedido_cancelado_exito");   //Por asi acaso cerramos estos son modales
                                closeModal("pedido_pagado_exito");      //Por asi acaso cerramos estos son modales
                                showModalAlertOk(result.message);
                                $.mobile.changePage( "#perfil", { transition: "slide", reverse: true });

                        }, function(error){
                            console.log(error.message);
                        });
                    });

                }else if(result.status == "notificado"){

                    //Si ya fue notificado el usuario, solo volvemos a mandar a llamar a dicha funcion nuevamente
                    if(!parar_conteo){
                        setTimeout(function(){ calcularPagoVunMinutos(); }, (1000 * 2) );
                    }

                }else{

                    //Aqui se hace el cobro despues de los 21 minutos, pero el cliente ya no requiere esto
                    if(!parar_conteo){
                        setTimeout(function(){ calcularPagoVunMinutos(); }, (1000 * 2) );
                    }

                }

            }
        });   
    }

}

 








function cancelarPedidoModal(){

    loading(true);
    var data = {};
    data.code = localStorage.getItem("code");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "cancelar_pedido",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            console.log(result);
            loading(false);
            if(result.close_modal){
                if(!result.do_charger){
                    $("#cancelar_pedido_sin_cargo .title").text(result.message);
                    $("#cancelar_pedido_sin_cargo .si_button").attr("onclick", "cancelarPedidoSinCargo('"+localStorage.getItem("code")+"')");
                    showModal("cancelar_pedido_sin_cargo");
                }else{
                    $("#cancelar_pedido_con_cargo .title").text(result.message);
                    $("#cancelar_pedido_con_cargo .title_type_4").text("$"+result.charger_amount);    
                    $("#cancelar_pedido_con_cargo .si_button").attr("onclick", "cancelarPedidoConCargo('"+localStorage.getItem("code")+"', '"+result.charger_amount+"')");                            
                    showModal("cancelar_pedido_con_cargo");
                }
            }else{
                    $("#cancelar_pedido_sin_cargo .title").text(result.message);
                    showModalAlert(result.message);
            }

        }
    });

}

 
function cancelarPedidoSinCargo(code){

    closeModal("cancelar_pedido_sin_cargo");
    closeModal("cancelar_pedido_con_cargo");
    loading(true);
    var data = {};
    data.code = code;
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "cancelar_pedido_sin_cargos",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            
            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

                    parar_conteo = true;
                    var productos = new Array();
                    for(var i = 0; i < rs.rows.length; i++){
                        var row_pro = {};
                        row_pro.tipo = rs.rows.item(i).tipo;
                        row_pro.product_id = rs.rows.item(i).product_id;
                        row_pro.cantidad = rs.rows.item(i).cantidad;
                        productos.push(row_pro);
                    }

                    //Regresamos todos los pedidos, ya que se cancelo
                    var data = {};
                    data.productos = productos;
                    data.APP_VERSION = APP_VERSION;
                    $.ajax({
                        url: webservice + "return_products",
                        data:data,
                        type:"POST",
                        timeout   : time_out,
                        dataType: "JSON",
                        error: function(e){
                            console.log(e);
                        },
                        success: function(result){

                            //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                            console.log(result);
                            if(!result.error){

                                db.transaction(function (tx) {
                                    tx.executeSql("DROP TABLE productos", [], function(tx, rs){
                                        
                                        db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                        db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });

                                            CANCELADO_POR_USUARIO = true;
                                            timer_down.setDate( new Date( Date.now()-(864E5*2) ) );
                                            localStorage.removeItem("category_id");
                                            //localStorage.removeItem("code");
                                            localStorage.removeItem("end_delivery_time");
                                            localStorage.removeItem("LAT_coordinate");
                                            localStorage.removeItem("LON_coordinate");
                                            localStorage.removeItem("shipping");
                                            localStorage.removeItem("type");
                                            LATITUDE_service = "";
                                            LONGITUDE_service = "";
                                            //showModalAlert("Pedido cancelado con exito");
                                            //$.mobile.changePage( "#perfil", { transition: "slide", reverse: true });

                                    }, function(error){
                                        console.log(error.message);
                                    });
                                }); 


                            }else{
                                showModalAlert(result.message);
                            }

                        }
                    });
            

                }, function(error){
                    console.log(error.message);
                });
            }); 


        }
    });   

}


function cancelarPedidoConCargo(code, amount){
      

    //Traemos los datos de la tarjeta elegida
    loading(true);
    var data = {};
    data.code = code;
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_credit_card",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            console.log(result);
            if(!result.error){
                    tokenParams_cancel_order =  {
                        "card": {
                            "number":       result.data.number,
                            "name":         result.data.type,
                            "exp_year":     result.data.year_expiration,
                            "exp_month":    result.data.month_expiration,
                            "cvc":          result.data.cvv,
                        },
                        "payment": amount,
                    };
                    Conekta.token.create(tokenParams_cancel_order, successResponseHandler_cancel_order, errorResponseHandler_cancel_order);
            }else{
                loading(false);
                showModalAlert(result.message);
            }

        }
    });    

 
}


successResponseHandler_cancel_order = function(token) {

    loading(true);
    closeModal("cancelar_pedido_sin_cargo");
    closeModal("cancelar_pedido_con_cargo");
    var data = {};
    data.code = localStorage.getItem("code");
    data.user_id = localStorage.getItem("user_id");
    data.token_id = token.id;
    data.token_params = tokenParams_cancel_order;
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "cancelar_pedido_con_cargos",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){

            console.log(result);
            if(!result.error){

                    db.transaction(function (tx) {
                        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){

                            var productos = new Array();
                            for(var i = 0; i < rs.rows.length; i++){
                                var row_pro = {};
                                row_pro.tipo = rs.rows.item(i).tipo;
                                row_pro.product_id = rs.rows.item(i).product_id;
                                row_pro.cantidad = rs.rows.item(i).cantidad;
                                productos.push(row_pro);
                            }

                            //Regresamos todos los pedidos, ya que se cancelo
                            var data = {};
                            data.productos = productos;
                            data.APP_VERSION = APP_VERSION;
                            $.ajax({
                                url: webservice + "return_products",
                                data:data,
                                type:"POST",
                                timeout   : time_out,
                                dataType: "JSON",
                                error: function(e){
                                    console.log(e);
                                },
                                success: function(result){

                                    //Cuando termine eliminamos todos los productos y datos de compra ya que se cancelo
                                    console.log(result);
                                    if(!result.error){

                                        db.transaction(function (tx) {
                                            tx.executeSql("DROP TABLE productos", [], function(tx, rs){
                                                
                                                db = window.openDatabase("products_compra", 1, "products_compra", 52428800);
                                                db.transaction(inicializarDB, function(error){      console.log("ERROR AL GENERAR BD: " + error.message );      }, function(){  console.log("GENERO BD");   });

                                                    CANCELADO_POR_USUARIO = true;
                                                    timer_down.setDate( new Date( Date.now()-(864E5*2) ) );                
                                                    localStorage.removeItem("category_id");
                                                    localStorage.removeItem("code");
                                                    localStorage.removeItem("end_delivery_time");
                                                    localStorage.removeItem("LAT_coordinate");
                                                    localStorage.removeItem("LON_coordinate");
                                                    localStorage.removeItem("shipping");
                                                    localStorage.removeItem("type");            
                                                    LATITUDE_service = "";
                                                    LONGITUDE_service = "";

                                            }, function(error){
                                                console.log(error.message);
                                            });
                                        }); 


                                    }else{

                                        loading(false);
                                        showModalAlert(result.message);
                                    }

                                }
                            });
                    

                        }, function(error){
                            console.log(error.message);
                        });
                    }); 

 

            }else{

                loading(false);
                showModalAlert(result.message);
            }

        }
    });

};

errorResponseHandler_cancel_order = function(error) {

    closeModal("cancelar_pedido_sin_cargo");
    closeModal("cancelar_pedido_con_cargo");    
    loading(false);
    showModalAlert("No se ha podido cancelar el pedido, intentelo nuevamente.");
};


function pedidoCanceladoExito(){

    closeModal("pedido_cancelado_exito");
    closeModal("pedido_pagado_exito");
    $.mobile.changePage( "#perfil", { transition: "slide", reverse: true });
 
}

 
function ToComprasRealizadas(){


    loading(true);
    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.APP_VERSION = APP_VERSION;
    $.ajax({
        url: webservice + "get_purchase_by_user",
        type: "POST",
        timeout   : time_out,
        dataType: "JSON",
        data: data,
        error: function(e){
            console.log(e);
        },
        success: function(result){
            loading(false);
            console.log(result);
            $("#compras_realizadas #compras_realizadas_section").html(result);
            $.mobile.changePage( "#compras_realizadas", { transition: "slide" });            
        }
    });  

    
}





function refreshQuantityProducts(){

    console.log("REFRESCA");
    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM productos", [], function(tx, rs){     

            var html = "";
            for(var i = 0; i < rs.rows.length; i++){
                $("input[product-id='"+rs.rows.item(i).product_id+"'][data-type='"+rs.rows.item(i).tipo+"']").val( rs.rows.item(i).cantidad );
            }
 
        }, function(error){
            console.log(error.message);
        });
    });

}


function choose_contact_picker(){

    navigator.contacts.pickContact(function(contact){

        var numero_contacto = "";
        var numero = contact["phoneNumbers"];
        for (var i = 0; i < numero.length; i++) {
            console.log(numero[i]);
            if(numero[i].type == "mobile"){
                numero_contacto = numero[i].value;
                numero_contacto = numero_contacto.replace(" ", "");
                numero_contacto = numero_contacto.replace(" ", "");
                numero_contacto = numero_contacto.replace(" ", "");
            }
        }

        $("#invitar_amigo input[name='telefono']").val(numero_contacto);

    },function(err){
        console.log('Error: ' + err);
    });

}



function todayDateResumen(){

    var today = new Date();
    var day = today.getDate();
    var month = today.getMonth();
    var year = today.getFullYear();

    switch(month){
        case 0:     month = "Enero";break;
        case 1:     month = "Febrero";break;
        case 2:     month = "Marzo";break;
        case 3:     month = "Abril";break;
        case 4:     month = "Mayo";break;
        case 5:     month = "Junio";break;
        case 6:     month = "Julio";break;
        case 7:     month = "Agosto";break;
        case 8:     month = "Septiembre";break;
        case 9:     month = "Octubre";break;
        case 10:    month = "Noviembre";break;
        case 11:    month = "Diciembre";break;
    }

    month = month.toUpperCase();
    return day + " " + month + " " + year;
}


function daysInMonth(month,year) {

    return new Date(year, month, 0).getDate();
}

function fillYearsField(){

    var d = new Date();
    var year = d.getFullYear();
    var select_html = "";

    for(var i = year - 19; i >= year - 70; i--){
        select_html += "<option value='"+i+"'>"+i+"</option>";
    }

    $("#crear_cuenta_correo select[name='anio']").append(select_html);
    $("#crear_cuenta_red_social select[name='anio']").append(select_html);
}

//function showModalAlertAjax(texto){
//    var message = "";
//
//    console.log( texto.errors );
//
//    for(var k in texto){
//        console.log(texto[k]);
//        message = texto[k];
//        $(".modal_message_error_background").css("display", "block");
//        $("#alert_modal .title").text(message);
//        $("#alert_modal").css("display", "block");
//        disableVerticalScroll(true);
//        return;
//    }
//
//}


function showModalAlertAjax(texto){

    // console.log(texto.errors);
    var error_message = "";
    // texto = texto.errors;
    for(var key in texto){

        var errores = texto.errors;
        for(var key_er in errores){
             console.log(errores[key_er]);
            error_message = errores[key_er];

             $(".modal_message_error_background").css("display", "block");
                    $("#alert_modal .title").text(error_message);
                    $("#alert_modal").css("display", "block");
                    disableVerticalScroll(true);
                    return;


            // disableVerticalScroll(fa);
            return;
        }

    }

}


function showModalAlert(texto){

    disableVerticalScroll(true); 
    $(".modal_message_error_background").css("display", "block");
    $("#alert_modal .title").html(texto);
    $("#alert_modal").css("display", "block");

    var window_height = $(window).height();
    var height_modal = $("#alert_modal").height();
    var padding_top = window_height - height_modal;
    padding_top = padding_top / 2;

    $("#alert_modal").css("margin-top", padding_top+"px");

}

function showModalAlertOk(texto){

    disableVerticalScroll(true); 
    $(".modal_message_error_background").css("display", "block");
    $("#alert_modal_ok .title").text(texto);
    $("#alert_modal_ok").css("display", "block");

    var window_height = $(window).height();
    var height_modal = $("#alert_modal_ok").height();
    var padding_top = window_height - height_modal;
    padding_top = padding_top / 2;

    $("#alert_modal_ok").css("margin-top", padding_top+"px");

}

function showModal(id_message){

    disableVerticalScroll(true); 
    $(".modal_message_error_background").css("display", "block");
    $("#"+id_message).css("display", "block");
    
    var window_height = $(window).height();
    var height_modal = $("#"+id_message).height();
    var padding_top = window_height - height_modal;
    padding_top = padding_top / 2;

    $("#"+id_message).css("margin-top", padding_top+"px");

}

function closeModal(id_message){

    disableVerticalScroll(false); 
    $(".modal_message_error_background").css("display", "none");
    $("#"+id_message).css("display", "none");

}

function disableVerticalScroll(disable){

    if(disable){
        $("body").css("overflow-y", "hidden");
    }else{
        $("body").css("overflow-y", "auto");
    }

}

function loading(show){

    var window_height = $(window).height();
    var gif_height = 33;
    var padding_top = ((window_height - gif_height) / 2);

    $("#loading img").css("margin-top",  padding_top);


    if(show){
        $("#loading").css("display", "block");
    }else{
        $("#loading").css("display", "none");
    }

}









