var DEBUG = false;
var APP_VERSION = "1.0.0";


var WEBSERVICE_URL = "http://metodika.com.mx/dsr/recsol/public//api/"+APP_VERSION+"/";
// var WEBSERVICE_URL = "http://localhost/recsol/ADMIN/public/api/"+APP_VERSION+"/";


if(!DEBUG){
    document.addEventListener("backbutton", function () {}, false);
    document.addEventListener("deviceready", inicializar_app, false);
}


$(document).ready(function(e){

    // AJAX defaults
    $.ajaxSetup({
        type: "POST",
        dataType: "JSON",
        error: function (e) {
            ajax_errors(e);
        },
        beforeSend: function () {
            loading(true);
        },
        complete: function () {
            loading(false);
        },
        timeout: 30000
    });

    WINDOW_HEIGHT = $(this).height();
    WINDOW_WIDTH =  $(this).width();

    if(DEBUG){
        inicializar_app();
    }


    $(function() {
        $(".menu").swipe( {
            swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
                if(direction == "left"){
                    action_menu(false);
                }
            }
        });
    });


    $(this).on("change", "#create_account select[name='nivel_de_estudios']", function (e) {
        var element = $(this).find('option:selected');
        var show_extra = element.attr("open_extra");
        if(show_extra == "true"){
            $("#especifica_nivel_estudio").slideDown(200);
        }else{
            $("#especifica_nivel_estudio").slideUp(200);
        }
    });


    $(this).on("change", "#perfil select[name='nivel_de_estudios_new']", function (e) {
        var element = $(this).find('option:selected');
        var show_extra = element.attr("open_extra");
        if(show_extra == "true"){
            $("#especifica_nivel_estudio_new").slideDown(200);
        }else{
            $("#especifica_nivel_estudio_new").slideUp(200);
        }
    });


    $(this).on("click", ".headers_tabs .tab", function (e) {
        e.preventDefault();

        $(".headers_tabs .tab").removeClass("active");

        var active_content = $(this).attr("type");

        $(this).addClass("active");
        $(".content_tabs .content").hide();
        $(".content_tabs ."+active_content+"_content").show();

    });


    $(this).on("click", ".radio_input", function (e) {
        e.preventDefault();

        var name = $(this).attr("name");
        $(".radio_input[name='"+name+"']").removeClass("active");

        $(this).addClass("active");

    });

    
    $(this).on("click", ".radio_input[name='licencia']", function (e) {
        e.preventDefault();

        var value = $(this).attr("value");
        if(value == "vigente"){
            $("#tipo_licencia").slideDown(200);
        }else{
            $("#tipo_licencia").slideUp(200);
        }

    });


    $(this).on("click", ".input_check", function (e) {
        e.preventDefault();

        if( $(this).hasClass("active") == true ){
            $(this).removeClass("active");
        }else{
            $(this).addClass("active");
        }

    });


    $(this).on("click", ".jobs_list .job_sin .content_description_job .plus", function (e) {
        e.preventDefault();

        var hide_or_show = "";
        var description_job = $(this).parent().parent().parent().find(".description_job");
        var image_plus = $(this).parent().parent().parent().find(".plus");

        //Verificamos si se mostrara o escondera
        if( description_job.css("white-space") == "pre-line" ){
            hide_or_show = "hide";
        }else{
            hide_or_show = "show";
        }

        //Escondemos todos
        $(".jobs_list .job_sin .description_job").css("white-space", "nowrap");
        $(".jobs_list .job_sin .content_description_job .plus").attr("src", "img/plus_icon.png");

        //Si tenemos que mostrar el texto, lo mostramos
        if(hide_or_show == "show"){
            description_job.css("white-space", "pre-line");
            image_plus.attr("src", "img/row_up.png");
        }

    });


    $(this).on("change", "select[name='puesto']", function (e) {

        var value = $(this).val();

        if( value == "Otro" ){
            $("#otro_puesto").slideDown(200);
        }else{
            $("#otro_puesto").slideUp(200);
        }

    });


    $(this).on("change", "select[name='puesto_edit']", function (e) {

        var value = $(this).val();

        if( value == "Otro" ){
            $("#otro_puesto_edit").slideDown(200);
        }else{
            $("#otro_puesto_edit").slideUp(200);
        }

    });


    $(this).on("click", "#perfil_estudios div[name='idioma']", function (e) {
        e.preventDefault();

        var id_language = $(this).attr("value");
        var enable = $(this).hasClass("active");

        var data = {};
        data.user_id = localStorage.getItem("user_id");
        data.id_language = id_language;
        data.enable = enable;
        $.ajax({
            url: WEBSERVICE_URL + "save_language",
            data: data,
            success: function (result) {
                console.log(result);
                if(!result.error){
                    // lnv.alert({
                    //     icon: "success",
                    //     title: 'Mensaje',
                    //     content: result.message,
                    // });
                }else{
                    lnv.alert({
                        icon: "info",
                        title: 'Mensaje',
                        content: result.message
                    });
                }
            }
        });

    })


    $(this).on("change", "#vacantes select[name='puesto'], #vacantes select[name='estado']", function (e) {
        get_vacantes();
    });


    $(this).bind('pagechange', function() {
        if( $.mobile.activePage.attr('id') == "dashboard" ){
            get_total_notifications();
        }
    });


});


function inicializar_app(){

    if( localStorage.getItem("user_id") != "" && localStorage.getItem("user_id") != undefined && localStorage.getItem("user_id") != "undefined" && localStorage.getItem("user_id") != "null" && localStorage.getItem("user_id") != null ){
        refreshUUID();
        // to_notificaciones();
        $.mobile.changePage( "#dashboard");
    }else{
        toLogin();
    }

}


function refreshUUID(){
    document.addEventListener("deviceready", devideReadyNotification, false);
}


function devideReadyNotification(){

    if( localStorage.getItem("user_id") != undefined ){

        //Registramos el usuario
        // window.FirebasePlugin.setUserId(localStorage.getItem("user_id"));

        // Enables analytics collection
        // window.FirebasePlugin.setAnalyticsCollectionEnabled(true);

        //Register device
        window.FirebasePlugin.getToken(function(token) {
            // save this server-side and use it to push notifications to this device
            console.log(token);

            //Actualizamos el uuid del dispositivo
            var data_s = {};
            data_s.user_id    =   localStorage.getItem("user_id");
            data_s.os_type    =   device.platform.toLowerCase();
            data_s.uuid       =   token;
            data_s.APP_VERSION=   APP_VERSION;
            $.ajax({
                url: WEBSERVICE_URL + "update_uuid",
                data: data_s,
                error: function(e){
                    //alert("TERMINO MAL")
                    console.log(e);
                },
                success: function(result){
                    //alert("TERMINO BIEN")
                    console.log(result);
                }
            });

        }, function(error) {
            console.error(error);
        });


        if(device.platform.toLowerCase() == "ios"){
            window.FirebasePlugin.hasPermission(function(data){
                if(data.isEnabled == false){
                    window.FirebasePlugin.grantPermission();
                }
            });
        }


        // //On notification open
        // window.FirebasePlugin.onNotificationOpen(function(notification) {
        //     // console.log(STATUS_APP);
        //     console.log(notification);
        //     NOTIFICACION_ACTIONS = notification;
        //
        //     if(STATUS_APP == "BACKGROUND"){
        //         show_map_from_notification(NOTIFICACION_ACTIONS);
        //     }else{
        //         cordova.plugins.notification.local.schedule({
        //             title: 'ALERTA PROTEGETEC',
        //             text: NOTIFICACION_ACTIONS.TEXTO_NOTIFICACION,
        //             foreground: true,
        //         });
        //     }
        //
        // }, function(error) {
        //     console.error(error);
        // });

        // cordova.plugins.notification.local.on('click', function (notification, eopts) {
        //     show_map_from_notification(NOTIFICACION_ACTIONS);
        // });


        STATUS_APP = "FOREGROUND";

    }

}


function toLogin(){

    $.mobile.changePage( "#login", {  });
    loading(false);
}


function loading(show){

    var window_height = $(window).height();
    var gif_height = 80;
    var padding_top = ((window_height - gif_height) / 2);

    $("#loading img").css("margin-top",  padding_top);

    if(show){
        $("#loading").css("display", "block");
        disableVerticalScroll(true);
    }else{
        $("#loading").css("display", "none");
        disableVerticalScroll(false);
    }

}


function disableVerticalScroll(disable){

    if(disable){
        $("body").css("overflow-y", "hidden");
    }else{
        $("body").css("overflow-y", "auto");
    }

}


function to_create_account() {

    $("#create_account input[name='nombre_completo']").val("");
    $("#create_account input[name='correo_electronico']").val("");
    $("#create_account input[name='telefono']").val("");
    $("#create_account input[name='confirma_telefono']").val("");
    $("#create_account select[name='estado']").val("");
    $("#create_account select[name='municipio']").html('<option value="">Selecciona un municipio</option>');
    $("#create_account input[name='password']").val("");
    $("#create_account input[name='confirm_password']").val("");
    $("#create_account select[name='nivel_de_estudios']").val("Primaria");
    $("#create_account textarea[name='especifica_nivel_estudios']").val("");
    $("#create_account textarea[name='area_experiencia_laboral']").val("");
    $("#especifica_nivel_estudio").hide();

    $.ajax({
        url: WEBSERVICE_URL + "get_estados",
        success: function (result) {
            $("#create_account select[name='estado']").html(result);
            $.mobile.changePage( "#create_account", { transition: "slide" });
        }
    });

}


function to_profile() {

    action_menu(false);

    var data = {};
    data.user_id = localStorage.getItem("user_id");
    $.ajax({
        url: WEBSERVICE_URL + "get_info_user",
        data: data,
        success: function (result) {
            console.log(result);

            /******************************** PERFIL ********************************/
            $("#perfil_perfil select[name='estado']").html(result.estado);
            $("#perfil_perfil select[name='municipio']").html(result.municipio);
            $("#perfil_perfil select[name='colonia']").html(result.colonia);


            $("#perfil_perfil input[name='nombre_completo']").val( result.user.name );
            $("#perfil_perfil input[name='correo_electronico']").val( result.user.email );
            $("#perfil_perfil input[name='telefono']").val( result.user.phone_number );
            $("#perfil_perfil input[name='telefono_confirmed']").val( result.user.phone_number );
            $("#perfil_perfil input[name='fecha_nacimiento']").val( result.user.birthdate );
            $("#perfil_perfil select[name='estado_civil']").val( result.user.civil_status );
            $("#perfil_perfil select[name='estado']").val( result.user.state_id );
            $("#perfil_perfil select[name='municipio']").val( result.user.municipaly_id );
            $("#perfil_perfil select[name='colonia']").val( result.user.colony_id );
            $("#perfil_perfil input[name='dependientes_economicos']").val( result.user.economic_dependency );

            $("#perfil_perfil div[name='licencia']").removeClass("active");
            if( result.user.driver_licence == "vigente"){
                $("#perfil_perfil div[name='licencia'][value='vigente']").addClass("active");
                $("#perfil_perfil select[name='tipo_licencia']").val( result.user.driver_licence_type );
                $("#tipo_licencia").show();
            }else{
                $("#perfil_perfil div[name='licencia'][value='vencida']").addClass("active");
            }

            $("#perfil_perfil input[name='nacionalidad']").val( result.user.nationality );
            $("#perfil_perfil input[name='curp']").val( result.user.curp );
            $("#perfil_perfil input[name='rfc']").val( result.user.rfc );
            $("#perfil_perfil select[name='sueldo_deseado']").val( result.user.salary );

            $("#perfil_perfil input[name='contrasena']").val("");
            $("#perfil_perfil input[name='contrasena_confirmed']").val("");


            /******************************** EXPERIENCIA ********************************/
            $("#experiencia_laboral_content").html( result.user.experience_html );



            /******************************** ESTUDIOS ********************************/
            $("#perfil_estudios #estudios_checks").html( result.idiomas );
            $("#perfil_estudios select[name='nivel_de_estudios_new']").val( result.studies.study_level );

            if( result.studies.study_level == "Técnica" || result.studies.study_level == "Técnica trunca" || result.studies.study_level == "Licenciatura" || result.studies.study_level == "Licenciatura trunca" || result.studies.study_level == "Otro"  ){
                $("#perfil_estudios #especifica_nivel_estudio_new").show();
                $("#perfil_estudios textarea[name='especifica_nivel_estudio']").val(result.studies.study_description);
            }

            $("#perfil_estudios textarea[name='certificaciones']").val(result.studies.certification);
            $("#perfil_estudios textarea[name='area_experiencia']").text(result.studies.experience_area);


            for ( var i = 0; i < result.languages.length; i++){
                $("#perfil_estudios div[name='idioma'][value='"+result.languages[i].language_id+"']").addClass("active");
            }


            $.mobile.changePage( "#perfil", { transition: "slide" });
        }
    });

}


var NEW_EXPERIENCE_MODAL = null;
function open_modal_experiencia( empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_empleado ) {

    //Primero traemos los empleos
    $.ajax({
        url: WEBSERVICE_URL + "get_jobs_select",
        success: function (puestos_html) {
            console.log(puestos_html);

            $.get( "experiencia_laboral_new_modal.html", function( data ) {

                NEW_EXPERIENCE_MODAL = lnv.confirm({
                    closeOnHandle: false,
                    title: 'Nueva experiencia laboral',
                    content: data,
                    closeOnHandle: false,
                    confirmHandler: function(){
                        save_experiencia();
                    },
                    cancelHandler: function(){
                        // cancel callback
                    }
                });

                $("#experiencia_laboral_new select[name='puesto']").html(puestos_html);

                $("#experiencia_laboral_new input[name='empresa']").val(empresa);
                $("#experiencia_laboral_new select[name='puesto']").val(puesto);

                if( puesto == "otro" ){
                    $("#experiencia_laboral_new #otro_puesto").show();
                    $("#experiencia_laboral_new input[name='especifica_puesto']").val(especifica_puesto);
                }

                $("#experiencia_laboral_new select[name='tiempo_laborado']").val(tiempo_laborado);

                $("#experiencia_laboral_new div[name='laborando_radio']").removeClass("active");
                if(actualmente_empleado == "si"){
                    $("#experiencia_laboral_new div[name='laborando_radio'][value='si']").addClass("active");
                }else{
                    $("#experiencia_laboral_new div[name='laborando_radio'][value='no']").addClass("active");
                }


            }, "html");

        }
    });

}


function do_login(){

    var data = {};
    data.telefono   = $("#login input[name='telefono']").val();
    data.contrasena = $("#login input[name='contrasena']").val();
    console.log(data);
    $.ajax({
        url: WEBSERVICE_URL + "do_login",
        data: data,
        success: function (result) {
            console.log(result);
            if(!result.error){

                $("#login input[name='telefono']").val("");
                $("#login input[name='contrasena']").val("");
                localStorage.setItem("user_id", result.user_id);
                localStorage.setItem("user_name", result.user_name);

                refreshUUID();
                to_dashboard();

            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function to_vacantes(){

    action_menu(false);
    $.ajax({
        url: WEBSERVICE_URL + "get_vacantes_filter",
        success: function (response) {
            console.log(response);
            $("#vacantes select[name='puesto']").html(response.puesto);
            $("#vacantes select[name='estado']").html(response.estados);

            get_vacantes();

            $.mobile.changePage( "#vacantes", { transition: "slide" });
        }
    });

}


function to_dashboard(){
    action_menu(false);
    $.mobile.changePage( "#dashboard", { transition: "slide" });
}


function to_notificaciones(){

    var data = {};
    data.user_id = localStorage.getItem("user_id");
    $.ajax({
        url: WEBSERVICE_URL + "get_notificaciones",
        data: data,
        success: function (result) {
            $("#notificaciones .notificaciones_list").html(result);
            $.mobile.changePage( "#notificaciones", { transition: "slide" });
            action_menu(false);
        }
    });

}


function close_sesion() {

    if(!DEBUG){
        window.FirebasePlugin.getToken(function(token) {
            // save this server-side and use it to push notifications to this device
            console.log(token);

            //Actualizamos el uuid del dispositivo
            var data_s = {};
            data_s.uuid       =   token;
            $.ajax({
                url: WEBSERVICE_URL + "remove_uuid",
                data: data_s,
                error: function(e){
                    //alert("TERMINO MAL")
                    console.log(e);
                },
                success: function(result){
                    //alert("TERMINO BIEN")
                    console.log(result);

                    localStorage.clear();
                    action_menu(false);
                    $.mobile.changePage( "#login", { transition: "slide" });
                }
            });

        }, function(error) {
            console.error(error);
            localStorage.clear();
            action_menu(false);
            $.mobile.changePage( "#login", { transition: "slide" });
        });
    }else{
        localStorage.clear();
        action_menu(false);
        $.mobile.changePage( "#login", { transition: "slide" });
    }

}


var EDIT_EXPERIENCE_MODAL = null;
function edit_experiencia_modal(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_empleado, id_experience) {


    $.ajax({
        url: WEBSERVICE_URL + "get_jobs_select",
        success: function (puestos_html) {
            console.log(puestos_html);

            $.get( "experiencia_laboral_edit_modal.html", function( data ) {

                EDIT_EXPERIENCE_MODAL = lnv.confirm({
                    title: 'Editar experiencia laboral',
                    content: data,
                    confirmHandler: function(){
                        edit_experiencia();
                    },
                    cancelHandler: function(){
                        // cancel callback
                    }
                });

                $("#experiencia_laboral_edit select[name='puesto']").html(puestos_html);

                $("#experiencia_laboral_edit input[name='id_experience']").val(id_experience);
                $("#experiencia_laboral_edit input[name='empresa']").val(empresa);
                $("#experiencia_laboral_edit select[name='puesto']").val(puesto);

                if( puesto == "Otro" ){
                    $("#experiencia_laboral_edit #otro_puesto").show();
                    $("#experiencia_laboral_edit input[name='especifica_puesto']").val(especifica_puesto);
                }

                $("#experiencia_laboral_edit select[name='tiempo_laborado']").val(tiempo_laborado);

                $("#experiencia_laboral_edit div[name='laborando_radio']").removeClass("active");
                if(actualmente_empleado == "1" || actualmente_empleado == "si"){
                    $("#experiencia_laboral_edit div[name='laborando_radio'][value='si']").addClass("active");
                }else{
                    $("#experiencia_laboral_edit div[name='laborando_radio'][value='no']").addClass("active");
                }

            }, "html");


        }
    });




}


function remove_experiencia_laboral() {
    editar_experiencia.destroy();
}


function action_menu(open) {

    //Revisamos que pagina esta activa para ponerla en blanco
    // mi_perfil
    // vacantes
    // notificationes
    var active_page = $.mobile.activePage.attr('id');
    $(".menu .option").removeClass("active");

    if(active_page == "perfil"){
        $(".menu #mi_perfil").addClass("active");
    }else if(active_page == "vacantes"){
        $(".menu #vacantes_menu").addClass("active");
    }else if(active_page == "notificaciones"){
        $(".menu #notificationes_menu").addClass("active");
    }else if(active_page == "dashboard"){
        $(".menu #dashboard_menu").addClass("active");
    }

    //Si es mandatorio el abrir o cerrar el menu verificamos que valor trae open
    if( open != undefined ){

        if(open == true){ //ABRIR
            $("#nombre_usuario_menu").text( localStorage.getItem("user_name") );
            scroll(false);
            $(".menu").css("left","0");
        }else{  //CERRAR
            scroll(true);
            $(".menu").css("left","-100%");
        }

    }else{

        //Si no, hacemos toggle al menu
        //Verificamos si esta escondido o se muestra
        var is_left = $(".menu").css("left");
        is_left = parseFloat( is_left.replace("px", "") );

        //Si es negativo, esta escondido
        if( is_left < 0 ){ //ABRIR
            $("#nombre_usuario_menu").text( localStorage.getItem("user_name") );
            scroll(false);
            $(".menu").css("left","0");
        }else{ //CERRAR
            scroll(true);
            $(".menu").css("left","-100%");
        }

    }

}


function scroll(active){

    if(active == true){
        $("body").css("overflow-y", "scroll")
    }else{
        $("body").css("overflow-y", "hidden")
    }

}


function show_notification(time_label, title, content) {

    lnv.alert({
        title: title,
        content: content,
        alertBtnText: 'Cerrar'
    });

}


function loading(display) {

    var loading_html = "";
    loading_html += '<div id="loading">';
    loading_html += '   <div class="content_loading">';
    loading_html += '       <img src="img/loading.gif" />';
    loading_html += '       <p class="label">Cargando</p>';
    loading_html += '   </div>';
    loading_html += '</div>';

    if(display){
        $("html").append(loading_html);
        $("#loading").show();
    }else{
        $("#loading").hide();
        $("#loading").remove();
    }

}


function recuperar_contrasena() {

    $.get( "recuperar_contrasena.html", function( data ) {

        editar_experiencia = lnv.confirm({
            title: 'Recuperar contraseña',
            content: data,
            confirmHandler: function(){

                var data = {};
                data.telefono = $("#recuperar_contrasena input[name='numero_telefonico']").val();
                $.ajax({
                    url: WEBSERVICE_URL + "do_recuperar_contrasena",
                    data: data,
                    success: function (result) {
                        console.log(result);
                        if(!result.error){
                            lnv.alert({
                                icon: "success",
                                title: 'Mensaje',
                                content: result.message,
                            });
                        }else{
                            lnv.alert({
                                icon: "info",
                                title: 'Mensaje',
                                content: result.message,
                                alertHandler: function () {
                                    recuperar_contrasena();
                                }
                            });
                        }
                    }
                });

            },
            cancelHandler: function(){
                // cancel callback
            }
        });

    }, "html");

}


function do_create_account() {

    var nombre_completo = $("#create_account input[name='nombre_completo']").val();
    var correo_electronico = $("#create_account input[name='correo_electronico']").val();
    var telefono = $("#create_account input[name='telefono']").val();
    var confirma_telefono = $("#create_account input[name='confirma_telefono']").val();
    var estado = $("#create_account select[name='estado']").val();
    var municipio = $("#create_account select[name='municipio']").val();
    var password = $("#create_account input[name='password']").val();
    var confirm_password = $("#create_account input[name='confirm_password']").val();
    var nivel_de_estudios = $("#create_account select[name='nivel_de_estudios']").val();
    var especifica_nivel_estudios = $("#create_account textarea[name='especifica_nivel_estudios']").val();
    var area_experiencia_laboral = $("#create_account textarea[name='area_experiencia_laboral']").val();

    var data = {};
    data.nombre_completo = nombre_completo;
    data.correo_electronico = correo_electronico;
    data.telefono = telefono;
    data.telefono_confirmation = confirma_telefono;
    data.estado = estado;
    data.municipio = municipio;
    data.password = password;
    data.password_confirmation = confirm_password;
    data.nivel_de_estudios = nivel_de_estudios;
    data.especifica_nivel_estudios = especifica_nivel_estudios;
    data.area_experiencia_laboral = area_experiencia_laboral;


    $.ajax({
        url: WEBSERVICE_URL + "do_create_account",
        data: data,
        success: function (result) {
            console.log(result);
            if(!result.error){

                localStorage.setItem("user_id", result.user_id);
                localStorage.setItem("user_name", result.user_name);

                lnv.alert({
                    icon: "success",
                    title: '¡Bienvenido!',
                    content: result.message,
                    alertHandler: function () {
                        refreshUUID();
                        to_dashboard();
                    }
                });

            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function get_municipio_by_state(value) {

    var data = {};
    data.estado = value.val();
    $.ajax({
        url: WEBSERVICE_URL + "get_municipio_by_state",
        data: data,
        success: function (result) {
            console.log(result);
            $("#create_account select[name='municipio']").html(result);
        }
    });

}


function ajax_errors(errors) {
    console.log(errors);

    disableVerticalScroll(true);
    if( errors.status == 404 ){
        lnv.alert({
            icon: "error",
            title: 'Mensaje',
            content: 'No hemos podido procesar tu solicitud, intentalo nuevamente.',
            alertHandler: function () {
                disableVerticalScroll(false);
            }
        });
    }else if(errors.statusText == "timeout"){
        lnv.alert({
            icon: "info",
            title: 'Mensaje',
            content: 'Se ha excedido el tiempo de espera, intentalo nuevamente.',
            alertHandler: function () {
                disableVerticalScroll(false);
            }
        });
    }else if( errors.status == 422 ){
        var message_error = errors.responseJSON.errors;
        for(var k in message_error) {
            lnv.alert({
                icon: "info",
                title: 'Mensaje',
                content: message_error[k][0],
                alertHandler: function () {
                    disableVerticalScroll(false);
                }
            });
            return;
        }
    }

}


function save_perfil() {

    var nombre_completo = $("#perfil_perfil input[name='nombre_completo']").val();
    var correo_electronico = $("#perfil_perfil input[name='correo_electronico']").val();
    var telefono = $("#perfil_perfil input[name='telefono']").val();
    var telefono_confirmed = $("#perfil_perfil input[name='telefono_confirmed']").val();
    var fecha_nacimiento = $("#perfil_perfil input[name='fecha_nacimiento']").val();
    var estado_civil = $("#perfil_perfil select[name='estado_civil']").val();
    var estado = $("#perfil_perfil select[name='estado']").val();
    var municipio = $("#perfil_perfil select[name='municipio']").val();
    var colonia = $("#perfil_perfil select[name='colonia']").val();
    var dependientes_economicos = $("#perfil_perfil input[name='dependientes_economicos']").val();
    var licencia = $("#perfil_perfil div[name='licencia'][class='radio_input active']").attr("value");
    var tipo_licencia = $("#perfil_perfil select[name='tipo_licencia']").val();
    var nacionalidad = $("#perfil_perfil input[name='nacionalidad']").val();
    var curp = $("#perfil_perfil input[name='curp']").val();
    var rfc = $("#perfil_perfil input[name='rfc']").val();
    var sueldo_deseado = $("#perfil_perfil select[name='sueldo_deseado']").val();
    var contrasena = $("#perfil_perfil input[name='contrasena']").val();
    var contrasena_confirmed = $("#perfil_perfil input[name='contrasena_confirmed']").val();


    var data = {};
    data.user_id                    = localStorage.getItem("user_id");
    data.nombre_completo            = nombre_completo;
    data.correo_electronico         = correo_electronico;
    data.telefono                   = telefono;
    data.telefono_confirmation      = telefono_confirmed;
    data.fecha_nacimiento           = fecha_nacimiento;
    data.estado_civil               = estado_civil;
    data.estado                     = estado;
    data.municipio                  = municipio;
    data.colonia                    = colonia;
    data.dependientes_economicos    = dependientes_economicos;
    data.licencia                   = licencia;
    data.tipo_licencia              = tipo_licencia;
    data.nacionalidad               = nacionalidad;
    data.curp                       = curp;
    data.rfc                        = rfc;
    data.sueldo_deseado             = sueldo_deseado;
    data.contrasena                 = contrasena;
    data.contrasena_confirmation    = contrasena_confirmed;

    $.ajax({
        url: WEBSERVICE_URL + "save_perfil",
        data: data,
        success: function (result) {
            console.log(result);
            if(!result.error){
                localStorage.setItem("user_name", nombre_completo);
                lnv.alert({
                    icon: "success",
                    title: 'Mensaje',
                    content: result.message,
                });
            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function save_experiencia() {

    var empresa = $("#experiencia_laboral_new input[name='empresa']").val();
    var puesto = $("#experiencia_laboral_new select[name='puesto']").val();
    var especifica_puesto = $("#experiencia_laboral_new input[name='especifica_puesto']").val();
    var tiempo_laborado = $("#experiencia_laboral_new select[name='tiempo_laborado']").val();
    var actualmente_laborando = $("#experiencia_laboral_new div[name='laborando_radio'][class='radio_input active']").attr("value");

    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.empresa = empresa;
    data.puesto = puesto;
    data.especifica_puesto = especifica_puesto;
    data.tiempo_laborado = tiempo_laborado;
    data.actualmente_laborando = actualmente_laborando;
    $.ajax({
        url: WEBSERVICE_URL + "save_experiencia",
        data: data,
        error: function(errors){
            if( errors.status == 404 ){
                lnv.alert({
                    icon: "error",
                    title: 'Mensaje',
                    content: 'No hemos podido procesar tu solicitud, intentalo nuevamente.',
                    alertHandler: function () {
                        open_modal_experiencia(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_laborando);
                    }
                });
            }else if(errors.statusText == "timeout"){
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: 'Se ha excedido el tiempo de espera, intentalo nuevamente.',
                    alertHandler: function () {
                        open_modal_experiencia(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_laborando);
                    }
                });
            }else if( errors.status == 422 ){
                var message_error = errors.responseJSON.errors;
                for(var k in message_error) {
                    lnv.alert({
                        icon: "info",
                        title: 'Mensaje',
                        content: message_error[k][0],
                        alertHandler: function () {
                        open_modal_experiencia(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_laborando);
                        }
                    });
                    return;
                }
            }
        },
        success: function (result) {
            console.log(result);
            if(!result.error){
                lnv.alert({
                    icon: "success",
                    title: 'Mensaje',
                    content: result.message,
                    alertHandler: function(){
                        NEW_EXPERIENCE_MODAL.destroy();
                        to_profile();
                    }
                });
            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function edit_experiencia() {

    var id_experience = $("#experiencia_laboral_edit input[name='id_experience']").val();
    var empresa = $("#experiencia_laboral_edit input[name='empresa']").val();
    var puesto = $("#experiencia_laboral_edit select[name='puesto']").val();
    var especifica_puesto = $("#experiencia_laboral_edit input[name='especifica_puesto']").val();
    var tiempo_laborado = $("#experiencia_laboral_edit select[name='tiempo_laborado']").val();
    var actualmente_laborando = $("#experiencia_laboral_edit div[name='laborando_radio'][class='radio_input active']").attr("value");

    var data = {};
    data.id_experience = id_experience;
    data.user_id = localStorage.getItem("user_id");
    data.empresa = empresa;
    data.puesto = puesto;
    data.especifica_puesto = especifica_puesto;
    data.tiempo_laborado = tiempo_laborado;
    data.actualmente_laborando = actualmente_laborando;
    $.ajax({
        url: WEBSERVICE_URL + "edit_experiencia",
        data: data,
        error: function(errors){
            if( errors.status == 404 ){
                lnv.alert({
                    icon: "error",
                    title: 'Mensaje',
                    content: 'No hemos podido procesar tu solicitud, intentalo nuevamente.',
                    alertHandler: function () {
                        edit_experiencia_modal(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_laborando);
                    }
                });
            }else if(errors.statusText == "timeout"){
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: 'Se ha excedido el tiempo de espera, intentalo nuevamente.',
                    alertHandler: function () {
                        edit_experiencia_modal(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_laborando);
                    }
                });
            }else if( errors.status == 422 ){
                var message_error = errors.responseJSON.errors;
                for(var k in message_error) {
                    lnv.alert({
                        icon: "info",
                        title: 'Mensaje',
                        content: message_error[k][0],
                        alertHandler: function () {
                            edit_experiencia_modal(empresa, puesto, especifica_puesto, tiempo_laborado, actualmente_laborando);
                        }
                    });
                    return;
                }
            }
        },
        success: function (result) {
            console.log(result);
            if(!result.error){
                lnv.alert({
                    icon: "success",
                    title: 'Mensaje',
                    content: result.message,
                    alertHandler: function(){
                        EDIT_EXPERIENCE_MODAL.destroy();
                        to_profile();
                    }
                });
            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function remove_experiencia_laboral() {

    //EDIT_EXPERIENCE_MODAL
    var id_experience = $("#experiencia_laboral_edit input[name='id_experience']").val();

    var data = {};
    data.id_experience = id_experience;
    $.ajax({
        url: WEBSERVICE_URL + "delete_experience",
        data: data,
        success: function (result) {
            console.log(result);
            if(!result.error){
                EDIT_EXPERIENCE_MODAL.destroy();
                lnv.alert({
                    icon: "success",
                    title: 'Mensaje',
                    content: result.message,
                    alertHandler: function(){
                        to_profile();
                    }
                });
            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function save_studies() {

    // var idiomas = new Array();
    var nivel_de_estudios_new = $("#perfil_estudios select[name='nivel_de_estudios_new']").val();
    var especifica_nivel_estudio = $("#perfil_estudios textarea[name='especifica_nivel_estudio']").val();
    var certificaciones = $("#perfil_estudios textarea[name='certificaciones']").val();
    var area_experiencia = $("#perfil_estudios textarea[name='area_experiencia']").val();

    // $("#perfil_estudios div[name='idioma'][class='input_check active']").each(function() {
    //     idiomas.push( $(this).attr('value') );
    // });

    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.nivel_de_estudios_new = nivel_de_estudios_new;
    data.especifica_nivel_estudio = especifica_nivel_estudio;
    data.certificaciones = certificaciones;
    data.area_experiencia = area_experiencia;
    $.ajax({
        url: WEBSERVICE_URL + "save_studies",
        data: data,
        success: function (result) {
            console.log(result);
            if(!result.error){
                lnv.alert({
                    icon: "success",
                    title: 'Mensaje',
                    content: result.message,
                    alertHandler: function(){
                        to_profile();
                    }
                });
            }else{
                lnv.alert({
                    icon: "info",
                    title: 'Mensaje',
                    content: result.message
                });
            }
        }
    });

}


function get_vacantes() {

    var data = {};
    data.user_id = localStorage.getItem("user_id");
    data.puesto = $("#vacantes select[name='puesto']").val();
    data.estado = $("#vacantes select[name='estado']").val();
    $.ajax({
        url: WEBSERVICE_URL + "get_jobs",
        data: data,
        success: function (result) {
            console.log(result);
            $("#vacantes .jobs_list").html(result);
        }
    });

}


function request_job(job_id) {

    lnv.confirm({
        icon: "info",
        title: 'Mensaje',
        content: '¿Deseas postularte a esta vacante?',
        confirmHandler: function(){

            var data = {};
            data.job_id = job_id;
            data.user_id = localStorage.getItem("user_id");
            $.ajax({
                url: WEBSERVICE_URL + "request_job",
                data: data,
                success: function (result) {
                    console.log(result);
                    if(!result.error){
                        lnv.alert({
                            icon: "success",
                            title: 'Mensaje',
                            content: result.message,
                            alertHandler: function(){
                                get_vacantes();
                            }
                        });
                    }else{
                        lnv.alert({
                            icon: "info",
                            title: 'Mensaje',
                            content: result.message
                        });
                    }
                }
            });

        },
        cancelHandler: function(){
            // cancel callback
        }
    });

}


function get_total_notifications() {

    var data = {};
    data.user_id = localStorage.getItem("user_id");
    $.ajax({
        url: WEBSERVICE_URL + "get_total_notification",
        data: data,
        beforeSend: function(){},
        success: function (result) {
            console.log(result);
            $("#dashboard .number_notificaciones").text(result);
        }
    });

}








